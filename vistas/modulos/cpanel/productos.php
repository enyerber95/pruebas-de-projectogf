<?php
  if($_SESSION["acceso"] === "Basico" ){

        echo '<script>


      swal({
        title: "¡No posees acceso a este modulo!",
        text: "comunicate con el administrador<br>",
        type:"error",
        showConfirmButton: true,
        confirmButtonColor: "#018ba7",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false

      }).then((result) => {
        if (result.value) {

          window.location ="inicio";

        }
      })

      </script>';

  exit();
  }
?>



<div class="content-wrapper">

  <section class="content-header">
    
    <h1>
      
      Administrar productos
    
    </h1>

    <ol class="breadcrumb">
      
      <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Productos</li>
    
    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-body">

        <div class="box-header with-border" style="margin-bottom: 10px;">
                                    
          <button class="btn btn-primary modalEditarPreferencias" data-toggle="modal" data-target="#modalEditarPreferencias"><i class="fa fa-list-alt"></i> Editar preferencias</button>

        </div>
        
       <table class="table table-bordered table-striped dt-responsive tablas">
         
        <thead>
         
         <tr>
          
           <th style="width:10px">#</th> 
           <th>Imagen</th>
           <th>Descripción</th>
           <th>Precio de venta</th> 
           <th>Opciones</th>

         </tr> 

        </thead>

        <tbody>
          
          <?php 

            $productos = ControladorProductos::ctrMostrarProductos();

              foreach ($productos as $key => $value){

                echo '<tr>

                      <td>'.$value["id_producto"].'</td>

                      <td><img src="../'.$value["imagen"].'" class="img-thumbnail"  width="250px"></td>

                      <td>'.$value["nombre"].'</td>

                      <td>Bs. '.$value["precio_base"].'</td>

                      <td>

                        <div class="btn-group">
                                    
                          <button class="btn btn-default btnEditarProducto" data-toggle="modal" data-target="#modalEditarProducto" idProducto="'.$value["id_producto"].'"><i class="fa fa-pencil-square-o"></i></button>

                        </div>

                      </td>

                    </tr>';

              }





           ?>

   
        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL EDITAR PRODUCTO
======================================-->

<div id="modalEditarProducto" class="modal fade" role="dialog">
  
  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#018ba7; color:white">
          
          <span class="col-md-12" style="padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px;" aling="center">

                <img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">

                Editar precio producto

              </h3>
              
          </span>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

    <div class="modal-body">

      <div class="box-body">

        

        <!-- ENTRADA PARA LA DESCRIPCIÓN -->

          <div class="form-group">
              
          <div class="input-group">
              
            <span class="input-group-addon"><i class="fa fa-product-hunt" style="font-size: 22px;"></i></span> 

             <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" disabled>

          </div>

        </div>

        <!-- ENTRADA PARA PRECIO VENTA -->

        <div class="col-xs-12" style="padding: 0px">
                  
          <div class="input-group">
                    
            <span class="input-group-addon"><strong style="font-size: 16px;">Bs.</strong></span> 

            <input type="number" class="form-control input-lg" id="editarPrecioVenta" name="editarPrecioVenta" min="0" placeholder="Ingresar precio de venta" required>

            <input type="hidden" id="idProducto" name="idProducto">

          </div>

        </div><br><br>

      </div>

    </div>


    <!--=====================================
    PIE DEL MODAL
    ======================================-->

    <div class="modal-footer">

      <button type="submit" class="btn btn-primary">Guardar cambios</button>

    </div>

    <?php 

      $editar = new ControladorProductos();
      $editar -> ctrEditarPrecioProducto();

     ?>


    </form>

    </div>

  </div>

</div>





<!--=====================================
MODAL EDITAR PREFERENCIAS
======================================-->

<div id="modalEditarPreferencias" class="modal fade" role="dialog">
  
  <div class="modal-dialog modal-sm">

    <div class="modal-content">

      <form role="form" method="post">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header" style="background:#018ba7; color:white; padding: 5px; padding-left: 0px; padding-right: 0px;">
          
          <span class="col-md-12" style="padding: 5px; padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px; font-size: 25px;" aling="center">

                <img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">

                Editar preferencias

              </h3>
              
          </span>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body col-md-12">

          <div class="col-md-12" style="padding: 10px; padding-bottom: 0px; padding-top: 0px;">

              <h4><strong>Preferencias del papel:</strong></h4>

              <!-- Preferencias del papel Bond -->

              <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarPBond" name="editarPBond" min="1" placeholder="Papel bond" value="" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div>

                </div>

              </div>

              <!-- Preferencias del papel Químico -->

              <div class="col-xs-12" style="padding: 0px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarPQuimico" name="editarPQuimico" min="1" placeholder="Papel químico" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

                </div>

              </div>

              <div class="col-xs-12" style="padding: 0px;"><hr style="margin-top: 30px; margin-bottom: 20px;"></div>
        
          </div>

          <div class="col-md-12" style="padding: 10px; padding-top: 0px; padding-bottom: 0px;">

              <h4><strong>Preferencias de color:</strong></h4>

              <!-- Preferencias de color Blanco y Negro -->

              <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarCBN" name="editarCBN" min="1" placeholder="Blanco y negro" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div>

                </div>

              </div>

              <!-- Preferencias de color Full Color -->

              <div class="col-xs-12" style="padding: 0px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarCFC" name="editarCFC" min="1" placeholder="Full color" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

                </div>

              </div>

              <div class="col-xs-12" style="padding: 0px;"><hr style="margin-top: 30px; margin-bottom: 20px;"></div>

          </div>

          <div class="col-md-12" style="padding: 10px; padding-top: 0px;">

              <h4><strong>Preferencias de impresión:</strong></h4>

              <!-- Preferencias e impresión 1 Copia-->

              <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarIO1" name="editarIO1" min="1" placeholder="Original y 1 Copia" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div>

                </div>

              </div>

              <!-- Preferencias e impresión 2 Copias -->

              <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarIO2" name="editarIO2" min="1" placeholder="Original y 2 Copias" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

                </div>

              </div>

              <!-- Preferencias e impresión 3 Copias-->

              <div class="col-xs-12" style="padding: 0px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="editarIO3" name="editarIO3" min="1" placeholder="Original y 3 Copias" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

                </div>

              </div>
              <br>
              <br>
              <h4><strong>IVA:</strong></h4>
                      <div class="col-xs-12" style="padding: 0px;">
                        
                <div class="input-group">

                  <input type="number" class="form-control input-lg" id="iva" name="iva" min="1" placeholder="IVA" required>

                  <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

                </div>

              </div>


          </div>

        </div>

        <div class="col-xs-12" style="padding: 0px;"><hr style="margin-top:5px; margin-bottom:15px;"></div>


        <!--=====================================
        PIE DEL MODAL
        ======================================-->

        <div class="modal-footer">

          <button type="submit" class="btn btn-primary">Guardar cambios</button>

        </div>

        <?php 

          $editar = new ControladorProductos();
          $editar -> ctrEditarPreferencias();

         ?>

      </form>

    </div>

  </div>

</div>



    

        <!-- Preferencias de impresión 

        <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">

          <label>Preferencias de impresión:</label>
                  
          <div class="input-group">

            <input type="number" class="form-control input-lg" id="editarPrecioVenta" name="editarPrecioVenta" min="0" placeholder="Ingresar precio de venta" required>

            <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

          </div>

        </div><br>

        Opciones de color 

        <div class="col-xs-12" style="padding: 0px; padding-bottom: 20px;">

          <label>Opciones de color:</label>
                  
          <div class="input-group">

            <input type="number" class="form-control input-lg" id="editarPrecioVenta" name="editarPrecioVenta" min="0" placeholder="Ingresar precio de venta" required>

            <div class="input-group-addon"><strong style="font-size: 16px;">%</strong></div> 

          </div>

        </div><br><br> -->