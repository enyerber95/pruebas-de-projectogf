<div class="content-wrapper">

  <section class="content-header">

    <h1>

      Administrar reportes de pago

    </h1>

    <ol class="breadcrumb">

      <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Reportes de pago</li>

    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-body">

        <div class="box-header with-border" style="margin-bottom: 10px;">

          <button type="button" class="btn btn-default pull-right" id="daterange-btn-Reporte">
            <span>
              <i class="fa fa-calendar"></i> Rango de fecha
            </span>
            <i class="fa fa-caret-down"></i>
          </button>

        </div>

       <table class="table table-bordered table-striped dt-responsive tablas rangoFechas">

        <thead>

         <tr>

           <th style="width:10px">#</th>
           <th>Orden</th>
           <th>Cliente</th>
           <th>Número de depósito o transferencia</th>
           <th>Monto final</th>
           <th>Fecha y hora</th>
           <th>Opciones</th>

         </tr>

        </thead>
        <tbody>

          <?php

      $reportes = ControladorReportes::ctrMostrarReportes();

              foreach ($reportes as $key => $value){

                echo '<tr>

                        <td>'.$value["id_reporte_pago"].'</td>

                        <td>'.$value["orden"].'</td>

                        <td>'.$value["nombre"].' '.$value["apellido"].'</td>

                        <td>Nro. '.$value["num_deposito_transferencia"].'</td>

                        <td>Bs. '.$value["monto_final"].'</td>

                        <td>'.$value["fecha_reporte"].'</td>

                        <td>

                          <div class="btn-group">

                            <button class="btn btn-default btnImprimirFacturaAdmin" onclick="imprimirFacturaAdmin(this)" idPedido="'.$value["id_pedido"].'" idReporte="'.$value["id_reporte_pago"].'" title="Imprimir factura Admin"><i class="fa fa-file-text-o " ></i></button>

                            <button class="btn btn-default btnVerDetallesPago" data-toggle="modal" data-toggle="modal" onclick="direcciones(this)" id"btnVerDetallesPago" data-target="#verDetallesReporte" idReporte="'.$value["id_pedido"].'"><i class="fa fa-info-circle"></i></button>

                          </div>

                        </td>

                      </tr>';


              }

          ?>



        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>


<!--=====================================
MODAL "VER DETALLES DE REPORTE"
======================================-->

<div id="verDetallesReporte" class="modal fade col-md" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header">

          <span class="col-md-12" style="padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px;" aling="center"><img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">Detalles de pago - Orden <strong id="verOrden" name="verOrden">Ejemplo</strong></h3>

          </span>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body" style="padding-top: 6px; padding-bottom: 20px;">

          <div class="box-body">

            <!-- DATOS DEL CLIENTE -->

            <h4 style="margin-top: 1px">Datos del cliente:</h4>

            <div class="box-body redondeado" style="border: 1px solid #dbdbdb">


              <!-- Nombre del cliente -->

              <div class="col-md-6">

                <p align="right"><strong>Nombre del cliente:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verNombre" name="verNombre">Ejemplo</p>

              </div>

              <!-- Teléfono -->

              <div class="col-md-6">

                <p align="right"><strong>Teléfono:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verTelf" name="verTelf">Ejemplo</p>

              </div>

              <!-- Correo electrónico -->

              <div class="col-md-6">

                <p align="right"><strong>Correo electrónico:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verCorreo" name="verCorreo">Ejemplo</p>

              </div>

            </div>

            <!-- DATOS DE LA TRANFERENCIA O DEPÓSITO -->

            <h4>Datos de la transferencia o depósito:</h4>

            <div class="box-body redondeado" style="border: 1px solid #dbdbdb">

              <!-- Banco emisor -->

              <div class="col-md-6">

                <p align="right"><strong>Banco emisor:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verBancoEmisor" name="verBancoEmisor">Ejemplo</p>

              </div>

              <!-- Banco receptor -->

              <div class="col-md-6">

                <p align="right"><strong>Banco receptor:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verBancoReceptor" name="verBancoReceptor">Ejemplo</p>

              </div>

               <!-- Monto final-->

              <div class="col-md-6">

                <p align="right"><strong>Monto final:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verMonto" name="verMonto">Ejemplo</p>

              </div>

              <!-- Número de la transferencia o depósito -->

              <div class="col-md-6">

                <p align="right"><strong>Número de transferencia o depósito:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verNumero" name="verNumero">Ejemplo</p>

              </div>

              <!-- Fecha de la transferencia o depósito -->

              <div class="col-md-6">

                <p align="right"><strong>Fecha de transferencia o depósito:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verFecha" name="verFecha">Ejemplo</p>

              </div>


            </div>

          </div>

        </div>


    </div>

  </div>

</div>
