<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>

        Administrar usuarios

      </h1>

      <ol class="breadcrumb">

        <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Usuarios</li>

      </ol>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box">

        <div class="box-header with-border">

          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">
            
            Agregar usuario

          </button>

        </div>


        <div class="box-body">

          <table class="table table-bordered table-striped dt-responsive tablas">
            
            <thead>
              
              <tr>
                
                <th style="width:10px">#</th>
                <th>Nombre</th>
                <th>Departamento</th>
                <th>Último acceso</th>
                <th>Estado</th>
                <th>Acciones</th>

              </tr>

            </thead>

            <tbody>
              
                <tr>
                    
                    <td>1</td>
                    <td>Usuario Administrador</td>
                    <td>Sistemas</td>
                    <td>0000-00-00 00:00:00</td>
                    <td><button class="btn btn-success btn-xs">Activo</button></td>
                    <td>
                      <div class="btn-group">

                        <button class="btn btn-warning"><i class="fa fa-pencil"></i></button>

                        <button class="btn btn-danger"><i class="fa fa-times"></i></button>

                      </div>
                    </td>

                </tr>

            </tbody>

          </table>

        </div>
 
      </div>

    </section>

  </div>


 <!--=====================================
 =      MODAL AGREGAR USUARIO            =
 ======================================-->

 <div id="modalAgregarUsuario" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <div class="modal-header" style="background:#3c8dbc; color:white">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h4 class="modal-title">Agregar usuario</h4>

      </div>

      <div class="modal-body">

        <div class="box-body">
          
          <!--=====================================
          =        ENTRADA PARA EL NOBMRE         =
          ======================================-->

          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-user"></i></span>

              <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingresar nombre" required>

            </div>

          </div><br>

          <!--=====================================
          =       ENTRADA PARA EL USUARIO         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-key"></i></span>

              <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingresar usuario" required>

            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA LA CONTRASEÑA         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>

              <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingresar contraseña" required>

            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA EL DEPARTAMENTO       =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>

              <input type="text" class="form-control input-lg" name="nuevoDepartamento" placeholder="Ingresar departamento" required>

            </div>

          </div><br>
          
          <!--=====================================
          =  ENTRADA PARA SELECCIONAR ACCESOS     =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-users"></i></span>

              <select class="form-control input-lg" name="nuevoAcceso">
                
                <option value="">Seleccione acceso</option>
                <option value="Acceso 1">Acceso 1</option>
                <option value="Acceso 2">Acceso 2</option>
                <option value="Acceso 3">Acceso 3</option>
                <option value="Acceso 4">Acceso 4</option>
                <option value="Acceso 5">Acceso 5</option>

              </select>

            </div>

          </div>



        </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>

      </div>

    </div>

  </div>

</div>
 