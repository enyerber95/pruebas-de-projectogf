<?php 
  if($_SESSION["acceso"] === "Basico" ){

        echo '<script>


      swal({
        title: "¡No posees acceso a este modulo!",
        text: "comunicate con el administrador<br>",
        type:"error",
        showConfirmButton: true,
        confirmButtonColor: "#018ba7",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false

      }).then((result) => {
        if (result.value) {

          window.location ="inicio";

        }
      })

      </script>';

  exit();
  }

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>

        Administrar usuarios

      </h1>

      <ol class="breadcrumb">

        <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Usuarios</li>

      </ol>

    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box">

        <div class="box-header with-border">

          <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario"><i class="fa fa-plus-circle"></i> Agregar usuario</button>

        </div>


        <div class="box-body">

          <table class="table table-bordered table-striped dt-responsive tablas">
            
            <thead>
              
              <tr>
                
                <th style="width:10px">#</th>
                <th>Nombre</th>
                <th>Departamento</th>
                <th>Estado</th>
                <th>Último acceso</th>
                <th>Opciones</th>

              </tr>

            </thead>

            <tbody>

            <?php 

              $item = null;
              $valor = null;

              $usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

              foreach ($usuarios as $key => $value){
                
                echo '<tr>

                        <td>'.$value["id"].'</td>

                        <td>'.$value["nombre"].'</td>

                        <td>'.$value["departamento"].'</td>';

                        if($value["estado"] != 0){

                          echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="0" nombreUsuario="'.$_SESSION["usuario"].'" Usuario="'.$_SESSION["usuario"].'" >Activo</button></td>';

                        }else{

                          echo '<td><button class="btn btn-danger btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="1" nombreUsuario="'.$_SESSION["usuario"].'" Usuario="'.$_SESSION["usuario"].'">Inactivo</button></td>';

                        }    

                    echo '<td>'.$value["ultimo_login"].'</td>

                        <td>
                          <div class="btn-group">

                            <button class="btn btn-default btnEditarUsuario" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"><i class="fa fa-pencil-square-o"></i></button>
                            
                            <button class="btn btn-default btnEliminarUsuario" idUsuario="'.$value["id"].'"><i class="fa fa-times"></i></button>

                          </div>
                        </td>

                      </tr>';

              } 


            ?>

            </tbody>

          </table>

        </div>
 
      </div>

    </section>

  </div>


 <!--=====================================
 =      MODAL AGREGAR USUARIO            =
 ======================================-->

 <div id="modalAgregarUsuario" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

      <div class="modal-header" style="background:#018ba7; color:white">
          
          <span class="col-md-12" style="padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px;" aling="center">

                <img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">

                Agregar usuario

              </h3>
              
          </span>

        </div>

       <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

      <div class="modal-body">

        <div class="box-body">
          
          <!--=====================================
          =         ENTRADA PARA EL NOMBRE        =
          ======================================-->

          <div class="from-group">

            <div class="input-group">

              <span class="input-group-addon"><i class="fa fa-user"></i></span>
				
              <input type="text" class="form-control input-lg" name="nuevoNombre" placeholder="Ingrese el nombre" required oninvalid="setCustomValidity('El campo nombre es obligatorio')">

            </div>

          </div><br>

          <!--=====================================
          =       ENTRADA PARA EL USUARIO         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-key"></i></span>

              <input type="text" class="form-control input-lg" name="nuevoUsuario" placeholder="Ingrese el usuario" id="nuevoUsuario" required oninvalid="setCustomValidity('El campo usuario es obligatorio')">

            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA LA CONTRASEÑA         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>

              <input type="password" class="form-control input-lg" name="nuevoPassword" placeholder="Ingrese la contraseña" required oninvalid="setCustomValidity('El campo contraseña es obligatorio')">


            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA EL DEPARTAMENTO       =
          ======================================-->
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-drivers-license"></i></span>

              <select class="form-control input-lg" name="nuevoDepartamento">
                
                <option value="">Seleccione un departamento</option>
                <option value="Ventas">Ventas</option>
                <option value="Administración">Administración</option>
                <option value="Recursos Humanos">Recursos Humanos</option>
                <option value="Diseño">Diseño</option>
                <option value="Producción">Producción</option>

              </select>

            </div>

          </div><br>
          
          <!--=====================================
          =  ENTRADA PARA SELECCIONAR ACCESOS     =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-users"></i></span>

              <select class="form-control input-lg" name="nuevoAcceso">
                
                <option value="">Seleccione un acceso</option>
                <option value="Basico">Básico</option>
                <option value="Soporte">Soporte</option>
                <option value="Administrador">Administrador</option>

              </select>

            </div>

          </div>

        </div>

      </div>

      <!--=====================================
      =             PIE DEL MODAL             =
      ======================================-->

      <div class="modal-footer" >

        <button type="submit" class="btn btn-primary">Guardar usuario</button>

        <?php 

          $crearUsuario = new ControladorUsuarios();
          $crearUsuario -> ctrCrearUsuario();

         ?>

      </div>

     </form>

    </div>

  </div>

</div>


<!--=====================================
 =      MODAL EDITAR USUARIO            =
 ======================================-->

 <div id="modalEditarUsuario" class="modal fade" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

      <div class="modal-header" style="background:#018ba7; color:white">
          
          <span class="col-md-12" style="padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px;" aling="center">

                <img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">

                Editar usuario

              </h3>
              
          </span>

        </div>

       <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

      <div class="modal-body">

        <div class="box-body">
          
          <!--=====================================
          =         ENTRADA PARA EL NOMBRE        =
          ======================================-->

          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-user"></i></span>

              <input type="text" class="form-control input-lg" id="editarNombre" name="editarNombre" value="" required>

            </div>

          </div><br>

          <!--=====================================
          =       ENTRADA PARA EL USUARIO         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-key"></i></span>

              <input type="text" class="form-control input-lg" id="editarUsuario" name="editarUsuario" value="" readonly>

            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA LA CONTRASEÑA         =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>

              <input type="password" class="form-control input-lg" name="editarPassword" placeholder="Escriba la nueva contraseña">
            
            </div>

          </div><br>

          <!--=====================================
          =    ENTRADA PARA EL DEPARTAMENTO       =
          ======================================-->
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-drivers-license"></i></span>

              <select class="form-control input-lg" name="editarDepartamento" required>
                
                <option value="" id="editarDepartamento"></option>
                <option value="Ventas">Ventas</option>
                <option value="Administración">Administración</option>
                <option value="Recursos Humanos">Recursos Humanos</option>
                <option value="Diseño">Diseño</option>
                <option value="Producción">Producción</option>

              </select>

            </div>

          </div><br>
          
          <!--=====================================
          =  ENTRADA PARA SELECCIONAR ACCESOS     =
          ======================================-->
          
          <div class="from-group">
            
            <div class="input-group">
              
              <span class="input-group-addon"><i class="fa fa-users"></i></span>

              <select class="form-control input-lg" name="editarAcceso">
                
                <option value="" id="editarAcceso"></option>
                <option value="Básico">Básico</option>
                <option value="Soporte">Soporte</option>
                <option value="Administrador">Administrador</option>

              </select>

            </div>

          </div>

        </div>

      </div>

      <!--=====================================
      =             PIE DEL MODAL             =
      ======================================-->

      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Guardar cambios</button>
        
        </div>

        <?php 

          $editarUsuario = new ControladorUsuarios();
          $editarUsuario -> ctrEditarUsuario();

         ?>

     </form>

    </div>

  </div>

</div>

<?php

  $borrarUsuario = new ControladorUsuarios();
  $borrarUsuario -> ctrBorrarUsuario();

?> 

 