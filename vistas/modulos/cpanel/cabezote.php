<header class="main-header">
	
	<!--==============================
	=            LOGOTIPO            =
	===============================-->
	
	<a href="inicio" class="logo" style="background-color:#01819b">
		
	
		<!-- logo mini -->
		<span class="logo-mini">
			
			<img src="../vistas/img/plantilla/logo-mini.png" class="img-responsive" style="padding: 3px" alt="">

		</span>

		<!-- logo normal -->
		<span class="logo-lg">
			
			<img src="../vistas/img/plantilla/logo-completo.png" class="img-responsive" style="padding: 7px 0px" alt="">

		</span>

	</a>
	
    <!--==============================
	=      BARRA DE NAVEGACIÓN       =
	===============================-->
	
	<nav class="navbar navbar-static-top" role="navigation">
		
		<!-- boton de navegación -->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        	
        	<span class="sr-only">Toggle navigation</span>

		</a>
		
		<!-- perfil de usuario -->

		<div class="navbar-custom-menu">
			
			<ul class="nav navbar-nav">
				
				<li class="dropdown user user-menu">
					
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"">
						
						<img src="../vistas/img/usuario/user.png" class="user-image" alt="">

						<span class="hidden-xs"><?php echo $_SESSION["nombre"]; ?></span>


					</a>
        			
        			<!-- Dropdown toggle -->

					<ul class="dropdown-menu">
			
						<li class="user-body">
				
							<div class="pull-right">
					
									<a href="salir" class="btn btn-default btn-flat">Salir</a>

							</div>

						</li>

					</ul>


				</li>

			</ul>

		</div>





	</nav>




</header>