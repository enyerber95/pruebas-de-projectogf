<div class="content-wrapper" id="form1">

  <section class="content-header">

    <h1>

      Administrar clientes

    </h1>

    <ol class="breadcrumb">

      <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
      
      <li class="active">Clientes</li>

    </ol>
  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button type="button" class="btn btn-default pull-right" id="daterange-btn">
          <span>
            <i class="fa fa-calendar"></i> Rango de fecha
          </span>
          <i class="fa fa-caret-down"></i>
        </button>  
       </div>

       <div class="box-body">

        <table class="table table-bordered table-striped dt-responsive tablas rangoFechas">

          <thead>

            <tr>

              <th style="width:10px">#</th>
              <th>Nombre</th>
              <th>Apellido</th>
              <th>Cédula</th>
              <th>Correo electrónico</th>
              <th>Teléfonos</th>
              <th>Estado</th>
              <th>Ingreso al sistema</th>

            </tr> 

          </thead>

          <tbody>

            <?php

                $item = null;
                $valor = null;

                $clientes = ControladorClientes::ctrMostrarClientes($item, $valor);

                foreach ($clientes as $key => $value) {
                  
                  echo '<tr>

                        <td>'.$value["id_cliente"].'</td>

                        <td>'.$value["nombre"].'</td>

                        <td>'.$value["apellido"].'</td>

                        <td>V-'.$value["cedula"].'</td>

                        <td>'.$value["email"].'</td>

                        <td>'.$value["telefono_local"].' / '.$value["telefono_movil"].'</td>';

                        if($value["estado"] == 1){
                          echo '<td><button class="btn btn-success btn-xs btnActivarCliente" idCliente="'.$value["id_cliente"].'" estadoCliente="0" usuario="'.$_SESSION["usuario"].'" onclick="activarCliente(this)" nombreusuario="'.$_SESSION["nombre"].'">Activo</button></td>';

                        }else{

                          echo '<td><button class="btn btn-danger btn-xs btnActivarCliente" idCliente="'.$value["id_cliente"].'" estadoCliente="1" usuario="'.$_SESSION["usuario"].'" onclick="activarCliente(this)" nombreusuario="'.$_SESSION["nombre"].'">Inactivo</button></td>';

                        }

                  echo '<td>'.$value["fecha_ingreso"].'</td>';
                }

            
            ?> 


          </tbody>

        </table>


      </div>

    </div>

  </section>

</div>