<?php 

	session_destroy();

	$accion='Cierre de Sesión';
	$descripcion="El usuario ".$_SESSION["usuario"]." ha salido del sistema";
	$bitacora = new ModeloAuditoria;
	$bitacora->mdlRegistrarBitacora($accion,$descripcion);

	echo '<script>

		window.location = "ingreso";

	</script>';


 ?>