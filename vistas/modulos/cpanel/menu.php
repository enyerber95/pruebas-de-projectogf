<aside class="main-sidebar">
	
	<section class="sidebar">
		
			<ul class="sidebar-menu">
				
				<li class="active">
					<a href="inicio">

						<i class="fa fa-home"></i>
						<span>Inicio</span>

					</a>

				</li>

				<li>
					<a href="pedidos">

						<i class="fa fa-shopping-cart"></i>
						<span>Pedidos</span>

					</a>

				</li>

				<li>
					<a href="clientes">

						<i class="fa fa-user"></i>
						<span>Clientes</span>

					</a>

				</li>

				<li>
					<a href="reportes">

						<i class="fa fa-bank"></i>
						<span>Reportes de Pago</span>

					</a>

				</li>
				<?php if ($_SESSION["acceso"] == 'Administrador' || $_SESSION["acceso"] == 'Soporte') {
					echo'<li>
					<a href="productos">

						<i class="fa fa-product-hunt"></i>
						<span>Productos</span>

					</a>

				</li>


				<li>
					<a href="usuarios">

						<i class="fa fa-users"></i>
						<span>Usuarios</span>

					</a>

				</li>';}
				if ($_SESSION["acceso"] == 'Administrador') {
					echo '<li>
					<a href="auditoria">

						<i class="fa fa-list"></i>
						<span>Auditoría</span>

					</a>

				</li>';
					# code...
				}else{
					
				}
				
				?>

			</ul>

	</section>

</aside>