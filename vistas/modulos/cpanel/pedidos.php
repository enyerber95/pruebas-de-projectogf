<input type="hidden" id="usuario" value="<?php echo $_SESSION["usuario"]?>">
<input type="hidden" id="nombre" value="<?php echo $_SESSION["nombre"]?>">


<div class="content-wrapper">

  <section class="content-header">

    <h1>

      Administrar pedidos

    </h1>

    <ol class="breadcrumb">

      <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Pedidos</li>

    </ol>

  </section>

  <section class="content">

    <div class="box">

      <div class="box-header with-border">
        <button type="button" class="btn btn-default pull-right" id="daterange-btn-pedidos">
          <span>
            <i class="fa fa-calendar"></i> Rango de fecha
          </span>
          <i class="fa fa-caret-down"></i>
        </button>
      </div>

      <div class="box-body">

       <table class="table table-bordered table-striped dt-responsive tablas rangoFechas">

        <thead>

         <tr>

           <th style="width:10px">#</th>
           <th>Orden</th>
           <th>Cliente</th>
           <th>Producto</th>
           <th>Monto</th>
           <th>Fecha de emisión</th>
           <th>Estado</th>
           <th>Opciones</th>

         </tr>

        </thead>

        <tbody>

          <?php


              $pedidos = ControladorPedidos::ctrMostrarTodosPedidos();

              foreach ($pedidos as $key => $value){
                $var=$value["id_pedido"];
                $val=ControladorPedidos::ctrlMostrarClientePedido($value['id_pedido']);
                echo '<tr>

                      <td>'.$value["id_pedido"].'</td>

                      <td>'.$value["orden"].'</td>
                      <td>'. $val["nombre"].' '.$val["apellido"]. '</td>



                      <td>'.ControladorProductos::ctrMostrarDetalleProductos($value["id_producto"],'nombre').'</td>
                      <td>'.$value["monto"].'</td>



                      <td>'.$value["fecha_emision"].'</td>';


                      if ($value["estado"] == 'Pendiente') {

                        echo '<td><button class="btn btn-warning btn-xs btnCambiarEstado" idPedido="'.$value["id_pedido"].'" onclick="cambiarEstado(this)" estadoPedido="Pendiente" usuario="'.$_SESSION["usuario"].'" nombreusuario="'.$_SESSION["nombre"].'">Pendiente</button></td>';

                      }elseif ($value["estado"] == 'Comprobar') {

                        echo '<td><button class="btn btn-success btn-xs btnCambiarEstado" idPedido="'.$value["id_pedido"].'" onclick="cambiarEstado(this)"  estadoPedido="Comprobar" usuario="'.$_SESSION["usuario"].'" nombreusuario="'.$_SESSION["nombre"].'">En Proceso</button></td>';

                      }elseif ($value["estado"] == 'Rechazado') {

                        echo '<td><button class="btn btn-danger btn-xs btnCambiarEstado" idPedido="'.$value["id_pedido"].'" onclick="cambiarEstado(this)"  estadoPedido="Rechazado" usuario="'.$_SESSION["usuario"].'" nombreusuario="'.$_SESSION["nombre"].'">Rechazado</button></td>';

                      }else{

                          echo '<td><button class="btn btn-primary btn-xs btnCambiarEstado" idPedido="'.$value["id_pedido"].'" onclick="cambiarEstado(this)"  estadoPedido="Finalizado" usuario="'.$_SESSION["usuario"].'" nombreusuario="'.$_SESSION["nombre"].'">Finalizado</button></td>';

                      }


                 echo '<td>

                        <div class="btn-group">
                          <button class="btn btn-default btnImprimirFactura" onclick="imprimirFactura(this)" idPedido="'.$value["id_pedido"].'" idProducto="'.$value["id_producto"].'" ><i class="fa fa-print " ></i></button>


                          <button class="btn btn-default btnVerDetallesEnvio" data-toggle="modal" data-target="#verDetallesEnvio" onclick="verDetalles(this)" idPedido="'.$value["id_pedido"].'"><i class="fa fa-truck"></i></button>

                        </div>

                      </td>

                    </tr>';


              }



           ?>



        </tbody>

       </table>

      </div>

    </div>

  </section>

</div>

<!--=====================================
MODAL "VER DETALLES DE ENVÍO"
======================================-->

<div id="verDetallesEnvio" class="modal fade col-md" role="dialog">

  <div class="modal-dialog">

    <div class="modal-content">

        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->

        <div class="modal-header">

          <span class="col-md-12" style="padding-right: 10px; padding-left: 10px;">

              <button type="button" class="close" data-dismiss="modal">&times;</button>

              <h3 style="margin-top: 0px; margin-bottom: 0px;" aling="center">

                <img src="../vistas/img/plantilla/logo-mini.png" style=" vertical-align: middle" width="60" height="55">

                Detalles de envío - Orden <strong id="verOrden" name="verOrden"></strong>

              </h3>

          </span>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body" style="padding-top: 6px; padding-bottom: 20px;">

          <div class="box-body">

            <h4 style="margin-top: 1px">Dirección de envío:</h4>

            <div class="box-body redondeado" style="border: 1px solid #dbdbdb">

              <!-- Nombre y apellido -->

              <div class="col-md-6">

                <p align="right"><strong>Nombre y apellido:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verNombre" name="verNombre">Ejemplo</p>

              </div>

              <!-- RIF o cédula -->

              <div class="col-md-6">

                <p align="right"><strong>RIF o cédula:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verRifCedula" name="verRifCedula">Ejemplo</p>

              </div>

              <!-- RIF o cédula -->

              <div class="col-md-6">

                <p align="right"><strong>Orden:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verOrdeasdan" clase="verOrden">Ejemplo</p>

              </div>

              <!-- Nombre de la empresa -->

              <div class="col-md-6">

                <p align="right"><strong>Nombre de la empresa:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verEmpresa" name="verEmpresa">Ejemplo</p>

              </div>

              <!-- Sector/Urbanización -->

              <div class="col-md-6">

                <p align="right"><strong>Sector/Urbanización:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verSector" name="verSector">Ejemplo</p>

              </div>

              <!-- Avenida/Calle -->

              <div class="col-md-6">

                <p align="right"><strong>Avenida/Calle:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verAvenida" name="verAvenida">Ejemplo</p>

              </div>

              <!-- Edificio/Quinta/Casa -->

              <div class="col-md-6">

                <p align="right"><strong>Edificio/Quinta/Casa:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verEdificio" name="verEdificio">Ejemplo</p>

              </div>

              <!-- Estado -->

              <div class="col-md-6">

                <p align="right"><strong>Estado:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verEstado" name="verEstado">Ejemplo</p>

              </div>

              <!-- Ciudad -->

              <div class="col-md-6">

                <p align="right"><strong>Ciudad:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verCiudad" name="verCiudad">Ejemplo</p>

              </div>

              <!-- Teléfono -->

              <div class="col-md-6">

                <p align="right"><strong>Teléfono:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" id="verTelf" name="verTelf">Ejemplo</p>

              </div>

              <!-- Información adicional -->

              <div class="col-md-6">

                <p align="right" style="margin-bottom: 0px"><strong>Información adicional:</strong></p>

              </div>

              <div class="col-md-6">

                <p align="left" style="margin-bottom: 0px" id="verInfo" name="verInfo">Ejemplo</p>

              </div>


            </div>

            <!-- MÉTODO DE ENVÍO -->

            <h4  id="verMetodoEnvio" name="verMetodoEnvio">Método de envío:</h4>

          </div>

        </div>


    </div>

  </div>

</div>
