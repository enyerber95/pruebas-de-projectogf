<?php 
  if($_SESSION["acceso"] != "Administrador"){

      echo '<script>


      swal({
        title: "¡No tienes permitido el acceso!<br> Porfavor comunicate con el administrador",
        text: "Gracias por elegirnos<br>",
        type:"error",
        showConfirmButton: true,
        confirmButtonColor: "#018ba7",
        confirmButtonText: "Cerrar",
        closeOnConfirm: false

      }).then((result) => {
        if (result.value) {

          window.location ="inicio#modalIniciarSesion";

        }
      })

      </script>';

  exit();
  }


?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <h1>
        Auditoría
      </h1>

      <ol class="breadcrumb">

        <li><a href="../../../index.php"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Auditoría</li>

      </ol>

    </section>



    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        
        <div class="box-header with-border">
        
          <h3 class="box-title">Bicatora</h3>

        

         <?php echo '<button type="button" class="btn btn-primary pull-right" id="respaldo" usuario="'.$_SESSION["usuario"].'" nombreusuario="'.$_SESSION["nombre"].'">
          <span>
               Respaldar Base de Datos
            </span>
          </button> ';?>


             <button type="button" class="btn btn-default pull-right" id="daterange-btn-au">
            <span>
              <i class="fa fa-calendar"></i> Rango de fecha
            </span>
            <i class="fa fa-caret-down"></i>
          </button> 
        </div>

        <div class="box-body">
           <table class="table table-bordered table-striped dt-responsive tablas rangoFechas">
            
            <thead>
              
              <tr>
                
                <th style="width:10px">#</th>
                <th>Usuario Responsable</th>
                <th>Nombre</th>
                <th>Acción</th>
                <th>Fecha Realizado</th>
                <th>Descripción</th>

              </tr>

            </thead>

            <tbody>

            <?php 

              

              $bitaora = ControladorAuditoria::ctrMostrarBitacora();

              foreach ($bitaora as $key => $value){
                
                echo '<tr>
                        <td>'.$value["id_bitacora"].'</td>

                        <td>'.$value["user_responsable"].'</td>

                        <td>'.$value["nombre_responsable"].'</td>

                        <td>'.$value["accion"].'</td>
                        
                        <td>'.$value["fecha_realizado"].'</td>

                        <td>'.$value["descripcion"].'</td>
                        ';

                       

              } 


            ?>

            </tbody>

          </table>
        </div>
      </div>
      <!-- /.box -->

    </section>
    <section class="content">
        <div class="box">
          <div class="box-header with-border">
        
          <h2 class="box-title" style="font-size: 50px;"> Restauración </h2>
            <div class="box-body">
             <?php
              if (! empty($response)) {
                  ?>
              <div class="response <?php echo $response["type"]; ?>
                  ">
                  <?php echo nl2br($response["message"]); ?>
              </div>
              <?php
              }
              ?>
              <form method="post" action="" enctype="multipart/form-data"
                  id="frm-restore">
                  <div class="form-row">
                      <div><h3>Elige tu Backup .sql</h3></div><br>
                      <div>
                          <input type="file" name="backup_file" class="input-file" />
                      </div>
                  </div>
                  <div>
                      <input type="submit" name="restore" value="Restore"
                          class="btn-action" />
                  </div>
              </form>

            </div>


          </div>

          
        </div>
      
    </section>
    <!-- /.content -->
  </div>
<?php
       
 if (! empty($_FILES)) {
    // Validating SQL file type by extensions
    if (! in_array(strtolower(pathinfo($_FILES["backup_file"]["name"], PATHINFO_EXTENSION)), array(
        "sql"
    ))) {
        $response = array(
            "type" => "error",
            "message" => "Tipo de archivo inválido"
        );
        
         echo '<script>
            swal({
              title: "¡Ha ocurrido un problema!",
              text: "¡Por favor Verifique el tipo de archivo!",
              type:"error",
                showConfirmButton: true,
              confirmButtonColor: "#018ba7",
              confirmButtonText: "Cerrar",
              closeOnConfirm: false

                }).then((result) => {
                if (result.value) {

                window.location ="auditoria";

                }
            })
          </script>';
    } else {
    $validate=1;

        if (is_uploaded_file($_FILES["backup_file"]["tmp_name"])) {
            move_uploaded_file($_FILES["backup_file"]["tmp_name"], $_FILES["backup_file"]["name"]);
            $response = ControladorAuditoria::restoreMysqlDB($_FILES["backup_file"]["name"]);
        }
    }
}
?>