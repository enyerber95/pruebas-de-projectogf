

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="formasLibres">Formas Libres</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Producto 1</a></li>
							<li><a href="#">Producto 2</a></li>
							<li><a href="#">Producto 3</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>PUBLICIDAD<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (FACTURAS)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style=" border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1 style="margin-bottom: 0px;"><strong>Quiénes somos</strong></h1>

					</div>

					<div class="col-md-12" style="text-align: justify; padding-top: 10px;">

						<h4><strong>Somos una Imprenta autorizada por el SENIAT</strong></h4>

						<p>GrafoFormas HB es una empresa fundada en el año 2005. Cuenta con personal calificado especializado en diseñar e imprimir facturas siguiendo las normativas del SENIAT.</p>
						<p>El servicio fácil y rápido de GrafoFormasHB.com permite compras en línea las 24 horas del día y un servicio a domicilio, lo que lo ha posicionado como el producto más exitoso de impresión en línea de documentos legales a nivel nacional.</p>

						<p>Contamos con la certificación del SENIAT para la elaboración de facturas, y demás documentos fiscales autorizado por providencia N° SENIAT/10/00425 de fecha 28-02-2008, cuya emisión se rige por la providencia administrativa Nº SNAT/2007/0592.</p>

					</div>

				</div>	

			</div>

			<div class="container col-md-9 pull-right" style="margin-top: 15px; text-align: center; text-align: center;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px; padding-bottom: 30px;">

					<img class="" src="vistas\img\clientesgf/Fachada.png" style="border-radius: 5px 5px 5px 5px;
					-moz-border-radius: 5px 5px 5px 5px;
					-webkit-border-radius: 5px 5px 5px 5px;
					border: 0px solid #000000; 
					-webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.5);
					-moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.5);
					box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.5);">


				</div>	

			</div>


			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-12" style="text-align: justify;  margin-bottom: 5px;">

						<p>Nuestra fortaleza principal es el trabajo en equipo e individual de todos los que integramos GrafoFormas HB, C.A., orientado al cliente a la mejor elección; teniendo en cuenta siempre que debe existir una comunicación abierta, gran disciplina y buscando la excelencia con la ética profesional, seguridad y gran sentido de pertenencia.</p>

					</div>

				</div>

			</div>

			<div class="container col-md-9 pull-right" style="margin-bottom: 0px; margin-top: 0px;">

				<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-6" style="text-align: justify; padding-left: 15px; padding-right: 10px;">

							<div class="panel panel-primary">

								<div class="panel-body">

									<div class="col-md-12" style="justify-content: center;">

										<h3><strong style="color:#DA4A8B;"><i class="fa fa-bullseye"></i> Misión</strong></h3>

										<p><strong>Elaborar formas de la más alta calidad para la satisfacción de nuestros clientes.</strong></p>

									</div>
									
								</div>
								
							</div>			

					</div>

					<div class="col-md-6" style="text-align: justify; padding-left: 10px; padding-right: 15px;">

						<div class="panel panel-primary">

							<div class="panel-body">

								<div class="col-md-12" style="justify-content: center;">

									<h3><strong style="color:#DA4A8B;"><i class="fa fa-eye"></i> Visión</strong></h3>

									<p><strong>Ser una empresa líder en la conversión, elaboración y comercialización de formas.</strong></p>

								</div>
									
							</div>
								
						</div>			

					</div>

				</div>	

			</div>




			</div>

			

							</div>

						</div> 	

					</div>		

				</div>

			</div>




		</div>	

	</div>

	
