
	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--=====================================
			   CONTENIDO (COTIZADOR EN LÍNEA)
			======================================-->

			<div class="container col-md-12 pull-right">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Formulario de pedido para rollos físcales</strong></h1>

						<p><strong>(Cantidad mínima 50 rollos de 11 pulgadas por diseño)</strong></p>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     FORMULARIO COTIZADOR 1             =
				   		======================================-->

				   		<div class="col-md-6" style="padding: 0px;" id="rollosform1">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">

				   				<div class="panel panel-primary">

				   					<div class="panel-heading" style="text-align: center"><strong>Paso 1 - Datos del cliente</strong></div>

				   					<div class="panel-body" >

				   						<form class="form col-md-12" style="padding: 0px;"> 

				   							<div class="from-group col-md-6" style="text-align: justify;">

				   								<div class="input-group">

				   									<label>Nombre y Apellido:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Teléfono:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Correo electrónico:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   							</div>

				   							<div class="from-group col-md-6" style="text-align: left;">

				   								<div class="input-group">

				   									<label>Nombre de la empresa:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Dirección de la empresa:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>RIF:</label>

				   									<input type="text" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Adjuntar RIF de la empresa (Actualizado):</label>

				   									<input type="file" class="form-control">

				   								</div><br>

				   							</div>

				   						</form>	

				   					</div>

				   				</div>

				   				<a href="rollos"><button class="btn btn-default pull-left"><i class="fa fa-chevron-left"></i> Regresar</button></a>

				   				<button class="btn btn-primary pull-right" value="Mostrar" onclick="mostrar()">Siguiente paso <i class="fa fa-chevron-right"></i></button>

				   			</div>

				   		</div>

				   	</div>	

						<!--=====================================
				   		=     FORMULARIO COTIZADOR 2             =
				   		======================================-->

				   		<div class="col-md-6" style="padding: 0px; display: none;" id="rollosform2">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">

				   				<div class="panel panel-primary">

				   					<div class="panel-heading" style="text-align: center"><strong>Paso 2 - Características del producto</strong></div>

				   					<div class="panel-body" >

				   						<form class="form col-md-12" style="padding: 0px;"> 

				   							<div class="from-group col-md-6" style="text-align: justify;">

				   								<div class="input-group">

				   									<label>Tipo de papel:</label>

				   									<select class="form-control">

				   										<option value="">Elegir una opción</option>
				   										<option value="">Papel bond</option>
				   										<option value="">Papel químico</option>

				   									</select>

				   								</div><br>

				   								<div class="input-group">

				   									<label>Número de partes:</label>

				   									<select class="form-control">

				   										<option value="">Elegir una opción</option>
				   										<option value="">Original y 1 copia</option>
				   										<option value="">Original y 2 copia</option>
				   										<option value="">Original y 3 copia</option>

				   									</select>

				   								</div><br>

				   								<div class="input-group">

				   									<label>Preferencias de color:</label>

				   									<select class="form-control">

				   										<option value="">Elegir una opción</option>
				   										<option value="">Blanco y negro</option>
				   										<option value="">Full color</option>

				   									</select>

				   								</div><br>

				   								<div class="input-group">

				   									<label>Cantidad:</label>

				   									<select class="form-control">

														
				   										<option value="">50</option>
				   										<option value="">100</option>
				   										<option value="">250</option>
				   										<option value="">300</option>
				   										<option value="">350</option>	
				   										<option value="">400</option>

				   									</select>

				   								</div><br>

				   							</div>

				   							<div class="from-group col-md-6" style="text-align: left;">

				   								<div class="input-group">

				   									<label>Número de rollo desde:</label>

				   									<input type="text" class="form-control" placeholder="000001">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Número de rollo hasta:</label>

				   									<input type="text" class="form-control" placeholder="000001">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Adjuntar modelo de rollo físcal. Puede ser escaneado o una foto:</label>

				   									<input type="file" class="form-control">

				   								</div><br>

				   								<div class="input-group">

				   									<label>Incluir logo:</label>

				   									<input type="file" class="form-control">

				   								</div><br>

				   							</div>

				   						</form>	

				   					</div>

				   				</div>

				   				<button class="btn btn-default pull-left" value="Mostrar" onclick="mostrar2()"><i class="fa fa-chevron-left"></i> Regresar</button>

				   				<button class="btn btn-primary pull-right">Seguir y comprar este producto <i class="fa fa-shopping-cart"></i></button>

				   			</div>

				   		</div>

					</div>

			   		</div>

				</div>	

			</div>

		</div>	

	</div>

