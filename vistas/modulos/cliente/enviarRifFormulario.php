

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="#">Formas Libres</a></li>
							<li><a href="#">Notas Físcales</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Tarjetas de Inventario</a></li>
							<li><a href="#">Tarjetas de Presentacíón</a></li>
							<li><a href="#">Recipes Médicos</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>ESPACIO<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (REPORTE DE PAGO)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px;">

				<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1><strong>Envía tu RIF</strong></h1>

					</div><br>

					<div class="col-md-12" style="padding-left: 0px; padding-top: 30px;">

						<div class="col-md-12" style="padding: 0px; text-align: justify;">
								
							<p>• Luego de haber creado la orden de impresión de facturas u otros documentos, es necesario enviar el RIF. Sí aún no lo has enviado utiliza el siguiente formulario.</p>

							<p>• La imagen del RIF puede ser obtenida a través de un escáner o cámara digital.</p>

							<p>• Ten especial cuidado en que la imagen no esté borrosa y sea completamente legible. La imagen no debe pesar más de 2MB.</p><br>
						</div>

						<div class="col-md-6" style="padding: 0px; text-align: justify;">

							<div class="panel panel-primary">

								<div class="panel-body"><br>

									<!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->

									<form class="form col-md-12" style="padding: 0px;"> 

										<!-- PARTE IZQUERDA -->

										<div class="from-group col-md-12" style="text-align: justify;">
											
											<div class="input-group col-md-12">

												<label>Selecciona el número de orden relacionada con el RIF que deseas enviar:</label>

												<select class="form-control">

													<option value="">Elegir número orden</option>

													<option value="Administrador">FR-000001</option>

													<option value="">FR-000002</option>

												</select>

											</div><br>

											<div class="col-md-12" style="padding: 0px;">

  												<p style="color: #FF5A5A;">No hay ordenes, presiona <a href="#"><b style="color: #cc4747;">Aquí</b></a> para regresar.</p>

											</div><br>

											<div class="input-group col-md-12">

												<label>Elije la razón social relacionada con el RIF que deseas enviar:</label>

												<select class="form-control">

													<option value="">Elegir razón social</option>

													<option value="Administrador">Inversiones T.I C.A</option>

												</select>

											</div><br>

											<div class="input-group col-md-12">

												<label>Selecciona el archivo a enviar:</label>

												<input type="file" class="form-control">

											</div>

										</div>

									</form>

								</div><br>

							</div>


						</div>

					</div>	

				</div>	

			</div>

			<div class="container col-md-9">

				<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-6" style="text-align: right;">

						<button class="btn btn-primary">Enviar RIF</button>

					</div>

				</div>	

			</div>	

			


		</div>	

	</div>
