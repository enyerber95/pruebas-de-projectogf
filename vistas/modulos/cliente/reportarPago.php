
	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="formasLibres">Formas Libres</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Tarjetas de Inventario</a></li>
							<li><a href="#">Tarjetas de Presentacíón</a></li>
							<li><a href="#">Recipes Médicos</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>ESPACIO<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (REPORTE DE PAGO)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom:0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style=" border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1><strong>Pago a través de depósito o transferencia</strong></h1>

					</div>

					<div class="col-md-6" style="padding-left: 0px;">

						<div class="col-md-12" style="padding-right: 0px; text-align: justify;">


							<p style="padding-bottom: 10px; margin-bottom: 0px; margin-top: 8px;">• Ya finalizastes tu pedido.</p>

							<p style="padding-bottom: 10px; margin-bottom: 0px; margin-top: 8px;">• Decidiste pagar con depósito bancario.</p>

							<p style="padding-bottom: 10px; margin-bottom: 0px; margin-top: 8px;">• Decidiste pagar a través de transferencia electrónica.</p>

							<p style="padding-bottom: 10px; margin-bottom: 0px; margin-top: 8px;">• Necesitas saber el número de cuenta.</p>

							<p style="padding-bottom: 10px; margin-bottom: 0px; margin-top: 8px;">• Quieres reportar el pago.</p>

						</div>

					</div>	

					<div class="col-md-6" style="padding-right: 0px; padding-right: 0px;">

						<div class="col-md-12" style="padding: 0px; text-align: right;">

							<img src="vistas/img/clientesgf/pagot.png" class="">

						</div>	

					</div>

				</div>	

			</div>

			<div class="container col-md-9 pull-right" style="margin-bottom:0px; margin-top:0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<br><h4><strong>Si decidiste realizar el pago a través de depósito o transferencia electrónica bancaria te sugerimos considerar lo siguiente:</strong></h4><br>

						<p style="padding-bottom: 10px;">• Procura que el banco de origen y destino de la transferencia sea el mismo. Si el banco de origen y destino son diferentes el comienzo del proceso de impresión de tu pedido podría retrasarse hasta (03) días hábiles.</p>

						<p style="padding-bottom: 10px;">• Sí el depósito lo realizaste con un cheque, es importante tener en cuenta que dependiendo de los bancos involucrados, se podría retrasar el comienzo del proceso de impresión de tu pedido en hasta dos (02) días hábiles, debido a los procesos internos administrativos de cada banco.</p>

						<p>• Es muy importante que reportes el pago realizado, en GrafoFormasHB.com no podríamos saber si lo realizaste o no a menos que lo reportes. Tu pedido no comenzará a imprimirse hasta que el pago sea reportado y verificado.</p><br>

					</div>

				</div>	

			</div>	

			<div class="container col-md-9 pull-right" style="margin-top: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<div class="col-md-9" style="padding-left: 0px">

							<br><h4><strong>Reporta el pago</strong></h4>

							<p>Reporta tu depósito o transferencia haciendo clic en el siguiente botón.</p><br>

						</div>

						<div class="col-md-3" style="padding-top: 38px; text-align: right;">	

							<a href="reportarPagoFormulario"><button class="btn btn-primary">Reporta tu pago</button></a>

						</div>

					</div>	

				</div>

			</div>
					
			<div class="container col-md-9 pull-right" style="padding-top: 0px; margin-top: 0px;">

				<div class="container col-md-12" style="padding:0px;">

					<div class="panel panel-primary">

						<div class="panel-heading"><strong>Información de cuentas bancarias</strong></div>

						<div class="panel-body">

							<div class="col-md-12">

								<strong>Beneficiario:</strong>

								<p style="padding-top: 10px;">Los pagos a través de depósitos y transferencias bancarias deben realizarse a la cuenta corriente a nombre de: <br> <h4>GrafoFormas HB, C.A. RIF: J-31328599-4.</h4></p><hr>

								<strong>Números de cuenta:</strong><br>


								<div class="col-md-3" style="padding-left: 0px; padding-top: 10px;">

									<img src="vistas\img\clientesgf/mercantil.png"><br><br>

									<img src="vistas\img\clientesgf/banesco.png">

								</div>

								<div class="col-md-9" style="font-size: 20px; padding-left: 5px;">

									<p style="padding-bottom: 40px; padding-top: 20px; margin-bottom: 0px;">0105-0035-40-32148623</p>

									<p style=" margin-bottom: 0px; padding-bottom: 10px;">0134-0031-89-031110011</p>

								</div>	

							</div>

						</div> 	

					</div>		

				</div>

			</div>




		</div>	

	</div>
