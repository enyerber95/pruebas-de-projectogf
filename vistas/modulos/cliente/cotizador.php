

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--=====================================
			   CONTENIDO (COTIZADOR EN LÍNEA)
			======================================-->

			<div class="container col-md-12 pull-right" style="margin-top: 0px;"

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Cotizador en línea</strong></h1>

					</div>

					<!--=====================================
					= BARRA SELECTOR DE PRODUCTOS (COTIZADOR)
					======================================-->

					<div class="col-md-3" style="padding-top: 35px; padding-left: 0px;">

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>Productos</strong></div>

							<div class="panel-body">

								<div class="nav nav-pills nav-stacked">
									<li><a class="producto" href="#" id="FC" idProducto="1">Facturas carta</a></li>
									<li><a class="producto" href="#" id="FMC" idProducto="7">Facturas media carta</a></li>
									<li><a class="producto" href="#" id="FLC" idProducto="13">Formas libres carta</a></li>
									<li><a class="producto" href="#" id="FLMC" idProducto="16">Formas libres media carta</a></li><hr>
									<li><a href="#">Producto 1</a></li>
									<li><a href="#">Producto 2</a></li>
									<li><a href="#">Producto 3</a></li>
								</div>

							</div>

						</div>

					</div>

					<!--=====================================
			   		=     FORMULARIO COTIZADOR              =
					======================================-->

					<div class="col-md-4" style="padding-left: 0px; padding-top: 35px;">

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>Opciones</strong></div>

							<div class="panel-body">

								<form class="form col-md-12" style="padding: 0px;"> 
										
									<div class="from-group col-md-12" style="text-align: justify;">

											<input type="hidden" id="precioBase" name="precioBase">

											<div class="input-group col-md-4">

												<label>Cantidad:</label>

									        	<select class="form-control col-md-12 preferencia" id="cantidad">

									        		<option value="1">1</option>

									        		<option value="2">2</option>

									        		<option value="3">3</option>

									        		<option value="4">4</option>

									        		<option value="5">5</option>

									        		<option value="6">6</option>

									        		<option value="8">8</option>

									        		<option value="10">10</option>

									        		<option value="20">20</option>

									        		<option value="30">30</option>

									        		<option value="40">40</option>

									        		<option value="50">50</option>

									        		<option value="60">60</option>

									        		<option value="70">70</option>

									        		<option value="80">80</option>

									        		<option value="90">90</option>

									        		<option value="100">100</option>

									        	</select>

											</div><br>

											<div class="input-group col-md-12">

												<label>Preferencias del papel:</label>

									        	<select class="form-control col-md-12 preferencia" id="pPapel">

									        		<option value="Papel bond">Papel bond</option>

									        		<option value="Papel químico">Papel químico</option>

									        	</select>

											</div><br>

											<div class="input-group col-md-12">

												<label>Preferencias de impresión:</label>

									        	<select class="form-control col-md-12 preferencia" id="pImpresion">

									        		<option value="Original y 1 copia">Original y 1 copia</option>

									        		<option value="Original y 2 copias">Original y 2 copias</option>

									        		<option value="Original y 3 copias">Original y 3 copias</option>

									        	</select>

											</div><br>

											<div class="input-group col-md-12">

												<label>Opciones de color:</label>

									        	<select class="form-control preferencia" id="pColor">

									        		<option value="Blanco y negro">Blanco y negro</option>

									        		<option value="Full color">Full color</option>

									        	</select>

											</div><br>

											<div class="input-group col-md-12">
												
												<h4><strong>Precio:</strong></h4>

												<input type="text" class="form-control input-lg" id="actualizarPrecio" name="actualizarPrecio" disabled>
												<br>
												<h4><strong>Precio BsS:</strong></h4>
												<input type="text" class="form-control input-lg" id="actualizarPrecioBsS" name="actualizarPrecio" disabled>

											</div>


											<div class="input-group col-md-12">

												<p style="margin-bottom: 20px;"><strong>Nota:</strong> Este precio no incluye I.V.A.</p>

											</div>	

									</div>
											
								</form>	

							</div>

						</div>

					</div>

					<!--=====================================
			   		=     INFORMACION PRODUCTO              =
					======================================-->

					<div class="col-md-5" style="padding-left: 0px; padding-right: 0px;padding-top: 35px;">

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>Facturas y formas físcales</strong></div>

							<div class="panel-body">

								<div class="col-md-12" style="padding-bottom: 30px; text-align: justify;">
									
									<p>• Créalas tu mismo en minutos.</p>
									<p>• Envío a toda Venezuela en 8 dias hábiles.</p>
									<p>• Inserta tu logo en la cabecera y también como marca de agua.</p>
									<p>• Impresión a full color o en blanco y negro (escala de grises).</p>
									<p>• Tamaño carta y media carta con papel autocopiante de 75 gr.</p>
									<p>• Facturas sueltas para impresora o talonario engomado.</p>
									<p>• Papel químico autocopiante (cargo adicional).</p>

								</div>

								<div class="col-md-12" style="padding-bottom: 20px; text-align: center">

									<img src="vistas\img\clientesgf/formaLibre.png">

								</div>	

							</div>

						</div>

					</div>	

				</div>	

			</div>

		</div>	

	</div>

    