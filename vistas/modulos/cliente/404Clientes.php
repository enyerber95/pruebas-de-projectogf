
    <section class="content">


      <div class="error-page">
        

        <h2 class="headline text-primary">404</h2>

        <div class="error-content">
          
          <h3><i class="fa fa-warning text-primary"></i>

            ¡Oooops! Página no encontrada.

          </h3>

          <p>Ingresa al menú superior y allí podrás encontrar las páginas disponibles. También puedes regresar haciendo clic <a href="inicio">Aquí.</a>
          </p>

        </div>

      </div>
     
    </section>