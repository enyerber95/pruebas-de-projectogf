
	<!--=====================================
	PARTE PRINCIPAL (MENÚ LATERAL Y CARRUSEL)
	=======================================-->

	<div class="container-fluid">
			
		<div class="container" style="padding-left:0px; padding-right: 0px;">


    <!--=====================================
	  BARRA LATERAL
	======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

						<div class="panel-body">
								
							<div class="nav nav-pills nav-stacked">
								<li><a href="facturas">Facturas</a></li>
								<li><a href="formasLibres">Formas Libres</a></li>
								<li><a href="rollos">Rollos Físcales</a></li>
							</div>

						</div>
						
					<div class="panel-heading"><strong>Otros Productos</strong></div>

						<div class="panel-body">

							<div class="nav nav-pills nav-stacked">
								<li><a href="#">Producto 1</a></li>
								<li><a href="#">Producto 2</a></li>
								<li><a href="#">Producto 3</a></li>
							</div>

					 	</div>

				</div>		 	

			</div>


	<!--=====================================
	   CARRUSEL
	======================================-->

	<div class="contenedor col-md-9 pull-right">
			
			<div class="col-md-12" style="padding-right: 0px;"">
				<div id="carousel-1" class="carousel slide" data-ride="carousel">
					<!-- Indicadores -->
					<ol class="carousel-indicators">
						<li data-target="#carousel-1" data-slide-to="0"	class="active" ></li>
						<li data-target="#carousel-1" data-slide-to="1"	></li>
						<li data-target="#carousel-1" data-slide-to="2"	></li>
					</ol>
					<!-- contenedor de los slide -->
					
					<div class="carousel-inner" role="listbox">
						<!-- #1 -->
						<div class="item  active">
							<img src="vistas\img\slider/B1.jpg" class="img-responsive" alt=""> <!-- style="width:822px;height:322px;" --> <!-- YA ME QUEDO CLARO POR QUE TODAS LAS IMAGENES MISMO TAMAÑO -->
							<div class="carousel-caption">
								<!-- <h3>Este es nuestro Slide #1</h3>
								<p>Lorem ipsum dolor sit amet.</p> -->
							</div>
						</div>
						<!-- #2 -->
						<div class="item ">
							<img src="vistas\img\slider/B2.jpg" class="img-responsive" alt="">
							<div class="carousel-caption">
								<!-- <h3>Este es nuestro Slide #2</h3>
								<p>Lorem ipsum dolor sit amet.</p> -->
							</div>
						</div>
						<!-- #3 -->
						<div class="item">
							<img src="vistas\img\slider/B3.jpg" class="img-responsive" alt="">
							<div class="carousel-caption">
				   				<!-- <h3>Este es nuestro Slide #3</h3>
								<p>Lorem ipsum dolor sit amet.</p> -->
							</div>
						</div>
					</div>
					<!-- Controles -->
					<a href="#carousel-1" class="left carousel-control" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Anterior</span>
					</a>
					<a href="#carousel-1" class="right carousel-control" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Siguiente</span>
					</a>
				</div>
			</div>
	</div>



		</div>	

	</div>
	
	<div class="container-fluid">
			
		<div class="container" style="padding-left: 0px;">
	
	<!--=====================================
	  PUBLICIDAD
	======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary";">

						<div class="panel-body" style="text-align: center;">
								
							<br><br><br><br><br><br><br><br><br><br><br><br><br><br>PUBLICIDAD<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>


						</div>

				</div>		 	

			</div>	

	<!--==========================================
	  ALGUNOS DE NUESTROS CLIENTES Y INFO SERVICIO
	===========================================-->	

			<div class="container-fluid">
					
				<div class="container" style="padding-left:0px; padding-right: 0px;">

					<div class="contenedor col-md-9 pull-right">

						<h2 style="margin-top: 0px;"><strong>Algunos de nuestros clientes</strong></h2>

						<div class="container col-md-12">
							
							<img src="vistas\img\clientesgf/barraClientes.png" class="img-responsive"  style="padding-bottom:10px;">

						</div>

					</div>	

					<div class="contenedor col-md-9 pull-right">
						
						<div class="panel panel-primary";">

							<div class="panel-body" style="text-align: center; background: #F1F1F1;">

								<!-- GRUPO DE LA IZQUIERDA -->

								<div class="contenedor col-md-6">
								
									<div class="contenedor col-md-12" style="text-align: justify;">

										
											<h1 style="color:#808080; display: inline;">1 | </h1><h4 style="color:#FE8C59; display:inline;"><strong>Crea y diseña tu pedido de facturas</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Es muy fácil crear y personalizar tu Factura. Escoge el formato que más te guste de nuestro catálogo de facturas, somos imprenta autorizada por el SENIAT.</p>
										
									</div>

									<div class="contenedor col-md-12" style="text-align: justify;">

										<h1 style="color:#808080; display: inline;">2 | </h1><h4 style="color:#FE8C59; display: inline;"><strong>Compra en línea tu factura</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Realiza pedidos de facturas desde cantidades muy pequeñas ajustadas a la normativa del SENIAT. Aceptamos como medio de pago transferencias y depósitos bancarios.</p>

									</div>

									<div class="contenedor col-md-12" style="text-align: justify;">

										<h1 style="color:#808080; display: inline;">3 | </h1><h4 style="color:#FE8C59; display: inline;"><strong>Envío a domicilio a toda Venezuela</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Tu pedido de facturas será enviado a toda Venezuela en 4 días hábiles por nuestros transportistas. Los tiempos pueden variar dependiendo de la cantidad de facturas solicitadas y ubicación geográfica.</p>

									</div>

								</div>
								
								<!-- GRUPO DE LA DERECHA -->

								<div class="contenedor col-md-6" style="padding-top: 25px;">

									<div class="contenedor col-md-12" style="text-align: justify; padding-bottom: 10px;">

										<h1 style="color:#808080; display: inline;"><i class="fa fa-bank"></i> | </h1><h4 style="color:#FE8C59; display: inline;"><strong>Cuentas Bancarias</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Si decidiste realizar el pago de tus talonarios a través de depósito o transferencia electrónica bancaria te sugerimos considerar.</p>
										
									</div>

									<div class="contenedor col-md-12" style="text-align: justify; padding-bottom: 10px;">

										<h1 style="color:#808080; display: inline;"><i class="fa fa-desktop"></i> | </h1><h4 style="color:#FE8C59; display: inline;"><strong>Solicita tu presupuesto en línea</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Solicita tu presupuesto de facturas ajustadas a la normativa del SENIAT a través de nuestro cotizador en línea.</p>
										
									</div>

									

									<div class="contenedor col-md-12" style="text-align: justify;">

										<h1 style="color:#808080; display: inline;"><i class="fa fa-users"></i> | </h1><h4 style="color:#FE8C59; display: inline;"><strong>Contáctanos</strong></h4>


										<p style="color:#808080; padding-top: 10px;">Envía una pregunta a nuestro equipo de Atención al Usuario.</p>
										
									</div>

								</div>	

							</div>

						</div>		

					</div>	

				</div>	

			</div>




		</div>	

	</div>

	<br><br>




	