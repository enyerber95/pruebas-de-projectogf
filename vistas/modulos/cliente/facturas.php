

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="formasLibres">Formas Libres</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Producto 1</a></li>
							<li><a href="#">Producto 2</a></li>
							<li><a href="#">Producto 3</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>PUBLICIDAD<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (FACTURAS)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style=" border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1 style="margin-bottom: 0px; margin-top: 0px;"><strong>Facturas</strong></h1>
						<p style="margin-bottom: 0px;"><strong>(Acorde a la Normativa del SENIAT)</strong></p>

					</div>

					<div class="col-md-8" style="padding-left: 0px;">

						<div class="col-md-12" style="padding-right: 0px; text-align: justify; padding-top: 10px;">

							<p>• 1 Talonario está comprendido por 50 facturas.</p>
							<p>• Créalas tu mismo en minutos.</p>
							<p>• Envío a toda Venezuela en 8 días hábiles.</p>
							<p>• Inserta tu logo en la cabecera y también como marca de agua.</p>
							<p>• Impresión a full color o en blanco y negro (escala de grises).</p>
							<p>• Tamaño carta y media carta con papel autocopiante de 75 gr.</p>
							<p>• Facturas sueltas para impresora o talonario engomado.</p>
							<p>• Papel químico autocopiante.</p>

						</div>

					</div>	

					<div class="col-md-4" style="padding: 0px; padding-right: 0px;">

						<div class="col-md-12" style="padding-top: 30px; padding-left: 0px; text-align: right; padding-right:0px;">

							<img src="vistas/img/clientesgf/mediaCarta.png" class="img-responsive">

						</div>	

					</div>

				</div>	

			</div>


			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<div class="col-md-9" style="padding-left: 0px">

							<br><h4><strong>Escoge un modelo de factura</strong></h4>

							<p>Selecciona un modelo de nuestro catálogo, personalízalo agregando tus datos y tu logo.</p><br>

						</div>

						<div class="col-md-3" style="padding-top: 40px; text-align: right;">	

							<a href="facturasCatalogo"><button class="btn btn-primary">Ir al catálogo</button></a>

						</div>

					</div>	

				</div>

			</div>

			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<div class="col-md-9" style="padding-left: 0px">

							<br><h4><strong>Solicita tu presupuesto en línea para facturas</strong></h4>

							<p>A través del cotizador en línea podrás solicitar un presupuesto antes de crear tu pedido.</p><br>

						</div>

						<div class="col-md-3" style="padding-top: 40px; text-align: right;">	

							<a href="cotizador"><button class="btn btn-primary">Cotizar ahora</button></a>

						</div>

					</div>	

				</div>

			</div>
					
			<div class="container col-md-9 pull-right" style="padding-top: 35px; margin-top: 0px; padding-top: 0px;">

				<div class="container col-md-12" style="padding:0px;">

					<div class="panel panel-primary">

						<div class="panel-heading"><strong>Lo más solicitado en Facturas</strong></div>

						<div class="panel-body">

							<div class="col-md-12" style="justify-content: center;">

								<div class="col-md-4" style="padding-left: 0px;">

									<div class="panel panel-primary">

										<div class="panel-body">

											<div class="panel-body" style="text-align: center">

												<img src="vistas\img\productos\facturas\tamaño media carta\cuadrada/FMC-Modelo 1.png" class="img-thumbnail">

												<p style="padding-top: 10px;"><strong>Factura Rayada - Modelo 1 (Tamaño Media Carta)</strong></p>

												<button class="btn btn-primary">Comprar <i class="fa fa-edit"></i></button>

											</div>

										</div>

									</div>	

								</div>

								<div class="col-md-4" style="padding-left: 0px;">

									<div class="panel panel-primary">

										<div class="panel-body">

											<div class="panel-body" style="text-align: center">

												<img src="vistas\img\productos\facturas\tamaño media carta\rayada/FMR-Modelo 2.png" class="img-thumbnail">

												<p style="padding-top: 10px;"><strong>Factura Cuadrada - Modelo 2 (Tamaño Media Carta)</strong></p>

												<button class="btn btn-primary">Comprar <i class="fa fa-edit"></i></button>

											</div>

										</div>

									</div>	

								</div>

								<div class="col-md-4" style="padding-left: 0px;padding-left: 0px;margin-right: 0px;padding-right: 0px;border-right-width: 15px;">

									<div class="panel panel-primary">

										<div class="panel-body">

											<div class="panel-body" style="text-align: center">

												<img src="vistas\img/productos\facturas\tamaño carta\rayada/FCR-Modelo 1.png" class="img-thumbnail">

												<p style="padding-top: 10px;"><strong>Factura Rayada - Modelo 1 (Tamaño Carta)</strong></p>

												<a href="editarFactura"><button class="btn btn-primary">Comprar <i class="fa fa-edit"></i></button></a>

											</div>

										</div>

									</div>	

								</div>

							</div>

						</div> 	

					</div>		

				</div>

			</div>




		</div>	

	</div>

	
