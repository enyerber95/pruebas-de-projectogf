
<?php 

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script> 
				
					
							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}
   	$id = $_GET["id"];

?> 
	<!--=====================================
	PARTE PRINCIPAL (RESUMEN DE COMPRAS)
	=======================================-->

	<div class="container-fluid eltodo">
	<form  method="post" class="form col-md-12" style="padding: 0px; text-align: justify;">

			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--===========================================================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Métodos de envío</strong></h1>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     CARRITO DE COMPRAS  (RECUMEN)     =
				   		======================================-->

				   		<div class="col-md-12" style="padding: 0px;">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">


				   				<div class="wizards">

					                <div class="progressbar">

					                    <div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div> <!-- 19.66% -->

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa  fa-shopping-cart"></i></div>

					                    <p>RESUMEN</p>

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa fa-user"></i></div>

					                    <p>INGRESO</p>

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa fa-home"></i></div>

					                    <p>DIRECCIONES</p>

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa fa-truck"></i></div>

					                    <p>ENVÍO</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa  fa-credit-card"></i></div>

					                    <p>PAGO</p>

					                </div>

					            </div><br><br>

					            <div class="panel panel-primary">

				   					<div class="panel-body">

				   						<div class="box-body col-md-12">

					   						<!-- MÉTODOS DE ENVÍO -->

				   							<div class="container col-md-12" style="text-align: justify; margin-top: 0px;">
				   								
				   								<h4><strong>Elija una dirección de envío para esta dirección:</strong> Mi dirección 1</h4><br>

					   							<div class="panel panel-default">

													<div class="panel-body">

														<div class="radio ajustarCentrar">

				      										<label><input type="radio" id="envio" name="opradio" value="Gratuito">

				      											<div class="col-md-12" style="border-left: 1px solid #ddd;">

																	<p style="margin-bottom: 0px;"><strong>Envío gratuito</strong><br> Envío directo únicamente a los siguientes estados: Aragua, Carabobo, Distrito Capital, Miranda, Guárico y Vargas.</p>

																</div>

				      										</label>

				    									</div>

													</div>

												</div>	

												<div class="panel panel-default">

													<div class="panel-body">

				    									<div class="radio">

				      										<label><input type="radio" id="envio" name="opradio" value="Cobro">

				      											<div class="col-md-12" style="border-left: 1px solid #ddd;">

				      												<p style="margin-bottom: 0px;"><strong>Envío cobro destino</strong><br> Envío en 24 horas después de finalizado el pedido con la empresa de su preferencia.</p>

				      											</div>

				      										</label>

				    									</div>

				    								</div>	

					   							</div>

				   							</div>

				   						</div>
				   						
				   					</div>
				   									<?php 
   	  											 $pedido = new ControladorPedidos();
                                				$pedido -> ctrAgregarPedido($id);

				   									?>
				   					<div class="container col-md-12" style="text-align: right; padding: 0px;">

					   					<button class="btn btn-primary">Ir a reportar pago <i class="fa fa-chevron-right"></i></button>
													
					   				</div>

				   				</div>

				   				</div>

				   			</div>

				   		</div>

			   		</div>

				</div>	

			</div>
			</form>
		</div>	

	</div>
