
<?php 

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script> 
				
					
							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}

?> 



	<!--=====================================
	FORMULARIO EDITAR PERFIL
	=======================================-->

	<div class="container-fluid">
			
		<div class="container">

			<h1 style="margin-top: 0px;"><strong>Editar perfil</strong></h1>
			<hr>

			<div class="container col-md-6" style="margin-bottom:0px;">

				<div class="col-md-12" style="padding: 0px;">

					<!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->

					<form role="form" method="post" enctype="multipart/form-data"> 

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>Información personal</strong></div>

							<div class="panel-body">


								<?php 

									echo '<input type="hidden" name="idCliente" value="'.$_SESSION["clienteId"].'">';

									

								 ?>


						        <div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Correo electrónico:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarCorreo" name="editarCorreo" value="'.$_SESSION["clienteCorreo"].'" required></input>';

										 ?>
										

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Contraseña:</label>

										<input type="password" class="form-control" id="editarPassword" name="editarPassword" placeholder="Ingrese nueva contraseña" onblur="revisarEstandar(this)" value="">

									</div><br>

								</div>
								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Nombre:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarNombre" name="editarNombre" value="'.$_SESSION["clienteNombre"].'" required></input>';

										 ?>

									</div><br>

								</div>	

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Repita Contraseña:</label>

										<input type="password" class="form-control" id="editarPassword2" name="editarPassword2" placeholder="Ingrese nueva contraseña" onblur="comprobarClave2(this)" value="">

									</div><br>
								</div>
								
								<div class="from-group col-md-6" style="text-align: justify;">	

									<label>Fecha de nacimiento:</label>

									<div class="input-group date fechaNac">

										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

										<?php 

											echo '<input type="text" class="form-control" id="editarFechaNac" name="editarFechaNac" value="'.$_SESSION["clienteFechaNac"].'">';

										 ?>

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Apellido:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarApellido" name="editarApellido" value="'.$_SESSION["clienteApellido"].'" required></input>';

										 ?>

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Cédula:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarCedula" name="editarCedula" value="'.$_SESSION["clienteCedula"].'" required></input>';

										 ?>

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Teléfono local:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarTelfLocal" name="editarTelfLocal" value="'.$_SESSION["clienteTelfLocal"].'" required>';

										 ?>

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Teléfono móvil:</label>

										<?php 

											echo '<input type="text" class="form-control" id="editarTelfMovil" name="editarTelfMovil" value="'.$_SESSION["clienteTelfMovil"].'" required>';

										 ?>

									</div>

								</div>

							</div>

							<div class="col-md-12" style="padding-top:20px; padding-right:0px; text-align: right;">

								<button type="submit" class="btn btn-primary">Guardar cambios</button>

							</div>

							<?php 

								$editarCliente = new ControladorClientes();
								$editarCliente -> ctrEditarCliente();

							?>		

						</div>

					</form>

				</div>		 	

			</div>

		</div>
	
	</div>

	</div>

	</div>


	<br><br>
<script type="text/javascript">

  function comprobarClave2(e){
  input = $('#editarPassword'); //input de la fecha
  input2 = $('#editarPassword2'); //input de la fecha
  clave1x =e.value;

  clave1 = $('#editarPassword').val();
  clave2 = $('#editarPassword2').val();


    if (clave1 === clave2){
      $("#padre").parent().before('<div class="alert alert-success"><strong>PERFECTO:</strong> Las contraseñas son iguales.</div>');

    }else{
    	 swal({
      type:"error",
      title: "Las dos claves son distintas...\nPor favor rectifique",
      width: '250px'
      })
      $("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Las contraseñas no coinciden.</div>');
      
        $('input[name="editarPassword"]').val('');
        $('input[name="editarPassword2"]').val('');
     }
}
function revisarEstandar(elemento){


  if (elemento.value!=""){

    var dato = elemento.value;

    var expresion =/^.*(?=.{2,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%*]).*$/;
    if(!expresion.test(dato)){
      elemento.className = 'error';
      swal({
      type:"error",
      title: "La contraseña no fue escrita adecuadamente.  \nDebe contener:\n, Mayusculas\nMinusculas\nNúmeros\nCaracteres Especiales\n",
      width: '250px'
      })
      swal();

      elemento.className = '';
      
    }else{
    }
  }
}
function dwec_tooltip_show (id) {

    setTimeout(function() { 
 
        id.style.display="block";
    
    }, 300);                
}



function dwec_tooltip_hide (id) {

    setTimeout(function() { 
                     
        id.style.display="";
        
    }, 300);                     
} 
</script>