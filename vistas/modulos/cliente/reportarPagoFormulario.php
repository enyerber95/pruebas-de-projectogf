	
<?php 

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script> 
				
					
							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}

?> 
	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="formasLibres">Formas Libres</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Tarjetas de Inventario</a></li>
							<li><a href="#">Tarjetas de Presentacíón</a></li>
							<li><a href="#">Recipes Médicos</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>ESPACIO<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (REPORTE DE PAGO)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px;">

				<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1><strong>Pago a través de depósito o transferencia</strong></h1>

					</div>

					<div class="col-md-12" style="padding-left: 0px; padding-top: 30px;">

						<div class="col-md-12" style="padding-right: 0px; text-align: justify;">


							<div class="panel panel-primary">

								<div class="panel-body"><br>

									<!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->

									<form class="form col-md-12" method="post" style="padding: 0px;"> 

										<!-- PARTE IZQUERDA -->

										<div class="from-group col-md-6" style="text-align: justify;">

											<div class="input-group col-md-12">

												<label>Nombre y apellido:</label>

												<?php
												echo '<input type="text" class="form-control" name="nombreCliente" value="'.$_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"].'"readonly>';
												?>

											</div><br>
											
											<div class="input-group col-md-12">

												<label>Número de orden:</label>

												<select class="form-control" id="orden" onclick="calcularMonto(this)"" required>

													<?php foreach (ControladorPedidos::ctrMostrarPedidosPorEstado($_SESSION["clienteId"]) as $key => $value){ ?>							 

														<?php echo '<option value='.$value["orden"].'>'.$value["orden"].'</option>'; ?>
														<?php }?>
												</select>
													<?php foreach (ControladorPedidos::ctrMostrarPedidosPorEstado($_SESSION["clienteId"]) as $key => $value){ ?>


													 <?php

													 $iva=ControladorProductos::ctrMontoInva($value["monto"]);

													  echo '<input type="hidden" id='.$value["orden"].' value='.$value["monto"].'>';

													 echo '<input type="hidden" id=iva'.$value["orden"].' value='.$iva.'>'; 
													 ?>
				 				
				 												
													<?php }?>
											</div>
											<br>
											<?php if((ControladorPedidos::ctrListarPedidos($_SESSION["clienteId"])==null)) { 
												echo '<div class="col-md-12" style="padding: 0px;">';
												echo'
  												<p style="color: #FF5A5A;">No hay ordenes, presiona <a href="facturasCatalogo"><b style="color: #cc4747;">Aquí</b></a> para regresar.</p>';

											echo'</div><br>';
											}
											?>

											<div class="input-group col-md-12">

												<label>Banco emisor:</label>

												<select class="form-control" name="BankEmi" id="BankEmi" required>

													<option value="">Elegir banco</option>

													<option value="Bancaribe">Bancaribe</option>

													<option value="Banesco">Banesco</option>

													<option value="Bicentenario">Bicentenario</option>

													<option value="B.O.D">B.O.D</option>

													<option value="Mercantil">Mercantil</option>

													<option value="Provincial">Provincial</option>

													<option value="Venezuela">Venezuela</option>

													<option value="otros">otros</option>

												</select>

											</div><br>

											<div class="input-group col-md-12" style="text-align: justify;">

												<label>Banco receptor:</label>

												<select class="form-control" id="BankRecp" name="BankRecp" required>

													<option value="">Elegir banco</option>

													<option value=Banesco>Banesco</option>

													<option value=Mercantil>Mercantil</option>

												</select>

											</div><br>

											<div class="input-group col-md-12" style="text-align: justify;">

												<label>Monto:</label>

												<input type="text" id="monto" class="form-control" readonly>


											</div>
											<label style="font-style: italic; text-align: left;"><strong></style> Bolivares.</strong><br></label>

										</div>

										<!-- PARTE DERECHA -->

										<div class="from-group col-md-6" style="text-align: justify;">

											<div class="input-group col-md-12">


												<label>Número de depósito o transferencia:</label>

												<input type="text" class="form-control" id="numtransf" name="numtransf" required></input>

											</div><br>

											<label>Fecha del depósito o transferencia:</label>

											<div class="input-group col-md-12 date fechaNac">
											
												<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

												<input type="tectrEnviarReportext" onchange="verificaDate(this)" name="fechaReporte" id="fechaReporte" class="form-control" required=>

											</div><br>

											<div class="input-group col-md-12">
											<input type="text" id="fecha" class="hide" value="<?php echo date("Y/m/d");?>"

												<label>Teléfono:</label>

												<?php
												echo '<input type="text" class="form-control" name="tlfCliente" value="'.$_SESSION["clienteTelfLocal"].'"readonly>';
												?>

											

											</div><br>

											<div class="input-group col-md-12">

												<label>Correo electrónico:</label>

								         	<?php
												echo '<input type="text" class="form-control" name="emailCliente" value="'.$_SESSION["clienteCorreo"].'"readonly>';
												?>


											</div>
											<input type="hidden" name="ordenpedido" id="ordenpedido" value="">
										</div>


									

								</div><br>

							</div>


						</div>

					</div>	

				</div>	

			</div>

			<div class="container col-md-9">

				<div class="col-md-12" style="padding: 0px;">
				   <?php
				   $vista=0;
				   $id=1;
                        $Reporte = new ControladorReportes();
                        $Reporte -> ctrEnviarReporte($id,$vista);

                    ?>

					<div class="col-md-12" style="text-align: right;">

						   <input type="submit" class="btn btn-primary pull-right btnreporte" value="Enviar Reporte">
						
					</div>

				</div>	

			</div>	

			
			</form>

		</div>	

	</div>
	