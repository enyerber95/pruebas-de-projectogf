<?php

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script>


							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}
   	   	$id = $_GET["id"];

?>

	<!--=====================================
	PARTE PRINCIPAL (RESUMEN DE COMPRAS)
	=======================================-->

	<div class="container-fluid eltodo">

		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--===========================================================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Direcciones</strong></h1>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     CARRITO DE COMPRAS  (RECUMEN)     =
				   		======================================-->

				   		<div class="col-md-12" style="padding: 0px;">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">

				   				<form method="post">

				   				<div class="wizards">

					                <div class="progressbar">

					                    <div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div> <!-- 19.66% -->

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa  fa-shopping-cart"></i></div>

					                    <p>RESUMEN</p>

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa fa-user"></i></div>

					                    <p>INGRESO</p>

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa fa-home"></i></div>

					                    <p>DIRECCIONES</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa fa-truck"></i></div>

					                    <p>ENVÍO</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa  fa-credit-card"></i></div>

					                    <p>PAGO</p>

					                </div>

					            </div><br><br>

					            <div class="panel panel-primary">

				   					<div class="panel-body">

				   						<!-- DIRECCION DE ENVIO -->

				   						<div class="box-body col-md-12">

				   							<!-- BOTÓN (AGREGAR DIRECCIÓN) -->

				   							<div class="container col-md-12" style="text-align: left; margin: 0px; margin-bottom: 15px;">

					   							<a href="#" data-toggle="modal" data-target="#modalAgregarDireccion"><button class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Agregar</button></a>

					   						</div>

					   						<!-- DIRECCION DE ENVIO -->

				   							<div class="container col-md-6" style="text-align: justify; margin: 0px;">
				   								<form method="post">
				   								<h4><strong>Dirección de envío</strong></h4>

												<div class="form-group">

					   								<select class="form-control" id="1" onchange ="cambiadireccion(this)" required>

					   									<option>Eliga una dirección</option>
															<?php foreach (ControladorDirecciones::ctrMostrarDirecciones('id_cliente', $_SESSION["clienteId"]) as $key => $value) {?>


															<?= '<option value="'.$value["id_direcciones"].'">'.$value["d_titulo"].'</option>'; ?>
						   									<?php } ?>

					   								</select>
					   							</div>

					   							<div class="panel panel-primary" id="content1" style="display: none;">

					   								<div class="panel-heading" style="text-align: center;">

					   									<strong>Su dirección de envío: <p id="titulo1"></p></strong>

					   								</div>

					   								<div class="panel-body">

					   									<!-- Nombre y apellido -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Nombre y apellido:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left">
					   										<?php
															echo ''.$_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"].'';?></p>

					   									</div><br>


					   									<!-- RIF o cédula -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>RIF o cédula:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left">
					   										<?=$_SESSION["clienteCedula"];?></p>

					   									</div><br>


					   									<!-- Nombre de la empresa -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Nombre de la empresa:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="nameEmp1"></p>

					   									</div><br>

					   									<!-- Sector/Urbanización -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Sector/Urbanización:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="secUrb1"></p>

					   									</div><br>

					   									<!-- Avenida/Calle -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Avenida/Calle:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="aveCll1"></p>

					   									</div><br>

					   									<!-- Edificio/Quinta/Casa -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Edificio/Quinta/Casa:</strong></p>

					   									</div>

					   									<div class="col-md-6">


					   										<p align="left" id="ediQuiCas1"></p>

					   									</div><br>

					   									<!-- Estado -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Estado:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="estado1"></p>

					   									</div><br>

					   									<!-- Ciudad -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Ciudad:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="ciudad1"></p>

					   									</div><br>

					   									<!-- Teléfono -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Teléfono:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="tlf1"></p>

					   									</div><br>

					   									<!-- Información adicional -->

					   									<div class="col-md-6">

					   										<p align="right" style="margin-bottom: 0px"><strong>Información adicional:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="infAdi1"></p>

					   									</div><br>

													</div>


					   							</div>
					   							</form>
				   							</div>

				   							<!-- DIRECCION DE FACTURACIÓN -->


				   							<div class="container col-md-6" style="text-align: justify; margin: 0px;">
				   								<form method="post">
				   								<h4><strong>Dirección de Facturacion</strong></h4>

												<div class="form-group">

					   								<select class="form-control" id="2" onchange ="cambiadireccion(this)" required>

					   									<option value="">Eliga una dirección</option>
															<?php foreach (ControladorDirecciones::ctrMostrarDirecciones('id_cliente', $_SESSION["clienteId"]) as $key => $value) {?>


															<?= '<option value="'.$value["id_direcciones"].'">'.$value["d_titulo"].'</option>'; ?>
						   									<?php } ?>

					   								</select>
					   							</div>

					   							<div class="panel panel-primary" id="content2" style="display: none;">

					   								<div class="panel-heading" style="text-align: center;">

					   									<strong>Su dirección de Facturacion: <p id="titulo2"></p></strong>

					   								</div>

					   								<div class="panel-body">

					   									<!-- Nombre y apellido -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Nombre y apellido:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left">
					   										<?php
															echo ''.$_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"].'';?></p>

					   									</div><br>


					   									<!-- RIF o cédula -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>RIF o cédula:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left">
					   										<?=$_SESSION["clienteCedula"];?></p>

					   									</div><br>


					   									<!-- Nombre de la empresa -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Nombre de la empresa:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="nameEmp2"></p>

					   									</div><br>

					   									<!-- Sector/Urbanización -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Sector/Urbanización:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="secUrb2"></p>

					   									</div><br>

					   									<!-- Avenida/Calle -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Avenida/Calle:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="aveCll2"></p>

					   									</div><br>

					   									<!-- Edificio/Quinta/Casa -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Edificio/Quinta/Casa:</strong></p>

					   									</div>

					   									<div class="col-md-6">


					   										<p align="left" id="ediQuiCas2"></p>

					   									</div><br>

					   									<!-- Estado -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Estado:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="estado2"></p>

					   									</div><br>

					   									<!-- Ciudad -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Ciudad:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="ciudad2"></p>

					   									</div><br>

					   									<!-- Teléfono -->

					   									<div class="col-md-6">

					   										<p align="right"><strong>Teléfono:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="tlf2"></p>

					   									</div><br>

					   									<!-- Información adicional -->

					   									<div class="col-md-6">

					   										<p align="right" style="margin-bottom: 0px"><strong>Información adicional:</strong></p>

					   									</div>

					   									<div class="col-md-6">

					   										<p align="left" id="infAdi2"></p>

					   									</div><br>

													</div>


					   							</div>
					   							</form>
				   							</div>

				   						</div>

				   					</div>

				   				</div>

								<!-- BOTÓN (IR A METODOS DE ENVÍO) -->

				   				<div class="container col-md-12" style="text-align: right; margin: 0px; padding: 0px;">
				   					<input type="hidden" id="idCarrito" value="<?= $id ?>">

					   				<a id="a" href="#"><button class="btn btn-primary">ir a método de envío <i class="fa fa-cart-arrow-down"></i></button></a>


					   			</div>


				   				</div>

				   			</div>

				   		</div>

			   		</div>

				</div>

			</div>

		</div>

	</div>



<!--==============================
 MODAL - AGREGAR DIRECCIÓN
===============================-->


<div id="modalAgregarDireccion" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <!-- CABECERA DEL MODAL -->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Agregar dirección</h1>

            </div>

            <form role="form" method="post" enctype="multipart/form-data">

            	<?php
								echo '<input type="hidden" name="idCliente" id="idCliente" value="'.$_SESSION["clienteId"].'">';
								echo '<input type="hidden" name="clienteCorreo" id="clienteCorreo" value="'.$_SESSION["clienteCorreo"].'">';

							?>


	            <div class="box-body col-md-12" style="padding: 50px; padding-bottom:0px; padding-top:15px;">

	            	<div class="input-group col-md-12">

		            		<label>Título de dirección:</label>

		            		<input type="text" class="form-control" id="nuevoTitulo" name="nuevoTitulo" onkeypress=" return soloLetras(event)" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>

		            <div class="box-body col-md-6" style="padding-left: 0px; padding-right: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Teléfono:</label><br>

	            			<input type="text" class="form-control" data-inputmask="'mask':'(9999) 999-9999'" data-mask id="nuevoTelf" onkeypress=" return soloNumeros(event)"  name="nuevoTelf" required>

	            		</div><br>
									<div class="input-group col-md-12">

		            		<label>Empresa/Razón social:</label>

		            		<input type="text" class="form-control" id="nuevoRazonSoc" name="nuevoRazonSoc" value="" maxlength="50" minlength="4" placeholder="Ej. Grafoformas HB" required>

		            	</div><br>
		            </div>

	            	<div class="input-group col-md-12">

	            		<label>Sector / Urbanización:</label>

	            		<input type="text" class="form-control" id="nuevoSU" name="nuevoSU"  maxlength="50" minlength="4" placeholder="Ej. Los Horcones" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Avenida / Calle:</label>

	            		<input type="text" class="form-control" id="nuevoAC" name="nuevoAC"  maxlength="50" minlength="4" placeholder="Ej. Av. Libertador con calle 33" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Edificio / Quinta / Casa:</label>

	            		<input type="text" class="form-control" id="nuevoEQC" name="nuevoEQC"  maxlength="50" minlength="4" placeholder="Ej. Casa #23-65 " required>

	            	</div><br>

	            	<div class="box-body col-md-6" style="padding: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Estado:</label>
	            			<select class="form-control col-md-12" id="nuevoEstado" name="nuevoEstado" required>
	            					<option value="" id="nEstado">Selecionar estado</option>
	            					<option value="Amazonas">Amazonas</option>
	            					<option value="Anzoátegui">Anzoátegui</option>
	            					<option value="Apure">Apure</option>
	            					<option value="Aragua">Aragua</option>
	            					<option value="Barinas">Barinas</option>
	            					<option value="Bolívar">Bolívar</option>
	            					<option value="Carabobo">Carabobo</option>
	            					<option value="Cojedes">Cojedes</option>
	            					<option value="Delta Amacuro">Delta Amacuro</option>
	            					<option value="Falcón">Falcón</option>
	            					<option value="Guárico">Guárico</option>
	            					<option value="Lara">Lara</option>
	            					<option value="Mérida">Mérida</option>
	            					<option value="Miranda">Miranda</option>
	            					<option value="Monagas">Monagas</option>
	            					<option value="Nueva Esparta">Nueva Esparta</option>
	            					<option value="Portuguesa">Portuguesa</option>
	            					<option value="Sucre">Sucre</option>
	            					<option value="Táchira">Táchira</option>
	            					<option value="Trujillo">Trujillo</option>
	            					<option value="Vargas">Vargas</option>
	            					<option value="Yaracuy">Yaracuy</option>
	            					<option value="Zulia">Zulia</option>
	            			</select>
	            		</div><br>

		            </div>

								<div class="box-body col-md-6" style="padding: 0px; padding-left: 20px; ">

	            		<div class="input-group col-md-12">

	            			<label>Ciudad:</label>

	            			<input type="text" class="form-control" id="nuevoCiudad" name="nuevoCiudad"  onkeypress=" return soloLetras(event)" maxlength="50" minlength="4" placeholder="Ej. Maracay"  required>

	            		</div><br>

	            	</div>

	            	<div class="input-group col-md-12">

	            		<label>Información adicional:</label>

	            		<textarea class="form-control" id="nuevoIA" name="nuevoIA" value=""  maxlength="150" minlength="4" placeholder="Ej. Esta cerca de un parque/cancha" required></textarea>

	            	</div>

	            </div>

	            <div class="modal-footer">

	                <div class="container col-md-12" style="margin: 0px;"><hr>
	                   <button type="submit" class="btn btn-primary">Agregar</button>
	                </div>

	                <?php
									$editar= 'direccionesCompras&'.$id;
										$AgregarDireccion = new ControladorDirecciones();
										$AgregarDireccion -> ctrAgregarDireccion($editar);

									?>

	            </div>

            </form>

        </div>

    </div>

</div>
