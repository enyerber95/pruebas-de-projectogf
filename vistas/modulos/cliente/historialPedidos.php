<?php 

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
   		echo '
			<section class="content">


      <div class="error-page">
         <h1 class="headline text-primary">Sesion no iniciada.</h1>

        <h2 class="headline text-primary">404</h2>

        <div class="error-content">
          
          <h3><i class="fa fa-warning text-primary"></i>

            ¡Oooops! Página no encontrada.

          </h3>

          <p>Ingresa al menú superior y allí podrás encontrar las páginas disponibles. También puedes regresar haciendo clic <a href="inicio">Aquí.</a>
          </p>

        </div>

      </div>
     
    </section>";';
exit();
   		
   	}

?> 
	<!--=====================================
	PARTE PRINCIPAL (RESUMEN DE COMPRAS)
	=======================================-->

	<div class="container-fluid eltodo">

		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--===========================================================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Historial de pedidos</strong></h1>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     LISTA DE PEDIDOS HECHOS           =
				   		======================================-->

				   		<div class="col-md-12" style="padding: 0px;">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">

				   				<p style="text-align: justify; padding-bottom: 20px;">En la siguiente tabla podrás encontrar el estado actual de tus pedidos.</p>

					            <div class="panel panel-primary">

				   					<div class="panel-body">

				   						<!-- TABLA DE RESUMEN PEDIDOS -->

				   						<div class="box-body">
				   							<table class="table table-bordered table-striped dt-responsive tablas">

				   								<thead>

				   									<tr>

				   										<th>Orden</th>
				   										<th>Fecha de creación</th>
				   										<th>Producto</th>
				   										<th>Precio total</th>
				   										<th>Estado</th>
				   										<th>Opciones</th>

				   									</tr>

				   								</thead>

				   								<tbody>

				   									<?php if (ControladorPedidos::ctrListarPedidos($_SESSION["clienteId"]) != null) {
				   										
				   									
				   									foreach (ControladorPedidos::ctrListarPedidos($_SESSION["clienteId"]) as $key => $value){ 
				   										 ?>

					   									<tr>
					   										<td>
					   											<?= $value["orden"]; ?>
					   										</td>

					   										<td>
					   											<?= $value["fecha_emision"]; ?>
					   										</td>

					   										<td>
					   											<img src= <?='"'.ControladorProductos::ctrMostrarDetalleProductos($value["id_producto"],$detalle="imagen").'"'?>class="img-thumbnail" width="100px">
					   										</td>

					   										<td> <?php 
				                                                $iva=ControladorProductos::ctrMontoInva($value["monto"]);
				                                                 $totalBsF=$value["monto"]+$iva;
				                                                 $totalBsS=$totalBsF/100000;?>
					   											
																<?php echo '<div style="text-align: justify;">'.$totalBsF.' BsF <br>'; echo ''.$totalBsS.' BsS</div>';?> 
					   										</td>

					   										<td>
					   											<?php if ($value["estado"] =='Pendiente'){ echo '

					   												<button class="btn btn-warning">'.$value["estado"].'</button>'?>

																	<?php }elseif ($value["estado"] =='Rechazado'){ echo '

					   												<button class="btn btn-danger">Rechazado</button>'?>

					   											<?php } elseif ($value["estado"] =='Finalizado'){ echo '

					   												<button class="btn btn-success">Enviado</button>'?>
					   											<?php } elseif ($value["estado"] =='Comprobar'){ echo '

					   												<button class="btn ">Comprobando</button>'?>


					   											<?php }?>
					   										</td>


					   										<td>
					   											<a href="detallesPedido&<?= $value["id_pedido"]; ?>"><button class="btn btn-primary">Ver detalles / Solicitar reimpresión</button></a>
					   										</td>
					   									</tr>
														<?php } }?>
				   								</tbody>



				   							</table>

				   						</div>

				   					</div>

				   				</div>

				   				</div>

				   			</div>

				   		</div>

			   		</div>

				</div>

			</div>

		</div>

	</div>