
	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

		    <!--=====================================
			  BARRAS LATERALES
			======================================-->

			<div class="col-md-3 col-xs-12" >

				<div class="panel panel-primary">

					<div class="panel-heading"><strong>Facturas y Formas Físcales</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="facturas">Facturas</a></li>
							<li><a href="formasLibres">Formas Libres</a></li>
							<li><a href="rollos">Rollos Físcales</a></li>
						</div>

					</div>

					<div class="panel-heading"><strong>Otros Productos</strong></div>

					<div class="panel-body">

						<div class="nav nav-pills nav-stacked">
							<li><a href="#">Producto 1</a></li>
							<li><a href="#">Producto 2</a></li>
							<li><a href="#">Producto 3</a></li>
						</div>

					</div>

				</div>	

				<div class="panel panel-primary">

					<div class="panel-body" style="text-align: center;">

						<br><br><br><br><br><br><br><br><br><br><br><br><br><br>PUBLICIDAD<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					</div>

				</div>

			</div>

			<!--=====================================
			   CONTENIDO (FACTURAS)
			======================================-->

			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style=" border-bottom: 1px solid #F1F1F1;padding-right: 0px; padding-bottom: 10px;">

						<h1 style="margin-bottom: 0px; margin-top: 0px;"><strong>Rollos físcales</strong></h1>
						<p style="margin-bottom: 0px;"><strong>(Acorde a la Normativa del SENIAT)</strong></p>

					</div>

					<div class="col-md-8" style="padding-left: 0px;">

						<div class="col-md-12" style="padding-right: 0px; text-align: justify; padding-top: 10px;">

							<p>• Venta al mayor (50 rollos por caja).</p>
							<p>• Envía tu información para el pedido.</p>
							<p>• Envío a toda Venezuela en 8 días hábiles.</p>
							<p>• Inserta tu logo en la cabecera y también como marca de agua.</p>
							<p>• Impresión a full color o en blanco y negro (escala de grises).</p>
							<p>• Rollos químicos 80x65 mm para máquinas fiscales.</p>
							<p>• Papel químico autocopiante.</p>

						</div>

					</div>	

					<div class="col-md-4" style="padding: 0px; padding-right: 0px;">

						<div class="col-md-12" style="padding-top: 15px; padding-left: 0px; text-align: right; padding-right:0px;">

							<img src="vistas/img/clientesgf/Rollos.jpg" class="img-responsive">

						</div>	

					</div>

				</div>	

			</div>


			<div class="container col-md-9 pull-right" style="margin-top: 0px; margin-bottom: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<div class="col-md-9" style="padding-left: 0px">

							<br><h4><strong>Envía tus requerimientos</strong></h4>

							<p>Llena nuestro formulario de pedido, personalízalo agregando tus datos y tu logo.</p><br>

						</div>

						<div class="col-md-3" style="padding-top: 40px; text-align: right;">	

							<a href="rollosFormulario"><button class="btn btn-primary">Ir al formulario</button></a>

						</div>

					</div>	

				</div>

			</div>

			<div class="container col-md-9 pull-right" style="margin-top: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="text-align: justify;">

						<div class="col-md-9" style="padding-left: 0px">

							<br><h4><strong>Solicita tu presupuesto en línea para rollos físcales</strong></h4>

							<p>A través del cotizador en línea podrás solicitar un presupuesto antes de crear tu pedido.</p><br>

						</div>

						<div class="col-md-3" style="padding-top: 40px; text-align: right;">	

							<a href="cotizador"><button class="btn btn-primary">Cotizar ahora</button></a>

						</div>

					</div>	

				</div>

			</div>



		</div>	

	</div>

