<!--==========================================
PIE DE PÁGINA
===========================================-->

<div class="container-fluid" style="background:#808080">

  <div class="container" style="margin-top:0px;">

    <div class="container col-md-3" style="margin:0px; padding-top:40px; padding-bottom:40px; text-align: justify;">

      <h3 style="color:#4B4B4B; display: inline;"><strong>GrafoFormas HB C.A</strong></h3>


      <p style="color:#4B4B4B; padding-top: 10px;">Imprenta de facturas autorizadas por SENIAT, compra por Internet permitiendo obtener su producto en toda Venezuela. Fácil y rápido. Av. Ppal el Hipódromo con calle Táchira Local Nro. 2 Barrio San Ignacio, Maracay Estado Aragua, Venezuela.</p>

    </div>

    <div class="container col-md-3" style="margin:0px; padding-top:40px; padding-bottom:40px; text-align: justify;">

      <h3 style="color:#4B4B4B; display: inline;"><strong>Sobre Nosotros</strong></h3>

      <p><ul>

        <li><a href="">- La empresa</a></li>
        <li><a href="">- Cuentas bancarias</a></li>
        <li><a href="">- Términos y condiciones</a></li>
        <li><a href="">- Normativa SENIAT</a></li>
        <li><a href="">- Mapa del sitio</a></li>

      </ul></p>

    </div>

    <div class="container col-md-3" style="margin:0px; padding-top:40px; padding-bottom:40px; text-align: justify;">

      <h3 style="color:#4B4B4B; display: inline;"><strong>Servicios</strong></h3>

      <p><ul>

        <li><a href="">- Atención al cliente</a></li>
        <li><a href="">- Cotización en línea</a></li>
        <li><a href="">- Carrito de compras</a></li>
        <li><a href="">- Reportar pago</a></li>

      </ul></p>

    </div>

    <div class="container col-md-3" style="margin:0px; padding-top:40px; padding-bottom:40px;">

      <h3 style="color:#4B4B4B; display: inline;"><strong>Redes Sociales</strong></h3>

      <div class="container col-md-6" style="font-size: 500%; padding:0px; margin: 0px;">

        <a href="#"><i class="fa fa-facebook-square"></i></a>

      </div>

      <div class="container col-md-6" style="font-size: 500%; padding:0px; margin: 0px;">

        <a href="#"><i class="fa fa-twitter-square"></i></a>

      </div>

      <div class="container col-md-6" style="font-size: 500%; padding:0px; margin: 0px;">

        <a href="#"><i class="fa fa-google-plus-square"></i></a>

      </div>

      <div class="container col-md-6" style="font-size: 500%; padding:0px; margin: 0px;">

        <a href="#"><i class="fa fa-youtube-square"></i></a>

      </div>

    </div>

  </div>

</div>

<!--==============================
MODAL - INICIAR SESIÓN
===============================-->

<div id="modalIniciarSesion" class="modal fade" role="dialog">

  <div class="modal-dialog" style="width:500px;">

    <div class="modal-content">

      <!-- CABECERA DEL MODAL -->

      <div class="modal-header" style="background:white; color:#018ba7">

        <button type="button" class="close" data-dismiss="modal">&times;</button>

        <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Inicio de Sesión</h1>

      </div>

      <div class="box-body">

        <!--=====================================
        =        USUARIO EXISTENTE             =
        ======================================-->

        <div class="box-body col-md-12">

          <div class="box-body">


            <form method="post">

              <!-- ENTRADA PARA EL CORREO -->

              <div class="from-group" style="text-align: justify;">

                <h3 style="margin-top: 0px;"><i class="fa fa-user"></i> <strong>Usuario Existente</strong></h3>
                <h4>Por favor, introduzca su correo electrónico y contraseña para ingresar al sistema.</h4><hr>

                <div class="input-group col-md-12">

                  <label><h4><strong>Correo electrónico:</strong></h4></label>

                  <input type="email" class="form-control input-lg" placeholder="Ingrese su correo electrónico" id="ingCorreo" name="ingCorreo" required>

                </div>

              </div><br>

              <!-- ENTRADA PARA LA CONTRASEÑA -->

              <div class="from-group">

                <div class="input-group col-md-12" style="margin-bottom: 11px;">

                  <label><h4><strong>Contraseña:</strong></h4></label>

                  <input type="password" class="form-control input-lg" placeholder="Ingrese su contraseña" id="ingPassword" name="ingPassword" required>

                </div>

              </div><br>

              <div class="container col-md-12" style="margin: 0px; padding: 0px; padding-bottom: 10px;">


                <?php

                $ingreso = new ControladorClientes();
                $ingreso -> ctrIngresoCliente();

                ?>
                <a href="#modalRecuperarContraseña" data-dismiss="modal" data-toggle="modal">
                  <p class="btn btn-default">¿Olvidaste tu contraseña?</p>
                </a>

                <input type="submit" class="btn btn-primary pull-right btnIngreso" value="Ingresar">

              </div>

            </form>

          </div>

          <div class="modal-footer" style="margin: 0px; padding-bottom: 0px; padding-right: 0px;">

            <div class="col-md-12">

              <p style="margin: 0px;">¿No tienes una cuenta registrada? | <a href="#modalRegistro" data-dismiss="modal" data-toggle="modal"><strong>Regístrate</strong></a></p>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

</div>

  <!--==============================
  MODAL - REGRISTRO
  ===============================-->

  <div id="modalRegistro" class="modal fade" role="dialog">

    <div class="modal-dialog">

      <div class="modal-content">

        <!-- CABECERA DEL MODAL -->

        <div class="modal-header" style="background:white; color:#018ba7">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Registro de Usuario</h1>

        </div>

        <div class="box-body">

          <!--=====================================
          =            USUARIO NUEVO              =
          ======================================-->

          <div class="box-body col-md-12" style="padding: 20px;">

            <div class="from-group" id="padre" style="text-align: justify;">

              <h3 style="margin-top: 0px;"><i class="fa fa-user-plus"></i> <strong>Nuevo Usuario</strong></h3>
              <h4>Por favor, complete el siguiente formulario de registro para poder ingresar al sistema.</h4><hr>

              <form method="post"  onsubmit="return registroCliente()">

                <div class="col-md-6" style="text-align: justify;">

                  <div class="input-group col-md-12">

                    <label>Correo electrónico:</label>
                    <input type="email" class="form-control input-md" id="regCorreo" name="regCorreo" onblur=" return validarCorreo(event)" maxlength="60" minlength="10" placeholder="tucorreo@empresa.com" required>



                  </div><br>

                  <div class="input-group col-md-12">

                    <label>Nombre:</label>

                    <input type="text" class="form-control" id="regNombre" name="regNombre" onkeypress=" return soloLetras(event)" maxlength="60" minlength="4" placeholder="Tu nombre" required></input>

                  </div><br>

                  <input type="text" id="fecha" class="hide" value="<?php echo date("Y/m/d");?>">
                  <label>Fecha de nacimiento:</label>

                  <div class="input-group date fechaNac col-md-12">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                    <input type="text" onchange="verificaMayorDeEdad(this)" name="regFechaNac" id="regFechaNac" class="form-control" required>


                  </div><br>

                  <div class="input-group col-md-12">

                    <label>Contraseña:</label>

                 <input type="password"  class="form-control input-md" id="regPassword" onblur="revisarEstandar(this)" onmouseover="dwec_tooltip_show(tooltip)" onmouseout="dwec_tooltip_hide(tooltip)" name="regPassword" maxlength="20" minlength="6" required>

                  <br><br><div class="tooltip" id="tooltip"> Debe incluir:<br> Mayusculas<br>  Minuscula<br>  1 caracter especial<br>   Y Numeros</div>

                  </div><br>
                  <div class="input-group col-md-12">

                  <label>Repita su Contraseña:</label><br>

                  <input type="password"  class="form-control input-md" onblur="comprobarClave(this)" id="regPassword2" name="regPassword2" maxlength="20" minlength="6"required>

                  </div><br><br>
                  <div class="input-group col-md-12">

                    <label>Pregunta secreta:</label>

                    <select class="form-control" name="PregReg1" id="PregReg1" required>

                      <option value="">Elegir Pregunta</option>

                      <option value="1">Nombre de mi primera mascota</option>

                      <option value="2">Cumpleaños de mi padre</option>

                      <option value="3">Lugar de Nacimiento de la madre</option>

                      <option value="4">Nombre de mi primer jefe</option>

                      <option value="5">Mi deporte faorito</option>


                    </select><br>
                    <label>Respuesta</label>
                    <input type="text" name="respuesta1" maxlength="30">
                  </div><br>



                </div>

                <div class="from-group col-md-6" style="text-align: justify;">

                  <div class="input-group col-md-12">

                    <label>Cédula:</label>

                    <input type="text" class="form-control " id="regCedula" name="regCedula"  onkeypress=" return soloNumeros(event)" maxlength="8" minlength="6" placeholder="12345678" required></input>

                  </div><br>

                  <div class="input-group col-md-12">

                    <label>Apellido:</label>

                    <input type="text" class="form-control" id="regApellido" name="regApellido" onkeypress=" return soloLetras(event)" maxlength="60" minlength="4" placeholder="Tu apellido" required></input>

                  </div><br>

                  <div class="input-group col-md-12">

                    <label>Teléfono móvil:</label>

                    <input type="text" class="form-control" id="regTelfMovil" name="regTelfMovil" minlength="15" data-inputmask="'mask':'(9999) 999-9999'"  data-mask required>

                  </div><br>
                  <div class="input-group col-md-12">

                    <label>Teléfono local:</label>

                    <input type="text" class="form-control" id="regTelfLocal" name="regTelfLocal" minlength="15"  data-inputmask="'mask':'(9999) 999-9999'" data-mask required>




                  </div><br>
                  <div class="input-group col-md-12">

                    <label>Pregunta secreta:</label>

                    <select class="form-control" name="PregReg2" id="PregReg" required>

                      <option value="">Elegir Pregunta</option>

                      <option value="6">Lugar de mi primer viaje</option>

                      <option value="7">Sabor favorito</option>

                      <option value="8">Segundo nombre de mi pareja</option>

                      <option value="9">Modelo de mi primer auto</option>

                      <option value="10">Canción favorita</option>

                    </select><br>
                    <label>Respuesta</label>
                    <input type="text" name="respuesta2" maxlength="30">

                  </div><br>

                </div><hr>

                <!-- CHECK PARA LOS TERMINOS Y CONDICIONES DE USO -->

                <div class="form-group col-md-8" style="padding: 0px; margin:0px; text-align: justify; padding-bottom: 10px; padding-top: 10px;">

                  <small>

                    <input type="checkbox" id="regTerminos"> Acepto los <a href="#">Términos y Condiciones</a> de GrafoFormas HB.</input>

                  </small>

                </div>

                <input type="submit" class="btn btn-primary pull-right btnRegistro" value="Continuar">


                <?php

                  $registro = new ControladorClientes();
                  $registro -> ctrCrearCliente();

                ?>

              </form>


              <div class="modal-footer form-group col-md-12" style="margin: 0px; padding-bottom: 0px; padding-right: 0px;">

                <div class="container col-md-12" style="margin:0px; padding: 0px;">

                  <p style="margin: 0px;">¿Ya tienes una cuenta registrada? | <a href="#modalIniciarSesion" data-dismiss="modal" data-toggle="modal"><strong>Inicia Sesión</strong></a></p>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>
  </div>


  <!--==============================
  MODAL - RECUPERAR CONTRASEÑA
  ===============================-->

  <div id="modalRecuperarContraseña" class="modal fade" role="dialog">

    <div class="modal-dialog">

      <div class="modal-content">

        <!-- CABECERA DEL MODAL -->

        <div class="modal-header" style="background:white; color:#018ba7">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Inicio de Sesión</h1>

        </div>

        <div class="box-body">

          <div class="box-body col-md-12">

            <div class="box-body">

              <!-- ENTRADA PARA EL CORREO -->
              <div class="from-group" style="text-align: justify;">

                <h3 style="margin-top: 0px;"><i class="fa fa-expeditedssl"></i> Recuperar contraseña</h3>
                <p>Por favor escribe tu correo electronico</p>
              <form method="post">

                <div class="input-group">

                  <label>Correo electrónico:</label>

                  <input type="text" name="CorreoRecupera" class="form-control" required></input>

                </div>
                <div class="from-group" style="text-align: justify;">


                <p>Por favor escribe selecciona tu pregunta secreta</p>
                <div class="input-group">

                  <label>pregunta:</label>

                 <select class="form-control" name="PregRecupera" id="Pregrecupera" required>

                      <option value="">Elegir Pregunta</option>

                      <option value="1">Nombre de mi primera mascota</option>

                      <option value="2">Cumpleaños de mi padre</option>

                      <option value="3">Lugar de Nacimiento de la madre</option>

                      <option value="4">Nombre de mi primer jefe</option>

                      <option value="5">Mi deporte faorito</option>

                      <option value="6">Lugar de mi primer viaje</option>

                      <option value="7">Sabor favorito</option>

                      <option value="8">Segundo nombre de mi pareja</option>

                    </select><br>

                </div>
                <div class="from-group" style="text-align: justify;">


                <p>Por escribe tu respuesta secreta</p>
                <div class="input-group">

                  <label>respuesta:</label>

                  <input type="text" name="respuestarecu" class="form-control" required></input>

                </div>

              </div><br>

            </div>

            <div class="modal-footer centrarAjustar">

              <div class="container col-md-3">

                 <input type="submit" class="btn btn-primary pull-right btnRecupera" value="Enviar">
              </div><br>
              <?php
                $recupera = new ControladorClientes();
                $recupera->ctrRecuperarContra();
                ?>
                </form>
              <div class="container col-md-9">

                <p style="margin-bottom: 0px;">¿No tienes una cuenta registrada? | <a href="#modalIniciarSesion" data-dismiss="modal" data-toggle="modal">Registrarse</a></p>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>


</div>
<script type="text/javascript">

  function comprobarClave(e){
  input = $('#regPassword'); //input de la fecha
  input2 = $('#regPassword2'); //input de la fecha
  clave1x =e.value;

  clave1 = $('#regPassword').val();
  clave2 = $('#regPassword2').val();


    if (clave1 === clave2){
      $("#padre").parent().before('<div class="alert alert-success"><strong>PERFECTO:</strong> Las contraseñas son iguales.</div>');

    }else{
       swal({
      type:"error",
      title:"Las dos claves son distintas...\nPor favor rectifique",
      width: '250px'
      })
      
      $("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Las contraseñas no coinciden.</div>');
        $('input[name="regPassword2"]').val('');
        $('input[name="regPassword"]').val('');
     }
}
function revisarEstandar(elemento){


  if (elemento.value!=""){

    var dato = elemento.value;

    var expresion =/^.*(?=.{2,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%*]).*$/;
    if(!expresion.test(dato)){
      elemento.className = 'error';
      swal({
      type:"error",
      title: "La contraseña no fue escrita adecuadamente.  \nDebe contener:\n, Mayusculas\nMinusculas\nNúmeros\nCaracteres Especiales\n",
      width: '250px'
      })
      swal();

      elemento.value = '';

    }else{
    }
  }
}
function dwec_tooltip_show (id) {

    setTimeout(function() {

        id.style.display="block";

    }, 300);
}



function dwec_tooltip_hide (id) {

    setTimeout(function() {

        id.style.display="";

    }, 300);
}
</script>
