
<?php

$url = Ruta::ctrRuta();

if (!isset($_SESSION["validarSesion"])){
	echo '<script>


	swal({
		title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
		text: "Gracias por elegirnos<br>",
		type:"error",
		showConfirmButton: true,
		confirmButtonColor: "#018ba7",
		confirmButtonText: "Cerrar",
		closeOnConfirm: false

	}).then((result) => {
		if (result.value) {

			window.location ="inicio#modalIniciarSesion";

		}
	})

	</script>';

	exit();
}

?>
<!--=====================================
PARTE PRINCIPAL (RESUMEN DE COMPRAS)
=======================================-->

<div class="container-fluid eltodo">

	<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

		<!--===========================================================================-->

		<div class="container col-md-12">

			<div class="col-md-12" style=" padding: 0px;">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

					<h1><strong>Resumen de lista de compras</strong></h1>

				</div>

				<div class="col-md-12" style="padding: 0px; text-align: center;">

					<!--=====================================
					=     CARRITO DE COMPRAS  (RECUMEN)     =
					======================================-->

					<div class="col-md-12" style="padding: 0px;">

						<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">


							<div class="wizards">

								<div class="progressbar">

									<div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div> <!-- 19.66% -->

								</div>

								<div class="form-wizard active">

									<div class="wizard-icon"><i class="fa  fa-shopping-cart"></i></div>

									<p>RESUMEN</p>

								</div>

								<div class="form-wizard">

									<div class="wizard-icon"><i class="fa fa-user"></i></div>

									<p>INGRESO</p>

								</div>

								<div class="form-wizard">

									<div class="wizard-icon"><i class="fa fa-home"></i></div>

									<p>DIRECCIONES</p>

								</div>

								<div class="form-wizard">

									<div class="wizard-icon"><i class="fa fa-truck"></i></div>

									<p>ENVÍO</p>

								</div>

								<div class="form-wizard">

									<div class="wizard-icon"><i class="fa  fa-credit-card"></i></div>

									<p>PAGO</p>

								</div>

							</div><br><br>

							<div class="panel panel-primary">

								<div class="panel-body">

									<!-- TABLA DE RESUMEN PEDIDOS -->

									<div class="box-body">

										<table class="table table-bordered table-striped dt-responsive tablas">

											<thead>

												<tr>

													<th>Producto</th>
													<th>Resumen de pedido</th>
													<th>Precio</th>
													<th>Opciones</th>

												</tr>

											</thead>
											<tbody>

												<?php foreach (ControladorPedidos::ctrMostrarCarrito($_SESSION["clienteId"]) as $key => $value) {?>
													<tr>


														<td>
															<img src= <?='"'.ControladorProductos::ctrMostrarDetalleProductos($value["id_producto_carr"],$detalle="imagen").'"'?>class="img-thumbnail" width="90px">
														</td>

														<td>
															<div style="text-align: justify;">
																<h4><strong>
																	<?php $valuex=ControladorProductos::ctrMostrarDetalleProductos($value["id_producto_carr"],$detalle="nombre");
																	echo $valuex;?>
																</strong></h4><hr>

																<h3><strong>Orden<?php echo ' '.$value["orden_carr"].'';?></strong></h3>


																<p><strong>Cantidad:</strong> <?php echo ' '. $value["cantidad_carr"].'';?></p>
																<p><strong>N° de factura inicial:</strong><?php echo ' '.$value["num_factura_ini_carr"].'';?></p>
																<p><strong>N° de control inicial:</strong> <?php echo ' '.$value["num_control_ini_carr"].'';?></p>
																<p><strong>Material:</strong> <?php echo ' '.$value["preferencia_impresion_carr"].'';?></p>
																<p><strong>Impresión:</strong> Duplicado (Original y 1 copia)</p>
																<p><strong>Color:</strong> <?php echo ' '.$value["opciones_color_carr"].'';?></p><hr>

																<p><strong>Total producto (sin IVA):</strong> Bs.<?php echo ''.$value["monto_carr"].'';?></p>
																<p><strong>Impuesto (12%):</strong> Bs. <?php
																$iva=ControladorProductos::ctrMontoInva($value["monto_carr"]);								echo ' '.$iva.'';
																?></p>
																<p><strong>TOTAL:</strong> BsF.<?php 
																$total=$iva+$value["monto_carr"];
																echo ' '.$total.'';?></p>

																<p><strong>TOTAL.:</strong> BsS.<?= ' '.($total/100000).'';?></p>


															</div>

														</td>

														<td style="text-align: left;">
														<?php echo ''.$value["monto_carr"].' BsF <br>'; echo ''.($value["monto_carr"]/100000).' BsS';?> </td>
														 

														<td>

															<div style="text-align: justify;">

																<a href="direccionesCompras&<?= $value["id_carrito"]; ?>"><button class="btn btn-primary">Comprar pedido <i class="fa fa-cart-arrow-down"></i></button></a>

																<a href="editarFactura&Editar-<?= $value["id_carrito"]; ?>"><button class="btn btn-primary btnEditarCliente">Editar pedido <i class="fa fa-edit"></i></button></a>
																<button class="btn btn-primary btnEliminarCarrito" idCarritox="<?= $value["id_carrito"]; ?>">Eliminar</button>

															</div>
														</td>
													</tr>
												<?php }?>
											</tbody>

										</table>

									</div>

								</div>
							</div>

						</div>

					</div>
				</div>

			</div>

		</div>

	</div>

</div>
