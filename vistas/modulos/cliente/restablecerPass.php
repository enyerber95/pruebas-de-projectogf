
<?php
$id = $_GET["id"];
$id=$id-204204;
?>
<div class="container-fluid">
			
		<div class="container">

			<h1 style="margin-top: 0px;"><strong>Restablecer Contraseña</strong></h1>
			<hr>

			<div class="container col-md-6" style="margin-bottom:0px;">

				<div class="col-md-12" style="padding: 0px;">

					<!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->

					<form role="form" method="post" enctype="multipart/form-data"> 

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>Información personal</strong></div>

							<div class="panel-body">


								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Contraseña:</label>

										<input type="password" class="form-control" id="newPass" name="newPass" placeholder="Ingrese nueva contraseña" onblur="revisarEstandar(this)" value="">

									</div><br>

								</div>

								<div class="from-group col-md-6" style="text-align: justify;">

									<div class="input-group">

										<label>Repita Contraseña:</label>

										<input type="password" class="form-control" id="newPass2" name="newPass2" placeholder="Ingrese nueva contraseña" value="" onblur="comprobarClavex(this)">

									</div><br>

								</div>

								
							<div class="col-md-12" style="padding-top:20px; padding-right:0px; text-align: right;">

								<button type="submit" class="btn btn-primary">Guardar cambios</button>

							</div>

							<?php 

								$editarCliente = new ControladorClientes();
								$editarCliente -> ctrRestablecerPass($id);

							?>		

						</div>

					</form>

				</div>		 	

			</div>

		</div>
	
	</div>

	</div>

	</div>


	<br><br>
	<script type="text/javascript">

  function comprobarClavex(e){
  input = $('#newPass'); //input de la fecha
  input2 = $('#newPass2'); //input de la fecha
  clave1x =e.value;

  clave1 = $('#newPass').val();
  clave2 = $('#newPass2').val();


    if (clave1 === clave2){
      $("#padre").parent().before('<div class="alert alert-success"><strong>PERFECTO:</strong> Las contraseñas son iguales.</div>');

    }else{
       alert("Las dos claves son distintas...\nPor favor rectifique");
      $("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Las contraseñas no coinciden.</div>');
        $('input[name="newPass"]').val('');
        $('input[name="newPass2"]').val('');
     }
}
function revisarEstandar(elemento){


  if (elemento.value!=""){

    var dato = elemento.value;

    var expresion =/^.*(?=.{2,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%*]).*$/;
    if(!expresion.test(dato)){
      elemento.className = 'error';
      swal({
      type:"error",
      title: "La contraseña no fue escrita adecuadamente.  \nDebe contener:\n, Mayusculas\nMinusculas\nNúmeros\nCaracteres Especiales\n",
      width: '250px'
      })
      swal();

      elemento.className = '';
      
    }else{
    }
  }
}
</script>