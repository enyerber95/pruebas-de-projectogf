
<!--=====================================
TOP
======================================-->

<div class="container-fluid barraSuperior" id="top">

    <div class="container" style="margin-top: 5px; margin-bottom: 5px;">

        <div class="row">

            <!--=====================================
            BARRA MAYOR SUPERIOR (AGUA MARINA)
            ======================================-->

            <div class="">

                <ul class="">

                    <li>
                        <a href="inicio" target="">
                            <img src="vistas/img/grafoformas/logo-mini.png" class="img-responsive logoCabezote social">
                        </a>
                    </li>

                    <li>

                        <?php

                            if (!isset($_SESSION["validarSesion"])){

                        ?>

                        <a href="#" data-toggle="modal" data-target="#modalIniciarSesion">Iniciar Sesión</a>

                        <?php

                            }else{
                         ?>

                        <a href="logout">Cerrar Sesión</a>

                    </li>

                     <li>
                        <a href="editarPerfil" class="btnEditarCliente" style="">Editar Perfil</a>
                    </li>

                    <?php
                            }

                         ?>

                    <li>
                        <a href="reportarPago" style="">Reportar Pago</a>
                    </li>


                    <li>
                        <a href="#">Ayuda</a>
                    </li>

                    <li class="centrarAjustar">

                        <form class="form col-md-3 col-sm-6 pull-right" method="get" style="padding-top: 8px;">

                            <div class="input-group">

                                <input class="form-control" type="text" placeholder="Buscar" name="user_query" required>

                                <span class="input-group-btn">

                                        <button type="submit" value="Search" name="search" class="btn btn-default"><i class="fa fa-search"></i></button>

                                    </span>

                            </div>

                        </form>

                    </li>

                </ul>


            </div>



        </div>

    </div>

</div>

<!--=====================================
LOGO GRANDE Y CORREOS Y TELÉFONOS
======================================-->

<div class="container-fluid">

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 logoGrande">

                <a href="inicio">

                    <img src="vistas/img/grafoformas/logo-grande.png" class="img-responsive logoGrande">

                </a>

            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 contactos">

                <div class="container-fluid contactos">

                    <div class="container-fluid numeros" style="text-align: right;">

                        <p style="margin-bottom: 0px; color:#808080;">(0243) 2405019<br>(0414) 3221507</p>

                        <div class="linea"></div>

                        <i class="fa fa-phone" style="font-size: 45px; padding-left: 10px; color:#808080; text-align: right;"></i>

                    </div>

                    <div class="container-fluid numeros" style="text-align: right;">

                        <p style="margin-bottom: 0px; color:#808080;">grafoformashb.vtas@yahoo.es<br>grafoformashb.vtas@hotmail.com</p>

                        <div class="linea"></div>

                        <i class="fa fa-envelope" style="font-size: 40px; padding-left: 10px; color:#808080;"></i>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<!--=====================================
MENÚ DE NAVEGACIÓN
======================================-->

<div class="container-fluid">

    <div class="container" style="margin-top: 0px; margin-bottom: 0px;">

        <br>

        <header>

            <nav class="navbar navbar-default">

                <div class="container-fluid" style="padding-right: 0px; padding-left: 0px;">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-left menuPrincipal">

                        <li><a href="inicio">Inicio<span class="sr-only"></span></a></li>

                        <li><a href="quienesSomos">Quiénes Somos</a></li>

                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Catálogo <span class="caret"></span></a>

                            <ul class="dropdown-menu">

                                <li><a href="facturasCatalogo">Facturas</a></li>
                                <li><a href="formasLibresCatalogo">Formas Libres</a></li>
                                <li><a href="rollos">Rollos Físcales</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Producto 1</a></li>
                                <li><a href="#">Producto 2</a></li>
                                <li><a href="#">Producto 3</a></li>

                            </ul>

                        </li>
                         <?php

                            if (isset($_SESSION["validarSesion"])){

                        ?>
                        <li class="dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pedidos <span class="caret"></span></a>

                            <ul class="dropdown-menu">

                                <li><a href="historialPedidos">Mis Pedidos</a></li>


                                <li><a href="direcciones">Mis Direcciones</a></li>

                            </ul>

                        </li>
                         <?php    } ?>
                        <li><a href="cotizador">Cotizador en Línea</a></li>

                    </ul>

                    <!--=====================================
                       CARRITO DE COMPRAS
                    ======================================-->

                    <ul class="nav navbar-nav navbar-right" style="padding-top: 5px;">

                        <li class="centrarAjustar" style="display: flex;">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding: 0px;">

                                <div class="col-md-12 centrarAjustar">

                                    <div class="linea" style=""></div>

                                    <span class="glyphicon glyphicon-shopping-cart" style="font-size: 150%; padding-left:10px;"></span>

                                    <span style="padding-left:10px;"><?php if (isset($_SESSION["validarSesion"])){ echo count(ControladorPedidos::ctrMostrarCarrito($_SESSION["clienteId"]));}else{echo 0;} ?> Artículos</span>

                                </div>

                            </a>

                            <div class="dropdown-menu" style="width: 280px; margin-top: 5px;padding-top: 0px; padding-bottom: 0px">

                                <div class="panel panel-default" style="margin-bottom: 0px; border-bottom-width: 0px; border-top-width: 0px;">

                                    <!-- CABECERA DE LA MINI TABLA -->

                                    <div class="panel-heading">

                                        <div class="row centrarAjustar">

                                            <div class="col-md-10 textoCarrito" style="text-align: center; font-size: 110%">

                                                <p>Pedidos recientemente agregados</p>

                                            </div>

                                        </div>

                                    </div>

                                    <!-- CUERPO DE LA MINI TABLA (PRODUCTOS) -->

                                    <?php

                                    if (isset($_SESSION["validarSesion"])){

                                    ?>
                                        <div class="panel-body">

                                            <div id="cart_product" class="textoCarrito">

                                                <?php foreach (ControladorPedidos::ctrMostrarCarrito($_SESSION["clienteId"]) as $key => $value) {?>
                                                    <div class="row centrarAjustar">

                                                        <div class="col-md-6">
                                                            <img src= <?='"'.ControladorProductos::ctrMostrarDetalleProductos($value["id_producto_carr"],$detalle="imagen").'"'?> class="img-thumbnail" width="100px">
                                                        </div>

                                                        <div class="col-md-6" style="padding-left:0px;">
                                                            <p><?= $value["orden_carr"];?></p><br>
                                                            <p>Bs. .<?= $value["monto_carr"]+($value["monto_carr"]*0.12);?></p>
                                                        </div>

                                                    </div>

                                                    <hr>
                                                <?php }?>
                                            </div>

                                        </div>

                                        <!-- PIE DE LA MINI TABLA (BOTÓN PROCESAR COMPRA) -->

                                        <hr style="margin: 0px;">

                                        <div class="panel-body centrarAjustar">

                                            <a href="resumenCompras"><button type="button" class="btn btn-default">Procesar Compra</button></a>

                                        </div>

                                    <?php }
                                        else{
                                    ?>
                                        <div class="panel-body" style="text-align: center;">
                                            <label >Por favor Inicie Sesion</label>
                                        </div>
                                    <?php } ?>

                                </div>

                            </div>

                        </li>

                    </ul>

                </div> <!-- /.navbar-collapse -->

    </div> <!-- /.container-fluid -->

    </nav>﻿

    </header>

</div>

</div>
