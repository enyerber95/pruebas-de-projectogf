<?php 
   $url = Ruta::ctrRuta();
   
     	if (!isset($_SESSION["validarSesion"])){
   	echo '<script> 
   			
   				
   						swal({
   							title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
   							text: "Gracias por elegirnos<br>",
   							type:"error",
   					  		showConfirmButton: true,
   							confirmButtonColor: "#018ba7",
   							confirmButtonText: "Cerrar",
   							closeOnConfirm: false
   
   							  }).then((result) => {
   								if (result.value) {
   
   								window.location ="inicio#modalIniciarSesion";
   
   								}
   							})
   
   						</script>';
   
     		exit();
     	}
     	   	$id = $_GET["id"];
   
   ?> 
<!--=====================================
   PARTE PRINCIPAL (RESUMEN DE COMPRAS)
   =======================================-->
<div class="container-fluid eltodo">
   <div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">
      <!--===========================================================================-->
      <div class="container col-md-12">
         <div class="col-md-12" style=" padding: 0px;">
            <div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">
               <h1><strong>Métodos de envío</strong></h1>
            </div>
            <div class="col-md-12" style="padding: 0px; text-align: center;">
               <!--=====================================
                  =     CARRITO DE COMPRAS  (RECUMEN)     =
                  ======================================-->
               <div class="col-md-12" style="padding: 0px;">
                  <div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">
                     <div class="wizards">
                        <div class="progressbar">
                           <div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div>
                           <!-- 19.66% -->
                        </div>
                        <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa  fa-shopping-cart"></i></div>
                           <p>RESUMEN</p>
                        </div>
                        <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa fa-user"></i></div>
                           <p>INGRESO</p>
                        </div>
                        <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa fa-home"></i></div>
                           <p>DIRECCIONES</p>
                        </div>
                        <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa fa-truck"></i></div>
                           <p>ENVÍO</p>
                        </div>
                        <div class="form-wizard active">
                           <div class="wizard-icon"><i class="fa  fa-credit-card"></i></div>
                           <p>PAGO</p>
                        </div>
                     </div>
                     <br><br>
                     <div class="panel panel-primary">
                        <div class="panel-body">
                           <div class="box-body col-md-12">
                              <!-- MÉTODOS DE ENVÍO -->
                              <div class="container col-md-12" style="text-align: justify; margin-top: 0px;">
                                  <a href="inicio"><button class="btn btn-primary">Reportar más tarde <i class="fa fa-cart-arrow-down"></i></button></a>                             <h4><strong>Puedes realizar el pago de tu orden a través de transferencia electrónica o depósito bancario llenando el siguiente formulario.</strong></h4>
                                 <br>
                                 <div class="panel panel-default">
                                    <div class="panel-body">
                                       <!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->
                                       <form method="post" class="form col-md-12" style="padding: 0px;">
                                          <!-- PARTE IZQUERDA -->
                                          <div class="from-group col-md-6" style="text-align: justify;">
                                             <div class="input-group col-md-12">
                                                <label>Nombre y apellido:</label>
                                                <?php
                                                   echo '<input type="text" class="form-control" name="nombreCliente" value="'.$_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"].'"readonly>';
                                                   ?>
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12">
                                                <label>Número de orden:</label>
                                                <select class="form-control" disabled>
                                                <?php foreach (ControladorPedidos::ctrMostrarDetalleEnvio($id) as $key => $value){ ?>

                                                 <?php echo '<option value='.$value["orden"].'>'.$value["orden"].'</option>'; }?>
                                                </select>
                                             </div>
                                             <div class="col-md-12" style="padding: 0px;">
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12">
                                                <label>Banco emisor:</label>
                                                <select class="form-control" name="BankEmi" required="">
                                                 <option value="">Elegir banco</option>

                                                      <option value="Bancaribe">Bancaribe</option>
                                                      <option value="Banesco">Banesco</option>
                                                      <option value="Bicentenario">Bicentenario</option>
                                                      <option value="B.O.D">B.O.D</option>
                                                      <option value="Mercantil">Mercantil</option>
                                                      <option value="Provincial">Provincial</option>
                                                      <option value="Venezuela">Venezuela</option>
                                                      <option value="otros">otros</option>
                                                      
                                                </select>
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12" style="text-align: justify;">
                                                <label>Banco receptor:</label>
                                                <select class="form-control" name="BankRecp" required>
                                                   <option value="">Elegir banco</option>
                                                   <option value="Banesco">Banesco</option>
                                                   <option value="Mercantil">Mercantil</option>
                                                </select>
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12" style="text-align: justify;">
                                                
                                             </div>
                                          </div>
                                          <!-- PARTE DERECHA -->
                                          <div class="from-group col-md-6" style="text-align: justify;">
                                             <div class="input-group col-md-12">
                                                <label>Número de depósito o transferencia:</label>
                                                <input type="text" class="form-control" name="numtransf" required></input>
                                             </div>
                                             <br>
                                             <label>Fecha del depósito o transferencia:</label>
                                             <div class="input-group col-md-12 date fechaNac">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                               <input type="tectrEnviarReportext" onchange="verificaDate(this)" name="fechaReporte" id="fechaReporte" class="form-control" required="">
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12">
                                              <input type="text" id="fecha" class="hide" value="<?php echo date("Y/m/d");?>">

                                                <label>Teléfono:</label>
                                                <?php
                                                   echo '<input type="text" class="form-control" name="tlfCliente" value="'.$_SESSION["clienteTelfLocal"].'"readonly>';
                                                   ?>
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12">
                                                <label>Correo electrónico:</label>
                                                <?php
                                                   echo '<input type="text" class="form-control" name="tlfCliente" value="'.$_SESSION["clienteCorreo"].'"readonly>';
                                                   ?>
                                             </div>
                                             <br>
                                             <div class="input-group col-md-12" style="text-align: justify;">
                                               <label>Monto:</label><br>
                                                <?php foreach (ControladorPedidos::ctrMostrarDetalleEnvio($id) as $key => $value){ ?>

                                                 <?php 
                                                $iva=ControladorProductos::ctrMontoInva($value["monto"]);
                                                 $totalBsF=$value["monto"]+$iva;
                                                 $totalBsS=$totalBsF/100000;
                                                 echo '<h4><label>'.$totalBsF.' BsF </label></h4>';}?>
                                                 <br>

                                                 <?php echo '<h4><label>'.$totalBsS.' BsS</label></h4>'; ?>

                                             </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="container col-md-12" style="text-align: right; padding: 0px;">
                          		<?php
                              $vista=1;
		                        $Reporte = new ControladorReportes();
		                        $Reporte -> ctrEnviarReporte($id,$vista);
                    			?>
                        <input type="submit" class="btn btn-primary pull-right btnreporte" value="Enviar Reporte">
                     </form>

                        <a href="inicio"><button class="btn btn-primary">Reportar más tarde <i class="fa fa-cart-arrow-down"></i></button></a>
                        
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
