
	<!--=====================================
	FORMULARIOS
	=======================================-->

	<div class="container-fluid">
			
		<div class="container">

			<h1 style="margin-top: 0px;"><strong>Mis direcciones</strong></h1>
			<hr>



			<div class="container col-md-6" style="margin-bottom:0px;">

				<div class="col-md-12" style="padding: 0px;">

					<div class="panel panel-primary">

						<div class="panel-heading" style="text-align: center"><strong>Información personal</strong></div>

							<div class="panel-body">

								<!-- FORMULARIO PARA INFORMACIÓN PERSONAL -->

								<form class="form col-md-12" style="padding: 0px;"> 
										
									<div class="from-group col-md-6" style="text-align: justify;">

							        	<div class="input-group">

								        	<label>Nombre:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        			<label>Fecha de nacimiento:</label>

					        			<div class="input-group date fechaNac">

								         	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>

					        				<input type="text" class="form-control">

					        			</div>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

					        			<div class="input-group">

								        	<label>Apellido:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

					        			<div class="input-group">

								        	<label>Cédula:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

							        	<div class="input-group">

								        	<label>Teléfono local:</label>

								         	<input type="text" class="form-control" data-inputmask="'mask':'(9999) 999-9999'" data-mask required>

					        			</div><br>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

					        			<div class="input-group">

								        	<label>Teléfono móvil:</label>

								         	<input type="text" class="form-control" data-inputmask="'mask':'(9999) 999-9999'" data-mask required>

					        			</div><br>

					        			

					        		</div><br>

					        		<div class="from-group col-md-12" style="text-align: justify;">

						        		<div class="input-group">

						        			<p style="margin-bottom: 0px;">¿Desea registrar alguna empresa? (Sólo si el registro es de Persona Jurídica).</p>

						        			<div class="radio">

	      										<label><input type="radio" name="opradio" value="1" onchange="javascript:mostrarOpcionesRegistro()">Sí</label>

	    									</div>

	    									<div class="radio">

	      										<label><input type="radio" name="opradio" value="1" onchange="javascript:ocultarOpcionesRegistro()">No</label>

	    									</div>

						        		</div>

						        	</div>	

					        		<div class="from-group col-md-12" id="contenido" style="text-align: justify; display: none; padding: 0px;">

							        	<div class="form-group col-md-6" style="margin-bottom:0px;">

								        	<label>Razón social:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div>

						        		<div class="form-group col-md-6" style="margin-bottom:0px;">

									        <label>Código postal:</label>

									        <input type="text" class="form-control" required></input>

						        		</div>

									</div>

								</form>

							</div><br>

					</div>

				</div>		 	

			</div>



			<div class="container col-md-6">

				<div class="col-md-12" style="padding: 0px;">

					<div class="panel panel-primary";">

						<div class="panel-heading" style="text-align: center"><strong>Dirección y ubicación</strong></div>

							<div class="panel-body">
									
								<!-- FORMULARIO PARA DIRECCIÓN Y UBICACIÓN -->

								<form class="form col-md-12" style="padding: 0px;"> 
										
									<div class="from-group col-md-6" style="text-align: justify;">

							        	<div class="input-group col-md-12">

								        	<label>Sector / Urbanización:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        			<div class="input-group col-md-12">

								        	<label>Avenida / Calle:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

					        			<div class="input-group col-md-12">

								        	<label>Ciudad:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        			<div class="input-group col-md-12">

								        	<label>Estado:</label>

								        	<select class="form-control col-md-12">

								        		<option value="">Selecionar estado</option>

								        		<option value="">Aragua</option>

								        		<option value="">Apure</option>

								        		<option value="">Amazonas</option>

								        	</select>


					        			</div><br>

					        		</div>

					        		<div class="from-group col-md-6" style="text-align: justify;">

							        	<div class="input-group col-md-12">

								        	<label>Edificio / Quinta / Casa:</label>

								         	<input type="text" class="form-control" required></input>

					        			</div><br>

					        		</div><br>

					        		<div class="from-group col-md-12" style="text-align: justify;">

					        			<div class="input-group col-md-12">

								        	<label>Punto de referencia:</label>

								         	<textarea class="form-control"></textarea>

					        			</div>

					        		</div><br>

								</form>

							</div><br>
							
					</div>

			</div>

		</div>

		<div class="container col-md-12" style="margin:0px;">
			
			<button class="btn btn-primary">Crear cuenta</button>
			<button class="btn btn-danger">Cancelar</button>

		</div>	
	
	</div>

	</div>

	</div>


	<br><br>
