
	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->
	<?php
		$id = $_GET["id"];
		if (strlen($_GET["id"]) > 7) {
			$ararydata=explode("-", $id);
			$carrito = array_reduce(ControladorPedidos::ctrMostrarDetalleCarrito($ararydata[1],'id_carrito'), 'array_merge', array());
			$factura = ControladorProductos::ctrMostrarInfoProducto($carrito["id_producto_carr"]);
		}else {
			$factura = ControladorProductos::ctrMostrarInfoProducto($_GET["id"]);
		}
	?>
	<div class="container-fluid eltodo">

		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">

			<!--=====================================
			   CONTENIDO (FACTURAS)
			======================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="padding-right: 0px; padding-left: 0px;">

						<h1 style="margin-bottom: 0px;"><strong><?= $factura["nombre"] ?></strong></h1><br>
					</div>

				</div>

				<div class="col-md-12" style="text-align: justify; padding-left: 0px;">

					<h4><strong>Crear un documento es muy sencillo, completa los campos de la columna lateral.</strong></h4>

					<h4><strong>NOTA IMPORTANTE:</strong></h4>

					<div class="col-md-12">

						<p>• GrafoFormasHB.com utiliza la base de datos del SENIAT para completar automáticamente el campo de “Nombre de Empresa / Otro”. El usuario podrá cambiar de mayúscula a minúscula cualquier letra que desee (ejemplo de cambio permitido: JUAN RAMON LOPEZ por Juan Ramón López). Sin embargo, el pedido no será procesado si se elimina o agrega información adicional (ejemplo de cambio no permitido: JUAN RAMON LOPEZ por Lic. Juan López).</p>
						<p>• GrafoFormasHB.com no procesará ningún pedido hasta no recibir copia del RIF, el cual debe estar vigente y legible.</p>
						<p>• Para ver un video instructivo de cómo llenar una factura, haga clic aquí.</p>

					</div>

				</div>

				<!--=====================================
				BARRA LATERAL (MENÚ DE TIPOS Y TAMAÑOS)
				======================================-->


				<form  method="post" class="form col-md-12" id="envio" style="padding: 0px; text-align: justify;">
					<div class="col-md-3" style="padding-top: 35px; padding-left: 0px;">

						<div class="panel panel-primary">

							<div class="panel-body" style="background: #f7f7f7;">

									<div class="form-group col-md-12">

										<div class="input-group col-md-12">

											<label><b style="color: #E74C3C;">*</b>RIF:</label>

											<input type="text" class="form-control" id="rif" name="rif" onkeyup="Doc(this)" onblur="revisarRIFF(this)" required maxlength="12" pattern= "^[J]{1}-[0-9]{8}-[0-9]{1}$" minlength="8">

											<p>Ejemplo: J-12345678-0</p>


											<label><b style="color: #E74C3C;">*</b>Nombre de la empresa / Otro:</label>

											<input type="text" class="form-control" id="nomEmp" name="nomEmp" onkeyup="Doc(this)" maxlength="50" minlength="4" placeholder="Ej: GrafoFormasHB" required>

											<p>Recuerde que su <strong>RAZÓN SOCIAL</strong> debe ser igual a la información que posee en su RIF.</p>

											<hr>


											<label><b style="color: #E74C3C;">*</b>Dirección fiscal (línea 1):</label>

											<input type="text" class="form-control" id="dire" name="dire" onkeyup="Doc(this)" maxlength="50" minlength="4" placeholder="Ej. Zona industrial 1 local 12" required>

											<p>Recuerde que su <strong>DIRECCIÓN FÍSCAL</strong> debe ser igual a la información que posee en su RIF.</p>
											<br>

											<label>Dirección fiscal (línea 2):</label>

											<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-bottom: 15px;">

												<input type="text" class="form-control" id="dire2" name="dire2" onkeyup="Doc(this)" maxlength="50" minlength="4" placeholder="Ej. Los horcones av 3 calle 12" >

											</div><br><hr>

											<label><b style="color: #E74C3C;">*</b>Teléfono / Otro:</label>

											<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-bottom: 15px;">

											<input type="text" class="form-control" name="tlf" data-inputmask="'mask':'(9999) 999-9999'" data-mask id="tlf" onkeyup="Doc(this)" required>


											</div>
											<br><hr>


											<label><b style="color: #E74C3C;">*</b>Seleccione el tipo de presentación:</label>

											<div class="input-group">

												<div class="radio">

													<label><input type="radio" name="tipPre" onclick="Doc(this)" value="Talonario" id="Talonario" required>Talonario</label>

												</div>

												<div class="radio">

													<label><input type="radio" name="tipPre"  onclick="Doc(this)" value="Hojas sueltas" id="HojasSueltas" required>Hojas sueltas</label>

												</div>

											</div>

											<div class="input-group col-md-4" style="padding-top: 10px;">

												<label><b style="color: #E74C3C;">*</b>Cantidad:</label>

											    <select class="form-control" name="canImp"  onchange="Doc(this)" id="canImp" required>

											        <option value="1">1</option>
											        <option value="2">2</option>
											        <option value="3">3</option>
											        <option value="4">4</option>
											        <option value="5">5</option>
											        <option value="6">6</option>
											        <option value="8">8</option>
											        <option value="10">10</option>
											        <option value="20">20</option>
											        <option value="30">30</option>
											        <option value="40">40</option>
											        <option value="50">50</option>
											        <option value="60">60</option>
											        <option value="70">70</option>
											        <option value="80">80</option>
											        <option value="90">90</option>
											        <option value="100">100</option>

											    </select>

											</div>

											<div class="input-group" style="padding-top: 10px;">

												<label><b style="color: #E74C3C;">*</b>N° de factura inicial:</label>

												<input type="text" class="form-control" id="facIni" name="facIni" value="000001" onkeyup="Doc(this)"  onkeypress=" return soloNumeros(event)" maxlength="6" minlength="6" required>

												<label style="margin-top: 10px;"><b style="color: #E74C3C;">*</b>N° de control inicial:</label>

												<input type="text" class="form-control" id="conIni" name="conIni" value="000001" onkeyup="Doc(this)"  onkeypress=" return soloNumeros(event)" maxlength="6" minlength="6" required>

											</div><br>

											<!-- SUBIR LOGO -->

											<div class="input-group">

												<div class="checkbox">

													<label><input type="checkbox" name="check" id="check" value="1" onchange="javascript:showContent()">Opción de logo</label>

												</div>

												<div id="content" style="display: none;">

													<select class="form-control" onchange="sizeImg(this)" id="size" name="size" style="margin-bottom: 10px">

														<option value="50">Elegir una opción</option>
														<option value="70">Pequeño</option>
														<option value="120">Mediano</option>
														<option value="150">Grande</option>

													</select>
													<div id="sizeLogo" style="display: none;">
														<label>Logo</label><br>

														<input type="file" class="form-control logo" id="logo" name="logo">
														<!-- onchange="DocImg(this)"-->

														<p class="help-block" style="font-size: small;">El tamaño del logo se ajustará automáticamente. El logo debe ser en JPG, JPEG o GIF (sin animación) y pesar menos de 2 MB.</p>
													</div>
												</div>

											</div>

											<!-- SUBIR FONDO -->


											<label style="padding-top: 10px;"><b style="color: #E74C3C;">*</b>Preferencias de impresión:</label>

											<select class="form-control" onchange="Doc(this)" id="preImp" name="preImp" required>

												<option value="">Elegir una opción</option>
												<option value="Original y 1 copia">Original y 1 copia</option>
												<option value="Original y 2 copia">Original y 2 copia</option>
												<option value="Original y 3 copia">Original y 3 copia</option>

											</select>

											<label style="padding-top: 10px;"><b style="color: #E74C3C;">*</b>Preferencias de papel:</label>

											<select class="form-control" onchange="Doc(this)" id="prePap" name="prePap" required>

												<option value="">Elegir una opción</option>
												<option value="Papel bond">Papel bond</option>
												<option value="Papel químico">Papel químico</option>

											</select>

											<label style="padding-top: 10px;"><b style="color: #E74C3C;">*</b>Preferencias de color:</label>

											<select class="form-control" onchange="Doc(this)" id="OpcCol" name="OpcCol" required>

												<option value="">Elegir una opción</option>
												<option value="Blanco y negro">Blanco y negro</option>
												<option value="Full color">Full color</option>

											</select>

											<div class="col-md-12" style="padding: 0px; padding-top: 20px;">

												<p><i>El RIF enviado debe estar vigente, legible y mostrar el nombre y la fecha de vencimiento. Si vas a enviar el RIF luego de realizar la orden, puedes hacerlo a través de el enlace "Enviar RIF".</i></p>

											</div>

											<label><b style="color: #E74C3C;">*</b>Copia de RIF:</label>

											<input type="file"  onchange="Doc(this)" id="img" name="imgRif"  class="form-control" required>

											<p class="help-block" style="font-size: small;">La imagen del RIF debe ser en JPG, JPEG o GIF (sin animación) y pesar menos de 2 MB. Por favor asegurate que el RIF sea legible haciendo clic en el botón "Revisión y Vista Previa".</p>

											<div class="col-md-12" style="padding: 0px; padding-top: 10px;">

												<div class="panel panel-primary">

													<div class="panel-body" style="background: #eee">

														<div class="input-group">

															<div class="checkbox">

																<label><input type="checkbox" id="acepto" required >He revisado y confirmo que la información agregada por mí es correcta y libre de errores. Igualmente, si mi orden incluye formas fiscales, autorizo a GrafoFormasHB.com, en conformidad con el artículo 44 de la providencia 0071 del SENIAT, a la impresión de mi pedido como aparece en Vista Previa.</label required>

															</div>

														</div>

													</div>

												</div>


											</div>


											<div class="col-md-12" style="padding: 0px; padding-bottom: 10px;">


												<h4><strong>Precio en BsF:</strong></h4>
												<input type="hidden" id="precio_base" value=<?= $factura["precio_base"] ?>>

												<input type="text" name="precio" id="precio" value=<?php echo $factura["precio_base"]; ?> class="form-control" disabled style="margin-bottom: 10px;">

												<h4><strong>Precio en BsS:</strong></h4>

												<input type="text" name="precio" id="precioBsS" value=<?php echo ($factura["precio_base"]/100000); ?> class="form-control" disabled style="margin-bottom: 10px;">

												<p style="margin-bottom: 0px;"><strong>Nota:</strong> Este precio no incluye I.V.A.</p>

												<p style="margin-bottom: 0px;"><strong>Envío:</strong> Ocho (8) días hábiles.</p>

											</div>

										</div>

									</div>

							</div>

						</div>

					</div>


					<!--=====================================
					VISTA DE LA FACTURA EN TIEMPO REAL
					======================================-->

					<div class="container col-md-9 pull-right" style="padding-top: 35px; padding-right: 0px; margin-top: 0px;">

						<div class="container col-md-12" style="padding:0px; margin-top: 0px;">

							<div class="panel panel-primary">

								<div class="panel-body esta" style="padding: 30px; padding-top:50px; padding-bottom:50px;">

									<div class="col-md-12 preview" id="ID_DIV"  disabled>

									  	<style type="text/css">
											.top-left { border-top-left-radius: 5px; border-collapse: initial;}
											.top-right { border-top-right-radius: 5px; border-collapse: initial;}
											.bottom-left{ border-bottom-left-radius: 5px; border-collapse: initial;}
											.bottom-right { border-bottom-right-radius: 5px; border-collapse: initial;}
											.marca-agua{
												background: url('vistas/img/plantilla/prueba.png');
											}
									  	</style>

										<div class="marca-agua mi-imagen">
											<input type="hidden" id="medida" value=<?= $factura["medida"]; ?>>

											<link rel="stylesheet" href="vistas/css/stylesheet.css">

											<?php
											  echo $factura["css"];
											  echo $factura["html"];
								   		?>
											<textarea  id="css"  style="display: none !important;"><?= $factura["css"] ?></textarea>
											<label name="tabla"></label><br><br><br><br>
											<div id="hidden" class="marca-agua mi-imagen" style="display: none;">

												<?php
													echo $factura["html"];
									   		?>
													<label name="tabla"></label><br><br><br><br><br><br><br><br>

													<div style="padding-left: 15pt; font-size:17.0pt;">
														<b>Modelo:</b> <?= $factura["nombre"] ?><p></p>
														Por favor revise los siguientes datos antes de completar su orden: <p></p>
														Nombre de la empresa o persona:<br>
														<label name='nomEmpDoc'></label><p></p><br>
														RIF: &nbsp; <label name="rifDoc"></label>
														N° de control inicial: &nbsp; <label name="conIniDoc">000001</label><br>
														N° de control final: &nbsp; <label name="conFinDoc"></label><br>
														N° de factura inicial: &nbsp; <label name="facIniDoc">000001</label><br>
														N° de factura final: &nbsp; <label name="facFinDoc"></label><br>
														Tipo de presentación : &nbsp; <label name="tipPreDoc"></label><br>
														Preferencias de impresión : &nbsp; <label name="preImpDoc"></label><br>
														Preferencias de papel : &nbsp; <label name="prePapDoc"></label><br>
														Opciones de color : &nbsp; <label name="OpcColDoc"></label><p></p><br>
														Cantidad de <label name="tipPreDoc"></label> a imprimir: <label name="canImpDoc"></label><p></p><br><br><br><br><br><br><br><br><br><br><br><br><br>
														<div class='nota' style="text-align: justify; font-size:12.0pt;">
															<b>Nota:</b> El usuario es el único responsable por los datos suministrados durante la edición del documento, como
															también de las posibles omisiones de caracteres o faltas de ortografía. La elección de un tamaño muy grande de
															logo y/o letra podrían causar que parte de la información suministrada por el usuario se refleje incorrectamente. El
															usuario debe asegurarse que toda la información introducida está siendo reflejada de forma satisfactoria.
														</div><p></p><br><br>
														<div class='nota' style="padding-left: 15pt; font-size:12.0pt;">
															Señores<br>
															GrafoFormas HB C.A.<br>
															Presente.<p></p><br><br><br>
															Estimados señores,<p></p><br><br>
															<p style="text-indent: 1cm;text-align: justify;">Por medio de la presente y en cumplimiento con el artículo 44 la providencia 0071 del Servicio Nacional Integrado sin de Administración Aduanera Muestra y Tributaria, confirmo la solicitud de impresión de mi pedido acorde a las siguientes especificaciones:</p><br><br>
															Nombre de la empresa o persona:<br> <label name='nomEmpDoc'></label><br><br>
															RIF: &nbsp; <label name="rifDoc"></label><br>
															Numero Facturas a imprimir: <label name="canImpDoc"></label><br>
															Numero de Factura inicial: &nbsp; <label name="facIniDoc">000001</label><br>
															Numero de Factura final: &nbsp; <label name="facFinDoc"></label><br>
															Numero de Control inicial: &nbsp; <label name="conIniDoc">000001</label><br>
															Numero de Control final: &nbsp; <label name="conFinDoc"></label><br><br>
															Atentamete,<p></p><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
															<div style="text-align: center;">
																_____________________________________ <br>
																			El Usuario
															</div>

													 	</div><p></p><br><br><br><br>
														<div style="padding-left: 15pt; font-size:12.0pt;">
															<h4><b>Imagen del RIF</b></h4><br>
															<br><br>
															<div id="imgDoc2" style="text-align: center;">

															</div>

													 	</div>

												 	</div>
							   			</div>
										</div>
									</div>

								</div>

							</div>

						</div>

					</div>
					<?php
							if (isset($ararydata[0])) {
					?>
						<div id="ocultosEdit">
							<input type="hidden" id="id_producto" name="id_producto" value=<?= $factura["id_producto"] ?>>
							<input type="hidden" id="nombre_producto" name="nombre_producto"  value=<?= $factura["nombre"] ?>>
							<input type="hidden" id="id_carrito" name="id_carrito" value=<?= $carrito["id_carrito"] ?>>
							<input type="hidden" id="nomEmpDoc" name="nomEmpDoc"  value="<?= $carrito["nombre_empresa_carr"] ?>">
							<input type="hidden" id="rifDoc" name="rifDoc"  value="<?= $carrito["rif_carr"] ?>">
							<input type="hidden" id="direDoc" name="direDoc"  value="<?= $carrito["direc_filcal_1_carr"] ?>">
							<input type="hidden" id="dire2Doc" name="dire2Doc"  value="<?= $carrito["direc_filcal_2_carr"] ?>">
							<input type="hidden" id="tlfDoc" name="tlfDoc"  value="<?= $carrito["telefono_otro_carr"] ?>">
							<input type="hidden" id="tipPreDoc" name="tipPreDoc"  value="<?= $carrito["tipo_presentacion_carr"] ?>">
							<input type="hidden" id="canImpDoc" name="canImpDoc"  value="<?= $carrito["cantidad_carr"] ?>">
							<input type="hidden" id="facIniDoc" name="facIniDoc"  value="<?= $carrito["num_factura_ini_carr"] ?>">
							<input type="hidden" id="conIniDoc" name="conIniDoc"  value="<?= $carrito["num_control_ini_carr"] ?>">
							<input type="hidden" id="preImpDoc" name="preImpDoc"  value="<?= $carrito["preferencia_impresion_carr"] ?>">
							<input type="hidden" id="prePapDoc" name="prePapDoc"  value="<?= $carrito["preferencia_papel_carr"] ?>">
							<input type="hidden"  name="orden_carr"  value="<?= $carrito["orden_carr"] ?>">
							<input type="hidden" id="OpcColDoc" name="OpcColDoc"  value="<?= $carrito["opciones_color_carr"] ?>">
							<input type="hidden" id="imgsend" name="imgsend"   value="<?= $carrito["img_rif_carr"] ?>">
							<input type="hidden" id="logoDoc" name="logoDoc"  value="<?= $carrito["logo_carr"] ?>">
							<input type="hidden" id="Preciosend" name="Preciosend"  value="<?= $carrito["monto_carr"] ?>">
							<input type="hidden" id="sizeDoc" name="sizeDoc"  value="<?= $carrito["tamano_logo_carr"] ?>">

						</div>
					<?php
						}
						else {
					?>
						<div id="ocultos">
							<input type="hidden" name="id_producto" value=<?= $factura["id_producto"] ?>>
							<input type="hidden" name="nombre_producto" value=<?= $factura["nombre"] ?>>
							<input type="hidden" name="nomEmpDoc" value="0">
							<input type="hidden" name="rifDoc" value="0">
							<input type="hidden" name="direDoc" value="0">
							<input type="hidden" name="dire2Doc" value="0">
							<input type="hidden" name="tlfDoc" value="0">
							<input type="hidden" name="tipPreDoc" value="Talonario">
							<input type="hidden" name="canImpDoc" value="1">
							<input type="hidden" name="facIniDoc" value="000001">
							<input type="hidden" name="conIniDoc" value="000001">
							<input type="hidden" name="preImpDoc" value="000001">
							<input type="hidden" name="prePapDoc" value="000001">
							<input type="hidden" name="OpcColDoc" value="Blanco y negro">
							<input type="hidden" name="imgsend"  value="0">
							<input type="hidden" name="logoDoc" value="0">
							<input type="hidden" name="Preciosend" value="0">
							<input type="hidden" name="sizeDoc" value="0">

						</div>
					<?php
						}
				 	?>


					<div class="container col-md-12" style="padding-bottom: 35px; padding-left: 0px;">


						<a onclick="javascript:window.imprimirDIV('ID_DIV');" class="btn btn-primary" style="margin-right: 10px;">Revisión y vista Previa <i class="fa fa-eye"></i></a>


						<?php
							if (!isset($carrito)) {
								if (!isset($_SESSION["validarSesion"])){

						          	echo '<a href="#modalIniciarSesion2" class="btn btn-primary" style="margin-right: 10px;" data-dismiss="modal" data-toggle="modal"><strong>Seguir y comprar este producto</strong><i class="fa fa-shopping-cart"></i></a></p>';

					          	}
								else{

						          	echo '<button class="btn btn-primary" style="margin-right: 10px;">Seguir y comprar este producto <i class="fa fa-shopping-cart"></i></button>';

						          	$dat=0;
						          	$ingreso = new ControladorClientes();
					             	$ingreso -> ctrIngresoClienteCarrito($dat);
		    					}
							}else {

								echo '<button class="btn btn-primary" style="margin-right: 10px;">Actualizar Factura <i class="fa fa-shopping-cart"></i></button>';

								$ingreso = new ControladorClientes();
								$ingreso -> ctrUpdateClienteCarrito();

							}
						?>

					</div>
				</form>




			</div>

		</div>

	</div>

<div id="modalIniciarSesion2" class="modal fade" role="dialog">

    <div class="modal-dialog" style="width:500px;">

        <div class="modal-content">

            <!— CABECERA DEL MODAL —>

            <div class="modal-header" style="background:white; color:#018ba7">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Inicio de Sesión</h1>

            </div>

            <div class="box-body">

                <!--=====================================
                =        USUARIO EXISTENTE             =
                ======================================-->

                <div class="box-body col-md-12">

                    <div class="box-body">


                        <form method="post">

                            <!— ENTRADA PARA EL CORREO —>

                            <div class="from-group" style="text-align: justify;">

                                <h3 style="margin-top: 0px;"><i class="fa fa-user"></i> <strong>Usuario Existente</strong></h3>
                                <h4>Por favor, introduzca su correo electrónico y contraseña para ingresar al sistema.</h4><hr>

                                <div class="input-group col-md-12">

                                    <label><h4><strong>Correo electrónico:</strong></h4></label>

                                    <input type="email" class="form-control input-lg" placeholder="Ingrese su correo electrónico" id="ingCorreo2" name="ingCorreo2" required>

                                </div>

                            </div><br>

                            <!—- ENTRADA PARA LA CONTRASEÑA -—>

                            <div class="from-group">

                                <div class="input-group col-md-12" style="margin-bottom: 11px;">

                                    <label><h4><strong>Contraseña:</strong></h4></label>

                                    <input type="password" class="form-control input-lg" placeholder="Ingrese su contraseña" id="ingPassword" name="ingPassword" required>

                                </div>

                            </div><br>

                            <div id="ocultos">
																<input type="hidden" name="id_producto" value=<?= $factura["id_producto"] ?>>
																<input type="hidden" name="nombre_producto" value=<?= $factura["nombre"] ?>>
																<input type="hidden" name="nomEmpDoc" value="0">
																<input type="hidden" name="rifDoc" value="0">
																<input type="hidden" name="direDoc" value="0">
																<input type="hidden" name="dire2Doc" value="0">
																<input type="hidden" name="tlfDoc" value="0">
																<input type="hidden" name="tipPreDoc" value="Talonario">
																<input type="hidden" name="canImpDoc" value="1">
																<input type="hidden" name="facIniDoc" value="000001">
																<input type="hidden" name="conIniDoc" value="000001">
																<input type="hidden" name="preImpDoc" value="000001">
																<input type="hidden" name="prePapDoc" value="000001">
																<input type="hidden" name="OpcColDoc" value="Blanco y negro">
																<input type="hidden" name="imgsend"  value="0">
																<input type="hidden" name="logoDoc" value="0">
																<input type="hidden" name="Preciosend" value="0">
																<input type="hidden" name="sizeDoc" value="0">
                            </div>

                            <div class="container col-md-12" style="margin: 0px; padding: 0px; padding-bottom: 10px;">

                                <?php
                                	$dat=1;
                                  $ingreso = new ControladorClientes();
                                  $ingreso -> ctrIngresoClienteCarrito($dat);
                                ?>

                                <a href="#modalRecuperarContraseña" data-dismiss="modal" data-toggle="modal">
                                    <p class="btn btn-default">¿Olvidaste tu contraseña?</p>
                                </a>

                                <input type="submit" class="btn btn-primary pull-right btnIngreso2" value="Ingresar">

                            </div>

                       </form>

                    </div>

                    <div class="modal-footer" style="margin: 0px; padding-bottom: 0px; padding-right: 0px;">

                        <div class="col-md-12">

                            <p style="margin: 0px;">¿No tienes una cuenta registrada? | <a href="#modalRegistro" data-dismiss="modal" data-toggle="modal"><strong>Regístrate</strong></a></p>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
