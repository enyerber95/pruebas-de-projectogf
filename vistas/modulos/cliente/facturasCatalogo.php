

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
 
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px; margin-top: 0px;">


			<!--=====================================
			   CONTENIDO (FACTURAS)
			======================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1; padding: 0px;">

					<div class="col-md-12" style="padding-right: 0px; padding-bottom: 10px;">

						<h1 style="margin-bottom: 0px;"><strong>Catálogo de Facturas</strong></h1>

					</div>




				</div>

			</div>

			<!--=====================================
			BARRA LATERAL (MENÚ DE TIPOS Y TAMAÑOS)
			======================================-->

			<div class="col-md-3" style="padding-top: 35px;">

				<div class="panel-group" id="accordion">

					<div class="panel panel-default">

						<div class="panel-heading">

							<h4 class="panel-title">

								<a class="producto_categoria" categoria="todo" data-toggle="collapse" data-parent="#accordion" href="#collapse0">

								<strong>Todos</strong></a>

							</h4>

						</div>

					</div>

					<div class="panel panel-default">

						<div class="panel-heading">

							<h4 class="panel-title">

								<a class="producto_categoria" categoria="carta" data-toggle="collapse" data-parent="#accordion" href="#collapse1">

								<strong>Tamaño Carta</strong></a>

							</h4>

						</div>

						<div id="collapse1" class="panel-collapse collapse in">

							<div class="panel-body tamaños">

									<a class="producto_tipo" tipo="rayada">Rayada</a><br><br>
									<a class="producto_tipo" tipo="cuadrada">Cuadrada</a><br><br>
									<a class="producto_tipo" tipo="blanco">En blanco</a>

							</div>

						</div>

					</div>


					<div class="panel panel-default">

						<div class="panel-heading">

							<h4 class="panel-title">

								<a class="producto_categoria" categoria="mediaCarta" data-toggle="collapse" data-parent="#accordion" href="#collapse2">

								<strong>Tamaño Media Carta</strong></a>

							</h4>

						</div>

						<div id="collapse2" class="panel-collapse collapse">

							<div class="panel-body tamaños">

								<a class="producto_tipo" tipo="rayada">Rayada</a><br><br>
								<a class="producto_tipo" tipo="cuadrada">Cuadrada</a><br><br>
								<a class="producto_tipo" tipo="blanco">En blanco</a>

							</div>

						</div>


					</div>

				</div>

			</div>


			<div class="container col-md-9 pull-right" style="padding-top: 35px; margin-top: 0px;">

				<div class="container col-md-12" style="padding:0px; margin-top: 0px;">

					<div class="panel panel-primary">

						<div class="panel-body" style="padding-top: 30px;">

							<div class="col-md-12" style="justify-content: center;">
								<?php

									$valor = 1;

									$facturas = ControladorProductos::ctrMostrarTodosProductos($valor);


									foreach ($facturas as $key => $value){

										echo '<div class="col-md-4 producto" medida="'.$value["medida"].'" tipo="'.$value["tipo"].'" style="padding: 10px; padding-bottom: 0px;">

												<div class="panel panel-primary">

													<div class="panel-body">

														<div class="panel-body" style="text-align: center">

															<img src="'.$value["imagen"].'" class="img-thumbnail">

															<p style="padding-top: 10px;"><strong>'.$value["nombre"].'</strong></p>

															<a href="editarFactura&'.$value["id_producto"].'"><button class="btn btn-primary">Comprar <i class="fa fa-edit"></i></button></a>

														</div>

													</div>

												</div>

											</div>';

									}
									
								?>


							</div>

						</div>

					</div>

				</div>

			</div>




		</div>

	</div>
