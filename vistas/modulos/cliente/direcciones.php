
<?php

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script>


							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}

?>



	<!--=====================================
	FORMULARIOS
	=======================================-->

	<div class="container-fluid">

		<div class="container">

			<h1 style="margin-top: 0px;"><strong>Mis direcciones</strong></h1><hr>

			<!-- BOTÓN (AGREGAR DIRECCIÓN) -->

			<div class="container col-md-12" style="text-align: left; margin: 0px; margin-bottom: 0px;">

				<a href="#" data-toggle="modal" data-target="#modalAgregarDireccion"><button class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Agregar</button></a>

			</div>


			<div class="container col-md-12" style="padding: 0px;">

				<?php

					$item = "id_cliente";
					$valor = $_SESSION["clienteId"];

					$direccion = ControladorDirecciones::ctrMostrarDirecciones($item, $valor);

					if($direccion != null){

						foreach ($direccion as $key => $value){


					echo '<div class="col-md-12" style="padding: 0px;">

					<div class="col-md-6" style="padding-top: 0px;">

						<div class="panel panel-primary">

							<div class="panel-heading" style="text-align: center"><strong>'.$value["d_titulo"].'</strong></div>

							<input type="hidden" name="idDireccion" id="idDireccion" value="'.$value["id_direcciones"].'" required>

							<div class="panel-body" style="padding=20px;">



								<!-- Nombre de la empresa -->

								<div class="col-md-6">

									<p align="right"><strong>Nombre de la empresa:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["d_nombre_empresa"].'</p>

								</div><br>

								<!-- Sector/Urbanización -->

								<div class="col-md-6">

									<p align="right"><strong>Sector/Urbanización:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["sector_urbanizacion"].'</p>

								</div><br>

								<!-- Avenida/Calle -->

								<div class="col-md-6">

									<p align="right"><strong>Avenida/Calle:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["avenida_calle"].'</p>

								</div><br>

								<!-- Edificio/Quinta/Casa -->

								<div class="col-md-6">

									<p align="right"><strong>Edificio/Quinta/Casa:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["edificio_quinta_casa"].'</p>

								</div><br>

								<!-- Estado -->

								<div class="col-md-6">

									<p align="right"><strong>Estado:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["estado"].'</p>

								</div><br>

								<!-- Ciudad -->

								<div class="col-md-6">

									<p align="right"><strong>Ciudad:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["ciudad"].'</p>

								</div><br>

								<!-- Teléfono -->

								<div class="col-md-6">

									<p align="right"><strong>Teléfono:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left">'.$value["d_telefono"].'</p>

								</div><br>

								<!-- Información adicional -->

								<div class="col-md-6">

									<p align="right" style="margin-bottom: 0px"><strong>Información adicional:</strong></p>

								</div>

								<div class="col-md-6">

									<p align="left" style="margin-bottom: 0px">'.$value["informacion_adicional"].'</p>

								</div><br>

							</div>

							<div class="panel-footer" style="text-align: right;">

								<button class="btn btn-primary btnEditarDireccion" idDireccion="'.$value["id_direcciones"].'" data-toggle="modal" data-target="#modalActualizarDireccion"><i class="fa fa-edit"></i> Editar</button>

								<button class="btn btn-danger btnEliminarDireccion" idDireccion2="'.$value["id_direcciones"].'">Eliminar</button>

							</div>

						</div>

					</div>

					</div>';


					}


					}else{

						echo '<h3>Aún no tiene direcciones añadidas.</h3>';

					}


				 ?>

			</div>

		</div>



	</div><br><br>



<!--==============================
 MODAL - AGREGAR DIRECCIÓN
===============================-->

<div id="modalAgregarDireccion" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <!-- CABECERA DEL MODAL -->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Agregar dirección</h1>

            </div>

            <form role="form" method="post" enctype="multipart/form-data">

            	<?php

					echo '<input type="hidden" name="idCliente" id="idCliente" value="'.$_SESSION["clienteId"].'">';
					echo '<input type="hidden" name="clienteCorreo" id="clienteCorreo" value="'.$_SESSION["clienteCorreo"].'">';

				?>


	            <div class="box-body col-md-12" style="padding: 50px; padding-bottom:0px; padding-top:15px;">

	            	<div class="input-group col-md-12">

		            		<label>Título de dirección:</label>

		            		<input type="text" class="form-control" id="nuevoTitulo" name="nuevoTitulo" onkeypress=" return soloLetras(event)" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>



		            <div class="box-body col-md-6" style="padding-left: 0px; padding-right: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Teléfono:</label><br>

	            			<input type="text" class="form-control" data-inputmask="'mask':'(9999) 999-9999'" data-mask id="nuevoTelf" onkeypress=" return soloNumeros(event)"  name="nuevoTelf" required>

	            		</div><br>
						<div class="input-group col-md-12">

		            		<label>Empresa/Razón social:</label>

		            		<input type="text" class="form-control" id="nuevoRazonSoc" name="nuevoRazonSoc" value="" maxlength="50" minlength="4" placeholder="Ej. Grafoformas HB" required>

		            	</div><br>


		            </div>

	            	<div class="input-group col-md-12">

	            		<label>Sector / Urbanización:</label>

	            		<input type="text" class="form-control" id="nuevoSU" name="nuevoSU"  maxlength="50" minlength="4" placeholder="Ej. Los Horcones" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Avenida / Calle:</label>

	            		<input type="text" class="form-control" id="nuevoAC" name="nuevoAC"  maxlength="50" minlength="4" placeholder="Ej. Av. Libertador con calle 33" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Edificio / Quinta / Casa:</label>

	            		<input type="text" class="form-control" id="nuevoEQC" name="nuevoEQC"  maxlength="50" minlength="4" placeholder="Ej. Casa #23-65 " required>

	            	</div><br>

	            	<div class="box-body col-md-6" style="padding: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Estado:</label>
	            			<select class="form-control col-md-12" id="nuevoEstado" name="nuevoEstado" required>
	            			<option value="" id="nEstado">Selecionar estado</option>
	            					<option value="Amazonas">Amazonas</option>
	            					<option value="Anzoátegui">Anzoátegui</option>
	            					<option value="Apure">Apure</option>
	            					<option value="Aragua">Aragua</option>
	            					<option value="Barinas">Barinas</option>
	            					<option value="Bolívar">Bolívar</option>
	            					<option value="Carabobo">Carabobo</option>
	            					<option value="Cojedes">Cojedes</option>
	            					<option value="Delta Amacuro">Delta Amacuro</option>
	            					<option value="Falcón">Falcón</option>
	            					<option value="Guárico">Guárico</option>
	            					<option value="Lara">Lara</option>
	            					<option value="Mérida">Mérida</option>
	            					<option value="Miranda">Miranda</option>
	            					<option value="Monagas">Monagas</option>
	            					<option value="Nueva Esparta">Nueva Esparta</option>
	            					<option value="Portuguesa">Portuguesa</option>
	            					<option value="Sucre">Sucre</option>
	            					<option value="Táchira">Táchira</option>
	            					<option value="Trujillo">Trujillo</option>
	            					<option value="Vargas">Vargas</option>
	            					<option value="Yaracuy">Yaracuy</option>
	            					<option value="Zulia">Zulia</option>

	            			</select>


	            		</div><br>

		            </div>

					<div class="box-body col-md-6" style="padding: 0px; padding-left: 20px; ">

	            		<div class="input-group col-md-12">

	            			<label>Ciudad:</label>

	            			<input type="text" class="form-control" id="nuevoCiudad" name="nuevoCiudad"  onkeypress=" return soloLetras(event)" maxlength="50" minlength="4" placeholder="Ej. Maracay"  required>

	            		</div><br>

	            	</div>

	            	<div class="input-group col-md-12">

	            		<label>Información adicional:</label>

	            		<textarea class="form-control" id="nuevoIA" name="nuevoIA" value=""  maxlength="150" minlength="4" placeholder="Ej. Esta cerca de un parque/cancha" required></textarea>

	            	</div>

	            </div>

	            <div class="modal-footer">

	                <div class="container col-md-12" style="margin: 0px;"><hr>

	                    <button type="submit" class="btn btn-primary">Agregar</button>

	                </div>

	                <?php
						$AgregarDireccion = new ControladorDirecciones();
						$AgregarDireccion -> ctrAgregarDireccion('direcciones');

					?>

	            </div>

            </form>

        </div>

    </div>

</div>


<!--==============================
 MODAL - ACTUALIZAR DIRECCIÓN
===============================-->

<div id="modalActualizarDireccion" class="modal fade" role="dialog">

    <div class="modal-dialog">

        <div class="modal-content">

            <!-- CABECERA DEL MODAL -->

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Editar dirección</h1>

            </div>

            <form role="form" method="post" enctype="multipart/form-data">

            	<?php

					echo '<input type="hidden" name="idCliente" value="'.$_SESSION["clienteId"].'">';

				?>

				<input type="hidden" name="direccionID" id="direccionID">

	            <div class="box-body col-md-12" style="padding: 50px; padding-bottom:0px; padding-top:15px;">

	            	<div class="input-group col-md-12">

		            		<label>Título de dirección:</label>

		            		<input type="text" class="form-control" id="editarTitulo" name="editarTitulo" value="" onkeypress=" return soloLetras(event)" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>



		            <div class="box-body col-md-6" style="padding-left: 0px; padding-right: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Teléfono:</label><br>

	            		<input type="text" class=class="form-control" data-inputmask="'mask':'(9999) 999-9999'" data-mask id="editarTelf" name="editarTelf" required>

	            		</div><br>

		            </div>
		            <div class="input-group col-md-12">

		            		<label>Empresa/Razón social:</label>

		            		<input type="text" class="form-control" id="editarRazonSoc" name="editarRazonSoc" value="" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

		            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Sector / Urbanización:</label>

	            		<input type="text" class="form-control" id="editarSU" name="editarSU" value=""  maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Avenida / Calle:</label>

	            		<input type="text" class="form-control" id="editarAC" name="editarAC" value="" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>

	            	<div class="input-group col-md-12">

	            		<label>Edificio / Quinta / Casa:</label>

	            		<input type="text" class="form-control" id="editarEQC" name="editarEQC" value="" maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            	</div><br>

	            	<div class="box-body col-md-6" style="padding: 0px;">

		            	<div class="input-group col-md-12">

	            			<label>Estado:</label>

	            			<select class="form-control col-md-12" name="editarEstado" required>

	            					<option value="" id="editarEstado">Selecionar estado</option>
	            					<option value="Amazonas">Amazonas</option>
	            					<option value="Anzoátegui">Anzoátegui</option>
	            					<option value="Apure">Apure</option>
	            					<option value="Aragua">Aragua</option>
	            					<option value="Barinas">Barinas</option>
	            					<option value="Bolívar">Bolívar</option>
	            					<option value="Carabobo">Carabobo</option>
	            					<option value="Cojedes">Cojedes</option>
	            					<option value="Delta Amacuro">Delta Amacuro</option>
	            					<option value="Falcón">Falcón</option>
	            					<option value="Guárico">Guárico</option>
	            					<option value="Lara">Lara</option>
	            					<option value="Mérida">Mérida</option>
	            					<option value="Miranda">Miranda</option>
	            					<option value="Monagas">Monagas</option>
	            					<option value="Nueva Esparta">Nueva Esparta</option>
	            					<option value="Portuguesa">Portuguesa</option>
	            					<option value="Sucre">Sucre</option>
	            					<option value="Táchira">Táchira</option>
	            					<option value="Trujillo">Trujillo</option>
	            					<option value="Vargas">Vargas</option>
	            					<option value="Yaracuy">Yaracuy</option>
	            					<option value="Zulia">Zulia</option>

	            			</select>

	            		</div><br>

		            </div>

					<div class="box-body col-md-6" style="padding: 0px; padding-left: 20px; ">

	            		<div class="input-group col-md-12">

	            			<label>Ciudad:</label>

	            			<input type="text" clicass="form-control" id="editarCiudad" name="editarCiudad" value="" onkeypress=" return soloLetras(event)"  maxlength="50" minlength="4" placeholder="Ej. Mi direccion personal" required>

	            		</div><br>

	            	</div>

	            	<div class="input-group col-md-12">

	            		<label>Información adicional:</label>

	            		<textarea class="form-control" id="editarIA" name="editarIA" value="" maxlength="150" minlength="4" placeholder="Ej. Mi direccion personal" required></textarea>

	            	</div>

	            </div>

	            <div class="modal-footer">

	                <div class="container col-md-12" style="margin: 0px;"><hr>

	                    <button type="submit" class="btn btn-primary">Guardar cambios</button>

	                </div>

	                <?php

	                $editar = new ControladorDirecciones();
	                $editar -> ctrActualizarDireccion();

	                ?>

	            </div>

            </form>

        </div>

    </div>

</div>



<?php

	$editar = new ControladorDirecciones();
	$editar -> ctrEliminarDireccion();

?>
