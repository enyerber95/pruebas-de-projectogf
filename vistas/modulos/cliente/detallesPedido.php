
<?php

	$url = Ruta::ctrRuta();

   	if (!isset($_SESSION["validarSesion"])){
		echo '<script>


							swal({
								title: "¡Parece que no has iniciado sesion!<br> Porfavor ingresa",
								text: "Gracias por elegirnos<br>",
								type:"error",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio#modalIniciarSesion";

									}
								})

							</script>';

   		exit();
   	}


		$id = $_GET["id"];
		$met='';
     ?>
	<!--=====================================
	PARTE PRINCIPAL (RESUMEN DE COMPRAS)
	=======================================-->

	<div class="container-fluid eltodo">

		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

			<!--===========================================================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Detalles - Orden <?php  foreach (ControladorPedidos::ctrMostrarDetalleEnvio($id) as $key => $value){ echo $value["orden"];
							$met=$value["metodo_envio"];
							}?> </strong></h1>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     LISTA DE PEDIDOS HECHOS           =
				   		======================================-->

				   		<div class="col-md-12" style="padding: 0px;">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 15px; padding-bottom: 30px;">

				   				<div style="text-align: justify; padding-bottom: 15px;">

				   					<a href="historialPedidos"><button class="btn btn-primary"><i class="fa fa-chevron-left"></i> Regresar</button></a>

				   				</div>

					            <div class="panel panel-primary">

				   					<div class="panel-body">

				   						<!-- TABLA DE DETALLE ORDEN -->

				   						<div class="box-body">

				   							<table class="table table-bordered table-striped dt-responsive tablas">

				   								<thead>

				   									<tr>

				   										<th>Producto</th>
				   										<th>Descripción</th>
				   										<th>Total</th>
				   										<th>Comentarios</th>
				   										<th>Estado</th>
				   										<th>Opciones</th>

				   									</tr>

				   								</thead>
				   								<tbody>
													<tr>

				   										<td>
					   											<img src= <?='"'.ControladorProductos::ctrMostrarDetalleProductos($value["id_producto"],$detalle="imagen").'"'?>class="img-thumbnail" width="100px">
					   										</td>

				   										<td><?=ControladorProductos::ctrMostrarDetalleProductos($value["id_producto"],$detalle="nombre");?></td>
				   										<td > <?php 
				                                                $iva=ControladorProductos::ctrMontoInva($value["monto"]);
				                                                 $totalBsF=$value["monto"]+$iva;
				                                                 $totalBsS=$totalBsF/100000;?> </td>

				   										<td>
																<?php echo '<div style="text-align: justify;">'.$totalBsF.' BsF <br>'; echo ''.$totalBsS.' BsS</div>';?> </td>

				   										<td><?= $value["orden"]; ?></td>

				   										<td>
					   										<?php if ($value["estado"] =='Pendiente'){ echo '

					   												<button class="btn btn-warning">'.$value["estado"].'</button>'?>

																	<?php }elseif ($value["estado"] =='Rechazado'){ echo '

					   												<button class="btn btn-danger">Rechazado</button>'?>

					   											<?php } elseif ($value["estado"] =='Finalizado'){ echo '

					   												<button class="btn btn-success">Enviado</button>'?>
					   											<?php 	} elseif ($value["estado"] =='Comprobar'){ echo '

					   												<button class="btn">Comprobando</button>'?>

					   											<?php }?>


					   									</td>


				   										<td>
				   										<form method="post">
				   											<?php
																echo '<input type="hidden" name="idimpresion" value='.$value["id_pedido"].'>';
																echo '<input type="hidden" name="iDireccion1" id="iDireccion1">';
				   											 echo '<input type="hidden" name="iDireccion2" id="iDireccion2" >';
				   											echo '<button value="'.$value["id_pedido"].'" class="btn btn-primary">Solicitar reimpresión <i class="fa fa-rotate-right"></i></button>';

				   												$reimpresion = new ControladorPedidos();
				   												$reimpresion->ctrReimpresion();

				   											?>
				   											</form>

				   										</td>

				   									</tr>
				   								</tbody>

				   							</table><br><br>

											<div class="col-md-6" style="text-align: justify;">

												<div class="panel panel-primary">

													<div class="panel-heading" style="text-align: center;">

														<strong>Su dirección de envío</strong>

													</div>

													<div class="panel-body">



														<p><strong>Nombre y apellido:</strong><?php echo  $_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"];?></p>

														<p><strong>Cédula o RIF:</strong> <?php echo  $_SESSION["clienteCedula"]?></p>

														<?php $id_direccion=ControladorPedidos::ctrMostrarDirecPedido($id,$detalle="id_direccion_dp",$detalle2="true");
														$direccion=$id_direccion["id_direccion_dp"];
														echo '<input type="hidden"  id="direccion1" value="'.$direccion.'">';

													  		foreach (ControladorDirecciones::ctrMostrarDirecciones($item="id_direcciones",$direccion) as $key => $value) {?>

																<p><strong>Nombre de la empresa:</strong>
																	<?= $value["d_nombre_empresa"];?>
																</p>

																<p><strong>Sector / Urbanización:</strong>
																	<?= $value["sector_urbanizacion"];?>
																</p>

																<p><strong>Avenida / Calle:</strong>
																	<?= $value["avenida_calle"];?>
																</p>
																<p><strong>Edificio / Quinta / Casa:</strong>
																	<?= $value["edificio_quinta_casa"];?>
																</p>
																<p><strong>Teléfono:</strong>
																	<?= $value["d_telefono"];?>
																</p>
																<p><strong>Estado:</strong>
																	<?= $value["estado"];?>
																</p>

																<p><strong>Ciudad:</strong>
																	<?= $value["ciudad"];?>
																</p>

																<p><strong>Información adicional:</strong>
																	<?= $value["informacion_adicional"];?>
																</p>
															<?php }?>

													</div>

					   							</div>

					   						</div>

											<div class="col-md-6" style="text-align: justify;">

												<div class="panel panel-primary">

													<div class="panel-heading" style="text-align: center;">

														<strong>Su dirección de facturación</strong>

													</div>

													<div class="panel-body">

														<p>
															<strong>Nombre y apellido:</strong>
																<?php echo  $_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"];?>
														</p>

														<p>
															<strong>Cédula o RIF:</strong>
															<?php echo  $_SESSION["clienteCedula"];?>
														</p>
														<?php $id_direccion=ControladorPedidos::ctrMostrarDirecPedido($id,$detalle="id_direccion_dp",$detalle2="2");
														$direccion=$id_direccion["id_direccion_dp"];
														echo '<input type="hidden"  id="direccion2" value="'.$direccion.'">';

													  		foreach (ControladorDirecciones::ctrMostrarDirecciones($item="id_direcciones",$direccion) as $key => $value) {?>

																<p><strong>Nombre de la empresa:</strong>
																	<?= $value["d_nombre_empresa"];?>
																</p>

																<p><strong>Sector / Urbanización:</strong>
																	<?= $value["sector_urbanizacion"];?>
																</p>

																<p><strong>Avenida / Calle:</strong>
																	<?= $value["avenida_calle"];?>
																</p>
																<p><strong>Edificio / Quinta / Casa:</strong>
																	<?= $value["edificio_quinta_casa"];?>
																</p>
																<p><strong>Teléfono:</strong>
																	<?= $value["d_telefono"];?>
																</p>
																<p><strong>Estado:</strong>
																	<?= $value["estado"];?>
																</p>

																<p><strong>Ciudad:</strong>
																	<?= $value["ciudad"];?>
																</p>

																<p><strong>Información adicional:</strong>
																	<?= $value["informacion_adicional"];?>
																</p>
															<?php }?>

													</div>

												</div>

											</div>


											<div class="col-md-12" style="text-align: justify;">

												<div class="panel panel-primary">

													<div class="panel-heading" style="text-align: center;">

														<strong>Método de envío</strong>

													</div>

														<div class="panel-body">

					      									<div class="col-md-12">

																<p style="margin-bottom: 0px;"><strong><?php echo ''.$met.''?></strong><br> Envío directo únicamente a los siguientes estados: Aragua, Carabobo, Distrito Capital, Miranda, Guárico y Vargas.</p>

															</div>

														</div>

												</div>

											</div>

				   						</div>

				   					</div>

				   				</div>

				   				</div>

				   			</div>

				   		</div>

			   		</div>

				</div>

			</div>

		</div>

	</div>
