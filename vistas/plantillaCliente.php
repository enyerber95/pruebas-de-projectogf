<?php

session_start();

?>

<!DOCTYPE html>
<html lang="es">
<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<meta name="title" content="GrafoFormas HB">

	<meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam accusantium enim esse eos officiis sit officia">

	<meta name="keyword" content="Lorem ipsum, dolor sit amet, consectetur, adipisicing, elit, Quisquam, accusantium, enim, esse">

	<title>Tienda GF</title>

	<!--=====================================
	=            PLUGINGS CSS               =
	======================================-->

	 <!-- Bootstrap 3.3.7 -->
	 <link rel="stylesheet" href="vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">

	 <!-- Font Awesome -->
	 <link rel="stylesheet" href="vistas/bower_components/font-awesome/css/font-awesome.min.css">

	 <!-- Ionicons -->
	 <link rel="stylesheet" href="vistas/bower_components/Ionicons/css/ionicons.min.css">

	 <!-- Theme style -->
	 <link rel="stylesheet" href="vistas/dist/css/AdminLTE.css">

	 <!-- AdminLTE Skins. Choose a skin from the css/skins
	      folder instead of downloading all of them to reduce the load. -->
	 <link rel="stylesheet" href="vistas/dist/css/skins/_all-skins.min.css">
	 <!-- Google Font -->
	 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	 <!-- DataTables -->
	 <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	 <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

	 <!-- iCheck for checkboxes and radio inputs -->
	 <link rel="stylesheet" href="vistas/plugins/iCheck/all.css">

	  <!-- Date picker -->
	 <link rel="stylesheet" href="vistas\bower_components\bootstrap-datepicker 1.6.4\css/bootstrap-datepicker.min.css">

	 <!-- Morris chart -->
	 <link rel="stylesheet" href="vistas/bower_components/morris.js/morris.css">

	 	<!-- Barra paso a paso -->
	 <link rel="stylesheet" href="vistas/css/barraPasos.css">


	<!--=====================================
	=        PLUGINGS JAVASCRIPT            =
	======================================-->

	<!-- jQuery 3 -->
	<script src="vistas/bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap 3.3.7 -->
	<script src="vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- SlimScroll -->
	<script src="vistas/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

	<!-- FastClick -->
	<script src="vistas/bower_components/fastclick/lib/fastclick.js"></script>

	<!-- AdminLTE App -->
	<script src="vistas/dist/js/adminlte.min.js"></script>

	<!-- La funcion que hace que me fucionen los calendarios -->
	<script src="vistas/dist/js/demo.js"></script>

	<!-- DataTables -->
	<script src="vistas/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="vistas/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="vistas/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>

	<!-- InputMask -->
	<script src="vistas/plugins/input-mask/jquery.inputmask.js"></script>
	<script src="vistas/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="vistas/plugins/input-mask/jquery.inputmask.extensions.js"></script>

	<!-- date picker -->
	<script src="vistas\bower_components\bootstrap-datepicker 1.6.4\js/bootstrap-datepicker.js"></script>

	<!-- Morris.js charts -->
	<script src="vistas/bower_components/raphael/raphael.min.js"></script>
	<script src="vistas/bower_components/morris.js/morris.min.js"></script>

	<!-- ChartJS -->
	<script src="vistas/bower_components/Chart.js/Chart.js"></script>

	<!-- SweetAlert 2 -->
	<script src="vistas/plugins/sweetalert2/sweetalert2.all.js"></script>

	<!-- Barra paso a paso -->
	<script src="vistas/js/barraPasos.js"></script>

	<!-- Activar y desactivar div's con los checkbox -->
	<script src="vistas/js/ocultarMostrarDivs.js"></script>

</script>

	<!--=====================================
	=        LO QUE YA TENIA                =
	======================================-->

	<link rel="icon" href="vistas\img\grafoformas/logo-mini.png">

	<link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Ubuntu|Ubuntu+Condensed" rel="stylesheet">

	<link rel="stylesheet" href="vistas/css/plantilla.css">

	<link rel="stylesheet" href="vistas/css/cabezote.css">

	<link rel="stylesheet" href="vistas/css/stylesheet.css">
	<style type="text/css">
		.tooltip{

        display:none;
        width:130px;
        height: 120px;
        color: #FFFFFF;
        background: #000000;
        line-height: 20px;
        text-align: left;
        border-radius: 6px;
        font-size:12px;
        margin-top: 10px;
        margin-left: 0px;
        border-top: 8px solid #000000;
        opacity: 1;
		}
		.preview{
			justify-content: center;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -khtml-user-select: none;
		    -ms-user-select:none;
		    background: #fff ;
		    border: 1px solid #808080;
		    border-radius: 4px;
		}
  </style>
</head>

<body>
<?php

//if(isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok"){


    echo '<div class="wrapper">';

    /*=============================================
    =                    CABEZOTE                 =
    =============================================*/

    include "modulos/cliente/header.php";

    /*=============================================
    =                     MENU LATERAL            =
    =============================================*/

    //include "modulos/cpanel/menu.php";

    /*=============================================
    =                      CONTENIDO              =
    =============================================*/

    if(isset($_GET["ruta"])){

        if($_GET["ruta"] == "inicio" ||
        	$_GET["ruta"] == "logout" ||
        	$_GET["ruta"] == "quienesSomos" ||
            $_GET["ruta"] == "cotizador" ||
            $_GET["ruta"] == "direcciones" ||
            $_GET["ruta"] == "editarFactura" ||
            $_GET["ruta"] == "editarPerfil" ||
            $_GET["ruta"] == "enviarRifFormulario" ||
            $_GET["ruta"] == "historialPedidos" ||
            $_GET["ruta"] == "detallesPedido" ||
            $_GET["ruta"] == "facturas" ||
            $_GET["ruta"] == "formasLibres" ||
            $_GET["ruta"] == "facturasCatalogo" ||
            $_GET["ruta"] == "formasLibresCatalogo" ||
            $_GET["ruta"] == "reportarPago" ||
            $_GET["ruta"] == "reportarPagoFormulario" ||
            $_GET["ruta"] == "resumenCompras" ||
            $_GET["ruta"] == "direccionesCompras" ||
            $_GET["ruta"] == "metodoEnvioCompras" ||
            $_GET["ruta"] == "reportarPagoCompras" ||
            $_GET["ruta"] == "rollos" ||
            $_GET["ruta"] == "restablecerPass" ||
            $_GET["ruta"] == "vista" ||
            $_GET["ruta"] == "rollosFormulario"){

            include "modulos/cliente/".$_GET["ruta"].".php";

        }else if ($_GET["ruta"] == "cpanel"){

            echo '<script>

								window.location = "cpanel/inicio";

							</script>';

        }else{

            include "modulos/cliente/404Clientes.php";

        }
    }else{

        include "modulos/cliente/inicio.php";

    }

    /*=============================================
    =                    FOOTER                   =
    =============================================*/

    include "modulos/cliente/footer.php";


    echo '</div>';

    /*Fin div wrappper*/

//}else{

  //  include "modulos/cpanel/login.php";

//}

?>

<script src="vistas/js/plantilla.js"></script>
<script src="vistas/js/clientes.js"></script>
<script src="vistas/js/productos.js"></script>
<script src="vistas/js/direcciones.js"></script>
<script src="vistas/js/cotizador.js"></script>
<script src="vistas/js/functions.js"></script>
<script src="vistas/js/app.js"></script>
<script src="vistas/js/pedidos.js"></script>
	<?php
		if (isset($ararydata[0])) {
	?>
		<script type="text/javascript">
				$(document).ready(function(){
					//rif
						$("input[name=rifDoc]").val($("#rifDoc").val());
						$("#rif").val($("#rifDoc").val());
						$("td[name=rifDoc]").html($("#rifDoc").val());
						$("label[name=rifDoc").html($("#rifDoc").val());
					//nomEmp
						$("input[name=nomEmpDoc]").val($("#nomEmpDoc").val());
						$("#nomEmp").val($("#nomEmpDoc").val());
						$("td[name=nomEmpDoc]").html($("#nomEmpDoc").val());
						$("label[name=nomEmpDoc").html($("#nomEmpDoc").val());
					//dire
						$("input[name=direDoc]").val($("#direDoc").val());
						$("#dire").val($("#direDoc").val());
						$("td[name=direDoc]").html($("#direDoc").val());
						$("label[name=direDoc").html($("#direDoc").val());
					//dire2
						$("input[name=dire2Doc]").val($("#dire2Doc").val());
						$("#dire2").val($("#dire2Doc").val());
						$("td[name=dire2Doc]").html($("#dire2Doc").val());
						$("label[name=dire2Doc").html($("#dire2Doc").val());
					//tlf
						$("input[name=tlfDoc]").val($("#tlfDoc").val());
						$("#tlf").val($("#tlfDoc").val());
						$("td[name=tlfDoc]").html($("#tlfDoc").val());
						$("label[name=tlfDoc").html($("#tlfDoc").val());
					//tipPre
						$("input[name=rifDoc]").val($("#rifDoc").val());
						$("#tipPre").val($("#tipPreDoc").val());
						$("td[name=rifDoc]").html($("#rifDoc").val());
						$("label[name=rifDoc").html($("#rifDoc").val());
					//canImp
						$("input[name=canImpDoc]").val($("#canImpDoc").val());
						$("#canImp").val($("#canImpDoc").val());
						$("td[name=canImpDoc]").html($("#canImpDoc").val());
						$("label[name=canImpDoc").html($("#canImpDoc").val());
					//facIni
						$("input[name=facIniDoc]").val($("#facIniDoc").val());
						$("#facIni").val($("#facIniDoc").val());
						$("td[name=facIniDoc]").html($("#facIniDoc").val());
						$("label[name=facIniDoc").html($("#facIniDoc").val());
					//conIni
						$("input[name=conIniDoc]").val($("#conIniDoc").val());
						$("#conIni").val($("#conIniDoc").val());
						$("td[name=conIniDoc]").html($("#conIniDoc").val());
						$("label[name=conIniDoc").html($("#conIniDoc").val());
					//facFinDoc
						$("input[name=facFinDoc]").val($("#canImpDoc").val()*50);
						$("label[name=facFinDoc").html($("#canImpDoc").val()*50);
					//conFinDoc
						$("input[name=conFinDoc]").val($("#canImpDoc").val()*50);
						$("label[name=conFinDoc").html($("#canImpDoc").val()*50);
					//tipPreDoc
						$("input[name=tipPreDoc]").val($("#tipPreDoc").val());
						if ($("#tipPreDoc").val()== 'Talonario') {
							$("#"+$("#tipPreDoc").val()).attr('checked', true);
						}else {
							$("#HojasSueltas").attr('checked', true);
						}
						$("td[name=tipPreDoc]").html($("#tipPreDoc").val());
						$("label[name=tipPreDoc").html($("#tipPreDoc").val());
					//check
						if ($("#logoDoc").val() != null) {
							$("#content").css('display','block');
							$("#sizeLogo").css('display','block');
						}
					//size
						$("input[name=sizeDoc]").val($("#sizeDoc").val());
						$("#size").val($("#sizeDoc").val());
						$("td[name=sizeDoc]").html($("#sizeDoc").val());
						$("label[name=sizeDoc").html($("#sizeDoc").val());
					//preImp
						$("input[name=preImpDoc]").val($("#preImpDoc").val());
						$("#preImp").val($("#preImpDoc").val());
						$("td[name=preImpDoc]").html($("#preImpDoc").val());
						$("label[name=preImpDoc").html($("#preImpDoc").val());
					//prePap
						$("input[name=prePapDoc]").val($("#prePapDoc").val());
						$("#prePap").val($("#prePapDoc").val());
						$("td[name=prePapDoc]").html($("#prePapDoc").val());
						$("label[name=prePapDoc").html($("#prePapDoc").val());
					//OpcCol
						$("input[name=OpcColDoc]").val($("#OpcColDoc").val());
						$("#OpcCol").val($("#OpcColDoc").val());
						$("td[name=OpcColDoc]").html($("#OpcColDoc").val());
						$("label[name=OpcColDoc").html($("#OpcColDoc").val());
					//precio
						$("input[name=Preciosend]").val($("#Preciosend").val());
						$("#precio").val($("#Preciosend").val());
						$("td[name=Preciosend]").html($("#Preciosend").val());
						$("label[name=Preciosend").html($("#Preciosend").val());
					//imgsend
						$("#imgDoc2").prepend('<img src="'+$("#imgsend").val()+'" id="imgDoc" width="600" height="600"/>');
					//logoDoc
						$("td[name=logoDoc]").prepend('<img name="img" src="'+$("#logoDoc").val()+'" width="'+$("#sizeDoc").val()+'" height="'+$("#sizeDoc").val()/1.2+'"/>');
						if ($("#sizeDoc").val() == 70) {
							$('td[name=nomEmpDoc]').css({'margin-left': '0px'});
							$('td[name=direDoc]').css({'margin-left': '0px'});
							$('td[name=tlfDoc]').css({'margin-left': '0px'});
							$('td[name=dire2Doc]').css({'margin-left': '0px'});
						}
						if ($("#sizeDoc").val() == 120) {
							$('td[name=nomEmpDoc]').css({'margin-left': '40px'});
							$('td[name=direDoc]').css({'margin-left': '40px'});
							$('td[name=tlfDoc]').css({'margin-left': '40px'});
							$('td[name=dire2Doc]').css({'margin-left': '40px'});
						}
						if ($("#sizeDoc").val() == 150) {
							$('td[name=nomEmpDoc]').css({'margin-left': '70px'});
							$('td[name=direDoc]').css({'margin-left': '70px'});
							$('td[name=tlfDoc]').css({'margin-left': '70px'});
							$('td[name=dire2Doc]').css({'margin-left': '70px'});
						}
					//rifDoc
					$("#img").attr('required','false');

				});
		</script>
	<?php
		}
 	?>
<script type="text/javascript">
  var size;
	var pBond = 0;
	var pQuimico = 0;
	var o1 = 0;
	var o2 = 0;
	var o3 = 0;
	var bN = 0;
	var fC = 0;
	var idProducto = 0;

	$(document).ready(function(){

		$("#iDireccion1").val($("#direccion1").val());
		$("#iDireccion2").val($("#direccion2").val());
		var id = 1;

		var datos = new FormData();
		datos.append("id", id);

		$.ajax({

			url:"ajax/productos.ajax.php",
			method:"POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "json",
			success: function(respuesta){

				pBond = parseFloat(respuesta["p_bond"]);
				pQuimico = parseFloat(respuesta["p_quimico"]);
				o1 = parseFloat(respuesta["o_1"]);
				o2 = parseFloat(respuesta["o_2"]);
				o3 = parseFloat(respuesta["o_3"]);
				bN = parseFloat(respuesta["blanco_negro"]);
				fC = parseFloat(respuesta["full_color"]);

			}

		});

	});
	function Doc(e) {

		var precio = parseInt($('#precio').val());
		var precioBase = parseInt($('#precio_base').val());
		$("td[name="+e.id+"Doc]").html(e.value);
		if (e.id =='nomEmp' || e.id =='rif' || e.id =='tipPre' || e.id =='canImp' || e.id =='facIni' || e.id =='conIni' || e.id =='preImp' || e.id =='prePap' || e.id =='OpcCol' ||  e.id =='size' ||  e.id =='dire' ||  e.id =='dire2' ||  e.id =='tlf' ) {
			$("label[name="+e.id+"Doc]").html(e.value);
			$("input[name="+e.id+"Doc]").val(e.value);
		}
		if (e.id =='tipPre' && e.value == "Talonario") {

		}
		//creo que no funciona
		if (e.id =='rif' ) {
			url =URL.createObjectURL(e.files[0]);
			$('#imgDoc').remove();
			$("input[name=imgsend]").val(url);
			$('#imgDoc2').prepend('<img id="imgDoc" src="'+url+'" width="600" height="600"/>');
		}
		if (e.id =='img' ) {
			if ( e.files[0].size < 2000000 || e.files[0].type == 'image/jpeg' || e.files[0].type == 'image/jpg' || e.files[0].type == 'image/gif' || e.files[0].type == 'image/png') {
				if (e.files[0].type != 'application/pdf') {
					var datos = new FormData((document.getElementById("envio")));
					//var image =this.files[0];
					datos.append("guardarRif",'url');
					$("input[name=imgsend]").val('');

					$.ajax({

						url:"ajax/pedidos.ajax.php",
						method: "POST",
						data: datos,
						cache: false,
						contentType: false,
						processData: false,
						success: function(respuesta){
							$("input[name=imgsend]").val(respuesta);
						},
						error: function (respuesta){
							swal({
								title: "¡ERROR!",
								text: "Error el archivo no es valido o su tamaño no es el adecuado.",
								type:"error",
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
							});
							$('#img').val('');
			      }
					});
				}
				else{
					swal({
						title: "¡ERROR!",
						text: "Error el archivo no es valido o su tamaño no es el adecuado.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
					});
					$('#img').val('');
				}
			}
			else{
				swal({
					title: "¡ERROR!",
					text: "Error el archivo no es valido o su tamaño no es el adecuado.",
					type:"error",
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
				});
				$('#img').val('');
			}
			url =URL.createObjectURL(e.files[0]);
			$('#imgDoc').remove();
			$('#imgDoc2').prepend('<img id="imgDoc" src="'+url+'" width="600" height="600"/>');
		}
		if ( e.id =='canImp' ) {

			$('label[name=conFinDoc]').html(e.value*50);
			$('label[name=facFinDoc]').html(e.value*50);
			$('#precio').val(precio*e.value);
			$('#precioBsS').val((precio*e.value)/100000);//Cambiar Precio base por cotizador*cantidad de hojas
			$('input[name=Preciosend]').val(precio*e.value);
		}

		//Cotizador Preferencias de impresion
		if ( e.id =='preImp' && e.value == 'Original y 1 copia' ) {
			$('#precio').val(precio+(precioBase*o1));
			$('#precioBsS').val((precio+(precioBase*o1))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*o1));
		}
		if ( e.id =='preImp' && e.value == 'Original y 2 copia' ) {

			$('#precio').val(precio+(precioBase*o2));
			$('#precioBsS').val((precio+(precioBase*o2))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*o2));
		}
		if ( e.id =='preImp' && e.value == 'Original y 3 copia' ) {
			$('#precio').val(precio+(precioBase*o3));
			$('#precioBsS').val((precio+(precioBase*o3))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*o3));
		}

		//Cotizador Preferencias de papel
		if ( e.id =='prePap' && e.value == 'Papel bond' ) {
			$('#precio').val(precio+(precioBase*pBond));
			$('#precioBsS').val((precio+(precioBase*pBond))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*pBond));
		}
		if ( e.id =='prePap' && e.value == 'Papel químico' ) {
			$('#precio').val(precio+(precioBase*pQuimico));
			$('#precioBsS').val((precio+(precioBase*pQuimico))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*pQuimico));
		}

		//Cotizador Preferencias de papel
		if ( e.id =='OpcCol' && e.value == 'Blanco y negro' ) {
			$('#precio').val(precio+(precioBase*bN));
			$('#precioBsS').val((precio+(precioBase*bN))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*bN));
		}
		if ( e.id =='OpcCol' && e.value == 'Full color' ) {
			$('#precio').val(precio+(precioBase*fC));
			$('#precioBsS').val((precio+(precioBase*fC))/100000);
			$('input[name=Preciosend]').val(precio+(precioBase*fC));
		}
	}
	function sizeImg(e) {
		size = e.value;
  		$('#sizeLogo').css('display', 'block');
		$("input[name=sizeDoc]").val(size);
		$('img[name=img]').height(size/1.2);
		$('img[name=img]').width(size);
		if (size == 70) {
			$('td[name=nomEmpDoc]').css({'margin-left': '0px'});
			$('td[name=direDoc]').css({'margin-left': '0px'});
			$('td[name=tlfDoc]').css({'margin-left': '0px'});
			$('td[name=dire2Doc]').css({'margin-left': '0px'});
		}
		if (size == 120) {
			$('td[name=nomEmpDoc]').css({'margin-left': '40px'});
			$('td[name=direDoc]').css({'margin-left': '40px'});
			$('td[name=tlfDoc]').css({'margin-left': '40px'});
			$('td[name=dire2Doc]').css({'margin-left': '40px'});
		}
		if (size == 150) {
			$('td[name=nomEmpDoc]').css({'margin-left': '70px'});
			$('td[name=direDoc]').css({'margin-left': '70px'});
			$('td[name=tlfDoc]').css({'margin-left': '70px'});
			$('td[name=dire2Doc]').css({'margin-left': '70px'});
		}
	}
  function imprimirDIV(contenido) {

    if ($('#rif').val() != '' && $('#nomEmp').val() != '' && $('#dire').val() != ''  && $('#tlf').val() != '' && $('#tipPre').val() != '' && $('#canImp').val() != '' && $('#facIni').val() != '' && $('#conIni').val() != '' && $('#preImp').val() != '' && $('#prePap').val() != '' && $('#OpcCol').val() != '' && $('#img').val() != '' && $('#acepto').val() != '' && $('#acepto').prop('checked') != false ) {
			$('#hidden').css('display', 'block');
			if ($('#medida').val() == 'mediaCarta') {
				$('label[name=tabla]').prepend('<br><br><br><br><br><br><br><br><br><br><br><br><br><br>');
			}
			if ($('#css').val() != '') {
				$('div.nota').css({'font-size': '15.0pt'});
			}
	    var ficha = document.getElementById(contenido);
	    var ventanaImpresion = window.open(' ', 'popUp');
	    ventanaImpresion.document.write(ficha.innerHTML);
	    ventanaImpresion.document.close();
	    ventanaImpresion.print();
	    ventanaImpresion.close();
	  	$('#hidden').css('display', 'none');

    } else {


					swal({
							type: "error",
							title: "Los campos marcados con * son requeridos",
							showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							})
    }
  }

</script>

</body>
</html>
