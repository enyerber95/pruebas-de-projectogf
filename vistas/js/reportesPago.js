
/*=============================================
=     MOSTRAR DIRECCION DE ENVIO              =
=============================================*/

function direcciones(e){
	var idReporte = $(e).attr("idReporte");


	var datos = new FormData();
	datos.append("idReporte", idReporte);

	$.ajax({

		url:"../ajax/reportes.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			$("#verOrden").html(respuesta["orden"]);

			$("#verNombre").html(respuesta["nombre"]+' '+respuesta["apellido"]);

			$("#verTelf").html(respuesta["telefono_movil"]);

			$("#verCorreo").html(respuesta["email"]);

			$("#verBancoEmisor").html(respuesta["banco_emisor"]);

			$("#verBancoReceptor").html(respuesta["banco_receptor"]);

			$("#verMonto").html("Bs. "+respuesta["monto_final"]);

			$("#verNumero").html(respuesta["num_deposito_transferencia"]);

			$("#verFecha").html(respuesta["fecha_reporte"]);


		}

	});

}


$('#daterange-btn-Reporte').daterangepicker(
	{
		ranges   : {
		  'Últimos 7 días' : [moment().subtract(6, 'days'), moment()],
		  'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
		  'Este mes'  : [moment().startOf('month'), moment().endOf('month')],
		  'Último mes'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		startDate: moment().subtract(29, 'days'),
		endDate  : moment()
	},
	function (start, end) {
		$('#daterange-btn-Reporte span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

		var fechaInicial = start.format('YYYY-M-D');

		var fechaFinal = end.format('YYYY-M-D');

		var datos = new FormData();
		datos.append("fechaInicial", fechaInicial);
		datos.append("fechaFinal", fechaFinal);

		$.ajax({

			url:"../ajax/reportes.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){

		  		$(".rangoFechas tbody").html("");
				$.each(respuesta, function(respuesta, index) {
		      		$(".rangoFechas tbody").append( 

			  		 	'<tr>'+

				            '<td>'+(index['id_reporte_pago'])+'</td>'+

				            '<td>'+index['orden']+'</td>'+

				            '<td>'+index['nombre']+' '+index['apellido']+'</td>'+

				            '<td> 	Nro. '+index['num_deposito_transferencia']+'</td>'+

				            '<td> Bs. '+index['monto_final']+'</td>'+

				            '<td>'+index['fecha_reporte']+'</td>'+
							
							'<td>'+'<button class="btn btn-default"><i class="fa fa-file-text"></i></button>'+

                            '<button class="btn btn-default btnVerDetallesPago" onclick="direcciones(this)" data-toggle="modal" data-toggle="modal" data-target="#verDetallesReporte" idReporte="'+index['id_pedido']+'"><i class="fa fa-info-circle"></i></button>'+'</td>'+
				          '</tr>'
		  			);
				});
		  	},
			error: function (respuesta){
				swal({
					type: "error",
					title: "error",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="reportes";

					}
				})
      		}
		})
	}
)