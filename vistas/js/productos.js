/*=============================================
=             EDITAR PREFERENCIAS             =
=============================================*/

$(document).ready(function(){

	var id = 1;

	var datos = new FormData();
	datos.append("id", id);

	$.ajax({

		url:"../ajax/productos.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			$("#editarPBond").val(100 * respuesta["p_bond"]);

			$("#editarPQuimico").val(100 * respuesta["p_quimico"]);

			//$("#editarPrecioVenta").val(100 * respuesta["precio_base"]);

			$("#editarCBN").val(100 * respuesta["blanco_negro"]);

			$("#editarCFC").val(100 * respuesta["full_color"]);

			$("#editarIO1").val(100 * respuesta["o_1"]);

			$("#editarIO2").val(100 * respuesta["o_2"]);

			$("#editarIO3").val(100 * respuesta["o_3"]);

			$("#iva").val(respuesta["iva"]);

		}

	});

})



/*=============================================
=             EDITAR PRECIO DE VENTA          =
=============================================*/

$(".btnEditarProducto").click(function(){

	var idProducto = $(this).attr("idProducto");

	var datos = new FormData();
	datos.append("idProducto", idProducto);

	$.ajax({

		url:"../ajax/productos.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			$("#idProducto").val(respuesta["id_producto"]);

			$("#editarNombre").val(respuesta["nombre"]);

			$("#editarPrecioVenta").val(respuesta["precio_base"]);

		}

	});

})



/*=============================================
=         FILTRAR PRODUCTOS                   =
=============================================*/

$(document).ready(function(){

	var catProducto ="";

	// FILTRANDO PRODUCTOS POR MEDIDA //

	$('.producto_categoria').click(function(){

		catProducto = $(this).attr('categoria');
		console.log(catProducto);

		// OCULTANDO PRODUCTOS //
		$('.producto').hide();

		// MOSTRANDO PRODUCTOS //
		$('.producto[medida="'+catProducto+'"]').show();


	});

	// FILTRANDO PRODUCTOS POR TIPO //

	$('.producto_tipo').click(function(){

		var tipoProducto = $(this).attr('tipo');
		console.log(tipoProducto);
		console.log(catProducto);

		if (catProducto == "carta"){

			// OCULTANDO PRODUCTOS //
			$('.producto').hide();

			// MOSTRANDO PRODUCTOS //
			$('.producto[tipo="'+tipoProducto+'"][medida="carta"]').show(); 

		}else{

			// OCULTANDO PRODUCTOS //
			$('.producto').hide();

			// MOSTRANDO PRODUCTOS //
			$('.producto[tipo="'+tipoProducto+'"][medida="mediaCarta"]').show(); 

		}

		

	});

	  // MOSTRANDO TODOS LOS PRODUCTOS //
	$('.producto_categoria[categoria="todo"]').click(function(){

		$('.producto').show();

	});

});

