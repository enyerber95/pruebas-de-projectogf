/*=============================================
CARPTURA DE RUTAS
=============================================*/

var rutaActual = location.href;

$(".btnIngreso").click(function(){

	localStorage.setItem("rutaActual", rutaActual);

})

$(".btnRegistro").click(function(){

	localStorage.setItem("rutaActual", rutaActual);

})


/*=============================================
FORMATEAR LAS ALERTAS DEL DIV "PADRE"
=============================================*/

$("input").focus(function(){

	$(".alert").remove();
})

/*=============================================
CLIENTES - CPANEL
=============================================*/

	function init(){

		mostrarform(false);

	}

	/*=============================================
	ACTIVAR CLIENTE
	=============================================*/
	function activarCliente(e){

		var idCliente = $(e).attr("idCliente");
		var estadoCliente = $(e).attr("estadoCliente");
		var usuario = $(e).attr("usuario");
		var nombreusuario = $(e).attr("nombreusuario");
		idCliente = idCliente.toString();

	  	
	  	swal({
				type: "warning",
				title: " ¿Esta seguro de cambiar el estado del cliente?<br> Esta acción sera registrada por seguridad",
		  		showCancelButton: true,
		        confirmButtonColor: "#018ba7",
		        cancelButtonColor: '#dd4b39',
		        cancelButtonText: 'Cancelar',
		        confirmButtonText: 'Sí, Cambiar estado'
		  		}).then((result) => {
				if (result.value) {

					
								

				  	if(estadoCliente == 0){

				  		$(e).removeClass('btn-success');
				  		$(e).addClass('btn-danger');
				  		$(e).html('Inactivo');
				  		$(e).attr('estadoCliente',1);
				  		accionBitacora="Cliente Desactivado";


				  	}else{

				  		$(e).removeClass('btn-danger');
				  		$(e).addClass('btn-success');
				  		$(e).html('Activo');
				  		$(e).attr('estadoCliente',0);
				  		accionBitacora="Cliente Activado";
				  	}
				  	var datos = new FormData();
				 	datos.append("idCliente", idCliente);
				  	datos.append("estadoCliente", estadoCliente);
				  	datos.append("cambiarAccion", accionBitacora);
				  	datos.append("usuario", usuario);
				  	datos.append("nombreusuario", nombreusuario);

					$.ajax({

					  url:"../ajax/clientes.ajax.php",
					  method: "POST",
					  data: datos,
					  cache: false,
				      contentType: false,
				      processData: false,
				      success: function(respuesta){

				      	console.log(respuesta);

				  	 }

				  	})
	  		}else{
	  			
	  		}
		})



	}

	/*=============================================
	CAMBIAR DE LA TABLA A LOS DETALLES DEL CLIENTE
	=============================================*/
	function mostrarform(flag){

		if (flag){

			$("#form1").hide();
			$("#form2").show();
			$("#VerDetallesCliente").prop("disabled",false);
			$("#VerDetallesCliente").hide();
		
		}else{

			$("#form1").show();
			$("#form2").hide();
			$("#VerDetallesCliente").show();

		}
	}

	function cancelarform(){

		mostrarform(false);
	}


	init();

/*==========================================================================================*/	


/*=============================================
CLIENTES - TIENDA
=============================================*/

	/*=============================================
	VALIDAR CORREO REPETIDO
	=============================================*/	

	var validarCorreoRepetido = false;

	$("#regCorreo").change(function(){

		var correo = $("#regCorreo").val();

		var datos = new FormData();
		datos.append("validarCorreo", correo);

		$.ajax({

			url:"ajax/clientes.ajax.php",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success: function(respuesta){

				if(respuesta == 1){
					$(".alert").remove();

					validarCorreoRepetido = false;

				}else{

					$("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> El correo electrónico ya existe en la base de datos, por favor ingrese otro diferente.</div>')

					validarCorreoRepetido = true;
					$("#regCorreo").val('');
				}

			}

		})


	})

	/*=============================================
	VALIDAR EL REGISTRO DE USUARIO PRINCIPAL
	=============================================*/
	function registroCliente(){

		/*=============================================
		VALIDAR EL CORREO
		=============================================*/

		var email = $("#regCorreo").val();

		if(email != ""){

			var expresion = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

			if(!expresion.test(email)){

				$("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> Escriba correctamente el correo electrónico.</div>')

				return false;

			}

		}else{

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio.</div>')

			return false;
		}


		/*=============================================
		VALIDAR LA CONTRASEÑA
		=============================================*/

		var password = $("#regPassword").val();

		if(password != ""){

			

		}else{

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio.</div>')

			return false;
		}

		/*=============================================
		VALIDAR EL NOMBRE
		=============================================*/

		var nombre = $("#regNombre").val();

		if(nombre != ""){

			var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

			if(!expresion.test(nombre)){

				$("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> No se permiten números ni caracteres especiales.</div>')
					
				$("#regNombre").val() = "";

				return false;

			}

		}else{

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio.</div>')

			return false;
		}

		/*=============================================
		VALIDAR EL APELLIDO
		=============================================*/

		var apellido = $("#regApellido").val();

		if(apellido != ""){

			var expresion = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*$/;

			if(!expresion.test(apellido)){

				$("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> No se permiten números ni caracteres especiales.</div>')

				return false;

			}

		}else{

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio.</div>')

			return false;
		}

		/*=============================================
		VALIDAR LA CÉDULA
		=============================================*/

		var cedula = $("#regCedula").val();

		if(cedula != ""){

			var expresion = /^[0-9]*$/;

			if(!expresion.test(cedula)){

				$("#padre").parent().before('<div class="alert alert-warning"><strong>ERROR:</strong> No se permiten letras ni caracteres especiales en la cédula.</div>')

				return false;

			}

		}else{

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Este campo es obligatorio.</div>')

			return false;
		}

		/*=============================================
		VALIDAR TERMINOS Y CONDICIONES
		=============================================*/

		var terminos = $("#regTerminos:checked").val();
		
		if(terminos != "on"){

			$("#padre").parent().before('<div class="alert alert-warning"><strong>ATENCIÓN:</strong> Debe aceptar nuestros terminos y condiciones.</div>')

			return false;

		}

		return true;
	}

	/*=============================================
	=                   EDITAR CLIENTE            =
	=============================================*/

	$(".btnEditarCliente").click(function(){

		var idCliente = $(this).attr("idCliente");

		var datos = new FormData();
		datos.append("idCliente", idCliente);

		$.ajax({

			url:"../ajax/clientes.ajax.php",
			method:"POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "json",
			success: function(respuesta){

				console.log(respuesta);

				$("#editarCorreo").val(respuesta["email"]);

				$("#editarNombre").val(respuesta["nombre"]);

				$("#editarApellido").val(respuesta["apellido"]);

				$("#editarFechaNac").val(respuesta["fecha_nacimiento"]);

				$("#editarCedula").val(respuesta["cedula"]);

				$("#editarTelfLocal").val(respuesta["telefono_local"]);

				$("#editarTelfMovil").val(respuesta["telefono_movil"]);

			}

		});

	})


/*==========================================================================================*/	


$('#daterange-btn').daterangepicker(
	{
		ranges   : {
		  'Últimos 7 días' : [moment().subtract(6, 'days'), moment()],
		  'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
		  'Este mes'  : [moment().startOf('month'), moment().endOf('month')],
		  'Último mes'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		startDate: moment().subtract(29, 'days'),
		endDate  : moment()
	},
	function (start, end) {
		$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

		var fechaInicial = start.format('YYYY-M-D');

		var fechaFinal = end.format('YYYY-M-D');

		var datos = new FormData();
		datos.append("fechaInicial", fechaInicial);
		datos.append("fechaFinal", fechaFinal);

		$.ajax({

			url:"../ajax/clientes.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){

		  		$(".rangoFechas tbody").html("");
				$.each(respuesta, function(respuesta, index) {

			  		if (index['estado'] == 1) {
			      		$(".rangoFechas tbody").append( 

				  		 	'<tr>'+

					            '<td>'+(index['id_cliente'])+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['apellido']+'</td>'+

					            '<td>V-'+index['cedula']+'</td>'+

					            '<td>'+index['email']+'</td>'+

					            '<td> '+index['telefono_local']+'/'+index['telefono_movil']+'</td>'+

								'<td>'+'<button class="btn btn-success btn-xs btnActivarCliente" onclick="activarCliente(this)" idCliente="'+index['id_cliente']+'" estadoCliente="0">Activo</button>'+'</td>'+
					            
					            '<td>'+index['fecha_ingreso']+'</td>'+

					          '</tr>'
			  			);
			  		}else{
			      		$(".rangoFechas tbody").append( 

				  		 	'<tr>'+

					            '<td>'+(index['id_cliente'])+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['apellido']+'</td>'+

					            '<td>V-'+index['cedula']+'</td>'+

					            '<td>'+index['email']+'</td>'+

					            '<td> '+index['telefono_local']+'/'+index['telefono_movil']+'</td>'+

								'<td>'+'<button class="btn btn-danger btn-xs btnActivarCliente" onclick="activarCliente(this)" idCliente="'+index['id_cliente']+'" estadoCliente="1">Inactivo</button>'+'</td>'+
					            
					            '<td>'+index['fecha_ingreso']+'</td>'+

					          '</tr>'
			  			);

			  		}
				});
		  	},
			error: function (respuesta){
				swal({
					type: "error",
					title: "error",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="clientes";

					}
				})
      		}
		})
	}
)

