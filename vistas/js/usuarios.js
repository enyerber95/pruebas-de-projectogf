/*=============================================
=                   EDITAR USUARIO            =
=============================================*/

$(".btnEditarUsuario").click(function(){

	var idUsuario = $(this).attr("idUsuario");

	var datos = new FormData();
	datos.append("idUsuario", idUsuario);

	$.ajax({

		url:"../ajax/usuarios.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			console.log(respuesta);

			$("#editarNombre").val(respuesta["nombre"]);

			$("#editarUsuario").val(respuesta["usuario"]);

			$("#editarDepartamento").html(respuesta["departamento"]);
			$("#editarDepartamento").val(respuesta["departamento"]);

			$("#editarAcceso").html(respuesta["acceso"]);
			$("#editarAcceso").val(respuesta["acceso"]);

			$("#passwordActual").val(respuesta["password"]);

		}

	});

})

/*=============================================
ACTIVAR USUARIO
=============================================*/
$(".btnActivar").click(function(){

	var idUsuario = $(this).attr("idUsuario");
	var estadoUsuario = $(this).attr("estadoUsuario");
	var Usuario = $(this).attr("Usuario");
	var nombreUsuario = $(this).attr("nombreUsuario");
	var accionUsuario="";
		swal({
		  type: "warning",
		  title: " ¿Esta seguro de cambiar el estado del Cliente?<br> Esta acción sera registrada por seguridad",
		  showCancelButton: true,
	      confirmButtonColor: "#018ba7",
	      cancelButtonColor: '#dd4b39',
	      cancelButtonText: 'Cancelar',
	      confirmButtonText: 'Sí, Cambiar estado'
		}).then((result) => {
		if (result.value) {

			

			  	if(estadoUsuario == 0){

			  		$(this).removeClass('btn-success');
			  		$(this).addClass('btn-danger');
			  		$(this).html('Inactivo');
			  		$(this).attr('estadoUsuario',1);
			  		accionUsuario="Usuario Desactivado";

			  	}else{

			  		$(this).addClass('btn-success');
			  		$(this).removeClass('btn-danger');
			  		$(this).html('Activo');
			  		$(this).attr('estadoUsuario',0);
			  		accionUsuario="Usuario Desactivado";


			  	}
			  	var datos = new FormData();
		 	datos.append("activarId", idUsuario);
		  	datos.append("activarUsuario", estadoUsuario);
		  	datos.append("UsuarioEstado", Usuario);
		  	datos.append("nombreUsuario", nombreUsuario);
		  	datos.append("accionUsuario", accionUsuario);

		  	$.ajax({

			  url:"../ajax/usuarios.ajax.php",
			  method: "POST",
			  data: datos,
			  cache: false,
		      contentType: false,
		      processData: false,

		      success: function(respuesta){
		      	console.log(respuesta);

			},
	 		error: function (respuesta){
						swal({
							type: "error",
							title: "error al cambiar estado",
							showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
					  	}).then((result) => {
							if (result.value) {

								window.location ="usuarios";

							}
						})
		      		}
	  			})
  			}
	})

})

/*=============================================
REVISAR SI EL USUARIO YA ESTÁ REGISTRADO
=============================================*/

$("#nuevoUsuario").change(function(){

	$(".alert").remove();

	var usuario = $(this).val();

	var datos = new FormData();
	datos.append("validarUsuario", usuario);

	 $.ajax({
	    url:"../ajax/usuarios.ajax.php",
	    method:"POST",
	    data: datos,
	    cache: false,
	    contentType: false,
	    processData: false,
	    dataType: "json",
	    success:function(respuesta){
	    	
	    	if(respuesta){

	    		$("#nuevoUsuario").parent().after('<br><div class="alert alert-warning">Este usuario ya existe en la base de datos.</div>');

	    		$("#nuevoUsuario").val("");

	    	}

	    }

	})

});


/*=============================================
ELIMINAR USUARIO
=============================================*/
$(".btnEliminarUsuario").click(function(){

  var idUsuariox = $(this).attr("idUsuario");
 
			swal({
		    title: '¿Está seguro de borrar el usuario?',
		    text: "¡Si no lo está puede cancelar la accíón!",
		    type: 'warning',
		    showCancelButton: true,
		    confirmButtonColor: "#018ba7",
		      cancelButtonColor: '#dd4b39',
		      cancelButtonText: 'Cancelar',
		      confirmButtonText: 'Sí, borrar usuario'
		  	}).then((result)=>{

		  			var datos = new FormData();

					datos.append("eliminarUsuario", idUsuariox);
		    		$.ajax({
				    url:"../ajax/usuarios.ajax.php",
				    method:"POST",
				    data: datos,
				    cache: false,
				    contentType: false,
				    processData: false,
				    success:function(respuesta){
				    	
				    	if(respuesta){

							      window.location.href = "usuarios&idUsuario-"+idUsuariox;
				    		 

							}

				    	},

				    
				   	error: function (respuesta){
							swal({
								type: "error",
								title: "error al borrar la direccion"+idUsuariox,
								showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false
						  	}).then((result) => {
								if (result.value) {

									window.location ="usuarios";

								}
							})
			      		}
				   })

		    

 })

});

 