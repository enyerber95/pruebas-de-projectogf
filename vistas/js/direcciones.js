/*=============================================
=                 EDITAR DIRECCIÓN            =
=============================================*/

$(".btnEditarDireccion").click(function(){

	var idDireccion = $(this).attr("idDireccion");

	$("#direccionID").val(idDireccion);

	var datos = new FormData();
	datos.append("idDireccion", idDireccion);

	$.ajax({

		url:"ajax/direcciones.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){
			console.log(respuesta);

			$("#editarTitulo").val(respuesta["d_titulo"]);

			$("#editarTelf").val(respuesta["d_telefono"]);

			$("#editarRazonSoc").val(respuesta["d_nombre_empresa"]);

			$("#editarSU").val(respuesta["sector_urbanizacion"]);

			$("#editarAC").val(respuesta["avenida_calle"]);

			$("#editarEQC").val(respuesta["edificio_quinta_casa"]);

			$("#editarEstado").html(respuesta["estado"]);
			$("#editarEstado").val(respuesta["estado"]);

			$("#editarCiudad").val(respuesta["ciudad"]);

			$("#editarIA").val(respuesta["informacion_adicional"]);

		}

	});

})



/*=============================================
ELIMINAR DIRECCIÓN
=============================================*/
$(".btnEliminarDireccion").click(function(){

  var idDireccion = $(this).attr("idDireccion2");

  swal({
    title: '¿Está seguro de borrar la direccion? <br> Esto puede ocasionar que pedidos se elimininen',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: "#018ba7",
      cancelButtonColor: '#dd4b39',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, borrar direccion'
  }).then((result)=>{

    if(result.value){

      window.location = "index.php?ruta=direcciones&idDireccion2="+idDireccion;

    }

  })

})

  	var	idDireccion1 = 0;
  	var	idDireccion2 = 0;

  function cambiadireccion(e) {
    numeroDireccion = e.id;
    idDireccion = e.value;
    $('#content'+numeroDireccion).css('display','block');
    if (idDireccion == '') {
	    $('#content'+numeroDireccion).css('display','none');
    }
		idCarrito = $('#idCarrito').val();
		var datos = new FormData();
		datos.append("idDireccion", idDireccion);
		if (numeroDireccion == 1) {
			idDireccion1 = idDireccion;
		}
		else{
			idDireccion2 = idDireccion;
		}
		$.ajax({

			url:"ajax/direcciones.ajax.php",
			method:"POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "json",
			success: function(respuesta){
				console.log(respuesta);

				$("#titulo"+numeroDireccion).html(respuesta["d_titulo"]);

				$("#tlf"+numeroDireccion).html(respuesta["d_telefono"]);

				$("#nameEmp"+numeroDireccion).html(respuesta["d_nombre_empresa"]);

				$("#secUrb"+numeroDireccion).html(respuesta["sector_urbanizacion"]);

				$("#aveCll"+numeroDireccion).html(respuesta["avenida_calle"]);

				$("#ediQuiCas"+numeroDireccion).html(respuesta["edificio_quinta_casa"]);

				$("#estado"+numeroDireccion).html(respuesta["estado"]);

				$("#ciudad"+numeroDireccion).html(respuesta["ciudad"]);

				$("#infAdi"+numeroDireccion).html(respuesta["informacion_adicional"]);

				//var href = $('#a').attr('href');
				if (idDireccion1==0 || idDireccion2==0) {
				$("#a").prop("href", '#');	
				}else{
				$("#a").prop("href", 'metodoEnvioCompras&'+idCarrito+'-'+idDireccion1+'-'+idDireccion2);
				}

			}

		});

  }
