/*=============================================
=     MOSTRAR DIRECCION DE ENVIO              =
=============================================*/

function verDetalles(e){

	var idPedido = $(e).attr("idPedido");


	var datos = new FormData();
	datos.append("idPedido", idPedido);

	$.ajax({

		url:"../ajax/pedidos.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			$(".metodo").remove();

			$("#verNombre").html(respuesta["nombre_apellido"]);

			$("#verRifCedula").html(respuesta["rif"]);

			$("#verEmpresa").html(respuesta["nombre_empresa"]);

			$("#verOrdeasdan").html(respuesta["orden"]);

			$("#verSector").html(respuesta["sector_urbanizacion"]);

			$("#verAvenida").html(respuesta["avenida_calle"]);

			$("#verEdificio").html(respuesta["edificio_quinta_casa"]);

			$("#verTelf").html(respuesta["d_telefono"]);

			$("#verInfo").html(respuesta["informacion_adicional"]);

			$("#verEstado").html(respuesta["estado"]);

			$("#verCiudad").html(respuesta["ciudad"]);

			if (respuesta["metodo_envio"] == 'Gratuito') {


				$("#verMetodoEnvio").after('<div class="box-body redondeado metodo" style="border: 1px solid #dbdbdb"><div class="col-md-12"><strong>Envío gratuito</strong><p style="margin-bottom: 0px">Envío directo únicamente a los siguientes estados: Aragua, Carabobo, Distrito Capital, Miranda, Guárico y Vargas.</p></div></div>')

			}else{

				$("#verMetodoEnvio").after('<div class="box-body redondeado metodo" style="border: 1px solid #dbdbdb"><div class="col-md-12"><strong>Envío cobro destino</strong><p style="margin-bottom: 0px">Envío en 24 horas después de finalizado el pedido con la empresa de su preferencia.</p></div></div>')

			}


		},
		error: function (respuesta){
			console.log(respuesta);

				swal({
					type: "error",
					title: "error al borrar la direccion",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="pedidos";

					}
				})
      		}
	});

}




/*=============================================
CAMBIAR ESTADO PEDIDO
=============================================*/
function cambiarEstado(e){

	var idPedido = $(e).attr("idPedido");
	var estadoPedido = $(e).attr("estadoPedido");
	var usuario = $(e).attr("usuario");
	var nombreusuario = $(e).attr("nombreusuario");
	idPedido=parseInt(idPedido);
	var accionBitacora = '';
		swal({
		  type: "warning",
		  title: " ¿Esta seguro de cambiar el estado del pedido?<br> Esta acción sera registrada por seguridad",
		  showCancelButton: true,
	      confirmButtonColor: "#018ba7",
	      cancelButtonColor: '#dd4b39',
	      cancelButtonText: 'Cancelar',
	      confirmButtonText: 'Sí, Cambiar estado'
		}).then((result) => {
		if (result.value) {




			if(estadoPedido == 'Pendiente'){

		  		$(e).removeClass('btn-warning');
		  		$(e).addClass('btn-success');
		  		$(e).html('En Proceso');
		  		$(e).attr('estadoPedido',"Comprobar");
		  		estadoPedido = 'Comprobar';
		  		accionBitacora = 'Comprobando Pedido';

		  	}else if(estadoPedido == 'Comprobar'){

		  		$(e).removeClass('btn-success');
		  		$(e).addClass('btn-danger');
		  		$(e).html('Rechazado');
		  		$(e).attr('estadoPedido','Rechazado');
		  		estadoPedido = 'Rechazado';
		  		accionBitacora = 'Rechazando Pedido';


		  	}else if(estadoPedido == 'Rechazado'){

		  		$(e).removeClass('btn-danger');
		  		$(e).addClass('btn-primary');
		  		$(e).html('Finalizado');
		  		$(e).attr('estadoPedido','Finalizado');
		  		estadoPedido = 'Finalizado';
		  		accionBitacora = 'Finalizando Pedido';


		  	}else if(estadoPedido == 'Finalizado'){

		  		$(e).removeClass('btn-primary');
		  		$(e).addClass('btn-warning');
		  		$(e).html('Pendiente');
		  		$(e).attr('estadoPedido','Pendiente');
		  		estadoPedido = 'Pendiente';
		  		accionBitacora = 'Pedido Pendiente';


		  	}

			var datos = new FormData();
		 	datos.append("cambiarPedido", idPedido);
		  	datos.append("cambiarEstado", estadoPedido);
		  	datos.append("cambiarAccion", accionBitacora);
		  	datos.append("usuario", usuario);
		  	datos.append("nombreusuario", nombreusuario);



		  	$.ajax({

			  url:"../ajax/pedidos.ajax.php",
			  method: "POST",
			  data: datos,
			  cache: false,
		      contentType: false,
		      processData: false,
		      success: function(respuesta){

		      	console.log(respuesta);

		      }

  			})

		}
	})

}

$(".btnEliminarCarrito").click(function(){


  swal({
    title: '¿Está seguro de borrar la direccion? <br> Esto puede ocasionar que pedidos se elimininen',
    text: "¡Si no lo está puede cancelar la accíón!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: "#018ba7",
      cancelButtonColor: '#dd4b39',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Sí, borrar direccion'
  }).then((result)=>{

    if(result.value){

		var datos = new FormData();
		datos.append("idCarritox", $(this).attr("idCarritox"));

		$.ajax({

			url:"ajax/pedidos.ajax.php",
			method: "POST",
			data: datos,
			cache: false,
			contentType: false,
			processData: false,
			success: function(respuesta){
				if(respuesta === 'ok'){
					swal({
						type: "success",
						title: "Su pedido ha sido borrada correctamente",
						showConfirmButton: true,
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
					}).then((result) => {
						if (result.value) {

						window.location ="resumenCompras";

						}
					})
				}else{

					swal({
						  type: "error",
						  title: "error al borrar la direccion",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
				  	}).then((result) => {
						if (result.value) {

							window.location ="resumenCompras";

						}
					})
				}
			},
			error: function (respuesta){
				swal({
					type: "error",
					title: "error al borrar la direccion",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="resumenCompras";

					}
				})
      		}

		});

	}

  });

});


$(".logo").change(function(){
	if ( this.files[0].type == 'image/jpeg' || this.files[0].type == 'image/jpg' || this.files[0].type == 'image/gif' || this.files[0].type == 'image/png' && this.files[0].size < 2000000 ) {
		if (this.files[0].type != 'application/pdf') {
			var datos = new FormData((document.getElementById("envio")));
			//var image =this.files[0];
			datos.append("urls",'url');
			$('input[name=logoDoc]').val('');

			$.ajax({

				url:"ajax/pedidos.ajax.php",
				method: "POST",
				data: datos,
				cache: false,
				contentType: false,
				processData: false,
				success: function(respuesta){
						$('input[name=logoDoc]').val(respuesta);
				},
				error: function (respuesta){
					swal({
						title: "¡ERROR!",
						text: "Error el archivo no es valido o su tamaño no es el adecuado.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
					});
					$('#logo').val('');
	      }

			});
		}else{
			swal({
				title: "¡ERROR!",
				text: "Error el archivo no es valido o su tamaño no es el adecuado.",
				type:"error",
				confirmButtonColor: "#018ba7",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
			});
			$('#logo').val('');
		}
	}else{
		swal({
			title: "¡ERROR!",
			text: "Error el archivo no es valido o su tamaño no es el adecuado.",
			type:"error",
			confirmButtonColor: "#018ba7",
			confirmButtonText: "Cerrar",
			closeOnConfirm: false
		});
		$('#logo').val('');
	}
	$('img[name=img]').remove();
	url =URL.createObjectURL(this.files[0]);
	$("td[name="+this.id+"Doc]").prepend('<img name="img" src="'+url+'"/>');
	$('img[name=img]').width(size);
	$('img[name=img]').height(size/1.2);
});


$('#daterange-btn-pedidos').daterangepicker(
	{
		ranges   : {
		  'Últimos 7 días' : [moment().subtract(6, 'days'), moment()],
		  'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
		  'Este mes'  : [moment().startOf('month'), moment().endOf('month')],
		  'Último mes'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		startDate: moment().subtract(29, 'days'),
		endDate  : moment()
	},
	function (start, end) {
		$('#daterange-btn-pedidos span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

		var fechaInicial = start.format('YYYY-M-D');

		var fechaFinal = end.format('YYYY-M-D');

		var datos = new FormData();
		datos.append("fechaInicial", fechaInicial);
		datos.append("fechaFinal", fechaFinal);
		var nombre = $('#nombre').val();
		var usuario = $('#usuario').val();

		$.ajax({

			url:"../ajax/pedidos.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){

		  		$(".rangoFechas tbody").html("");
				$.each(respuesta, function(respuesta, index) {
					if (index['estado'] == 'Pendiente') {
			      		$(".rangoFechas tbody").append(

				  		 	'<tr>'+
					            '<td>'+(index['id_pedido'])+'</td>'+

					            '<td>'+index['orden']+'</td>'+

					            '<td>'+index['nombreCliente']+' '+index['apellido']+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['monto']+'</td>'+

					            '<td> '+index['fecha_emision']+'</td>'+

								'<td>'+'<button class="btn btn-warning btn-xs btnCambiarEstado" onclick="cambiarEstado(this)"  idPedido="'+index['id_pedido']+'" estadoPedido="Pendiente" usuario="'+ usuario +'" nombreusuario="'+nombre+'">Pendiente</button></td>'+

								'<td>'+'<button class="btn btn-default"><i class="fa fa-file-text"></i></button>'+

	                            '<button class="btn btn-default btnVerDetallesEnvio" onclick="verDetalles(this)" data-toggle="modal" data-target="#verDetallesEnvio" idPedido="'+index['id_pedido']+'"><i class="fa fa-truck"></i></button>'+'</td>'+
					          '</tr>'
			  			);
			      	}
					if (index['estado'] == 'Comprobar') {
			      		$(".rangoFechas tbody").append(

				  		 	'<tr>'+
					            '<td>'+(index['id_pedido'])+'</td>'+

					            '<td>'+index['orden']+'</td>'+

					            '<td>'+index['nombreCliente']+' '+index['apellido']+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['monto']+'</td>'+

					            '<td> '+index['fecha_emision']+'</td>'+

								'<td>'+'<button class="btn btn-success btn-xs btnCambiarEstado" onclick="cambiarEstado(this)"  idPedido="'+index['id_pedido']+'" estadoPedido="Comprobar" usuario="'+usuario+'" nombreusuario="'+nombre+'">En Proceso</button></td>'+

								'<td>'+'<button class="btn btn-default"><i class="fa fa-file-text"></i></button>'+

	                            '<button class="btn btn-default btnVerDetallesEnvio" onclick="verDetalles(this)" data-toggle="modal" data-target="#verDetallesEnvio" idPedido="'+index['id_pedido']+'"><i class="fa fa-truck"></i></button>'+'</td>'+
					          '</tr>'
			  			);
			      	}
					if (index['estado'] == 'Rechazado') {
			      		$(".rangoFechas tbody").append(

				  		 	'<tr>'+
					            '<td>'+(index['id_pedido'])+'</td>'+

					            '<td>'+index['orden']+'</td>'+

					            '<td>'+index['nombreCliente']+' '+index['apellido']+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['monto']+'</td>'+

					            '<td> '+index['fecha_emision']+'</td>'+

								'<td>'+'<button class="btn btn-danger btn-xs btnCambiarEstado" onclick="cambiarEstado(this)"  idPedido="'+index['id_pedido']+'" estadoPedido="Rechazado" usuario="'+usuario+'" nombreusuario="'+nombre+'">Rechazado</button></td>'+

								'<td>'+'<button class="btn btn-default"><i class="fa fa-file-text"></i></button>'+

	                            '<button class="btn btn-default btnVerDetallesEnvio" onclick="verDetalles(this)" data-toggle="modal" data-target="#verDetallesEnvio" idPedido="'+index['id_pedido']+'"><i class="fa fa-truck"></i></button>'+'</td>'+
					          '</tr>'
			  			);
			      	}
					if (index['estado'] == 'Finalizado') {
			      		$(".rangoFechas tbody").append(

				  		 	'<tr>'+
					            '<td>'+(index['id_pedido'])+'</td>'+

					            '<td>'+index['orden']+'</td>'+

					            '<td>'+index['nombreCliente']+' '+index['apellido']+'</td>'+

					            '<td>'+index['nombre']+'</td>'+

					            '<td>'+index['monto']+'</td>'+

					            '<td> '+index['fecha_emision']+'</td>'+

								'<td>'+'<button class="btn btn-primary btn-xs btnCambiarEstado" onclick="cambiarEstado(this)"  idPedido="'+index['id_pedido']+'" estadoPedido="Finalizado" usuario="'+usuario+'" nombreusuario="'+nombre+'">Finalizado</button></td>'+

								'<td>'+'<button class="btn btn-default"><i class="fa fa-file-text"></i></button>'+

	                            '<button class="btn btn-default btnVerDetallesEnvio" onclick="verDetalles(this)" data-toggle="modal" data-target="#verDetallesEnvio" idPedido="'+index['id_pedido']+'"><i class="fa fa-truck"></i></button>'+'</td>'+
					          '</tr>'
			  			);
			      	}
				});
		  	},
			error: function (respuesta){
				swal({
					type: "error",
					title: "error",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="reportes";

					}
				})
      		}
		})
	}
)

  /*=============================================
  IMPRIMIR FACTURA
  =============================================*/

function imprimirFactura(e){
    var idPedido = $(e).attr("idPedido");
    var idProducto = $(e).attr("idProducto");

    window.open("../reportesPdf/tcpdf/pdf/factura"+idProducto+".php?pedido="+idPedido,'_blank');
}
  /*=============================================
  IMPRIMIR FACTURA Admin
  =============================================*/

function imprimirFacturaAdmin(e){
    var idPedido = $(e).attr("idPedido");
    var idReporte = $(e).attr("idReporte");

    window.open("../reportesPdf/tcpdf/pdf/facturaAdmin.php?idReporte="+idReporte+"&pedido="+idPedido,'_blank');
}
