
/*=============================================
=    TRAER PORCENTAJES DE LAS PREFERENCIAS    =
=============================================*/

var pBond = 0;
var pQuimico = 0;
var o1 = 0;
var o2 = 0;
var o3 = 0;
var bN = 0;
var fC = 0;
var idProducto = 0;

$(document).ready(function(){ 

	var id = 1;

	var datos = new FormData();
	datos.append("id", id);

	$.ajax({

		url:"ajax/productos.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			pBond = respuesta["p_bond"];
			pQuimico = respuesta["p_quimico"];
			o1 = respuesta["o_1"];
			o2 = respuesta["o_2"];
			o3 = respuesta["o_3"];
			bN = respuesta["blanco_negro"];
			fC = respuesta["full_color"];

		}

	});

})


/*=============================================
=    GUARDAR PRECIO BASE EN UN INPUT          =
=============================================*/

$(".producto").click(function(){

	idProducto = $(this).attr("idProducto");

	var datos = new FormData();
	datos.append("idProducto", idProducto);

	$.ajax({

		url:"ajax/productos.ajax.php",
		method:"POST",
		data: datos,
		cache: false,
		contentType: false,
		processData: false,
		dataType: "json",
		success: function(respuesta){

			var precioBase = respuesta["precio_base"];
			var precioFinal = respuesta["precio_base"];



			$("#precioBase").val(respuesta["precio_base"]);


			/*=============================================
			=   VALOR AGREGADO PREFERENCIAS DE PAPEL      =
			=============================================*/

			var pPapel = document.getElementById("pPapel").value;

			if (pPapel == "Papel bond"){

				precioFinal = (precioBase * pBond);


			}else{

				precioFinal = (precioBase * pQuimico);

			}

			/*=============================================
			=  VALOR AGREGADO PREFERENCIAS DE IMPRESIÓN   =
			=============================================*/

			var pImpresion = document.getElementById("pImpresion").value;

			if (pImpresion == "Original y 1 copia"){

				precioFinal = parseInt(precioFinal) + (precioBase * o1);

			}else if(pImpresion == "Original y 2 copias"){

				precioFinal = parseInt(precioFinal) + (precioBase * o2);

			}else{

				precioFinal = parseInt(precioFinal) + (precioBase * o3);

			}



			/*=============================================
			=   VALOR AGREGADO PREFERENCIAS DE COLOR      =
			=============================================*/

			var pColor = document.getElementById("pColor").value;

			if (pColor == "Blanco y negro"){

				precioFinal = parseInt(precioFinal) + (precioBase * bN);
			}else{

				precioFinal = parseInt(precioFinal) + (precioBase * fC);

			}


			/*=============================================
			=          VALOR AGREGADO CANTIDAD            =
			=============================================*/


			var cantidad = document.getElementById("cantidad").value;
			precioFinal = parseInt(precioFinal) + (precioBase * cantidad);
			precioBsS = precioFinal/100000;

			$("#actualizarPrecio").val("Bs. "+precioFinal);
			$("#actualizarPrecioBsS").val("BsS. "+precioBsS);

		}

	});

})



/*=============================================
=    GUARDAR PRECIO BASE EN UN INPUT 2        =
=============================================*/

$(".preferencia").change(function(){

	var precioBase = document.getElementById("precioBase").value;
	var precioFinal = document.getElementById("precioBase").value;


			/*=============================================
			=   VALOR AGREGADO PREFERENCIAS DE PAPEL      =
			=============================================*/

			var pPapel = document.getElementById("pPapel").value;

			if (pPapel == "Papel bond"){

				precioFinal = (precioBase * pBond);

			}else{

				precioFinal = (precioBase * pQuimico);

			}

			/*=============================================
			=  VALOR AGREGADO PREFERENCIAS DE IMPRESIÓN   =
			=============================================*/

			var pImpresion = document.getElementById("pImpresion").value;

			if (pImpresion == "Original y 1 copia"){

				precioFinal = parseInt(precioFinal) + (precioBase * o1);

			}else if(pImpresion == "Original y 2 copias"){

				precioFinal = parseInt(precioFinal) + (precioBase * o2);

			}else{

				precioFinal = parseInt(precioFinal) + (precioBase * o3);

			}



			/*=============================================
			=   VALOR AGREGADO PREFERENCIAS DE COLOR      =
			=============================================*/

			var pColor = document.getElementById("pColor").value;

			if (pColor == "Blanco y negro"){


				precioFinal = parseInt(precioFinal) + (precioBase * bN);
			}else{

				precioFinal = parseInt(precioFinal) + (precioBase * fC);

			}


			/*=============================================
			=          VALOR AGREGADO CANTIDAD            =
			=============================================*/

			var cantidad = document.getElementById("cantidad").value;
			precioFinal = parseInt(precioFinal) + (precioBase * cantidad);
			precioBsS = precioFinal/100000;

			$("#actualizarPrecio").val("Bs. "+precioFinal);
			$("#actualizarPrecioBsS").val("BsS. "+precioBsS);

		})