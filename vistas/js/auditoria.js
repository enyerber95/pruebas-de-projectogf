
$('#respaldo').click(function() {
	swal({
	type: "warning",
		  title: " ¿Esta seguro que desea Generar Respaldo?<br> Esta acción sera registrada por seguridad",
		  showCancelButton: true,
	      confirmButtonColor: "#018ba7",
	      cancelButtonColor: '#dd4b39',
	      cancelButtonText: 'Cancelar',
	      confirmButtonText: 'Sí, Generar Ahora'
  	}).then((result) => {
		if (result.value) {
		respaldo='hoy';	
		var usuario = $(this).attr("usuario");
		var nombreusuario = $(this).attr("nombreusuario");
		var datos = new FormData();
		datos.append("respaldo", respaldo);
		datos.append("usuariorespaldo", usuario);
		datos.append("nombreusu", nombreusuario);

		$.ajax({

			url:"../ajax/auditoria.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){
		  		console.log(respuesta)
		  		swal({
				type: "success",
				title: "Respaldo Generado",
				showConfirmButton: true,
				confirmButtonColor: "#018ba7",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
		  	}).then((result) => {
		  		window.location ="../../pruebas-de-projectogf/respaldos/"+respuesta;

		  		})


		  	},
		  	error: function (respuesta){
		  		res = respuesta.responseText
		  		res =res.split('"') 
		  		console.log(res[1])
		  	
				swal({
					type: "success",
					title: "Respaldo Generado",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {
		  				window.location ="../../pruebas-de-projectogf/respaldos/"+res[1];


					}
				})
      		}

		})

		}
	})

});

$('#restore').click(function() {
	swal({
	type: "warning",
		  title: " ¿Esta seguro que desea Generar una restauración total de la BD??<br> Esta acción sera registrada por seguridad",
		  showCancelButton: true,
	      confirmButtonColor: "#018ba7",
	      cancelButtonColor: '#dd4b39',
	      cancelButtonText: 'Cancelar',
	      confirmButtonText: 'Sí, Generar Ahora'
  	}).then((result) => {
		if (result.value) {
		respaldo='hoy';	
		var usuario = $(this).attr("usuario");
		var nombreusuario = $(this).attr("nombreusuario");
		var datos = new FormData();
		datos.append("restore", respaldo);
		datos.append("usuariorespaldo", usuario);
		datos.append("nombreusu", nombreusuario);

		$.ajax({

			url:"../ajax/auditoria.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){
		  		console.log(respuesta)
		  		swal({
				type: "success",
				title: "Respaldo Generado",
				showConfirmButton: true,
				confirmButtonColor: "#018ba7",
				confirmButtonText: "Cerrar",
				closeOnConfirm: false
		  	}).then((result) => {
		  		window.location ="../../pruebas-de-projectogf/respaldos/"+respuesta;

		  		})


		  	},
		  	error: function (respuesta){
		  		console.log(respuesta)
		  	
				swal({
					type: "error",
					title: "error",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="auditoria";

					}
				})
      		}

		})

		}
	})

});

$('#daterange-btn-au').daterangepicker(
	{
		ranges   : {
		  'Últimos 7 días' : [moment().subtract(6, 'days'), moment()],
		  'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
		  'Este mes'  : [moment().startOf('month'), moment().endOf('month')],
		  'Último mes'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		},
		startDate: moment().subtract(29, 'days'),
		endDate  : moment()
	},
	function (start, end) {
		$('#daterange-btn-au span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))

		var fechaInicial = start.format('YYYY-M-D');

		var fechaFinal = end.format('YYYY-M-D');
		
		console.log(fechaInicial+' '+fechaFinal);
		var datos = new FormData();
		datos.append("fechaInicial", fechaInicial);
		datos.append("fechaFinal", fechaFinal);
		$.ajax({

			url:"../ajax/auditoria.ajax.php",
		  	method: "POST",
		  	data: datos,
		  	cache: false,
		  	contentType: false,
		  	processData: false,
		  	dataType:"json",
		  	success:function(respuesta){
			console.log(respuesta);
		  		
		  		$(".rangoFechas tbody").html("");
				$.each(respuesta, function(respuesta, index) {
		      		$(".rangoFechas tbody").append( 

			  		 	'<tr>'+

				            '<td>'+(index['id_bitacora'])+'</td>'+

				            '<td>'+index['user_responsable']+'</td>'+

      						'<td>'+index['nombre_responsable']+'</td>'+

      						'<td>'+index['accion']+'</td>'+

				            '<td>'+index['fecha_realizado']+'</td>'+

				            '<td> '+index['descripcion']+'</td>'+

				          '</tr>'
		  			);
				});
		  	},
			error: function (respuesta){
				swal({
					type: "error",
					title: "error",
					showConfirmButton: true,
					confirmButtonColor: "#018ba7",
					confirmButtonText: "Cerrar",
					closeOnConfirm: false
			  	}).then((result) => {
					if (result.value) {

						window.location ="auditoria";

					}
				})
      		}
		})
	}
)