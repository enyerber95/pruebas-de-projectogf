<!--=====================================
TOP
======================================-->

<div class="container-fluid barraSuperior" id="top">
	
	<div class="container">
		
		<div class="row">
	
			<!--=====================================
			BARRA MAYOR SUPERIOR (AGUA MARINA)
			======================================-->

			<div class="">
				
				<ul class="">	

					<li>
						<a href="#" target="">
							<img src="vistas/img/grafoformas/logo-mini.png" class="img-responsive logoCabezote social">
						</a>
					</li>

					<li>

						<a href="#" data-toggle="modal" data-target="#modalIniciarSesion">Iniciar Sesión</a>
						
					</li>

					<li>
						<a href="#" style="">Reportar Pago</a>
					</li>


					<li>
						<a href="#">Ayuda</a>
					</li>

					<li class="centrarAjustar">
						
							<form class="form col-md-3 col-sm-6 pull-right" method="get" style="padding-top: 8px;">

								<div class="input-group">

									<input class="form-control" type="text" placeholder="Buscar" name="user_query" required>

									<span class="input-group-btn">

										<button type="submit" value="Search" name="search" class="btn btn-default"><i class="fa fa-search"></i></button>

									</span>

								</div>

							</form>

					</li>
	
				</ul>



			</div>



		</div>	

	</div>

</div>

	<!--=====================================
	LOGO GRANDE Y CORREOS Y TELÉFONOS
	======================================-->

	<div class="container-fluid">
			
		<div class="container">

			<div class="row">	

				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 logoGrande">
					
					<img src="vistas/img/grafoformas/logo-grande.png" class="img-responsive logoGrande">

				</div>

				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 contactos">

					<div class="container-fluid contactos">

						<div class="container-fluid numeros" style="text-align: right;">

							<p style="margin-bottom: 0px; color:#808080;">(0243) 2405019<br>(0414) 3221507</p>

							<div class="linea"></div>
						
							<i class="fa fa-phone" style="font-size: 45px; padding-left: 10px; color:#808080; text-align: right;"></i>

						</div>	

						<div class="container-fluid numeros" style="text-align: right;">

							<p style="margin-bottom: 0px; color:#808080;">grafoformashb.vtas@yahoo.es<br>grafoformashb.vtas@hotmail.com</p>

							<div class="linea"></div>
						
							<i class="fa fa-envelope" style="font-size: 40px; padding-left: 10px; color:#808080;"></i>

						</div>		
					
					</div>

				</div>		

			</div>

		</div>

	</div>


	<!--=====================================
	MENÚ DE NAVEGACIÓN
	======================================-->	
	
	<div class="container-fluid">
			
		<div class="container">
			
			<br>

			<header>
				
				<nav class="navbar navbar-default">

					<div class="container-fluid" style="padding-right: 0px; padding-left: 0px;"">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">

							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>

							</button>

						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

							<ul class="nav navbar-nav navbar-left menuPrincipal">

								<li><a href="#">Inicio<span class="sr-only"></span></a></li>

								<li><a href="#">Conócenos</a></li>

								<li class="dropdown">

									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Catálogo <span class="caret"></span></a>

									<ul class="dropdown-menu">
										
										<li><a href="#">Facturas</a></li>
										<li><a href="#">Formas Libres</a></li>
										<li><a href="#">Notas Físcales</a></li>
										<li><a href="#">Rollos Físcales</a></li>
										<li role="separator" class="divider"></li>
										<li><a href="#">  Tarjetas de Inventario</a></li>
										<li><a href="#">  Tarjetas de Presentación</a></li>
										<li><a href="#">  Recipes Médicos</a></li>

									</ul>

								</li>

								<li class="dropdown">

									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pedidos <span class="caret"></span></a>

									<ul class="dropdown-menu">

										<li><a href="#">Mis Pedidos</a></li>
										<li><a href="#">Validar Documentos</a></li>

									</ul>

								</li>

								<li><a href="#">Cotizador en Línea</a></li>

							</ul>

							<!--=====================================
							   CARRITO DE COMPRAS
							======================================-->

							<ul class="nav navbar-nav navbar-right" style="padding-top: 5px;">

								<li class="centrarAjustar" style="display: flex;">

									<a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding: 0px;">

										<div class="col-md-12 centrarAjustar">
			
											<div class="linea" style=""></div>

											<span class="glyphicon glyphicon-shopping-cart" style="font-size: 150%; padding-left:10px;"></span>
											
											<span style="padding-left:10px;">2 Artículos</span>

										</div>

									</a>

									<div class="dropdown-menu" style="width: 280px; margin-top: 5px;padding-top: 0px; padding-bottom: 0px">

										<div class="panel panel-default" style="margin-bottom: 0px; border-bottom-width: 0px; border-top-width: 0px;">

											<!-- CABECERA DE LA MINI TABLA -->

											<div class="panel-heading">

												<div class="row centrarAjustar">

													<div class="col-md-10 textoCarrito" style="text-align: center; font-size: 110%">

														<p>Pedidos recientemente agregados</p>

													</div>

												</div>

											</div>

											<!-- CUERPO DE LA MINI TABLA (PRODUCTOS) -->

											<div class="panel-body">

												<div id="cart_product" class="textoCarrito">

													<div class="row centrarAjustar">

														<div class="col-md-6">
																	
															<img src="vistas\img\productos\facturas\tamaño carta\rayada/FCR-Modelo 1.png" class="img-thumbnail" width="100px">

														</div>

														<div class="col-md-6" style="padding-left:0px;">
															<p>FR-00002</p><br>
															<p>Bs. 80.200.000,00</p>
														</div>

													</div>

											        <hr>

													<div class="row centrarAjustar">

														<div class="col-md-6">
																	
															<img src="vistas\img\productos\facturas\tamaño media carta\rayada/FMR-Modelo 2.png" class="img-thumbnail" width="100px">

														</div>

															<div class="col-md-6" style="padding-left:0px;">
																<p>FR-00001</p><br>
																<p>Bs. 202.365.000,00</p>
															</div>

													</div>

												</div>

											</div>

											<!-- PIE DE LA MINI TABLA (BOTÓN PROCESAR COMPRA) -->

											<hr style="margin: 0px;">

											<div class="panel-body centrarAjustar">
													
												<button type="button" class="btn btn-default">Procesar Compra</button>

											</div>	
									
										</div>

									</div>	

								</li>
								
							</ul>

						</div> <!-- /.navbar-collapse -->

					</div> <!-- /.container-fluid -->

				</nav>﻿

			</header>

		</div>

	</div>

	<!--=====================================
	PARTE PRINCIPAL (BARRAS LATERAL Y CONTENIDO)
	=======================================-->

	<div class="container-fluid eltodo">
			
		<div class="container eltodo2" style="padding-left:0px; padding-right: 0px;">

			<!--=====================================
			   CONTENIDO (COTIZADOR EN LÍNEA)
			======================================-->

			<div class="container col-md-12">

				<div class="col-md-12" style=" padding: 0px;">

					<div class="col-md-12" style="border-bottom: 1px solid #F1F1F1;padding-right: 0px;">

						<h1><strong>Resumen de lista de compras</strong></h1>

					</div>

					<div class="col-md-12" style="padding: 0px; text-align: center;">

						<!--=====================================
				   		=     CARRITO DE COMPRAS                =
				   		======================================-->

				   		<div class="col-md-12" style="padding: 0px;">

				   			<div class="col-md-12" style="padding-right: 0px; padding-left: 0px; padding-top: 35px; padding-bottom: 30px;">

				   				<form action="save.php" method="post">

				   				<div class="wizards">

					                <div class="progressbar">

					                    <div class="progress-line" data-now-value="12.11" data-number-of-steps="5" style="width: 12.11%;"></div> <!-- 19.66% -->

					                </div>

					                <div class="form-wizard active">

					                    <div class="wizard-icon"><i class="fa  fa-shopping-cart"></i></div>

					                    <p>RESUMEN</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa fa-user"></i></div>

					                    <p>INGRESO</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa fa-home"></i></div>

					                    <p>DIRECCIONES</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa fa-truck"></i></div>

					                    <p>ENVÍO</p>

					                </div>

					                <div class="form-wizard">

					                    <div class="wizard-icon"><i class="fa  fa-credit-card"></i></div>

					                    <p>PAGO</p>

					                </div>

					            </div>

					            <div class="panel panel-primary">

				   					<div class="panel-body">

				   						<div class="wizard-buttons">

					                   		<button type="button" class="btn btn-next">Siguiente</button>

					                	</div>

				   					</div>

				   				</div>

				   				</div>

				   			</div>

				   		</div>

			   		</div>

				</div>	

			</div>

		</div>	

	</div>

	

	<!--==========================================
	  PIE DE PÁGINA
	===========================================-->	

	<div class="container-fluid" style="background:#808080">

		<div class="container">

			<div class="container col-md-3" style="padding-top:40px; padding-bottom:40px; text-align: justify;">
	
				<h3 style="color:#4B4B4B; display: inline;"><strong>GrafoFormas HB C.A</strong></h3>


				<p style="color:#4B4B4B; padding-top: 10px;">Imprenta de facturas autorizadas por SENIAT, compra por Internet permitiendo obtener su producto en toda Venezuela. Fácil y rápido. Av. Ppal el Hipódromo con calle Táchira Local Nro. 2 Barrio San Ignacio, Maracay Estado Aragua, Venezuela.</p>

			</div>

			<div class="container col-md-3" style="padding-top:40px; padding-bottom:40px; text-align: justify;">
	
				<h3 style="color:#4B4B4B; display: inline;"><strong>Sobre Nosotros</strong></h3>

				<p><ul>

					<li><a href="">- La empresa</a></li>
					<li><a href="">- Cuentas bancarias</a></li>
					<li><a href="">- Términos y condiciones</a></li>
					<li><a href="">- Normativa SENIAT</a></li>
					<li><a href="">- Mapa del sitio</a></li>

				</ul></p>

			</div>	

			<div class="container col-md-3" style="padding-top:40px; padding-bottom:40px; text-align: justify;">
		
					<h3 style="color:#4B4B4B; display: inline;"><strong>Servicios</strong></h3>

					<p><ul>
							
						<li><a href="">- Atención al cliente</a></li>
						<li><a href="">- Cotización en línea</a></li>
						<li><a href="">- Carrito de compras</a></li>
						<li><a href="">- Reportar pago</a></li>
							
					</ul></p>	

			</div>	

			<div class="container col-md-3" style="padding-top:40px; padding-bottom:40px;">

				<h3 style="color:#4B4B4B; display: inline;"><strong>Redes Sociales</strong></h3>

				<div class="container col-md-6" style="font-size: 500%; padding:0px;">
					
					<a href="#"><i class="fa fa-facebook-square"></i></a>

				</div>

				<div class="container col-md-6" style="font-size: 500%; padding:0px;">
					
					<a href="#"><i class="fa fa-twitter-square"></i></a>

				</div>

				<div class="container col-md-6" style="font-size: 500%; padding:0px;">
					
					<a href="#"><i class="fa fa-google-plus-square"></i></a>

				</div>

				<div class="container col-md-6" style="font-size: 500%; padding:0px;">
					
					<a href="#"><i class="fa fa-youtube-square"></i></a>

				</div>

			</div>

  		</div>	

	</div>

	<!--==============================
	 MODAL - INICIAR SESIÓN
	===============================-->	

	<div id="modalIniciarSesion" class="modal fade" role="dialog">

		<div class="modal-dialog">

			<div class="modal-content">

				<!-- CABECERA DEL MODAL -->

				<div class="modal-header" style="background:white; color:#018ba7">

					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Inicio de Sesión</h1>

				</div>

			<div class="box-body">

				<!--=====================================
				=        USUARIO REGISTRADO             =
				======================================-->
				
				<div class="box-body col-md-6">

					<div class="box-body" style="padding-bottom: 32px;">

					    <!-- ENTRADA PARA EL CORREO -->

					    <div class="from-group" style="text-align: justify;">

					    	<h3 style="margin-top: 0px;"><i class="fa fa-user"></i> Usuario Existente</h3>
				    		<p>Por favor, introduzca su correo electrónico y contraseña para ingresar al sistema.</p><hr>
				        	<div class="input-group">

				        		<label>Correo electrónico:</label>

				         		<input type="text" class="form-control input-md" required>

				        	</div>

				        </div><br>

				        <!-- ENTRADA PARA LA CONTRASEÑA -->
				          
				        <div class="from-group">

				            <div class="input-group" style="margin-bottom: 11px;">

				            	<label>Contraseña:</label>

				          		<input type="password" class="form-control input-md" required>

				          	</div>

				        </div><br>

				        <!-- BOTÓN PARA RECUPERAR CONTRASEÑA -->
				          
				        <div class="from-group centrarAjustar">

				            <div class="input-group" style="margin-bottom: 11px;">

				            	<a href="#" data-toggle="modal" data-target="#modalRecuperarContraseña"><button type="button" class="btn btn-default" data-dismiss="modal">¿Olvidaste tu contraseña?</button></a>

				          	</div>

				        </div>

		  			</div>
 
		  			<div class="modal-footer">

		  				<div class="container col-md-12">

		  					<button type="button" class="btn btn-primary" data-dismiss="modal">Ingresar</button>
						
						</div>

					</div>

				</div>

				<!--=====================================
				=            USUARIO NUEVO              =
				======================================-->
				
				<div class="box-body col-md-6">

				<div class="box-body">

				    <!-- ENTRADA PARA EL NOBMRE -->

				    <div class="from-group" style="text-align: justify;">
						
						<h3 style="margin-top: 0px;"><i class="fa fa-user-plus"></i> Nuevo Usuario</h3>
				    	<p>Por favor, introduzca un correo electrónico y contraseña para el registro.</p><hr>

			          	<div class="input-group col-md-12">

			          		<label>Correo electrónico:</label>

			          		<input type="text" class="form-control input-md" required>

			          	</div>

			        </div><br>

			        <!-- ENTRADA PARA EL USUARIO -->
			          
			        <div class="from-group">

			          	<div class="input-group col-md-12">

			          		<label>Contraseña:</label>

			          		<input type="password" class="form-control input-md" required>

			          	</div>

			        </div><br>

			        <!-- ENTRADA PARA CONFIRMAR LA CONTRASEÑA -->
				          
				    <div class="from-group">

				        <div class="input-group col-md-12">

				            <label>Confirmar contraseña:</label>

				          	<input type="password" class="form-control input-md" required>

				        </div>

				    </div><br>

	  			</div>

	  			<div class="modal-footer">

		  				<div class="container col-md-12">

		  					<a href="vistas\modulos\crearCuenta.html.php"><button type="button" class="btn btn-primary" data-dismiss="modal">Continuar</button></a>
						
						</div>

					</div>

	  			</div>

			</div>

			</div>

		


		</div>

	</div>	


	<!--==============================
	 MODAL - RECUPERAR CONTRASEÑA
	===============================-->	

	<div id="modalRecuperarContraseña" class="modal fade" role="dialog">

		<div class="modal-dialog">

			<div class="modal-content">

				<!-- CABECERA DEL MODAL -->

				<div class="modal-header" style="background:white; color:#018ba7">

					<button type="button" class="close" data-dismiss="modal">&times;</button>

					<h1 class="modal-title"><img src="vistas/img/grafoformas/logo-mini.png" style=" vertical-align: middle" width="60" height="55"> Inicio de Sesión</h1>

				</div>

			<div class="box-body">
				
				<div class="box-body col-md-12">

					<div class="box-body">

					    <!-- ENTRADA PARA EL CORREO -->

					    <div class="from-group" style="text-align: justify;">

					    	<h3 style="margin-top: 0px;"><i class="fa fa-expeditedssl"></i> Recuperar contraseña</h3>
				    		<p>Escribe el correo electrónico con el que estás registrado, allí le enviaremos un enlace para reestablecer su contraseña:</p>
				        	<div class="input-group">

				        		<label>Correo electrónico:</label>

				         		<input type="text" class="form-control" required></input>

				        	</div>

				        </div><br>
				        
		  			</div>
 
		  			<div class="modal-footer centrarAjustar">

		  				<div class="container col-md-3">

		  					<button type="button" class="btn btn-primary" data-dismiss="modal">Enviar</button>
						
						</div>

						<div class="container col-md-9">
							
							<p style="margin-bottom: 0px;">¿No tienes una cuenta registrada? | <a href="#modalIniciarSesion" data-dismiss="modal" data-toggle="modal">Registrarse</a></p>

						</div>	

					</div>

				</div>

			</div>

			</div>

		


		</div>

	</div>	


	