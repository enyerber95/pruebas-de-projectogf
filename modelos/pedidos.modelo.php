<?php

require_once "conexion.php";

	class ModeloPedidos{

		static public function mdlMostrarClientePedido($valor){
			if ($valor != null){
				$stmt =Conexion::conectar() -> prepare("SELECT clientes.*, direcciones.id_cliente, dir_pedidos.id_pedido_dp, direcciones.id_direcciones, dir_pedidos.tipo_dp FROM clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE pedidos.id_pedido = :id_pedido AND dir_pedidos.tipo_dp = 1");
				$stmt -> bindParam(":id_pedido", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();

	 		}

			$stmt -> close();

			$stmt = null;

		}

		static public function mdlRangoFechasPedidos($fechaInicial, $fechaFinal){
			if($fechaInicial == $fechaFinal){
				$stmt =Conexion::conectar() -> prepare("SELECT pedidos.*, clientes.nombre as nombreCliente, clientes.apellido, productos.nombre FROM clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido INNER JOIN productos ON pedidos.id_producto = productos.id_producto WHERE dir_pedidos.tipo_dp = 1 and fecha_emision like '%$fechaFinal%'");
				$stmt -> bindParam(":id_pedido", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();
			}else{

	 			$stmt = Conexion::conectar() -> prepare("SELECT pedidos.*, clientes.nombre as nombreCliente, clientes.apellido, productos.nombre FROM clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido INNER JOIN productos ON pedidos.id_producto = productos.id_producto WHERE dir_pedidos.tipo_dp = 1 and fecha_emision BETWEEN '$fechaInicial' AND '$fechaFinal'");

				$stmt -> execute();

				return $stmt -> fetchAll();
			}

			$stmt -> close();

			$stmt = null;

		}

		/*=============================================
		MOSTRAR PEDIDOS
		=============================================*/
		static public function unique_multidim_array($array, $key) {
		    $temp_array = array();
		    $i = 0;
		    $key_array = array();

		    foreach($array as $val) {
		        if (!in_array($val[$key], $key_array)) {
		            $key_array[$i] = $val[$key];
		            $temp_array[$i] = $val;
		        }
		        $i++;
		    }
		    return $temp_array;
		}

		static public function MdlMostrarTodosPedidos($tabla){

			if ($tabla != null) {

				$stmt= Conexion::conectar() -> prepare("SELECT * FROM $tabla");
				$stmt -> execute();

				return $stmt -> fetchAll();

			}
			$stmt -> close();

			$stmt = null;


		}

		static public function MdlMostrarPedidos($valor){

	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT clientes.id_cliente, direcciones.id_direcciones, dir_pedidos.tipo_dp, pedidos.* FROM clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE clientes.id_cliente = :id_cliente");

	 			$stmt -> bindParam(":id_cliente", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();

	 		}

			$stmt -> close();

			$stmt = null;

		}

		static public function MdlVerPedido($valor, $detalle){

	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM pedidos WHERE $detalle = :valor");

				$stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();

	 		}

			$stmt -> close();

			$stmt = null;

		}
		static public function MdlVerDireccion($valor, $detalle){

	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM direcciones WHERE $detalle = :valor");

				$stmt -> bindParam(":valor", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetch();

	 		}

			$stmt -> close();

			$stmt = null;

		}

		static public function mdlItemPedido($valor, $item ,$detalle){
			if($valor != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT $detalle FROM pedidos WHERE $item = $valor");


			$stmt -> execute();

			return $stmt -> fetch();

			}
		}


		static public function mdldireccionpedido($tabla, $valor, $item ,$item2, $detalle, $detalle2){
			if($valor != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT $detalle FROM $tabla WHERE $item = $valor AND $item2 = $detalle2");


			$stmt -> execute();

			return $stmt -> fetch();

			}
		}


		static public function mdlBorrarPedido($tabla, $valor){

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_pedido = :id_pedido");

			$stmt -> bindParam(":id_pedido", $valor, PDO::PARAM_INT);

			if($stmt -> execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();

			$stmt = null;


		}


		static public function MdlUpdateCarrito($tabla, $datos){

		    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET id_cliente_carr = :id_cliente_carr, id_producto_carr = :id_producto_carr,  orden_carr = :orden_carr, monto_carr = :monto_carr, rif_carr = :rif_carr, nombre_empresa_carr = :nombre_empresa_carr, direc_filcal_1_carr = :direc_filcal_1_carr, direc_filcal_2_carr = :direc_filcal_2_carr, telefono_otro_carr =  :telefono_otro_carr, tipo_presentacion_carr = :tipo_presentacion_carr, num_factura_ini_carr = :num_factura_ini_carr,   num_control_ini_carr = :num_control_ini_carr, cantidad_carr = :cantidad_carr, preferencia_impresion_carr = :preferencia_impresion_carr, preferencia_papel_carr = :preferencia_papel_carr, opciones_color_carr = :opciones_color_carr, img_rif_carr = :img_rif_carr, logo_carr = :logo_carr, tamano_logo_carr = :tamano_logo_carr WHERE id_carrito = :id_carrito");

				$stmt->bindParam(":id_cliente_carr", $datos["id_cliente"], PDO::PARAM_STR);
		    $stmt->bindParam(":id_carrito", $datos["id_carrito"], PDO::PARAM_STR);
		    $stmt->bindParam(":id_producto_carr", $datos["id_producto"], PDO::PARAM_STR);
		    $stmt->bindParam(":orden_carr", $datos["orden_carr"], PDO::PARAM_STR);
		    $stmt->bindParam(":monto_carr", $datos["Preciosend"], PDO::PARAM_STR);
		    $stmt->bindParam(":rif_carr", $datos["rifDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":nombre_empresa_carr", $datos["nomEmpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":direc_filcal_1_carr", $_POST["direDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":direc_filcal_2_carr", $_POST["dire2Doc"], PDO::PARAM_STR);
		    $stmt->bindParam(":telefono_otro_carr", $_POST["tlfDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":tipo_presentacion_carr", $datos["tipPreDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":num_factura_ini_carr", $datos["facIniDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":num_control_ini_carr", $datos["conIniDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":cantidad_carr", $datos["canImpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":preferencia_impresion_carr", $datos["preImpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":preferencia_papel_carr", $datos["prePapDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":opciones_color_carr", $datos["OpcColDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":img_rif_carr", $datos["imgsend"], PDO::PARAM_STR);
		    $stmt->bindParam(":logo_carr", $datos["logoDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":tamano_logo_carr", $datos["tamlogoUrl"], PDO::PARAM_STR);



		    if($stmt->execute()){

		      return 'ok';

		    }else{

		      return 'error';

		    }

		    $stmt -> close();

		    $stmt = null;
		}


		static public function MdlAddCarrito($tabla, $datos){

		    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_cliente_carr, id_producto_carr,  orden_carr, monto_carr , rif_carr, nombre_empresa_carr,direc_filcal_1_carr, direc_filcal_2_carr, telefono_otro_carr,tipo_presentacion_carr,num_factura_ini_carr,num_control_ini_carr,cantidad_carr,preferencia_impresion_carr,preferencia_papel_carr,opciones_color_carr,img_rif_carr,logo_carr,tamano_logo_carr) VALUES (:id_cliente_carr, :id_producto_carr,  :orden_carr, :monto_carr, :rif_carr, :nombre_empresa_carr,:direc_filcal_1_carr, :direc_filcal_2_carr, :telefono_otro_carr, :tipo_presentacion_carr,  :num_factura_ini_carr, :num_control_ini_carr,:cantidad_carr, :preferencia_impresion_carr, :preferencia_papel_carr, :opciones_color_carr, :img_rif_carr, :logo_carr, :tamano_logo_carr)");

		    $stmt->bindParam(":id_cliente_carr", $datos["id_cliente"], PDO::PARAM_STR);
		    $stmt->bindParam(":id_producto_carr", $datos["id_producto"], PDO::PARAM_STR);
		    $stmt->bindParam(":orden_carr", $datos["orden_carr"], PDO::PARAM_STR);
		    $stmt->bindParam(":monto_carr", $datos["Preciosend"], PDO::PARAM_STR);
		    $stmt->bindParam(":rif_carr", $datos["rifDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":nombre_empresa_carr", $datos["nomEmpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":direc_filcal_1_carr", $_POST["direDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":direc_filcal_2_carr", $_POST["dire2Doc"], PDO::PARAM_STR);
		    $stmt->bindParam(":telefono_otro_carr", $_POST["tlfDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":tipo_presentacion_carr", $datos["tipPreDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":num_factura_ini_carr", $datos["facIniDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":num_control_ini_carr", $datos["conIniDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":cantidad_carr", $datos["canImpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":preferencia_impresion_carr", $datos["preImpDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":preferencia_papel_carr", $datos["prePapDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":opciones_color_carr", $datos["OpcColDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":img_rif_carr", $datos["imgsend"], PDO::PARAM_STR);
		    $stmt->bindParam(":logo_carr", $datos["logoDoc"], PDO::PARAM_STR);
		    $stmt->bindParam(":tamano_logo_carr", $datos["tamlogoUrl"], PDO::PARAM_STR);



		    if($stmt->execute()){

		      return 'ok';

		    }else{

		      return 'error';

		    }

		    $stmt -> close();

		    $stmt = null;
		}

   		static public function MdlMostrarCarrito($valor,$detalle){

	     if($valor != null){

	       $stmt = Conexion::conectar() -> prepare("SELECT * FROM carrito WHERE $detalle = $valor");

	      $stmt -> execute();

	      return $stmt -> fetchAll();

	     }

		    $stmt -> close();

		    $stmt = null;

	 	}


	 	static public function mdlAgregarPedido($datos){


			$stmt = Conexion::conectar()->prepare("INSERT INTO pedidos(id_producto, orden, monto, fecha_emision, estado, rif, nombre_empresa, direc_filcal_1, direc_filcal_2, telefono_otro, tipo_presentacion, metodo_envio, num_factura_ini, num_control_ini, cantidad, preferencia_impresion, preferencia_papel, opciones_color, img_rif, logo, tamano_logo) VALUES (:id_producto, :orden, :monto, :fecha_emision, :estado, :rif, :nombre_empresa, :direc_filcal_1, :direc_filcal_2, :telefono_otro, :tipo_presentacion, :metodo_envio, :num_factura_ini, :num_control_ini, :cantidad, :preferencia_impresion, :preferencia_papel, :opciones_color, :img_rif, :logo, :tamano_logo)");


			$stmt->bindParam(":id_producto", $datos["id_producto"], PDO::PARAM_STR);
			$stmt->bindParam(":orden", $datos["orden"], PDO::PARAM_STR);
			$stmt->bindParam(":monto", $datos["monto"], PDO::PARAM_STR);
			$stmt->bindParam(":fecha_emision", $datos["fecha_emision"], PDO::PARAM_STR);
			$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
			$stmt->bindParam(":rif", $datos["rif"], PDO::PARAM_STR);
			$stmt->bindParam(":nombre_empresa", $datos["nombre_empresa"], PDO::PARAM_STR);
			$stmt->bindParam(":direc_filcal_1", $datos["direc_filcal_1"], PDO::PARAM_STR);
			$stmt->bindParam(":direc_filcal_2", $datos["direc_filcal_2"], PDO::PARAM_STR);
			$stmt->bindParam(":telefono_otro", $datos["telefono_otro"], PDO::PARAM_STR);
			$stmt->bindParam(":tipo_presentacion", $datos["tipo_presentacion"], PDO::PARAM_STR);
			$stmt->bindParam(":metodo_envio", $datos["metodo_envio"], PDO::PARAM_STR);
			$stmt->bindParam(":num_factura_ini", $datos["num_factura_ini"], PDO::PARAM_STR);
			$stmt->bindParam(":num_control_ini", $datos["num_control_ini"], PDO::PARAM_STR);
			$stmt->bindParam(":cantidad", $datos["cantidad"], PDO::PARAM_STR);
			$stmt->bindParam(":preferencia_impresion", $datos["preferencia_impresion"], PDO::PARAM_STR);
			$stmt->bindParam(":preferencia_papel", $datos["preferencia_papel"], PDO::PARAM_STR);
			$stmt->bindParam(":opciones_color", $datos["opciones_color"], PDO::PARAM_STR);
			$stmt->bindParam(":img_rif", $datos["img_rif"], PDO::PARAM_STR);
			$stmt->bindParam(":logo", $datos["logo"], PDO::PARAM_STR);
			$stmt->bindParam(":tamano_logo", $datos["tamano_logo"], PDO::PARAM_STR);

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();

			$stmt = null;
		}

		static public function hexadecimalAzar($caracteres){

		    $caracteresPosibles = "0123456789QWERTYUIOPASDFGHJKLZXCVBNM";
		    $azar = 0;
			do {
				$azar = '';
				$azar = 'QR-';
			    for($i=0; $i<$caracteres; $i++){

			        $azar .= $caracteresPosibles[rand(0,strlen($caracteresPosibles)-1)];

			    }


			} while (count(ModeloPedidos::MdlBuscarOrden($azar,'carrito','orden_carr')) != 0 && count(ModeloPedidos::MdlBuscarOrden($azar,'pedidos','orden')) != 0);
		    return $azar;

		}

	 	static public function MdlBuscarOrden($valor,$tabla,$detalle){

			if($valor != null){

				$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $detalle = $valor");

				$stmt -> execute();

				return $stmt -> fetchAll();

			}

			$stmt -> close();

		    return 0;
		}

		static public function MdlBorrarCarrito($valor){
			$stmt = Conexion::conectar()->prepare("DELETE FROM carrito WHERE id_carrito = :id_carrito");

			$stmt -> bindParam(":id_carrito", $valor, PDO::PARAM_INT);

			if($stmt -> execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();

			$stmt = null;

		}



	 	static public function mdlAgregarPuentePedido($idPago,$id_direc,$tipo_dp){


			$stmt = Conexion::conectar()->prepare("INSERT INTO dir_pedidos(id_pedido_dp, id_direccion_dp, tipo_dp) VALUES ($idPago, $id_direc, $tipo_dp)");

			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();

			$stmt = null;
		}
		static public function MdlUpdatePedido($valor){
			$tabla='pedidos';
		    $estado='Comprobar';

		    $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = :estado WHERE id_pedido = :id_pedido");


			$stmt -> bindParam(":id_pedido", $valor, PDO::PARAM_INT);

			$stmt->bindParam(":estado", $estado, PDO::PARAM_STR);



		    if($stmt->execute()){

		      return 'ok';

		    }else{

		      return 'error';

		    }

		    $stmt -> close();

		    $stmt = null;
		}

		static public function MdlMostrarPedidosEstado($valor,$estado){
	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT clientes.id_cliente, direcciones.id_direcciones, dir_pedidos.tipo_dp, pedidos.* FROM clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE clientes.id_cliente = :id_cliente AND pedidos.estado = :estado");

	 			$stmt -> bindParam(":id_cliente", $valor, PDO::PARAM_STR);
	 			$stmt -> bindParam(":estado", $estado, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();

	 		}

			$stmt -> close();

			$stmt = null;

		}

		/*=============================================
		CAMBIAR ESTADO PEDIDO
		=============================================*/

		static public function MdlCambiarEstado($tabla, $item, $valor){

			$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET estado = :estado WHERE id_pedido = :id_pedido");

			$stmt -> bindParam(":id_pedido", $item, PDO::PARAM_STR);
			$stmt -> bindParam(":estado", $valor, PDO::PARAM_STR);

			if($stmt -> execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();

			$stmt = null;

		}

}


?>
