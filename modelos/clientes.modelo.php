<?php 

require_once "conexion.php";

class ModeloClientes{

	/*=============================================
	REGISTRO DE CLIENTE
	=============================================*/

	static public function mdlRegistrarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(email, password, nombre, apellido, fecha_nacimiento, cedula, telefono_local, telefono_movil, estado) VALUES (:email, :password, :nombre, :apellido, :fecha_nacimiento, :cedula, :telefono_local, :telefono_movil, :estado)");

		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt->bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt->bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);
		$stmt->bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono_local", $datos["telefono_local"], PDO::PARAM_STR);
		$stmt->bindParam(":telefono_movil", $datos["telefono_movil"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt -> close();
		
		$stmt = null;
	}

	/*=============================================
	EDITAR DE CLIENTE
	=============================================*/

	static public function mdlEditarCliente($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET email = :email, password = :password,  nombre = :nombre, apellido = :apellido, fecha_nacimiento = :fecha_nacimiento, cedula = :cedula, telefono_local = :telefono_local, telefono_movil = :telefono_movil WHERE email = :email");

		$stmt -> bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
		$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
		$stmt -> bindParam(":apellido", $datos["apellido"], PDO::PARAM_STR);
		$stmt -> bindParam(":fecha_nacimiento", $datos["fecha_nacimiento"], PDO::PARAM_STR);
		$stmt -> bindParam(":cedula", $datos["cedula"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono_local", $datos["telefono_local"], PDO::PARAM_STR);
		$stmt -> bindParam(":telefono_movil", $datos["telefono_movil"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt -> close();
		
		$stmt = null;
		
	}
	static public function MdlNewPass($pass, $valor, $tabla){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET  password = :password WHERE id_cliente = :id_cliente");

		$stmt -> bindParam(":id_cliente", $valor, PDO::PARAM_STR);
		$stmt -> bindParam(":password", $pass, PDO::PARAM_STR);
		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt -> close();
		
		$stmt = null;
		


	}

	/*=============================================
	RANGO FECHAS
	=============================================*/	

	static public function mdlRangoFechasClientes($tabla, $fechaInicial, $fechaFinal){

		if($fechaInicial == $fechaFinal){

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha_ingreso like '%$fechaFinal%'");

			$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();


		}else{

			$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE fecha_ingreso BETWEEN '$fechaInicial' AND '$fechaFinal'");

			$stmt -> execute();

			return $stmt -> fetchAll();


		}
		
		$stmt -> close();

		$stmt = null;	

	}
	/*=============================================
	MOSTRAR CLIENTES
	=============================================*/

	static public function MdlMostrarClientes($tabla, $item, $valor){

 		if($item != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = :$item");

			$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetch();

 		}else{

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();
		
 			}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR CLIENTE
	=============================================*/

	static public function mdlMostrarCliente($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt-> close();

		$stmt = null;

	}

	/*=============================================
	ACTUALIZAR ESTADO CLIENTE
	=============================================*/

	static public function mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}
	static public function mdlRegistrarPreguntas($preg,$respuesta,$id_cliente_p){
		$tabla="Preguntas";
		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_cliente, pregunta, respuesta) VALUES (:id_cliente, :pregunta, :respuesta)");

		$stmt->bindParam(":id_cliente", $id_cliente_p, PDO::PARAM_STR);
		$stmt->bindParam(":pregunta", $preg, PDO::PARAM_STR);
		$stmt->bindParam(":respuesta", $respuesta, PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";	

		}else{

			return "error";
		
		}

		$stmt -> close();
		
		$stmt = null;
	}
	static public function MdlRecupera($id_cliente,$pregunta,$resRecupera){
		$tabla="Preguntas";
		$item="id_cliente";
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :id_cliente AND pregunta = :pregunta AND respuesta = :respuesta");

		$stmt -> bindParam(":id_cliente", $id_cliente, PDO::PARAM_STR);
		$stmt -> bindParam(":pregunta", $pregunta, PDO::PARAM_STR);
		$stmt -> bindParam(":respuesta", $resRecupera, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt-> close();

		$stmt = null;


	}

	


}

 ?>