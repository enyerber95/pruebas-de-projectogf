<?php

require_once "conexion.php";


class ModeloAuditoria{

    static public function mdlRegistrarBitacora($accion, $descripcion){
    $user_responsable = $_SESSION["usuario"];
    $nombre_responsable = $_SESSION["nombre"];
      $tabla = "Bitacora";
     $fecha_realizado = date("Y-m-d H:i:s");
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(user_responsable, nombre_responsable, accion, fecha_realizado, descripcion) VALUES (:user_responsable, :nombre_responsable, :accion, :fecha_realizado, :descripcion)");

    $stmt->bindParam(":user_responsable", $user_responsable, PDO::PARAM_STR);
    $stmt->bindParam(":nombre_responsable", $nombre_responsable, PDO::PARAM_STR);
    $stmt->bindParam(":accion", $accion, PDO::PARAM_STR);
    $stmt->bindParam(":fecha_realizado", $fecha_realizado, PDO::PARAM_STR);
    $stmt->bindParam(":descripcion", $descripcion, PDO::PARAM_STR);

    if($stmt->execute()){

      return "ok";

    }else{

      return "error";

    }

    $stmt -> close();

    $stmt = null;
  }
  static public function mdlRegistrarBitacora2($accion, $descripcion,$usuario,$nombreusuario){

     $fecha_realizado = date("Y-m-d H:i:s");
     $tabla="Bitacora";
    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(user_responsable, nombre_responsable, accion, fecha_realizado, descripcion) VALUES (:user_responsable, :nombre_responsable, :accion, :fecha_realizado, :descripcion)");

    $stmt->bindParam(":user_responsable", $usuario, PDO::PARAM_STR);
    $stmt->bindParam(":nombre_responsable", $nombreusuario, PDO::PARAM_STR);
    $stmt->bindParam(":accion", $accion, PDO::PARAM_STR);
    $stmt->bindParam(":fecha_realizado", $fecha_realizado, PDO::PARAM_STR);
    $stmt->bindParam(":descripcion", $descripcion, PDO::PARAM_STR);

    if($stmt->execute()){

      return "ok";

    }else{

      return "error";

    }

    $stmt -> close();

    $stmt = null;
  }

  static public function mdlMostrarBitacora(){

     $stmt = Conexion::conectar() -> prepare("SELECT * FROM Bitacora");

    $stmt -> execute();

    return $stmt -> fetchAll();
 
   }


  static public function mdlRangoFechasAuditoria($fechaInicial, $fechaFinal){
    if($fechaInicial == $fechaFinal){

       $stmt = Conexion::conectar() -> prepare("SELECT * FROM Bitacora WHERE fecha_realizado like '%$fechaFinal%'");

      $stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

      $stmt -> execute();

      return $stmt -> fetchAll();
    }else{

       $stmt = Conexion::conectar() -> prepare("SELECT * FROM Bitacora WHERE fecha_realizado BETWEEN '$fechaInicial' AND '$fechaFinal'");

      $stmt -> execute();

      return $stmt -> fetchAll();
    }

    $stmt -> close();

    $stmt = null;

  }
}
?>
