<?php

require_once "conexion.php";

	class ModeloReportes{

		/*=============================================
		MOSTRAR REPORTES
		=============================================*/

		static public function MdlMostrarReportes($tabla){

	 		if($tabla != null){
	 			$stmt = Conexion::conectar() -> prepare("SELECT reporte_pago.*, clientes.nombre, clientes.apellido, pedidos.orden FROM reporte_pago, clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE  dir_pedidos.tipo_dp = 1 and pedidos.id_pedido = reporte_pago.id_pedido ORDER BY orden ASC");

				$stmt -> execute();

				return $stmt -> fetchAll();

	 		}

			$stmt -> close();

			$stmt = null;

		}



		/*=============================================
		MOSTRAR REPORTES FECHAS
		=============================================*/

		static public function mdlRangoFechasReportes($fechaInicial, $fechaFinal){

			if($fechaInicial == $fechaFinal){

	 			$stmt = Conexion::conectar() -> prepare("SELECT reporte_pago.*, clientes.nombre, clientes.apellido, pedidos.orden FROM reporte_pago, clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE  dir_pedidos.tipo_dp = 1 and pedidos.id_pedido = reporte_pago.id_pedido and fecha_reporte like '%$fechaFinal%' ORDER BY orden ASC");

				$stmt -> bindParam(":fecha", $fechaFinal, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetchAll();
			}else{

	 			$stmt = Conexion::conectar() -> prepare("SELECT reporte_pago.*, clientes.nombre, clientes.apellido, pedidos.orden FROM reporte_pago, clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE  dir_pedidos.tipo_dp = 1 and pedidos.id_pedido = reporte_pago.id_pedido and fecha_reporte BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY orden ASC");

				$stmt -> execute();

				return $stmt -> fetchAll();
			}

			$stmt -> close();

			$stmt = null;

		}

		/*=============================================
		MOSTRAR DETALLES REPORTE
		=============================================*/

		static public function mdlMostrarDetalleReportes($valor){

	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT reporte_pago.*, clientes.*, pedidos.orden FROM reporte_pago, clientes INNER JOIN direcciones ON clientes.id_cliente = direcciones.id_cliente INNER JOIN dir_pedidos ON direcciones.id_direcciones = dir_pedidos.id_direccion_dp INNER JOIN pedidos ON dir_pedidos.id_pedido_dp = pedidos.id_pedido WHERE dir_pedidos.tipo_dp = 1 and reporte_pago.id_pedido = $valor and reporte_pago.id_pedido= pedidos.id_pedido");

	 			$stmt -> bindParam(":reporte_pago", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetch();

	 		}

			$stmt -> close();

			$stmt = null;

		}

		/*=============================================
		MOSTRAR DETALLE del REPORTE
		=============================================*/

		static public function mdlMostrarDetalleDelReporte($valor){

	 		if($valor != null){

	 			$stmt = Conexion::conectar() -> prepare("SELECT fecha_reporte FROM reporte_pago WHERE id_reporte_pago = :reporte_pago");

	 			$stmt -> bindParam(":reporte_pago", $valor, PDO::PARAM_STR);

				$stmt -> execute();

				return $stmt -> fetch();

	 		}

			$stmt -> close();

			$stmt = null;

		}



		static public function MdlRegistrarReporte($datos,$tabla){

			if($datos != null){

		    $stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_pedido, control, banco_emisor, banco_receptor, num_deposito_transferencia, monto_final, fecha_reporte) VALUES (:id_pedido, :control, :banco_emisor, :banco_receptor, :num_deposito_transferencia, :monto_final, :fecha_reporte)");


		    $stmt->bindParam(":id_pedido", $datos["id_pedido"], PDO::PARAM_STR);
		    $stmt->bindParam(":control", $datos["control"], PDO::PARAM_STR);
		    $stmt->bindParam(":banco_emisor", $datos["banco_emisor"], PDO::PARAM_STR);
		    $stmt->bindParam(":banco_receptor", $datos["banco_receptor"], PDO::PARAM_STR);
		    $stmt->bindParam(":num_deposito_transferencia", $datos["num_deposito_transferencia"], PDO::PARAM_STR);
		    $stmt->bindParam(":monto_final", $datos["monto_final"], PDO::PARAM_STR);
		    $stmt->bindParam(":fecha_reporte", $datos["fecha_reporte"], PDO::PARAM_STR);

		    if($stmt->execute()){

		      return "ok";

		    }else{

		      return "error";

		    }

		    $stmt -> close();

		    $stmt = null;
		  }
		}
}



?>
