<?php

require_once "conexion.php";

class ModeloProductos{

	/*=============================================
	MOSTRAR PREFERENCIAS
	=============================================*/

	static public function MdlMostrarPreferencias(){

 		$stmt = Conexion::conectar() -> prepare("SELECT * FROM preferencias");

		$stmt -> execute();

		return $stmt -> fetch();

 	}
 
	/*=============================================
	MOSTRAR PRECIO PRODUCTO (CPANEL)
	=============================================*/

	static public function MdlMostrarPrecioProducto($tabla, $item, $valor){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = $valor");

			$stmt -> execute();

			return $stmt -> fetch();

 		}

 	}
 	/*=============================================
	MOSTRAR IMAGEN PRODUCTO (CPANEL)
	=============================================*/

	static public function MdlMostrarDetalleProducto($tabla, $item, $valor, $detalle){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT $detalle FROM $tabla WHERE $item = $valor");

			$stmt -> execute();

			return $stmt -> fetch();

 		}

 	}



	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function MdlMostrarProducto($tabla, $item, $valor){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item = :$item");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt-> close();

		$stmt = null;

	}

 	/*=============================================
	MOSTRAR PRODUCTOS (CPANEL)
	=============================================*/

	static public function MdlMostrarProductosCpanel($tabla){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla ORDER BY id_producto ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

 		}

 	}


 	/*=============================================
	MOSTRAR TODOS PRODUCTOS
	=============================================*/

	static public function MdlMostrarTodosProductos($tabla, $valor){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE categoria = $valor ORDER BY id_producto ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();

 		}

 	}

 	/*=============================================
	EDITAR PRECIO PRODUCTO
	=============================================*/

 	static public function MdlEditarPrecioProducto($tabla, $item, $valor){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("UPDATE $tabla SET precio_base = :precio_base WHERE id_producto = :id_producto");

			$stmt -> bindParam(":id_producto", $valor, PDO::PARAM_STR);
			$stmt -> bindParam(":precio_base", $item, PDO::PARAM_STR);


			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();
			$stmt = null;

 		}

 	}

 	/*=============================================
	EDITAR PREFERENCIAS
	=============================================*/

 	#static public function MdlEditarPreferencias($tabla, $datos){
 	static public function MdlEditarPreferencias($tabla, $id, $p_bond, $p_quimico, $blanco_negro, $full_color, $o_1, $o_2, $o_3, $iva){

 		if($tabla != null){
 			$stmt = Conexion::conectar() -> prepare("UPDATE $tabla SET p_bond = :p_bond, p_quimico = :p_quimico, blanco_negro = :blanco_negro, full_color = :full_color, o_1 = :o_1, o_2 = :o_2, o_3 = :o_3, iva = :iva WHERE id_preferencias = :id");

 			$stmt -> bindParam(":id", $id, PDO::PARAM_STR);
			$stmt -> bindParam(":p_bond", $p_bond, PDO::PARAM_STR);
			$stmt -> bindParam(":p_quimico", $p_quimico, PDO::PARAM_STR);
			$stmt -> bindParam(":blanco_negro", $blanco_negro, PDO::PARAM_STR);
			$stmt -> bindParam(":full_color", $full_color, PDO::PARAM_STR);
			$stmt -> bindParam(":o_1", $o_1, PDO::PARAM_STR);
			$stmt -> bindParam(":o_2", $o_2, PDO::PARAM_STR);
			$stmt -> bindParam(":o_3", $o_3, PDO::PARAM_STR);
			$stmt -> bindParam(":iva", $iva, PDO::PARAM_STR);


			if($stmt->execute()){

				return "ok";

			}else{

				return "error";

			}

			$stmt -> close();
			$stmt = null;

 		}

 	}
 	



 	}

?>
