<?php

require_once "conexion.php";

class ModeloDirecciones{

	/*=============================================
	MOSTRAR ESTADOS
	=============================================*/

	static public function MdlMostrarEstados($tabla){

 		if($tabla != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla ORDER BY nombre_estado ASC ");

			$stmt -> execute();

			return $stmt -> fetchAll();

 		}

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	AGREGAR DIRECCIÓN
	=============================================*/

	static public function mdlAgregarDireccion($tabla, $datos){


		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(id_cliente, d_titulo, d_nombre_empresa, sector_urbanizacion, avenida_calle, edificio_quinta_casa, d_telefono, informacion_adicional, estado, ciudad) VALUES (:id_cliente, :d_titulo, :d_nombre_empresa, :sector_urbanizacion, :avenida_calle, :edificio_quinta_casa, :d_telefono, :informacion_adicional, :estado, :ciudad)");

		$stmt->bindParam(":id_cliente", $datos["id_cliente"], PDO::PARAM_STR);
		$stmt->bindParam(":d_titulo", $datos["d_titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":d_nombre_empresa", $datos["d_nombre_empresa"], PDO::PARAM_STR);
		$stmt->bindParam(":d_titulo", $datos["d_titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":sector_urbanizacion", $datos["sector_urbanizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":avenida_calle", $datos["avenida_calle"], PDO::PARAM_STR);
		$stmt->bindParam(":edificio_quinta_casa", $datos["edificio_quinta_casa"], PDO::PARAM_STR);
		$stmt->bindParam(":d_telefono", $datos["d_telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":informacion_adicional", $datos["informacion_adicional"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
		$stmt->bindParam(":ciudad", $datos["ciudad"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	ACTUALIZAR DIRECCIÓN
	=============================================*/

	static public function mdlActualizarDireccion($tabla, $datos){
		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET d_titulo = :d_titulo, d_nombre_empresa = :d_nombre_empresa, sector_urbanizacion = :sector_urbanizacion, avenida_calle = :avenida_calle, edificio_quinta_casa = :edificio_quinta_casa, d_telefono = :d_telefono, informacion_adicional = :informacion_adicional, estado = :estado, ciudad = :ciudad WHERE id_direcciones = :id_direcciones");

		$stmt->bindParam(":id_direcciones", $datos["id_direcciones"], PDO::PARAM_STR);
		$stmt->bindParam(":d_titulo", $datos["d_titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":d_nombre_empresa", $datos["d_nombre_empresa"], PDO::PARAM_STR);
		$stmt->bindParam(":d_titulo", $datos["d_titulo"], PDO::PARAM_STR);
		$stmt->bindParam(":sector_urbanizacion", $datos["sector_urbanizacion"], PDO::PARAM_STR);
		$stmt->bindParam(":avenida_calle", $datos["avenida_calle"], PDO::PARAM_STR);
		$stmt->bindParam(":edificio_quinta_casa", $datos["edificio_quinta_casa"], PDO::PARAM_STR);
		$stmt->bindParam(":d_telefono", $datos["d_telefono"], PDO::PARAM_STR);
		$stmt->bindParam(":informacion_adicional", $datos["informacion_adicional"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
		$stmt->bindParam(":ciudad", $datos["ciudad"], PDO::PARAM_STR);

		if($stmt->execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;
	}

	/*=============================================
	MOSTRAR DIRECCIONES CLIENTE PARTICULAR
	=============================================*/

	static public function MdlMostrarDirecciones($tabla, $item, $valor){

 		$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = $valor");

		$stmt -> bindParam(":".$item, $valor, PDO::PARAM_STR);

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR DIRECCIÓN (UNA SOLA)
	=============================================*/

	static public function MdlMostrarDireccion($tabla, $item, $valor){

 		if($item != null){

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla WHERE $item = $valor");

			$stmt -> execute();

			return $stmt -> fetch();

 		}else{

 			$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla");

			$stmt -> execute();

			return $stmt -> fetchAll();

 			}

		$stmt -> close();

		$stmt = null;

	}


	/*=============================================
	BORRAR DIRECCIÓN
	=============================================*/

	static public function mdlBorrarDireccion($tabla, $valor){

		$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE id_direcciones = :id_direcciones");

		$stmt -> bindParam(":id_direcciones", $valor, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;


	}
	static public function mdlBuscaPedidos($tabla2, $valor){

		$stmt = Conexion::conectar() -> prepare("SELECT * FROM $tabla2 WHERE id_direccion = :id_direccion");

		$stmt -> bindParam(":id_direccion", $valor, PDO::PARAM_INT);

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt -> close();

		$stmt = null;


	}


}

 ?>
