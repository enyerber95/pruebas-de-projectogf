

<?php
Class ControladorDirecciones
{


	/*==========================================
	=     REGISTRAR NUEVA DIRECCIÓN            =
	==========================================*/
	static public

	function ctrAgregarDireccion($action){
		if (isset($_POST["idCliente"])) {
			if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["clienteCorreo"])) {
				$tabla = "direcciones";
				$datos = array(
					"id_cliente" => $_POST["idCliente"],
					"d_titulo" => $_POST["nuevoTitulo"],
					"d_nombre_empresa" => $_POST["nuevoRazonSoc"],
					"sector_urbanizacion" => $_POST["nuevoSU"],
					"avenida_calle" => $_POST["nuevoAC"],
					"edificio_quinta_casa" => $_POST["nuevoEQC"],
					"d_telefono" => $_POST["nuevoTelf"],
					"informacion_adicional" => $_POST["nuevoIA"],
					"estado" => $_POST["nuevoEstado"],
					"ciudad" => $_POST["nuevoCiudad"]
				);
				$respuesta = ModeloDirecciones::mdlAgregarDireccion($tabla, $datos);


				if ($respuesta == "ok") {
					echo '<script>
							swal({
								title: "¡OK!",
								text: "¡Su dirección ha sido agregada satisfactoriamente!",
								type:"success",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="'.$action.'";

								}
								})

							</script>';
				}
				else {
					echo '<script>

						swal({
							title: "¡ERROR!",
							text: "Error al agregar su dirección, revisa los datos introducidos y vuelve a intentarlo.",
							type:"error",
						  		showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){
								if(isConfirm){
									window.location = "direcciones";

								}
								});

								</script>';
				}
			}
			else {
				echo '<script>

						swal({
							title: "¡ERROR!",
							text: "Error al agregar su dirección, revisa los datos introducidos y vuelve a intentarlo.",
							type:"error",
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){
								if(isConfirm){
									window.location = "direcciones";
								}
								});

								</script>';
			}
		}
	}

	/*==========================================
	=     ACTUALIZAR DIRECCIÓN                 =
	==========================================*/
	static public

	function ctrActualizarDireccion()
	{
			unset($datos);

			if (isset($_POST["direccionID"])) {
			$tabla = "direcciones";
			$datos = array(
				"id_direcciones" => $_POST["direccionID"],
				"d_titulo" => $_POST["editarTitulo"],
				"d_nombre_empresa" => $_POST["editarRazonSoc"],
				"sector_urbanizacion" => $_POST["editarSU"],
				"avenida_calle" => $_POST["editarAC"],
				"edificio_quinta_casa" => $_POST["editarEQC"],
				"d_telefono" => $_POST["editarTelf"],
				"informacion_adicional" => $_POST["editarIA"],
				"estado" => $_POST["editarEstado"],
				"ciudad" => $_POST["editarCiudad"]
			);
			$respuesta = ModeloDirecciones::mdlActualizarDireccion($tabla, $datos);
			if ($respuesta == "ok") {
				echo '<script>

					swal({
						title: "¡OK!",
						text: "¡Su dirección ha sido actualizada satisfactoriamente!",
						type:"success",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},

						function(isConfirm){
							if(isConfirm){
								window.location = "direcciones";

							}
							});

						</script>';
			}
			else {
				echo '<script>

					swal({
						title: "¡ERROR!",
						text: "Error al actualizar su dirección, revisa los datos introducidos y vuelve a intentarlo.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},

						function(isConfirm){
							if(isConfirm){
								window.location = "direcciones";
							}
							});

						</script>';
			}
			}
	}

	/*==========================================
	=           MOSTRAR DIRECIONES             =
	==========================================*/
	static public

	function ctrMostrarDirecciones($item, $valor)
	{
		$tabla = "direcciones";
		$respuesta = ModeloDirecciones::MdlMostrarDirecciones($tabla, $item, $valor);
		return $respuesta;
	}

	/*==========================================
	=           MOSTRAR DIRECIÓN               =
	==========================================*/
	static public

	function ctrMostrarDireccion($item, $valor)
	{
		$tabla = "direcciones";
		$respuesta = ModeloDirecciones::MdlMostrarDireccion($tabla, $item, $valor);
		return $respuesta;
	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/
	static public

	function ctrEliminarDireccion()
	{
		$tabla = "direcciones";
		$tabla2 = "pedidos";
		$delet=true;

		// verifico si hay id a eliminar

		if (isset($_GET["idDireccion2"])){
			$valor = $_GET["idDireccion2"];

			// verifico que no tenga pedidos

			$cantidadPedido = count($buscarPedidos = ModeloPedidos::MdlMostrarPedidos($valor));
			if ($cantidadPedido == 0) {

				// elimino direccion y mando comprobación

				$respuesta = ModeloDirecciones::mdlBorrarDireccion($tabla, $valor);
				if ($respuesta === "ok") { //si fue exitoso notifico
					echo '<script>

						swal({
							  type: "success",
							  title: "La dirección ha sido borrada correctamente",
							  showConfirmButton: true,
							  confirmButtonColor: "#018ba7",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							  }).then((result) => {
										if (result.value) {

										window.location ="direcciones";

										}
									})

						</script>';
				}
				else {//si NO fue exitoso notifico
					echo '<script>

					swal({
						  type: "error",
						  title: "error al borrar la direccion",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

					</script>';
				}
			}
			else {

				// valido el estado de todos los pedidos de esta direccion

				foreach($buscarPedidos as $key => $value) {
					$id_pedido_dp = $value["id_pedido_dp"];
					$item = "id_pedido";
					$detalle = "estado";
					$estadoPed = ModeloPedidos::mdlItemPedido($id_pedido_dp, $item, $detalle);
					$de = $estadoPed[0];
					$msj=false;

					if ($de == "Comprobar" || $de == "Verificado") {
						#CAMBIO VARIABLE DE CONTROL, LA DIRECCION YA NO SE PUEDE ELIMINAR
						$delet = false;
					}
					#PREGUNTO AL USUARIO SI ESTA SEGURO QUE NECESITA BORRAR DICHA DIRECCION
					if($de == 'Pendiente') {
						$delet= false;
						$msj=true;
							echo '<script>

						swal({
						  type: "warning",
						  title: "Tiene Pedidos Pendientes o verificados ligados a esta direccion<br /> ¿Estas seguro que deseas eliminar?",
						  showConfirmButton: true,
						  showCancelButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Borrar de todos modos",
      					  cancelButtonText: "Cancelar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {
      								window.location = "index.php?ruta=direcciones&idDireccionD="+'.$valor.';


									}
								})

						</script>';

						# code...
					}
					if ($de == 'Rechazado') {
						# code...
						$ped_rechazados_elim[]=$id_pedido_dp;
					}


				}

				if($delet == true){
					$res = ModeloDirecciones::mdlBorrarDireccion($tabla, $valor);
						if ($res == "ok") {
							foreach ($ped_rechazados_elim as $key => $value) {

									$res2 = ModeloPedidos::mdlBorrarPedido($tabla2, $value);
							}

						}else{
							$res2 = "error";
						}

					if ($res2 == "ok") {
							# code...
						echo '<script>

						swal({
						  type: "warning",
						  title: "ELIMINADO EXITOSAMENTE",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

						</script>';

						}else{
							echo '<script>

						swal({
						  type: "warning",
						  title: "NO SE PUDO ELIMINAR",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

						</script>';
						die();

						}

					}
					if($msj==true){
							echo '<script>

						swal({
						  type: "warning",
						  title: "Tiene Pedidos en proceso de comprobanción,<br /> no es posible eliminar la dirección",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

						</script>';

					}
					//mensajes segun resultados
				}
			}


		if (isset($_GET["idDireccionD"])){
			$dir_delet = $_GET["idDireccionD"];
			$buscarPedidos = ModeloPedidos::MdlMostrarPedidos($dir_delet);
			///var_dump($buscarPedidos);


			foreach ($buscarPedidos as $key => $value) {
				foreach ($value as $key => $values) {
					# code...
					$res2 = ModeloPedidos::mdlBorrarPedido($tabla2, $values);
				}
			}

			$res = ModeloDirecciones::mdlBorrarDireccion($tabla, $dir_delet);


			if ($res2 == "error") {
				# code...
				echo '<script>

						swal({
						  type: "error",
						  title: "error al borrar la direccion",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

						</script>';
			}else{
				echo '<script>

						swal({
						  type: "success",
						  title: "Oprecion realizada con exito",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false
						  }).then((result) => {
									if (result.value) {

									window.location ="direcciones";

									}
								})

						</script>';

			}


		}

	}
}

?>
