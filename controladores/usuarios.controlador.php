<?php


class ControladorUsuarios
{

		/*==========================================
		=            INGRESO DE USUARIO            =
		==========================================*/

		static public function ctrIngresoUsuario(){

			if(isset($_POST["ingUsuario"])){

				if(preg_match('/^[a-zA-Z0-9@._]+$/', $_POST["ingUsuario"]) &&
			  	   preg_match('/^[a-zA-Z0-9]+$/', $_POST["ingPassword"])){

					$tabla = "usuarios";
					$item = "usuario";
					$valor = $_POST["ingUsuario"];

					$respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);

					$verifica=$_POST["ingPassword"];
					$pass=$respuesta["password"];
					if($respuesta["usuario"] == $_POST["ingUsuario"]){
					if (password_verify($verifica, $pass)) {


						if($respuesta["estado"] == 1){

						$_SESSION["iniciarSesion"] = "ok";
						$_SESSION["id"] = $respuesta["id"];
						$_SESSION["nombre"] = $respuesta["nombre"];
						$_SESSION["usuario"] = $respuesta["usuario"];
						$_SESSION["acceso"] = $respuesta["acceso"];
						$_SESSION["passwordpanel"] = $respuesta["password"];

						/*==========================================
						=            INGRESO DE USUARIO            =
						==========================================*/

						date_default_timezone_set('America/Caracas');

						$fecha = date('Y-m-d');
						$hora = date('H:i:s');

						$fechaActual =  $fecha.' '.$hora;

						$item1 = "ultimo_login";
						$valor1 = $fechaActual;

						$item2 = "id";
						$valor2 = $respuesta["id"];

						$ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);
						$accion='Inicio de sesion';
						$descripcion="el usuario".$_SESSION["usuario"]." ha ingresado al sistema";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);
						


						if($ultimoLogin == "ok"){

							echo '<script>

								window.location = "inicio";

							</script>';

						}

					}else{

						echo '<br><div class="alert alert-danger" style="margin-bottom:5px;">El usuario aún no está activado.</div>';

					}

					}
					}else{

						echo '<br><div class="alert alert-danger" style="margin-bottom:5px;">Error al ingresar, vuelva a intentarlo.</div>';

					}

				}

			}

		}


		/*==========================================
		=           REGISTRO DE USUARIO            =
		==========================================*/

		static public function ctrCrearUsuario(){

			if(isset($_POST["nuevoUsuario"])){

				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["nuevoNombre"]) &&
				   preg_match('/^[a-zA-Z0-9]+$/', $_POST["nuevoUsuario"])){

					$pass = $_POST["nuevoPassword"];
					$opciones = ['cost' => 10, ];
					$pass_hash = password_hash($pass, PASSWORD_BCRYPT, $opciones);

					$tabla = "usuarios";

					$datos = array("nombre" => $_POST["nuevoNombre"],
								   "usuario" => $_POST["nuevoUsuario"],
								   "password" => $pass_hash,
								   "departamento" => $_POST["nuevoDepartamento"],
								   "acceso" => $_POST["nuevoAcceso"],
								   "estado" => 1
								   );
					$respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos);
					if($respuesta == "ok"){
						$accion="Usuario Registrado";
						$descripcion="nuevo miembro ".$_POST["nuevoUsuario"]."registrado con exito";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);

						echo '<script>

							swal({

								type: "success",
								title: "¡El usuario ha sido guardado correctamente!",
								showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

							}).then((result)=>{

								if(result.value){

									window.location = "usuarios";

								}

							});

							</script>';

					}

				}else{
						$accion="Usuario Registrado";
						$descripcion="nuevo miembro registrado con exito";
						$descripcion="Fallo en conexión, error al registrar ".$_POST["nuevoUsuario"]."";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);

					echo '<script>

					swal({

							type: "error",
							title: "¡El usuario no puede ir vacío o llevar caracteres especiales!",
							showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false

						}).then((result)=>{

							if(result.value){

								window.location = "usuarios";

							}

						});

					</script>';

				}

			}

		}


   /*==========================================
	=           MOSTRAR USUARIO               =
	==========================================*/

		static public function ctrMostrarUsuarios($item, $valor){

			$tabla = "usuarios";

			$respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);

			return $respuesta;
		}


	/*==========================================
	=           EDITAR USUARIO               =
	==========================================*/

		static public function ctrEditarUsuario(){
			$decision='';
			$passNoModificado='';
			if(isset($_POST["editarUsuario"])){

				if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/', $_POST["editarNombre"])){

					$tabla = "usuarios";

					if($_POST["editarPassword"] != ""){

						if(preg_match('/^[a-zA-Z0-9]+$/', $_POST["editarPassword"])){

								$pass = $_POST["editarPassword"];
								$opciones = ['cost' => 10, ];
								$passModificado = password_hash($pass, PASSWORD_BCRYPT, $opciones);
								$decision = 1;


						}else{

							echo '<script>

									swal({

											type: "error",
											title: "¡El usuario no puede ir vacío o llevar caracteres especiales!",
											showConfirmButton: true,
											confirmButtonColor: "#018ba7",
											confirmButtonText: "Cerrar",
											closeOnConfirm: false

										}).then((result)=>{

											if(result.value){

												window.location = "usuarios";

											}

										});

								</script>';

						}

					}else{
					$valor=$_POST["editarUsuario"];

					$tabla = "usuarios";
					$item = "usuario";

					$respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);
					$passNoModificado = $respuesta['password'];
					
					$decision = 0;

				}


				/*  CONDICIONAL PARA LLENAR EL ARRAY CON EL PASSWORD CORRECTO  */

				if($decision == 1){

					$datos = array("nombre" => $_POST["editarNombre"],
							   "password" => $passModificado,
							   "departamento" => $_POST["editarDepartamento"],
							   "usuario" => $_POST["editarUsuario"],
							   "acceso" => $_POST["editarAcceso"]);

				}else{ 

					$datos = array("nombre" => $_POST["editarNombre"],
							   "password" => $passNoModificado,
							   "departamento" => $_POST["editarDepartamento"],
							   "usuario" => $_POST["editarUsuario"],
							   "acceso" => $_POST["editarAcceso"]);

				}


				$respuesta = ModeloUsuarios::mdlEditarUsuario($tabla, $datos);
				if($respuesta == "ok"){

						$accion="Usuario Editado";
						$descripcion="El usuario: ".$_POST["editarUsuario"]." ha sido editado exitosamente";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);

					echo'<script>

					swal({
						  type: "success",
						  title: "El usuario ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false

						  }).then((result) => {
									if (result.value) {

									window.location = "usuarios";

									}
								})

					</script>';

				}

			}else{
				$accion="Usuario Editado";
						$descripcion="ERROR usuario: ".$_POST["editarUsuario"]." NO ha podido editarse";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);


				echo'<script>

					swal({
						  type: "error",
						  title: "¡El nombre no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false

						  }).then((result) => {
							if (result.value) {

							window.location = "usuarios";

							}
						})

			  	</script>';

			}

    	}

	}

	/*=============================================
	BORRAR USUARIO
	=============================================*/

	static public function ctrBorrarUsuario(){
		if(isset($_GET["id"])){
			$id=$_GET["id"];
			$ararydata = explode("-", $id);
				if ($ararydata[0] == "idUsuario") {
					if ($ararydata[1] == $_SESSION["id"]) {

						echo'<script>

						swal({
							  type: "error",
							  title: "No puedes borrarte a ti mismo",
							  showConfirmButton: true,
							  confirmButtonColor: "#018ba7",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							  }).then((result) => {
										if (result.value) {

										window.location = "usuarios";

										}
									})

						</script>';

						# code...
					}else{
					# code...
					
					$idx=$ararydata[1];
					$item="id";
					$tabla ="usuarios";
					$user=ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$idx);
					$respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $idx);
					if($respuesta == "ok"){
						$accion="Usuario eliminado";
						$descripcion="El usuario: ".$user["usuario"]." ha sido eliminado exitosamente";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);


						echo'<script>

						swal({
							  type: "success",
							  title: "El usuario ha sido borrado correctamente",
							  showConfirmButton: true,
							  confirmButtonColor: "#018ba7",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							  }).then((result) => {
										if (result.value) {

										window.location = "usuarios";

										}
									})

						</script>';

					}else{

						$item="id";
						$tabla ="usuarios";
						$user=ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$idx);
						$accion="Usuario eliminado";
						$descripcion="ERROR usuario: ".$user["usuario"]." NO ha podido eliminarse";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);
						echo'<script>

						swal({
							  type: "error",
							  title: "ERROR<br>No se ha podido eliminar el usuario ,
							  showConfirmButton: true,
							  confirmButtonColor: "#018ba7",
							  confirmButtonText: "Cerrar",
							  closeOnConfirm: false
							  }).then((result) => {
										if (result.value) {

										window.location = "usuarios";

										}
									})

						</script>';

					}
				}
			}
		}

	}
	static public function ctrlCambioEstado($valor,$nombreusuario,$usuario,$accion){

		$item="id";
		$tabla ="usuarios";
		$user=ModeloUsuarios::MdlMostrarUsuarios($tabla,$item,$valor);
		$descripcion="Estado de  usuario:".$user["usuario"]." Modificado";
		$accion="Estado usuario";
		$respuesta=ModeloAuditoria::mdlRegistrarBitacora2($accion,$descripcion,$usuario,$nombreusuario);
		var_dump($respuesta);
		var_dump($descripcion);
	}

}

?>
