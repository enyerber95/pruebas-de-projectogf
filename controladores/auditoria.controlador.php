<?php 
Class ControladorAuditoria
{
	static public function ctrMostrarBitacora(){
		$respuesta = ModeloAuditoria::mdlMostrarBitacora();
		return $respuesta;
	}
	
	static public function ctrGeneraRespaldo($usuariorespaldo, $nombreusu){

			$db_host ='localhost';
			$db_name ='proyectogf';
			$db_user ='rootuser';
			$db_pass ='1234';

			$fecha = date("Ymd_His");
			$salida_sql=$db_name.'_'.$fecha.'.sql';
			$command = 'C:\xampp\mysql\bin\mysqldump --opt -u '.$db_user.' -p'.$db_pass.' '.$db_name.' >'.$salida_sql.'';

			//PARA LINUX CAMBIAR POR ESTE///
			$dump = "mysqldump -h".$db_host." -u".$db_user." -p".$db_pass." --opt ".$db_name." > ".$salida_sql."";
			//---------------------------//

			system($command, $output);
			$zip = new ZipArchive();
			$salida_zip = $db_name.'_'.$fecha.'.zip';

			if ($zip->open($salida_zip, ZipArchive::CREATE) == true){
				$zip->addFile($salida_sql);
				$zip->close();

				$fuente='"'.$salida_zip.'"';
				$destino='"../respaldos/'.$fuente.'"';
				$copiacomando='copy '.$fuente.', '.$destino.'';
				//Copia para windows//
				system($copiacomando, $output);
				//-----------------------------------------//
					$accion='Generar Respaldo';
					$descripcion='Respaldo Generado Exitosamente';
					$bitResult = ModeloAuditoria::mdlRegistrarBitacora2($accion, $descripcion,$usuariorespaldo,$nombreusu);
					unlink($salida_sql);
					unlink($salida_zip);

				return $salida_zip;
			} 

	}
	public static function ctrConectarRestore(){
		return mysqli_connect("localhost", "rootuser", "1234", "proyectogf");
	
	}




function restoreMysqlDB($filePath)
{
$conn = mysqli_connect("localhost", "rootuser", "1234", "proyectogf");

    $sql = '';
    $error = '';
    
    if (file_exists($filePath)) {
        $lines = file($filePath);
        
        foreach ($lines as $line) {
            
            // Ignoring comments from the SQL script
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }
            
            $sql .= $line;
            
            if (substr(trim($line), - 1, 1) == ';') {
                $result = mysqli_query($conn, $sql);
                if (! $result) {
                    $error .= mysqli_error($conn) . "\n";

                }
                $sql = '';
            }
        } // end foreach
        
        if ($error) {
           echo '<script>
    				swal({
							title: "¡Ha ocurrido un problema!",
							text: "¡Por favor Verifique el tipo de archivo!",
							type:"error",
					  		showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false

							  }).then((result) => {
								if (result.value) {

								window.location ="auditoria";

								}
						})
    			</script>';
        } else {
        	echo '<script>
    				swal({
									title: "¡Respaldo Restaurado!",
									text: "¡Por favor cancele el monto el pedido para relizar el envío!",
									type:"success",
							  		showConfirmButton: true,
									confirmButtonColor: "#018ba7",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false

									  }).then((result) => {
										if (result.value) {

										window.location ="auditoria";

										}
							})
    			</script>';
            
        }
    }
    	 // end if file exists
    
}

	
}

 ?>