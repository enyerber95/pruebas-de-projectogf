<?php


class ControladorProductos{

	/*==========================================
	=           MOSTRAR PRODUCTOS             =
	==========================================*/

		static public function ctrMostrarProductos(){

			$tabla = "productos";

			$respuesta = ModeloProductos::MdlMostrarProductosCpanel($tabla);

			return $respuesta;
		}


	/*==========================================
	=           MOSTRAR PRODUCTOS             =
	==========================================*/

		static public function ctrMostrarTodosProductos($valor){

			$tabla = "productos";

			$respuesta = ModeloProductos::MdlMostrarTodosProductos($tabla, $valor);

			return $respuesta;
		}
	/*==========================================
	=           MOSTRAR IMAGEN PRODUCTOS             =
	==========================================*/

		static public function ctrMostrarDetalleProductos($valor,$detalle){
			$tabla="productos";
			$item="id_producto";

			$respuesta = ModeloProductos::MdlMostrarDetalleProducto($tabla, $item, $valor,$detalle);
			
			return $respuesta[0];
		}


	/*==========================================
	=           MOSTRAR IMAGEN PRODUCTOS             =
	==========================================*/


		static public function ctrMostrarInfoProducto($valor){

			$tabla = "productos";
			$item="id_producto";

			$respuesta = ModeloProductos::MdlMostrarProducto($tabla, $item, $valor);

			return $respuesta;

		}

	/*==========================================
	=       EDITAR PRECIO PRODUCTO             =
	==========================================*/

		static public function ctrEditarPrecioProducto(){

			if(isset($_POST["editarPrecioVenta"])){

					echo '<script>

						console.log('.$_POST["editarPrecioVenta"].');
						console.log('.$_POST["idProducto"].');

						</script>';

					$tabla = "productos";

					$item = $_POST["editarPrecioVenta"];

					$valor = $_POST["idProducto"];


					$respuesta = ModeloProductos::MdlEditarPrecioProducto($tabla, $item, $valor);

					if($respuesta == "ok"){
						$accion="Producto Editado";
						$descripcion="El producto".$_POST["idProducto"]."ha sido modificado con exito";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);


						echo'<script>

						swal({
							type: "success",
							title: "El precio ha sido editado correctamente",
							showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false

							}).then((result) => {
								if (result.value) {

									window.location = "productos";

								}
							})

						</script>';

					}else{
						$accion="Producto Editado";
						$descripcion="ERROR: El producto".$_POST["idProducto"]."no ha sido modificado con exito";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);
						echo'<script>

							swal({

								type: "error",
								title: "¡El precio no puede ir vacío o llevar caracteres especiales!",
								showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

							}).then((result) => {

								if (result.value) {

									window.location = "productos";

								}
							})

						</script>';

					}

			}

    	}

    /*==========================================
	=       EDITAR PREFERENCIAS                =
	==========================================*/

		static public function ctrEditarPreferencias(){

			if(isset($_POST["editarPBond"])){

					$tabla = "preferencias";

					$id = "1";
					$p_bond = ($_POST["editarPBond"] / 100);
					$p_quimico = ($_POST["editarPQuimico"] / 100);
					$blanco_negro = ($_POST["editarCBN"] / 100);
					$full_color = ($_POST["editarCFC"] / 100);
					$o_1 = ($_POST["editarIO1"] / 100);
					$o_2 = ($_POST["editarIO2"] / 100);
					$o_3 = ($_POST["editarIO3"] / 100);
					$iva = ($_POST["iva"]);

					$respuesta = ModeloProductos::MdlEditarPreferencias($tabla, $id, $p_bond, $p_quimico, $blanco_negro, $full_color, $o_1, $o_2, $o_3, $iva);
					
					if($respuesta == "ok"){
						
						$accion="Preferencias Editadas";
						$descripcion="Se han modificado los porcentajes";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);

						echo'<script>

						swal({
							type: "success",
							title: "Las preferencias ha sido guardadas correctamente",
							showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false

							}).then((result) => {
								if (result.value) {

									window.location = "productos";

								}
							})

						</script>';

					}else{

						$accion="Preferencias Editadas";
						$descripcion="ERROR No se han Podido modificar los porcentajes";
						$bitacora=ModeloAuditoria::mdlRegistrarBitacora($accion,$descripcion);


						echo'<script>

							swal({

								type: "error",
								title: "¡Las preferencias no pueden ir vacías o llevar caracteres especiales!",
								showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

							}).then((result) => {

								if (result.value) {

									window.location = "productos";

								}
							})

						</script>';

					}

			}

    	}
    	static public function ctrMontoInva($monto){
    		$preferencia = ModeloProductos::MdlMostrarPreferencias();
    		return $monto*($preferencia["iva"]/100);
    	}
 
}





	

?>
