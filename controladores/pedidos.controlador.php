<?php

class ControladorPedidos

{
	/*==========================================
	=     REGISTRAR NUEVO PEDIDO               =
	==========================================*/
	static public

	function ctrAgregarPedido($id)
	{
		unset($datos);
		if (isset($_POST["opradio"])) {
			$ararydata = explode("-", $id);
			$id_carrito = $ararydata[0];
			$id_direc_envio = $ararydata[1];
			$id_direc_facturacion = $ararydata[2];
			$detalle = 'id_carrito';
			$hoy = date("Y-m-d H:i:s");
			$respuesta = ModeloPedidos::MdlMostrarCarrito($id_carrito, $detalle);
			$datos = array(
				"id_producto" => $respuesta[0]["id_producto_carr"],
				"orden" => $respuesta[0]["orden_carr"],
				"monto" => $respuesta[0]["monto_carr"],
				"fecha_emision" => $hoy,
				"estado" => "Pendiente",
				"rif" => $respuesta[0]["rif_carr"],
				"nombre_empresa" => $respuesta[0]["nombre_empresa_carr"],
				"direc_filcal_1" => $respuesta[0]["direc_filcal_1_carr"],
				"direc_filcal_2" => $respuesta[0]["direc_filcal_2_carr"],
				"telefono_otro" => $respuesta[0]["telefono_otro_carr"],
				"tipo_presentacion" => $respuesta[0]["tipo_presentacion_carr"],
				"metodo_envio" => $_POST["opradio"],
				"num_factura_ini" => $respuesta[0]["num_factura_ini_carr"],
				"num_control_ini" => $respuesta[0]["num_control_ini_carr"],
				"cantidad" => $respuesta[0]["cantidad_carr"],
				"preferencia_impresion" => $respuesta[0]["preferencia_impresion_carr"],
				"preferencia_papel" => $respuesta[0]["preferencia_papel_carr"],
				"opciones_color" => $respuesta[0]["opciones_color_carr"],
				"img_rif" => $respuesta[0]["img_rif_carr"],
				"logo" => $respuesta[0]["logo_carr"],
				"tamano_logo" => $respuesta[0]["tamano_logo_carr"]
			);

		$registroPedido = ModeloPedidos::mdlAgregarPedido($datos);
		$detalle2 = "orden";
		$valor = $respuesta[0]["orden_carr"];


		// registro el pedido
		if ($registroPedido == "ok") {

			// busco el pedido registrado

			$buscaActual = ModeloPedidos::MdlVerPedido($valor, $detalle2);
			$idPago = $buscaActual[0]["id_pedido"];
			$tipo_dp = 1;

			// registro relacion en tablas puente

			$registroPuente = ModeloPedidos::mdlAgregarPuentePedido($idPago, $id_direc_envio, $tipo_dp);
			$tipo_dp = 2;
			$registroPuente = ModeloPedidos::mdlAgregarPuentePedido($idPago, $id_direc_facturacion, $tipo_dp);

			// Borro del carrito el pedido procesado

			$deletecarrito = ModeloPedidos::MdlBorrarCarrito($id_carrito);
			if ($deletecarrito == "ok") {
				echo '<script>
								swal({
									title: "¡Pedido Registrado!",
									text: "¡Por favor cancele el monto el pedido para relizar el envío!",
									type:"success",
							  		showConfirmButton: true,
									confirmButtonColor: "#018ba7",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false

									  }).then((result) => {
										if (result.value) {

										window.location ="reportarPagoCompras&' . $idPago . '";

										}
									})

						</script>';
			}
			else {
				echo '<script>
								swal({
									title: "¡Ups!<br />Al parecer algo falló",
									text: "¡Por favor intente mas tarde!",
									type:"error",
							  		showConfirmButton: true,
									confirmButtonColor: "#018ba7",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false

									  }).then((result) => {
										if (result.value) {

										window.location ="reportarPagoCompras&' . $idPago . '";

										}
									})

						</script>';
			}
		}
		}
		unset($datos);
	}

	/*==========================================
	=           MOSTRAR PEDIDOS                =
	==========================================*/
	static public

	function ctrMostrarPedidos()
	{
		$tabla1 = "pedidos";
		$tabla2 = "clientes";
		$respuesta = ModeloPedidos::MdlMostrarPedidos($tabla1);
		return $respuesta;
	}
	static public

	function ctrMostrarTodosPedidos()
	{
		$tabla = "pedidos";
		$respuesta = ModeloPedidos::MdlMostrarTodosPedidos($tabla);
		return $respuesta;

	}
	static public

	function ctrlMostrarClientePedido($valor)
	{
		$item='id_pedido';
		$respuesta = ModeloPedidos::mdlMostrarClientePedido($valor);
		return $respuesta[0];

	}
	/*
	/*==========================================
	= MOSTRAR DIRECCIÓN DE ENVÍO  =
	==========================================*/
	static public

	function ctrReimpresion()
	{
		if (isset($_POST["idimpresion"])) {
			$valor = $_POST["idimpresion"];
			$detalle = "id_pedido";
			$hoy = date("Y-m-d H:i:s");
			$respuesta = ModeloPedidos::MdlVerPedido($valor, $detalle);
			$orden = ModeloPedidos::hexadecimalAzar(4);
			$datos = array(
				"id_producto" => $respuesta[0]["id_producto"],
				"orden" => $orden,
				"monto" => $respuesta[0]["monto"],
				"fecha_emision" => $hoy,
				"estado" => "Pendiente",
				"rif" => $respuesta[0]["rif"],
				"nombre_empresa" => $respuesta[0]["nombre_empresa"],
				"direc_filcal_1" => $respuesta[0]["direc_filcal_1"],
				"direc_filcal_2" => $respuesta[0]["direc_filcal_2"],
				"telefono_otro" => $respuesta[0]["telefono_otro"],
				"tipo_presentacion" => $respuesta[0]["tipo_presentacion"],
				"metodo_envio" => $respuesta[0]["metodo_envio"],
				"num_factura_ini" => $respuesta[0]["num_factura_ini"],
				"num_control_ini" => $respuesta[0]["num_control_ini"],
				"cantidad" => $respuesta[0]["cantidad"],
				"preferencia_impresion" => $respuesta[0]["preferencia_impresion"],
				"preferencia_papel" => $respuesta[0]["preferencia_papel"],
				"opciones_color" => $respuesta[0]["opciones_color"],
				"img_rif" => $respuesta[0]["img_rif"],
				"logo" => $respuesta[0]["logo"],
				"tamano_logo" => $respuesta[0]["tamano_logo"]
			);
			$registroPedido = ModeloPedidos::mdlAgregarPedido($datos);
			$detalle2 = "orden";
			if ($registroPedido == "ok") {

				// busco el pedido registrado

				$buscaActual = ModeloPedidos::MdlVerPedido($orden, $detalle2);
				$idPago = $buscaActual[0]["id_pedido"];
				$tipo_dp = 1;

				// registro relacion en tablas puente

				$registroPuente = ModeloPedidos::mdlAgregarPuentePedido($idPago,$_POST["iDireccion1"], $tipo_dp);
				$tipo_dp = 2;
				$registroPuente = ModeloPedidos::mdlAgregarPuentePedido($idPago,$_POST["iDireccion2"], $tipo_dp);

				// Borro del carrito el pedido procesado

				$deletecarrito = ModeloPedidos::MdlBorrarCarrito($id_carrito);
				if ($deletecarrito == "ok") {
					echo '<script>
											swal({
												title: "¡Pedido Registrado!",
												text: "¡Por favor cancele el monto el pedido para relizar el envío!",
												type:"success",
										  		showConfirmButton: true,
												confirmButtonColor: "#018ba7",
												confirmButtonText: "Cerrar",
												closeOnConfirm: false

												  }).then((result) => {
													if (result.value) {

													window.location ="reportarPagoCompras&' . $idPago . '";

													}
												})

									</script>';
				}
				else {
					echo '<script>
											swal({
												title: "¡Ups!<br />Al parecer algo falló",
												text: "¡Por favor cancele el monto el pedido para relizar el envío!",
												type:"error",
										  		showConfirmButton: true,
												confirmButtonColor: "#018ba7",
												confirmButtonText: "Cerrar",
												closeOnConfirm: false

												  }).then((result) => {
													if (result.value) {

													window.location ="reportarPagoCompras&' . $idPago . '";

													}
												})

									</script>';
				}


			}
			else {
				echo '<script>
								swal({
									title: "¡Ups!<br />Al parecer algo falló",
									text: "¡Por favor intente mas tarde!",
									type:"error",
							  		showConfirmButton: true,
									confirmButtonColor: "#018ba7",
									confirmButtonText: "Cerrar",
									closeOnConfirm: false

									  }).then((result) => {
										if (result.value) {

										window.location ="reportarPagoCompras&' . $idPago . '";

										}
									})

						</script>';
			}
		}
	}

	static public

	function ctrListarPedidos($valor)
	{
		$item = "id_pedido";
		$respuesta = '';
		$respuesta = ModeloPedidos::MdlMostrarPedidos($_SESSION["clienteId"]);
		$respuesta = ModeloPedidos::unique_multidim_array($respuesta, $item);
		if ($respuesta == null) {
			return $respuesta = 0;
		}
		else {
			return $respuesta;
		}
	}

	static public

	function ctrMostrarDetalleEnvio($valor)
	{
		$detalle = "id_pedido";
		$respuesta = ModeloPedidos::MdlVerPedido($valor, $detalle);
		return $respuesta;
	}
		static public

	function ctrMostrarDetalleParaEnvio($valor)
	{
		$detalle = "id_pedido";
		$respuesta = ModeloPedidos::MdlVerPedido($valor, $detalle);
		$respuesta2= ControladorPedidos::ctrlMostrarClientePedido($valor);

		$item="id_direcciones";
		$id_direc=$respuesta2["id_direcciones"];
		$direccion = ModeloPedidos::MdlVerDireccion($id_direc,$item);

			$datos = array(
				"nombre_apellido" => $respuesta2["nombre"].' '.$respuesta2["apellido"],
				"rif" => $respuesta[0]["rif"],
				"orden" => $respuesta[0]["orden"],
				"nombre_empresa" => $respuesta[0]["nombre_empresa"],
				"sector_urbanizacion" =>  $direccion["sector_urbanizacion"],
				"avenida_calle" => $direccion["avenida_calle"],
				"edificio_quinta_casa" => $direccion["edificio_quinta_casa"],
				"estado" => $direccion["estado"],
				"ciudad" => $direccion["ciudad"],
				"d_telefono" => $direccion["d_telefono"],
				"informacion_adicional" => $direccion["informacion_adicional"],
				"metodo_envio" => $respuesta[0]["metodo_envio"]
			);
		return $datos;
	}

	static public

	function ctrMostraritemPedido($valor, $detalle)
	{
		$item = "id_pedido";
		$respuesta = ModeloPedidos::mdlItemPedido($valor, $item, $detalle);
		return $respuesta;
	}

	static public

	function ctrMostrarDirecPedido($valor, $detalle, $detalle2)
	{
		$item = "id_pedido_dp";
		$tabla = "dir_pedidos";
		$item2 = "tipo_dp";
		$respuesta = ModeloPedidos::mdldireccionpedido($tabla, $valor, $item, $item2, $detalle, $detalle2);
		return $respuesta;
	}

	public static

	function ctrMostrarCarrito($valor)
	{
		$valor = $_SESSION["clienteId"];
		$detalle = "id_cliente_carr";
		$respuesta = ModeloPedidos::MdlMostrarCarrito($valor, $detalle);
		return $respuesta;
	}

	public static

	function ctrMostrarPedidosPorEstado($valor)
	{
		$item = "id_pedido";
		$estado="Pendiente";
		$respuesta = ModeloPedidos::MdlMostrarPedidosEstado($valor, $estado);
		$respuesta = ModeloPedidos::unique_multidim_array($respuesta, $item);
		return $respuesta;
	}

	public static
	function ctrMostrarDetalleCarrito($valor, $detalle)
	{
		return ModeloPedidos::MdlMostrarCarrito($valor, $detalle);
	}

	public static
	function ctrlCambiaEstado($tabla, $item, $valor,$accion, $descripcion, $usuario, $nombreusuario)
	{
  			$bitResult = ModeloAuditoria::mdlRegistrarBitacora2($accion, $descripcion,$usuario,$nombreusuario);
			$respuesta = ModeloPedidos::MdlCambiarEstado($tabla, $item, $valor);
	}
}


?>
