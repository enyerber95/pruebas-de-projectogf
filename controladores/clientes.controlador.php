<?php

Class ControladorClientes
{
	/*==========================================
	=            INGRESO DE CLIENTE            =
	==========================================*/
	/*==========================================
	=           REGISTRO DE CLIENTE            =
	==========================================*/
	static public

	function ctrCrearCliente()
	{
		if (isset($_POST["regCorreo"])) {
			$tabla="clientes";
			$item="email";
			$valor=$_POST["regCorreo"];
			if(ModeloClientes::mdlMostrarCliente($tabla, $item, $valor) != false){
				echo '<script>

					swal({
						title: "¡ERROR!",
						text: "Error al registrar la cuenta, El correo ya existe.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},

						function(isConfirm){

							if(isConfirm){
								window.location = localStorage.getItem("rutaActual");
							}
							});

							</script>';

			
			die();
			}

			if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["regCorreo"])){
				$pass = $_POST["regPassword"];

				$opciones = ['cost' => 10, ];
				$pass_hash = password_hash($pass, PASSWORD_BCRYPT, $opciones);

				$datos = array(
					"email" => $_POST["regCorreo"],
					"password" => $pass_hash,
					"nombre" => $_POST["regNombre"],
					"apellido" => $_POST["regApellido"],
					"fecha_nacimiento" => $_POST["regFechaNac"],
					"cedula" => $_POST["regCedula"],
					"telefono_local" => $_POST["regTelfLocal"],
					"telefono_movil" => $_POST["regTelfMovil"],
					"estado" => 1
				);

				
				$respuesta = ModeloClientes::mdlRegistrarCliente($tabla, $datos);
				if ($respuesta == "ok") {
					$item="cedula";
					$valor=$_POST["regCedula"];

					$buscaCliente=ModeloClientes::mdlMostrarCliente($tabla,$item,$valor);


					$preg1=$_POST["PregReg1"];
					$preg2=$_POST["PregReg2"];
					$respuesta1=$_POST["respuesta1"];
					$respuesta2=$_POST["respuesta2"];

					$id_cliente_p=$buscaCliente["id_cliente"];

					$confirma1=ModeloClientes::mdlRegistrarPreguntas($preg1,$respuesta1,$id_cliente_p);
					$confirma2=ModeloClientes::mdlRegistrarPreguntas($preg2,$respuesta2,$id_cliente_p);
					echo '<script>
						swal({
							title: "¡OK!",
							text: "¡Su cuenta ha sido creada satisfactoriamente! Para ingresar al sistema ve al formulario Iniciar Sesión.",
							type:"success",
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){

								if(isConfirm){
									window.location = localStorage.getItem("rutaActual");
								}
								});

						</script>';
				}
				else {
					echo '<script>

					swal({
						title: "¡ERROR!",
						text: "Error al registrar la cuenta, revisa los datos introducidos y vuelve a intentarlo.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},

						function(isConfirm){

							if(isConfirm){
								window.location = localStorage.getItem("rutaActual");
							}
							});

							</script>';
				}
			}
			else {
				echo '<script>

					swal({
						title: "¡ERROR!",
						text: "Error al registrar la cuenta, revisa los datos introducidos y vuelve a intentarlo.",
						type:"error",
						confirmButtonColor: "#018ba7",
						confirmButtonText: "Cerrar",
						closeOnConfirm: false
						},

						function(isConfirm){

							if(isConfirm){
								window.location = localStorage.getItem("rutaActual");
							}
							});

							</script>';
			}
		}else{}
	}

	/*==========================================
	=           EDITAR CLIENTE                 =
	==========================================*/
	static public

	function ctrEditarCliente()
	{
		if (isset($_POST["editarCorreo"])) {
			if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["editarCorreo"])) {
				$tabla = "clientes";
				if ($_POST["editarPassword"] != "") {
						$passModificado = $_POST["editarPassword"];

						$opciones = ['cost' => 10, ];
						$pass_hash = password_hash($passModificado, PASSWORD_BCRYPT, $opciones);
						$decision = 1;
				}
				else {
					$passNoModificado = $_SESSION['clientePassword'];
					$decision = 0;
				}

				/*  CONDICIONAL PARA LLENAR EL ARRAY CON EL PASSWORD CORRECTO Y ACTUALIZAR VARIABLES DE SESIÓN  */
				if ($decision == 1) {

					$passModificado=
					$datos = array(
						"email" => $_POST["editarCorreo"],
						"password" => $pass_hash,
						"nombre" => $_POST["editarNombre"],
						"apellido" => $_POST["editarApellido"],
						"fecha_nacimiento" => $_POST["editarFechaNac"],
						"cedula" => $_POST["editarCedula"],
						"telefono_local" => $_POST["editarTelfLocal"],
						"telefono_movil" => $_POST["editarTelfMovil"]
					);
					$_SESSION["clienteCorreo"] = $_POST["editarCorreo"];
					$_SESSION["clientePassword"] = $passModificado;
					$_SESSION["clienteNombre"] = $_POST["editarNombre"];
					$_SESSION["clienteApellido"] = $_POST["editarApellido"];
					$_SESSION["clienteFechaNac"] = $_POST["editarFechaNac"];
					$_SESSION["clienteCedula"] = $_POST["editarCedula"];
					$_SESSION["clienteTelfLocal"] = $_POST["editarTelfLocal"];
					$_SESSION["clienteTelfMovil"] = $_POST["editarTelfMovil"];
				}
				else {
					$datos = array(
						"email" => $_POST["editarCorreo"],
						"password" => $passNoModificado,
						"nombre" => $_POST["editarNombre"],
						"apellido" => $_POST["editarApellido"],
						"fecha_nacimiento" => $_POST["editarFechaNac"],
						"cedula" => $_POST["editarCedula"],
						"telefono_local" => $_POST["editarTelfLocal"],
						"telefono_movil" => $_POST["editarTelfMovil"]
					);
					$_SESSION["clienteCorreo"] = $_POST["editarCorreo"];
					$_SESSION["clientePassword"] = $passNoModificado;
					$_SESSION["clienteNombre"] = $_POST["editarNombre"];
					$_SESSION["clienteApellido"] = $_POST["editarApellido"];
					$_SESSION["clienteFechaNac"] = $_POST["editarFechaNac"];
					$_SESSION["clienteCedula"] = $_POST["editarCedula"];
					$_SESSION["clienteTelfLocal"] = $_POST["editarTelfLocal"];
					$_SESSION["clienteTelfMovil"] = $_POST["editarTelfMovil"];
				}

				$respuesta = ModeloClientes::mdlEditarCliente($tabla, $datos);
				if ($respuesta == "ok") {
					echo '<script>

					swal({
						  type: "success",
						  title: "El perfil ha sido editado correctamente",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false

						  }).then((result) => {
									if (result.value) {

									window.location = localStorage.getItem("rutaActual");

									}
								})

					</script>';
				}
			}
			else {
				echo '<script>

					swal({
						  type: "error",
						  title: "¡El nombre no puede ir vacío o llevar caracteres especiales!",
						  showConfirmButton: true,
						  confirmButtonColor: "#018ba7",
						  confirmButtonText: "Cerrar",
						  closeOnConfirm: false

						  }).then((result) => {
							if (result.value) {

							window.location = localStorage.getItem("rutaActual");

							}
						})

			  	</script>';
			}
		}
	}

	/*=============================================
	MOSTRAR CLIENTE (UNO SOLO)
	=============================================*/
	static public

	function ctrMostrarCliente($item, $valor)
	{
		$tabla = "clientes";
		$respuesta = ModeloClientes::mdlMostrarCliente($tabla, $item, $valor);
		return $respuesta;
	}

	/*==========================================
	=           MOSTRAR CLIENTES               =
	==========================================*/
	static public

	function ctrMostrarClientes($item, $valor)
	{
		$tabla = "clientes";
		$respuesta = ModeloClientes::MdlMostrarClientes($tabla, $item, $valor);
		return $respuesta;
	}

	/*=============================================
	ACTUALIZAR CLIENTE
	=============================================*/
	/*=============================================
	INGRESO DE CLIENTE
	=============================================*/
	static public

	function ctrIngresoCliente()
	{

		// INICIO DE SESION

		if (isset($_POST["ingCorreo"])) {
			if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingCorreo"])) {
				$tabla = "clientes";
				$item = "email";
				$valor = $_POST["ingCorreo"];
				$verifica = $_POST["ingPassword"];
				$respuesta = ModeloClientes::MdlMostrarClientes($tabla, $item, $valor);
				if ($respuesta["email"] == $_POST["ingCorreo"]) {
					$pass = $respuesta["password"];
					if (password_verify($verifica, $pass)) {
						$_SESSION["validarSesion"] = "ok";
						$_SESSION["clienteId"] = $respuesta["id_cliente"];
						$_SESSION["clienteCorreo"] = $respuesta["email"];
						$_SESSION["clientePassword"] = $respuesta["password"];
						$_SESSION["clienteNombre"] = $respuesta["nombre"];
						$_SESSION["clienteApellido"] = $respuesta["apellido"];
						$_SESSION["clienteFechaNac"] = $respuesta["fecha_nacimiento"];
						$_SESSION["clienteCedula"] = $respuesta["cedula"];
						$_SESSION["clienteTelfLocal"] = $respuesta["telefono_local"];
						$_SESSION["clienteTelfMovil"] = $respuesta["telefono_movil"];
						
					echo '<script>

			          swal({
			            title: "¡Biencenido '.$_SESSION["clienteNombre"].' '.$_SESSION["clienteApellido"].'!",
			            text: "Gracias por elegirnos.",
			            type:"success",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "OK",
			            closeOnConfirm: false
			            }).then((result) => {
										if (result.value) {

										window.location = localStorage.getItem("rutaActual")

										}
									});

			              </script>';
					}
					else {
						echo '<script>

			          swal({
			            title: "¡ERROR!",
			            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
			            type:"error",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "Cerrar",
			            closeOnConfirm: false
			            }).then((result) => {
										if (result.value) {

										window.location = localStorage.getItem("rutaActual")

										}
									});

			              </script>';
					}
				}
				else {
					echo '<script>

			          swal({
			            title: "¡ERROR!",
			            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
			            type:"error",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "Cerrar",
			            closeOnConfirm: false
			            }).then((result) => {
										if (result.value) {

										window.location = localStorage.getItem("rutaActual")

										}
									});

			              </script>';
				}
			}
		}
	}

	function ctrIngresoClienteCarrito($dat)
	{
		$dat = $dat;
		if ($dat === 1) {
			if (isset($_POST["ingCorreo2"])) {
				if (preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["ingCorreo2"])) {
					$tabla = "clientes";
					$item = "email";
					$valor = $_POST["ingCorreo2"];
					$verifica = $_POST["ingPassword"];
					$respuesta = ModeloClientes::MdlMostrarClientes($tabla, $item, $valor);
					if ($respuesta["email"] == $_POST["ingCorreo2"]) {
						$pass = $respuesta["password"];
						if (password_verify($verifica, $pass)) {
							$_SESSION["validarSesion"] = "ok";
							$_SESSION["clienteId"] = $respuesta["id_cliente"];
							$_SESSION["clienteCorreo"] = $respuesta["email"];
							$_SESSION["clientePassword"] = $respuesta["password"];
							$_SESSION["clienteNombre"] = $respuesta["nombre"];
							$_SESSION["clienteApellido"] = $respuesta["apellido"];
							$_SESSION["clienteFechaNac"] = $respuesta["fecha_nacimiento"];
							$_SESSION["clienteCedula"] = $respuesta["cedula"];
							$_SESSION["clienteTelfLocal"] = $respuesta["telefono_local"];
							$_SESSION["clienteTelfMovil"] = $respuesta["telefono_movil"];
						}
						else {
						}
					}
					else {
						echo '<script>

			          swal({
			            title: "¡ERROR!",
			            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
			            type:"error",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "Cerrar",
			            closeOnConfirm: false
			            },

			            function(isConfirm){

			              if(isConfirm){
			                window.location = localStorage.getItem("rutaActual");
			              }
			              });

			              </script>';
					}
				}

				if (isset($_POST["rifDoc"])) {
					unset($datos);
					$tabla = "carrito";
					$datos = array(
						"id_cliente" => $_SESSION["clienteId"],
						"id_producto" => $_POST["id_producto"],
						"direDoc" => $_POST["direDoc"],
						"dire2Doc" => $_POST["dire2Doc"],
						"tlfDoc" => $_POST["tlfDoc"],
						"nombre_producto" => $_POST["nombre_producto"],
						"nomEmpDoc" => $_POST["nomEmpDoc"],
						"rifDoc" => $_POST["rifDoc"],
						"tipPreDoc" => $_POST["tipPreDoc"],
						"canImpDoc" => $_POST["canImpDoc"],
						"facIniDoc" => $_POST["facIniDoc"],
						"conIniDoc" => $_POST["conIniDoc"],
						"orden_carr" => ModeloPedidos::hexadecimalAzar(4) ,
						"prePapDoc" => $_POST["prePapDoc"],
						"preImpDoc" => $_POST["preImpDoc"],
						"OpcColDoc" => $_POST["OpcColDoc"],
						"imgsend" => $_POST["imgsend"],
						"logoDoc" => $_POST["logoDoc"],
						"tamlogoUrl" => $_POST["sizeDoc"],
						"Preciosend" => $_POST["Preciosend"]
					);
					$respuesta = ModeloPedidos::MdlAddCarrito($tabla, $datos);
					if ($respuesta == 'ok') {
						echo '<script>

		              window.location ="resumenCompras";

		            </script>';
					}
					else {
						var_dump($respuesta);
						echo '<script>

		          swal({
		            title: "¡Error al guardar plantilla!",
		            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
		            type:"error",
		            confirmButtonColor: "#018ba7",
		            confirmButtonText: "Cerrar",
		            closeOnConfirm: false
		            },

		            function(isConfirm){

		              if(isConfirm){
		                window.location = "facturasCatalogo";
		              }
		              });

		              </script>';
					}
				}
			}
		}

		if ($dat === 0) {

			// CARGA CARRITO

			if (isset($_POST["rifDoc"])) {
				unset($datos);
				$tabla = "carrito";
				$datos = array(
					"id_cliente" => $_SESSION["clienteId"],
					"id_producto" => $_POST["id_producto"],
					"direDoc" => $_POST["direDoc"],
					"dire2Doc" => $_POST["dire2Doc"],
					"tlfDoc" => $_POST["tlfDoc"],
					"nombre_producto" => $_POST["nombre_producto"],
					"nomEmpDoc" => $_POST["nomEmpDoc"],
					"rifDoc" => $_POST["rifDoc"],
					"tipPreDoc" => $_POST["tipPreDoc"],
					"canImpDoc" => $_POST["canImpDoc"],
					"facIniDoc" => $_POST["facIniDoc"],
					"conIniDoc" => $_POST["conIniDoc"],
					"orden_carr" => ModeloPedidos::hexadecimalAzar(4) ,
					"prePapDoc" => $_POST["prePapDoc"],
					"preImpDoc" => $_POST["preImpDoc"],
					"OpcColDoc" => $_POST["OpcColDoc"],
					"imgsend" => $_POST["imgsend"],
					"logoDoc" => $_POST["logoDoc"],
					"tamlogoUrl" => $_POST["sizeDoc"],
					"Preciosend" => $_POST["Preciosend"]
				);
				$respuesta = ModeloPedidos::MdlAddCarrito($tabla, $datos);
				if ($respuesta == 'ok') {
					echo '<script>

		              window.location ="resumenCompras";

		            </script>';
				}
				else {
					/*		echo '<script>

				  console.log(' . $_POST["direDoc"] . ');
				  console.log(' . $_POST["dire2Doc"] . ');
				  console.log(' . $_POST["tlfDoc"] . ');
				  console.log(' . $_POST["nombre_producto"] . ');
				  console.log(' . $_POST["nomEmpDoc"] . ')
				  console.log(' . $_POST["rifDoc"] . ');
				  console.log(' . $_POST["tipPreDoc"] . ' asdsadsa);
				  console.log(' . $_POST["canImpDoc"] . ');
				  console.log(' . $_POST["facIniDoc"] . ');
				  console.log(' . $_POST["conIniDoc"] . ');
				  console.log(' . $_POST["prePapDoc"] . ');
				  console.log(' . $_POST["preImpDoc"] . ');
				  console.log(' . $_POST["OpcColDoc"] . ');
				  console.log(' . $_POST["imgsend"] . ');
				  console.log(' . $_POST["logoDoc"] . ');
				  console.log(' . $_POST["sizeDoc"] . ');


					</script>';*/
					echo '<script>

		          swal({
		            title: "¡Error al guardar plantilla!",
		            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
		            type:"error",
		            confirmButtonColor: "#018ba7",
		            confirmButtonText: "Cerrar",
		            closeOnConfirm: false
		            },

		            function(isConfirm){

		              if(isConfirm){
		                window.location = "facturasCatalogo";
		              }
		              });

		              </script>';
				}
			}
			else {
				/*echo '<script>
				console.log(' . $_POST["direDoc"] . ');
				console.log(' . $_POST["dire2Doc"] . ');
				console.log(' . $_POST["tlfDoc"] . ');
				console.log(' . $_POST["nombre_producto"] . ');
				console.log(' . $_POST["nomEmpDoc"] . ')
				console.log(' . $_POST["rifDoc"] . ');
				console.log(' . $_POST["tipPreDoc"] . ' asdsadsa);
				console.log(' . $_POST["canImpDoc"] . ');
				console.log(' . $_POST["facIniDoc"] . ');
				console.log(' . $_POST["conIniDoc"] . ');
				console.log(' . $_POST["prePapDoc"] . ');
				console.log(' . $_POST["preImpDoc"] . ');
				console.log(' . $_POST["OpcColDoc"] . ');
				console.log(' . $_POST["imgsend"] . ');
				console.log(' . $_POST["logoDoc"] . ');
				console.log(' . $_POST["sizeDoc"] . ');
				</script>';*/
			}
		}
	}

	function ctrUpdateClienteCarrito(){
			if (isset($_POST["rifDoc"])){
				unset($datos);
				$tabla="carrito";
				$datos = array(
					"id_cliente" => $_SESSION["clienteId"],
					"id_producto" => $_POST["id_producto"],
					"nombre_producto" => $_POST["nombre_producto"],
					"id_carrito" => $_POST["id_carrito"],
					"nomEmpDoc" => $_POST["nomEmpDoc"],
					"rifDoc" => $_POST["rifDoc"],
					"direDoc" => $_POST["direDoc"],
					"dire2Doc" => $_POST["dire2Doc"],
					"tlfDoc" => $_POST["tlfDoc"],
					"tipPreDoc" => $_POST["tipPreDoc"],
					"canImpDoc" => $_POST["canImpDoc"],
					"facIniDoc" => $_POST["facIniDoc"],
					"conIniDoc" => $_POST["conIniDoc"],
					"preImpDoc" => $_POST["preImpDoc"],
					"prePapDoc" => $_POST["prePapDoc"],
					"orden_carr" => $_POST["orden_carr"],
					"OpcColDoc" => $_POST["OpcColDoc"],
					"imgsend" => $_POST["imgsend"],
					"logoDoc" => $_POST["logoDoc"],
					"Preciosend" => $_POST["Preciosend"],
					"tamlogoUrl" => $_POST["sizeDoc"]
				);
				$respuesta = ModeloPedidos::MdlUpdateCarrito($tabla,$datos);
				if ($respuesta == 'ok'){
					echo '<script>

		              window.location ="resumenCompras";

		            </script>';
				}
				else{
						echo '<script>

							  


							</script>';
						echo '<script>

			          swal({
			            title: "¡Error al guardar plantilla!",
			            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
			            type:"error",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "Cerrar",
			            closeOnConfirm: false
			            },

			            function(isConfirm){

			              if(isConfirm){
			                window.location = "facturasCatalogo";
			              }
			              });

          	</script>';
				}
			}
			else{
				echo '<script>
				</script>';
			}
	}
	static public 
	function ctrRecuperarContra(){
		if (isset($_POST["CorreoRecupera"])) {

			$tabla="clientes";
			$item="email";
			$valor=$_POST["CorreoRecupera"];
			$respuesta = ModeloClientes::MdlMostrarCliente($tabla, $item, $valor);

				

			if (isset($respuesta)){

				$id_cliente=$respuesta["id_cliente"];
				$pregunta=$_POST["PregRecupera"];
				$resRecupera=$_POST["respuestarecu"];
				echo '<script>
				console.log('.$pregunta.');
				console.log('.$resRecupera.');
				console.log('.$id_cliente.');
				</script>';
				
				if ($consulta=ModeloClientes::MdlRecupera($id_cliente,$pregunta,$resRecupera)){
			
				$data = $respuesta["id_cliente"]+204204;
				

				echo '<script>

	          	swal({
	            title: "¡Pregunta verificada, proceda a editar su perfil!",
	            text: "Puede editar su perfil",
	            type:"success",
	            confirmButtonColor: "#018ba7",
	            confirmButtonText: "Cerrar",
	            closeOnConfirm: true
	            }).then((result) => {
										if (result.value) {

										window.location ="restablecerPass&'.$data	.'";

										}
									})
  				</script>';
				}else{
					echo '<script>

			          swal({
			            title: "Datos invalidos!",
			            text: "Error al ingresar al sistema, revisa los datos introducidos y vuelve a intentarlo.",
			            type:"error",
			            confirmButtonColor: "#018ba7",
			            confirmButtonText: "Cerrar",
			            closeOnConfirm: false
			            },

			            function(isConfirm){

			              if(isConfirm){
			                window.location = "facturasCatalogo";
			              }
			              });

          	</script>';
				}

			}

			
		}
		
	}
	static public function ctrRestablecerPass($valor){
		if (isset($_POST["newPass"])) { 	
			$pass=$_POST["newPass"];

			$opciones = ['cost' => 10, ];
			$pass_hash = password_hash($pass, PASSWORD_BCRYPT, $opciones);
			$tabla="clientes";
			$respuesta=ModeloClientes::MdlNewPass($pass_hash,$valor,$tabla);
			if ($respuesta == "ok") {
				echo '<script>

	          	swal({
	            title: "¡Clave cambiada!",
	            text: "Puede editar su perfil",
	            type:"success",
	            confirmButtonColor: "#018ba7",
	            confirmButtonText: "Cerrar",
	            closeOnConfirm: true
	            }).then((result) => {
										if (result.value) {

										window.location ="inicio";

										}
									})
  				</script>';
				# code...
			}else{
					echo '<script>

	          	swal({
	            title: "¡Falló la restauracion!",
	            text: "intente de nuevo",
	            type:"error",
	            confirmButtonColor: "#018ba7",
	            confirmButtonText: "Cerrar",
	            closeOnConfirm: true
	            }).then((result) => {
										if (result.value) {

										window.location ="inicio";

										}
									})
  				</script>';

			}
			# code...
		}
	}
	static public function ctrCambiaEstado($cambiarAccion,$nombreusuario,$usuario,$valor2){
		$tabla="clientes";
		$item="id_cliente";
		$respuesta = ModeloClientes::MdlMostrarCliente($tabla,$item,$valor2);
		$cliente=$respuesta["nombre"].' '.$respuesta["apellido"];
		$descripcion="El cliente: ".$cliente." ha sido cambiado de estado";
  		$bitResult = ModeloAuditoria::mdlRegistrarBitacora2($cambiarAccion, $descripcion,$usuario,$nombreusuario);

	
	}


}

?>
