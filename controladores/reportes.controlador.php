<?php


	class ControladorReportes{

		/*==========================================
		=           MOSTRAR REPORTES               =
		==========================================*/

		static public function ctrMostrarReportes(){

			$tabla = "reporte_pago";

			$respuesta = ModeloReportes::MdlMostrarReportes($tabla);
			return $respuesta;
		}

		/*==========================================
		= MOSTRAR DETALLES REPORTES                =
		==========================================*/

		static public function ctrMostrarDetalleReportes($valor){

			$respuesta = ModeloReportes::MdlMostrarDetalleReportes($valor);

			return $respuesta;
		}


		static public function ctrEnviarReporte($valor,$vista){
			$detalle='';
			if ($vista == 1) {
				# realizco reporte desde carrito
				$detalle="id_pedido";
			}
			if ($vista == 0) {
				# realizo reporte desde formulario externo
				if (isset($_POST["ordenpedido"])){
					$detalle="orden";
					$valor=$_POST["ordenpedido"];
				}
			}
			$tabla="reporte_pago";

			if($pedidoreporte=ModeloPedidos::MdlVerPedido($valor, $detalle)){
				$pedidosresum=$pedidoreporte[0]["id_pedido"];
			}

			if (isset($_POST["BankEmi"]))
			{
					$datos = array(
						"id_pedido" => $pedidoreporte[0]["id_pedido"],
						"control" => 0,
						"banco_emisor" => $_POST["BankEmi"],
						"banco_receptor" => $_POST["BankRecp"],
						"num_deposito_transferencia" => $_POST["numtransf"],
						"monto_final" => $pedidoreporte[0]["monto"],
						"fecha_reporte" => $_POST["fechaReporte"],

					);
			    $respuesta=ModeloReportes::MdlRegistrarReporte($datos,$tabla);
			    if($respuesta == "ok"){
			    	$actualizapedido=ModeloPedidos::MdlUpdatePedido($pedidosresum);

						echo '<script>
							swal({
								title:"¡Su Reporte ha sido enviado satisfactoriamente!<br> En breve se realizará su envío.",
								text:"Gracias por elegir GrafoFormas",
								type:"success",
						  		showConfirmButton: true,
								confirmButtonColor: "#018ba7",
								confirmButtonText: "Cerrar",
								closeOnConfirm: false

								  }).then((result) => {
									if (result.value) {

									window.location ="inicio";

									}
								})

							</script>';
				}
				else {
					echo '<script>

						swal({
							title: "¡Error al Enviar Reporte!",
							text: "Revise los datos introducidos y vuelve a intentarlo.",
							type:"error",
						  		showConfirmButton: true,
							confirmButtonColor: "#018ba7",
							confirmButtonText: "Cerrar",
							closeOnConfirm: false
							},

							function(isConfirm){
								if(isConfirm){
									window.location = "inicio";

								}
								});

								</script>';
				}
			}

	    }
	}
?>
