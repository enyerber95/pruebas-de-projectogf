<?php

require_once "controladores/plantilla.controlador.php";
require_once "controladores/pedidos.controlador.php";
require_once "controladores/productos.controlador.php";
require_once "controladores/reportes.controlador.php";
require_once "controladores/usuarios.controlador.php";
require_once "controladores/auditoria.controlador.php";
require_once "controladores/clientes.controlador.php";
require_once "controladores/direcciones.controlador.php";
require_once "controladores/auditoria.controlador.php";

require_once "modelos/pedidos.modelo.php";
require_once "modelos/productos.modelo.php";
require_once "modelos/reportes.modelo.php";
require_once "modelos/usuarios.modelo.php";
require_once "modelos/auditoria.modelo.php";
require_once "modelos/clientes.modelo.php";
require_once "modelos/auditoria.modelo.php";
require_once "modelos/direcciones.modelo.php";

$plantilla = new ControladorPlantilla();
$plantilla -> ctrPlantilla();

?>