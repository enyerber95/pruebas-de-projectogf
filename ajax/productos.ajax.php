<?php 

	require_once "../controladores/productos.controlador.php";
	require_once "../modelos/productos.modelo.php";

	class AjaxProductos{
 
		/*=============================================
		=              MOSTRAR PRECIO PRODUCTO        =
		=============================================*/

		public $idProducto;
		
		public function ajaxMostrarPrecioProducto(){

			$tabla = "productos";
			$item = "id_producto";
			$valor = $this->idProducto;

			$respuesta = ModeloProductos::MdlMostrarPrecioProducto($tabla, $item, $valor);

			echo json_encode($respuesta);

		}


		/*=============================================
		=              MOSTRAR PREFERENCIAS           =
		=============================================*/
		
		public function ajaxMostrarPreferencias(){

			$respuesta = ModeloProductos::MdlMostrarPreferencias();

			echo json_encode($respuesta);

		}


	}


/*=============================================
MOSTRAR PRECIO DE VENTA (CPANEL)
=============================================*/

if(isset($_POST["idProducto"])){

	$editar = new AjaxProductos();
	$editar -> idProducto = $_POST["idProducto"];
	$editar -> ajaxMostrarPrecioProducto();

}

/*=============================================
MOSTRAR PREFERENCIAS (CPANEL)
=============================================*/

if(isset($_POST["id"])){

	$editar = new AjaxProductos();
	$editar -> ajaxMostrarPreferencias();

}

		
 ?>