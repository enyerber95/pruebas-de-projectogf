<?php

	require_once "../controladores/pedidos.controlador.php";
	require_once "../modelos/pedidos.modelo.php";
	require_once "../modelos/auditoria.modelo.php";


	class AjaxPedidos{

		/*=============================================
		=       MOSTRAR DIRECCION DE ENVIO            =
		=============================================*/

		public $idPedido;

		public function ajaxVerDetallesEnvio(){

			$valor = $this->idPedido;

			$respuesta = ControladorPedidos::ctrMostrarDetalleParaEnvio($valor);

			echo json_encode($respuesta);

		}


		/*=============================================
		CAMBIAR ESTADO PEDIDO
		=============================================*/

		public $cambiarPedido;
		public $cambiarEstado;


		public function ajaxCambiarEstado(){

			$tabla = "pedidos";
			$valor = $this->cambiarEstado;
			$item = $this->cambiarPedido;
			$accion = $this->cambiarAccion;
			$usuario = $this->usuario;
			$nombreusuario = $this->nombreusuario;
  			$descripcion="El pedido #".$item." Ha sido cambiado de estado";
  			$respuesta=ControladorPedidos::ctrlCambiaEstado($tabla, $item, $valor,$accion, $descripcion, $usuario, $nombreusuario);

		}

		public function ajaxEliminaCarrito(){

			echo  ModeloPedidos::MdlBorrarCarrito($this->eliminacarrito);
		}

		public function ajaxRegistrarImagen(){


			$chain = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
			$nameImage = "";
			for($i=0;$i<12;$i++) {
				$nameImage .= substr($chain,rand(0,62),1);
			}
			$typeImage = $this->archivo["type"];
			$nameTmp = $this->archivo['tmp_name'];
			$sizeImagen = $this->archivo['size']; // Leemos el tamaño del fichero
			$sizeMax="2000000"; // Tamaño maximo permitido
	    switch ($typeImage){
				case 'image/jpeg':
				$ext = ".jpg";
				break;

				case 'image/gif':
				$ext = ".gif";
				break;

				case 'image/png':
				$ext = ".png";
				break;
			}

			$destination = '../vistas/img/facturas/' ; // Carpeta donde se guardata
			// Si el tipo de imagen a subir es el mismo de los permitidos
			move_uploaded_file ( $nameTmp, $destination.$nameImage.$ext);  // Subimos el archivo
			echo 'vistas/img/facturas/'.$nameImage.$ext;
		}
		
		/*=============================================
		Buscar por fechas
		=============================================*/


		public $fechaInicial;
		public $fechaFinal;

		public function ajaxfiltrarFechaPedidos(){

			if($this->fechaInicial == "" && $this->fechaFinal == ""){

				$fechaInicial = null;
				$fechaFinal = null;

			}else{

				$fechaInicial = $this->fechaInicial;
				$fechaFinal = $this->fechaFinal;

			}

			$respuesta = ModeloPedidos::mdlRangoFechasPedidos($fechaInicial, $fechaFinal);

			echo json_encode($respuesta);

		}
}

	    /*=============================================
		MOSTRAR DIRECCION DE ENVIO
		=============================================*/

		if(isset($_POST["idPedido"])){

			$editar = new AjaxPedidos();
			$editar -> idPedido = $_POST["idPedido"];
			$editar -> ajaxVerDetallesEnvio();
		}


		/*=============================================
		CAMBIAR ESTADO USUARIO
		=============================================*/

		if(isset($_POST["cambiarEstado"])){

			$cambiarEstado = new AjaxPedidos();
			$cambiarEstado -> cambiarEstado = $_POST["cambiarEstado"];
			$cambiarEstado -> cambiarPedido = $_POST["cambiarPedido"];
			$cambiarEstado -> cambiarAccion = $_POST["cambiarAccion"];
			$cambiarEstado -> usuario = $_POST["usuario"];
			$cambiarEstado -> nombreusuario = $_POST["nombreusuario"];
			
			$cambiarEstado -> ajaxCambiarEstado();
		}

		if(isset($_POST["idCarritox"])){
			$eliminacarrito = new AjaxPedidos();
			$eliminacarrito -> eliminacarrito = $_POST["idCarritox"];
			$eliminacarrito -> ajaxEliminaCarrito();
		}


		if(isset($_POST["urls"])){
			$archivo = new AjaxPedidos();
			$archivo -> archivo =  $_FILES['logo'];
			$archivo -> ajaxRegistrarImagen();
		}

		if(isset($_POST["guardarRif"])){
			$archivo = new AjaxPedidos();
			$archivo -> archivo =  $_FILES['imgRif'];
			$archivo -> ajaxRegistrarImagen();
		}

		/*=============================================
		Buscar por fechas
		=============================================*/

		if(isset( $_POST["fechaInicial"])){

			$fecPedidos = new AjaxPedidos();
			$fecPedidos -> fechaInicial = $_POST["fechaInicial"];
			$fecPedidos -> fechaFinal = $_POST["fechaFinal"];
			$fecPedidos -> ajaxfiltrarFechaPedidos();

		}
 ?>
