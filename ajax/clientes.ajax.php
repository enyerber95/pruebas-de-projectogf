<?php 

	require_once "../controladores/clientes.controlador.php";
	require_once "../modelos/clientes.modelo.php";
	require_once "../modelos/auditoria.modelo.php";
	

	class AjaxClientes{

		/*=============================================
		=                   EDITAR USUARIO            =
		=============================================*/

		public $idCliente;
		
		public function ajaxEditarCliente(){

			$item = "id";
			$valor = $this->idCliente;

			$respuesta = ControladorClientes::ctrMostrarClientes($item, $valor);

			echo json_encode($respuesta);

		}


		/*=============================================
		VALIDAR CORREO
		=============================================*/	

		public $validarCorreo;

		public function ajaxValidarCorreo(){

			if (ControladorClientes::ctrMostrarCliente("email", $this->validarCorreo) == false) {
				echo json_encode(1);
			}
			else{
				echo json_encode(0);
			}
		}


		/*=============================================
		ACTIVAR CLIENTE
		=============================================*/	

		public $estadoCliente;
		public $activarId;

		public function ajaxActivarCliente(){

			$tabla = "clientes";

			$item1 = "estado";
			$valor1 = $this->estadoCliente;
			$item2 = "id_cliente";
			$valor2 = $this->activarId;
			$cambiarAccion = $this->cambiarAccion;
			$usuario = $this->usuario;
			$nombreusuario = $this->nombreusuario;
			$respuesta = ModeloClientes::mdlActualizarCliente($tabla, $item1, $valor1, $item2, $valor2);
			$bitacora = ControladorClientes::ctrCambiaEstado($cambiarAccion,$nombreusuario,$usuario,$valor2);

		}

		/*=============================================
		Buscar por fechas
		=============================================*/


		public $fechaInicial;
		public $fechaFinal;

		public function ajaxfiltrarFechaClientes(){

			if($this->fechaInicial == "" && $this->fechaFinal == ""){

				$fechaInicial = null;
				$fechaFinal = null;

			}else{

				$fechaInicial = $this->fechaInicial;
				$fechaFinal = $this->fechaFinal;

			}		

			$respuesta = ModeloClientes::mdlRangoFechasClientes('clientes',$fechaInicial, $fechaFinal);

			echo json_encode($respuesta);

		}
	}

	/*=============================================
	EDITAR USUARIO
	=============================================*/

	if(isset($_POST["idCliente"])){

		$editar = new AjaxClientes();
		$editar -> idCliente = $_POST["idCliente"];
		$editar -> ajaxEditarCliente();

	}

	/*=============================================
	VALIDAR CORREO EXISTENTE
	=============================================*/	

	if(isset($_POST["validarCorreo"])){

		$valCorreo = new AjaxClientes();
		$valCorreo -> validarCorreo = $_POST["validarCorreo"];
		$valCorreo -> ajaxValidarCorreo();
	}


	/*=============================================
	ACTIVAR CLIENTE
	=============================================*/	

	if(isset($_POST["estadoCliente"])){

		$activar = new AjaxClientes();
		$activar -> estadoCliente = $_POST["estadoCliente"];
		$activar -> activarId = $_POST["idCliente"];
		$activar -> cambiarAccion = $_POST["cambiarAccion"];
		$activar -> usuario = $_POST["usuario"];
		$activar -> nombreusuario = $_POST["nombreusuario"];
		$activar -> ajaxActivarCliente();

	}

	/*=============================================
	Buscar por fechas
	=============================================*/

	if(isset( $_POST["fechaInicial"])){

		$fecCliente = new AjaxClientes();
		$fecCliente -> fechaInicial = $_POST["fechaInicial"];
		$fecCliente -> fechaFinal = $_POST["fechaFinal"];
		$fecCliente -> ajaxfiltrarFechaClientes();

	}


?>