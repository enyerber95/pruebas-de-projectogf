<?php 

	require_once "../controladores/auditoria.controlador.php";
	require_once "../modelos/auditoria.modelo.php";

	class AjaxAuditoria{

		/*=============================================
		Buscar por fechas
		=============================================*/


		public $fechaInicial;
		public $fechaFinal;

		public function ajaxfiltrarFechaAuditoria(){

			if($this->fechaInicial == "" && $this->fechaFinal == ""){

				$fechaInicial = null;
				$fechaFinal = null;

			}else{

				$fechaInicial = $this->fechaInicial;
				$fechaFinal = $this->fechaFinal;

			}

			$respuesta = ModeloAuditoria::mdlRangoFechasAuditoria($fechaInicial, $fechaFinal);

			echo json_encode($respuesta);

		}
	
	public function ajaxRespaldo(){

			$usuariorespaldo = $this->usuariorespaldo;
			$nombreusu = $this->nombreusu;

			$respuesta = ControladorAuditoria::ctrGeneraRespaldo($usuariorespaldo,$nombreusu);

			echo json_encode($respuesta);

		}
	public function ajaxRestore(){

		$usuariorespaldo = $this->usuariorespaldo;
		$nombreusu = $this->nombreusu;

		$respuesta = ControladorAuditoria::ctrCargaSql($usuariorespaldo,$nombreusu);
		
		echo json_encode($respuesta);

	}
	}

	/*=============================================
	Buscar por fechas
	=============================================*/

	if(isset( $_POST["fechaInicial"])){

		$fecAuditoria = new AjaxAuditoria();
		$fecAuditoria -> fechaInicial = $_POST["fechaInicial"];
		$fecAuditoria -> fechaFinal = $_POST["fechaFinal"];
		$fecAuditoria -> ajaxfiltrarFechaAuditoria();

	}
	if(isset( $_POST["respaldo"])){

		$realizarRespaldo = new AjaxAuditoria();

		$realizarRespaldo -> usuariorespaldo = $_POST["usuariorespaldo"];
		$realizarRespaldo -> nombreusu = $_POST["nombreusu"];
		$realizarRespaldo -> ajaxRespaldo();

	}
	if(isset( $_POST["restore"])){

		$realizarRestore = new AjaxAuditoria();

		$realizarRestore -> usuariorespaldo = $_POST["usuariorespaldo"];
		$realizarRestore -> nombreusu = $_POST["nombreusu"];
		$realizarRestore -> ajaxRestore();

	}
 ?>