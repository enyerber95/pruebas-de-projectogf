<?php 

	require_once "../controladores/reportes.controlador.php";
	require_once "../modelos/reportes.modelo.php";

	class AjaxReportes{

		/*=============================================
		=       MOSTRAR DETALLE REPORTE               =
		=============================================*/

		public $idReporte;
		
		public function ajaxVerDetallesReporte(){

			$valor = $this->idReporte;

			$respuesta = ModeloReportes::mdlMostrarDetalleReportes($valor);

			echo json_encode($respuesta);

		}

		/*=============================================
		Buscar por fechas
		=============================================*/


		public $fechaInicial;
		public $fechaFinal;

		public function ajaxfiltrarFechaReportes(){

			if($this->fechaInicial == "" && $this->fechaFinal == ""){

				$fechaInicial = null;
				$fechaFinal = null;

			}else{

				$fechaInicial = $this->fechaInicial;
				$fechaFinal = $this->fechaFinal;

			}

			$respuesta = ModeloReportes::mdlRangoFechasReportes($fechaInicial, $fechaFinal);

			echo json_encode($respuesta);

		}
	}

    /*=============================================
	MOSTRAR DETALLE REPORTE
	=============================================*/

	if(isset($_POST["idReporte"])){

		$mostrar = new AjaxReportes();
		$mostrar -> idReporte = $_POST["idReporte"];
		$mostrar -> ajaxVerDetallesReporte();

	}
		

	/*=============================================
	Buscar por fechas
	=============================================*/

	if(isset( $_POST["fechaInicial"])){

		$fecReportes = new AjaxReportes();
		$fecReportes -> fechaInicial = $_POST["fechaInicial"];
		$fecReportes -> fechaFinal = $_POST["fechaFinal"];
		$fecReportes -> ajaxfiltrarFechaReportes();

	}
 ?>