<?php

	require_once "../controladores/direcciones.controlador.php";
	require_once "../modelos/direcciones.modelo.php";

	class AjaxDirecciones{

		/*=============================================
		=             ACTUALIZAR DIRECCIÓN            =
		=============================================*/

		public $idDireccion;

		public function ajaxActualizarDireccion(){
			$item = "id_direcciones";
			$valor = $this->idDireccion;
			$respuesta = ControladorDirecciones::ctrMostrarDireccion($item, $valor);

			echo json_encode($respuesta);

		}

		public function ajaxAgregarDireccion(){
			echo json_encode( ModeloDirecciones::mdlAgregarDireccion("direcciones", $this->$datos));
		}
	}

	    /*=============================================
		EDITAR USUARIO
		=============================================*/

		if(isset($_POST["idDireccion"])){

			$editar = new AjaxDirecciones();
			$editar -> idDireccion = $_POST["idDireccion"];
			$editar -> ajaxActualizarDireccion();

		}

		if(isset($_POST["clienteCorreo"])){

			$crear = new AjaxDirecciones();
			$datos = array(
				"id_cliente" => $_POST["idCliente"],
				"d_titulo" => $_POST["clienteCorreo"],
				"d_titulo" => $_POST["nuevoTitulo"],
				"d_nombre_empresa" => $_POST["nuevoRazonSoc"],
				"sector_urbanizacion" => $_POST["nuevoSU"],
				"avenida_calle" => $_POST["nuevoAC"],
				"edificio_quinta_casa" => $_POST["nuevoEQC"],
				"d_telefono" => $_POST["nuevoTelf"],
				"informacion_adicional" => $_POST["nuevoIA"],
				"estado" => $_POST["nuevoEstado"],
				"ciudad" => $_POST["nuevoCiudad"]
			);
			$crear -> $datos;
			$crear -> ajaxAgregarDireccion();

		}


 ?>
