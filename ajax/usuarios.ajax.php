<?php 

	require_once "../controladores/usuarios.controlador.php";
	require_once "../modelos/usuarios.modelo.php";
	require_once "../modelos/auditoria.modelo.php";


	class AjaxUsuarios{

		/*=============================================
		=                   EDITAR USUARIO            =
		=============================================*/

		public $idUsuario;
		
		public function ajaxEditarUsuario(){

			$item = "id";
			$valor = $this->idUsuario;

			$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

			echo json_encode($respuesta);

		}

		/*=============================================
		ACTIVAR USUARIO
		=============================================*/

		public $activarUsuario;
		public $activarId;


		public function ajaxActivarUsuario(){

			$tabla = "usuarios";

			$item1 = "estado";
			$valor1 = $this->activarUsuario;

			$item2 = "id";
			$valor2 = $this->activarId;

			$usuariobit= $this->UsuarioEstado;

			$nombrebit= $this->nombreUsuario;
			$accion = $this->accionUsuario;
			$bitacora= ControladorUsuarios::ctrlCambioEstado($valor2,$nombrebit,$usuariobit,$accion);

			$respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);

		} 

		/*=============================================
		VALIDAR NO REPETIR USUARIO
		=============================================*/	

		public $validarUsuario;

		public function ajaxValidarUsuario(){

			$item = "usuario";
			$valor = $this->validarUsuario;

			$respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

			echo json_encode($respuesta);

		}
		
		/*=============================================
		Buscar por fechas
		=============================================*/


		public $fechaInicial;
		public $fechaFinal;

		public function ajaxfiltrarFechaUsuario(){

			if($this->fechaInicial == "" && $this->fechaFinal == ""){

				$fechaInicial = null;
				$fechaFinal = null;

			}else{

				$fechaInicial = $this->fechaInicial;
				$fechaFinal = $this->fechaFinal;

			}		

			$respuesta = ModeloUsuarios::mdlRangoFechasUsuarios('usuarios',$fechaInicial, $fechaFinal);

			echo json_encode($respuesta);

		}

		public $idUsuarioX;
		
		public function ajaxEliminarUsuario(){

			$tabla = "usuarios";

			$item1 = "id";
			$valor = $this->idUsuariox;

			$respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $valor);

		}

	}

	    /*=============================================
		EDITAR USUARIO
		=============================================*/

		if(isset($_POST["idUsuario"])){

			$editar = new AjaxUsuarios();
			$editar -> idUsuario = $_POST["idUsuario"];
			$editar -> ajaxEditarUsuario();

		}

		/*=============================================
		ACTIVAR USUARIO
		=============================================*/	

		if(isset($_POST["activarUsuario"])){

			$activarUsuario = new AjaxUsuarios();
			$activarUsuario -> activarUsuario = $_POST["activarUsuario"];
			$activarUsuario -> activarId = $_POST["activarId"];
			$activarUsuario -> UsuarioEstado = $_POST["UsuarioEstado"];
			$activarUsuario -> accionUsuario = $_POST["accionUsuario"];
			$activarUsuario -> nombreUsuario = $_POST["nombreUsuario"];
			$activarUsuario -> ajaxActivarUsuario();

		}

		/*=============================================
		VALIDAR NO REPETIR USUARIO
		=============================================*/

		if(isset( $_POST["validarUsuario"])){

			$valUsuario = new AjaxUsuarios();
			$valUsuario -> validarUsuario = $_POST["validarUsuario"];
			$valUsuario -> ajaxValidarUsuario();

		}

		/*=============================================
		Buscar por fechas
		=============================================*/

		if(isset( $_POST["fechaInicial"])){

			$fecUsuario = new AjaxUsuarios();
			$fecUsuario -> fechaInicial = $_POST["fechaInicial"];
			$fecUsuario -> fechaFinal = $_POST["fechaFinal"];
			$fecUsuario -> ajaxfiltrarFechaUsuario();
		}

		if(isset( $_POST["eliminarUsuario"])){
			$eliminarUsuario = new AjaxUsuarios();
			$eliminarUsuario -> eliminarUsuario = $_POST["eliminarUsuario"];
			$eliminarUsuario -> ajaxEliminarUsuario();
		}
		

		

		

 ?>