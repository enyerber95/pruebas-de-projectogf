<?php
// Include the main TCPDF library (search for installation path).
require_once("tcpdf_include.php");
require_once "../../../controladores/pedidos.controlador.php";
require_once "../../../modelos/pedidos.modelo.php";
require_once "../../../controladores/productos.controlador.php";
require_once "../../../modelos/productos.modelo.php";
require_once "../../../modelos/direcciones.modelo.php";
require_once "../../../modelos/reportes.modelo.php";


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

class imprimirFactura{
  public function formateo($numero)
  {
        //Si no se necesita los decimales cambiar $decimales[0] por $numero
    $diferencia = 5 - strlen($numero);
    $numero_con_ceros = '';
    for($i = 0 ; $i < $diferencia; $i++)
    {
            $numero_con_ceros .= 0;
    }

    $numero_con_ceros .= $numero;

    return $numero_con_ceros;
  }

  public $codigo;

  public function traerImpresionFactura(){
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor("GrafoFormas HB C.A");
    $pdf->SetTitle("Plantilla");
    $pdf->SetSubject("TCPDF Tutorial");
    $pdf->SetKeywords("TCPDF, PDF, example, test, guide");
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetLeftMargin(0);
    $pdf->SetTopMargin(0);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
        require_once(dirname(__FILE__)."/lang/eng.php");
        $pdf->setLanguageArray($l);
    }
    $date = date_create(ModeloReportes::mdlMostrarDetalleDelReporte($_GET["idReporte"])[0]);
    $date =date_format($date, 'd/m/Y');
    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();
    // define some HTML content with style
    $html = '

      	<style type="text/css">
      		.top-left { border-top-left-radius: 5px; border-collapse: initial;}
      		.top-right { border-top-right-radius: 5px; border-collapse: initial;}
      		.bottom-left{ border-bottom-left-radius: 5px; border-collapse: initial;}
      		.bottom-right { border-bottom-right-radius: 5px; border-collapse: initial;}
      		.marca-agua{
      			background: url("vistas/img/plantilla/prueba.png");
      		}
          tr
          {mso-height-source:auto;}
          col
            {mso-width-source:auto;}
          br
            {mso-data-placement:same-cell;}
          .style0
            {mso-number-format:General;
            text-align:general;
            vertical-align:bottom;
            white-space:nowrap;
            mso-rotate:0;
            mso-background-source:auto;
            mso-pattern:auto;
            color:black;
            font-size:11.0pt;
            font-weight:400;
            font-style:normal;
            text-decoration:none;
            font-family:Calibri, sans-serif;
            mso-font-charset:0;
            border:none;
            mso-protection:locked visible;
            mso-style-name:Normal;
            mso-style-id:0;}
          td
            {mso-style-parent:style0;
            padding:0px;
            mso-ignore:padding;
            color:black;
            font-size:11.0pt;
            font-weight:400;
            font-style:normal;
            text-decoration:none;
            font-family:Calibri, sans-serif;
            mso-font-charset:0;
            mso-number-format:General;
            text-align:general;
            vertical-align:bottom;
            border:none;
            mso-background-source:auto;
            mso-pattern:auto;
            mso-protection:locked visible;
            white-space:nowrap;
            mso-rotate:0;}
          .xl65
            {mso-style-parent:style0;
            text-align:center;
            border:.5pt solid windowtext;}
          .xl66
            {mso-style-parent:style0;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl67
            {mso-style-parent:style0;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl68
            {mso-style-parent:style0;
            text-align:center;}
          .xl69
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl70
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl71
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl72
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl73
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl74
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl75
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl76
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl77
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl78
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl79
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl80
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl81
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl82
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl83
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl84
            {mso-style-parent:style0;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl85
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl86
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border:.5pt solid windowtext;
            white-space:normal;}
          .xl87
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border:.5pt solid windowtext;}
          .xl88
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            border:.5pt solid windowtext;
            white-space:normal;}
          .xl89
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl90
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl91
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl92
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            border:.5pt solid windowtext;}
          .xl93
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl94
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl95
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl96
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl97
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl98
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl99
            {mso-style-parent:style0;
            font-weight:700;
            text-align:center;
            vertical-align:middle;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
      	</style>

        <table border="0" cellpadding="0" cellspacing="0" width="800" style="border-collapse:
         collapse;table-layout:fixed;width:601pt">
         <col width="32" style="mso-width-source:userset;mso-width-alt:1170;width:24pt">
         <col width="123" style="mso-width-source:userset;mso-width-alt:4498;width:92pt">
         <col width="80" span="2" style="mso-width-source:userset;mso-width-alt:2925;
         width:60pt">
         <col width="43" style="mso-width-source:userset;mso-width-alt:1572;width:32pt">
         <col width="138" style="mso-width-source:userset;mso-width-alt:5046;width:104pt">
         <col width="4" style="mso-width-source:userset;mso-width-alt:146;width:3pt">
         <col width="120" style="mso-width-source:userset;mso-width-alt:4388;width:90pt">
         <col width="32" style="mso-width-source:userset;mso-width-alt:1170;width:24pt">
         <col width="34" span="2" style="mso-width-source:userset;mso-width-alt:1243;
         width:26pt">
         <col width="80" style="width:60pt">
         <tr height="24" style="mso-height-source:userset;height:18.0pt">
          <td height="24" width="32" style="height:18.0pt;width:24pt"></td>
          <td width="123" style="width:92pt"></td>
          <td width="80" style="width:60pt"></td>
          <td width="80" style="width:60pt"></td>
          <td width="43" style="width:32pt"></td>
          <td width="138" style="width:104pt"></td>
          <td width="4" style="width:3pt"></td>
          <td width="120" style="width:90pt"></td>
          <td width="32" style="width:24pt"></td>
          <td width="34" style="width:26pt"></td>
          <td width="34" style="width:26pt"></td>
          <td width="80" style="width:60pt"></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td colspan="12" ><img width="690" height="120" src="images/image002.png"></td>
         </tr>
         <tr height="38" style="mso-height-source:userset;height:28.5pt">
          <td height="38" style="height:28.5pt"></td>
          <td class="xl86" width="123" style="width:92pt">Nombre y Apellido<span
          style="mso-spacerun:yes">  </span>o Razón Social:</td>
          <td colspan="4" class="xl69" style="border-right:.5pt solid black;border-left:
          none">'.$this->infoDirec['d_nombre_empresa'].'</td>
          <td></td>
          <td colspan="4" class="xl89" style="border-right:.5pt solid black">FECHA DE
          EMISIÓN</td>
          <td></td>
         </tr>
         <tr height="35" style="mso-height-source:userset;height:26.25pt">
          <td height="35" style="height:26.25pt"></td>
          <td class="xl88" width="123" style="border-top:none;width:92pt">Dirección o
          Domicilio Fiscal:</td>
          <td colspan="4" class="xl69" style="border-right:.5pt solid black;border-left:
          none">'.$this->infoDirec['ciudad'].'/'.$this->infoDirec['estado'].'</td>
          <td></td>
          <td colspan="4" class="xl69" style="border-right:.5pt solid black">'.$date.'</td>
          <td></td>
         </tr>
         <tr height="33" style="mso-height-source:userset;height:24.75pt">
          <td height="33" style="height:24.75pt"></td>
          <td colspan="5" class="xl69" style="border-right:.5pt solid black"> Urb.'.$this->infoDirec['sector_urbanizacion'].'-'.$this->infoDirec['avenida_calle'].' - '.$this->infoDirec['edificio_quinta_casa'].'</td>
          <td></td>
          <td colspan="4" class="xl89" style="border-right:.5pt solid black">FACTURA N°</td>
          <td></td>
         </tr>
         <tr height="33" style="mso-height-source:userset;height:24.75pt">
          <td height="33" style="height:24.75pt"></td>
          <td class="xl86" width="123" style="border-top:none;width:92pt">Telefono</td>
          <td colspan="2" class="xl69" style="border-right:.5pt solid black;border-left:
          none">'.$this->infoDirec['d_telefono'].'</td>
          <td class="xl87" style="border-top:none;border-left:none">Rif.</td>
          <td class="xl65" style="border-top:none;border-left:none">'.$this->pedido[0]["rif"].'</td>
          <td></td>
          <td colspan="4" class="xl69" style="border-right:.5pt solid black">'.$this->formateo($_GET["idReporte"]).'</td>
          <td></td>
         </tr>
         <tr height="0">
          <td height="0" colspan="12" ></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td class="xl92">CANTIDAD</td>
          <td colspan="4" class="xl93" style="border-right:.5pt solid black;border-left:
          none">DESCRIPCIÓN</td>
          <td colspan="2" class="xl93" style="border-right:.5pt solid black;border-left:
          none">PRECIO UNITARIO<span style="mso-spacerun:yes"> </span></td>
          <td colspan="3" class="xl93" style="border-right:.5pt solid black;border-left:
          none">TOTAL Bs</td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td rowspan="24" class="xl75" style="border-bottom:.5pt solid black;border-top:
          none"><p></p>1</td>
          <td colspan="4" rowspan="24" class="" style="border-right:.5pt solid black;
          border-bottom:.5pt solid black"><p></p>
            <b>  Orden: </b>'.$this->pedido[0]["orden"].' - '.ControladorProductos::ctrMostrarDetalleProductos($this->pedido[0]["id_producto"],'nombre').'<br>
            <b>  Cantidad: </b>'.$this->pedido[0]["cantidad"].'<br>
            <b>  N° de control inicial: </b>&nbsp;'.$this->pedido[0]["num_control_ini"].'<br>
            <b>	 N° de factura inicial: </b>&nbsp;'.$this->pedido[0]["num_factura_ini"].'<br>
            <b>  Material: </b>'.$this->pedido[0]["preferencia_papel"].'<br>
            <b>  Impresión: </b>'.$this->pedido[0]["preferencia_impresion"].'<br>
            <b>  Color: </b> &nbsp;'.$this->pedido[0]["opciones_color"].'<br>
          </td>
          <td colspan="2" rowspan="24" class="xl72" style="border-right:.5pt solid black;
          border-bottom:.5pt solid black"><p></p>'.($this->pedido[0]["monto"]/100000).'<br></td>
          <td colspan="3" rowspan="24" class="xl72" style="border-right:.5pt solid black;
          border-bottom:.5pt solid black"><p></p>'.($this->pedido[0]["monto"]/100000).'</td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" style="height:15.0pt"></td>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr>
          <td></td>
         </tr>
         <tr height="28" style="mso-height-source:userset;height:21.0pt">
          <td height="28" style="height:21.0pt"></td>
          <td class="xl83" style="border-top:none">&nbsp;</td>
          <td class="xl85" style="border-top:none">&nbsp;</td>
          <td class="xl85" style="border-top:none">&nbsp;</td>
          <td class="xl85" style="border-top:none">&nbsp;</td>
          <td class="xl85" style="border-top:none">&nbsp;</td>
          <td colspan="2" class="xl89" style="border-right:.5pt solid black">SUB-TOTAL</td>
          <td colspan="3" class="xl69" style="border-right:.5pt solid black;border-left:
          none">'.($this->pedido[0]["monto"]/100000).'</td>
          <td></td>
         </tr>
         <tr height="31" style="mso-height-source:userset;height:23.25pt">
          <td height="31" style="height:23.25pt"></td>
          <td class="xl84" >&nbsp;</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2" class="xl96" style="border-right:.5pt solid black">I.V.A. % SOBRE</td>
          <td colspan="3" class="xl72" style="border-right:.5pt solid black;border-left:
          none">'.(($this->pedido[0]["monto"]/100000)*0.12).'</td>
          <td></td>
         </tr>
         <tr height="14" style="mso-height-source:userset;height:10.5pt">
          <td height="14" style="height:10.5pt"></td>
          <td class="xl84" >&nbsp;</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2" rowspan="2" class="xl98" style="border-right:.5pt solid black;
          border-bottom:.5pt solid black">TOTAL A PAGAR</td>
          <td colspan="3" rowspan="2" class="xl72" style="border-right:.5pt solid black;
          border-bottom:.5pt solid black">'.((($this->pedido[0]["monto"]/100000)*0.12)+$this->pedido[0]["monto"]/100000).'</td>
          <td></td>
         </tr>
         <tr height="16" style="mso-height-source:userset;height:12.0pt">
          <td height="16" style="height:12.0pt"></td>
          <td class="xl66" >&nbsp;</td>
          <td class="xl67" >&nbsp;</td>
          <td class="xl67" >&nbsp;</td>
          <td class="xl67" >&nbsp;</td>
          <td class="xl67" >&nbsp;</td>
          <td></td>
         </tr>
         <tr height="4" style="mso-height-source:userset;height:3.0pt">
          <td height="4" colspan="7" style="height:3.0pt;mso-ignore:colspan"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
         </tr>
         <tr height="20" style="height:15.0pt">
          <td height="20" colspan="12" style="height:15.0pt;mso-ignore:colspan"></td>
         </tr>
         <tr height="7" style="mso-height-source:userset;height:5.25pt">
          <td height="7" colspan="12" style="height:5.25pt;mso-ignore:colspan"></td>
         </tr>
        </table>

        ';

    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, "");
    //Close and output PDF document
    $pdf->Output("Plantilla", "I");

    //============================================================+
    // END OF FILE
    //============================================================+
  }
}

$a = new imprimirFactura();
$a -> pedido = ControladorPedidos::ctrMostrarDetalleEnvio($_GET["pedido"]);
$a -> infoDirec =  ModeloDirecciones::MdlMostrarDireccion( "direcciones", 'id_direcciones', ModeloPedidos::mdldireccionpedido("dir_pedidos", $_GET["pedido"], "id_pedido_dp", "tipo_dp", 'id_direccion_dp', 1)[0]);
$a -> traerImpresionFactura();
