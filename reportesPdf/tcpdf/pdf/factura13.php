<?php
// Include the main TCPDF library (search for installation path).
require_once("tcpdf_include.php");
require_once "../../../controladores/pedidos.controlador.php";
require_once "../../../modelos/pedidos.modelo.php";
require_once "../../../controladores/productos.controlador.php";
require_once "../../../modelos/productos.modelo.php";


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $img_file = K_PATH_IMAGES."prueba.png";
        $this->Image($img_file, 0, 0, 210, 297, "", "", "", false, 300, "", false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

class imprimirFactura{

  public $codigo;

  public function traerImpresionFactura(){
    // create new PDF document
    $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor("GrafoFormas HB C.A");
    $pdf->SetTitle("Plantilla");
    $pdf->SetSubject("TCPDF Tutorial");
    $pdf->SetKeywords("TCPDF, PDF, example, test, guide");
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, "", PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, "", PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    $pdf->SetLeftMargin(10);
    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__)."/lang/eng.php")) {
        require_once(dirname(__FILE__)."/lang/eng.php");
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // add a page
    $pdf->AddPage();
    // define some HTML content with style
    if ($this->pedido[0]["tamano_logo"]!= 0) {
      $html = '
      	<style>
      		tr
      			{mso-height-source:auto;}
      		col
      			{mso-width-source:auto;}
      		br
      			{mso-data-placement:same-cell;}
      		.style0
      			{mso-number-format:General;
      			text-align:general;
      			vertical-align:bottom;
      			white-space:nowrap;
      			mso-rotate:0;
      			mso-background-source:auto;
      			mso-pattern:auto;
      			color:black;
      			font-size:11.0pt;
      			font-weight:400;
      			font-style:normal;
      			text-decoration:none;
      			font-family:Calibri, sans-serif;
      			mso-font-charset:0;
      			border:none;
      			mso-protection:locked visible;
      			mso-style-name:Normal;
      			mso-style-id:0;}
      		td
      			{mso-style-parent:style0;
      			padding-top:1px;
      			padding-right:1px;
      			padding-left:1px;
      			mso-ignore:padding;
      			color:black;
      			font-size:11.0pt;
      			font-weight:400;
      			font-style:normal;
      			text-decoration:none;
      			font-family:Calibri, sans-serif;
      			mso-font-charset:0;
      			mso-number-format:General;
      			text-align:general;
      			vertical-align:bottom;
      			border:none;
      			mso-background-source:auto;
      			mso-pattern:auto;
      			mso-protection:locked visible;
      			white-space:nowrap;
      			mso-rotate:0;}
      		.xl65
      			{mso-style-parent:style0;
      			border:.5pt solid windowtext;}
      		.xl66
      			{mso-style-parent:style0;
      			text-align:center;
      			border:.5pt solid windowtext;}
      		.xl67
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl68
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl69
      			{mso-style-parent:style0;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl70
      			{mso-style-parent:style0;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl71
      			{mso-style-parent:style0;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl72
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl73
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl74
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl75
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl76
      			{mso-style-parent:style0;
      			font-size:8.0pt;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl77
      			{mso-style-parent:style0;
      			font-weight:700;}
      		.xl78
      			{mso-style-parent:style0;
      			text-align:left;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl79
      			{mso-style-parent:style0;
      			text-align:left;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl80
      			{mso-style-parent:style0;
      			text-align:left;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl81
      			{mso-style-parent:style0;
      			text-align:left;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl82
      			{mso-style-parent:style0;
      			text-align:right;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl83
      			{mso-style-parent:style0;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl84
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl85
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl86
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl87
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl88
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl89
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl90
      			{mso-style-parent:style0;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl91
      			{mso-style-parent:style0;
      			font-size:9.0pt;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl92
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl93
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl94
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl95
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl96
      			{mso-style-parent:style0;
      			text-align:center;}
      		.xl97
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl98
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl99
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl100
      			{mso-style-parent:style0;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl101
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;}
      		.xl102
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl103
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border:1.0pt solid windowtext;}
      		.xl104
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border:1.0pt solid windowtext;}
      		.xl105
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl106
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl107
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:left;
      			border:1.0pt solid windowtext;}
      		.xl108
      			{mso-style-parent:style0;
      			font-size:8.0pt;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;}
      		.xl109
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;}
      		.xl110
      			{mso-style-parent:style0;
      			font-weight:700;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;}
      		.xl111
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl112
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl113
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl114
      			{mso-style-parent:style0;
      			font-size:9.0pt;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl115
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl116
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:right;
      			border:1.0pt solid windowtext;}
      		.xl117
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl118
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl119
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl120
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl121
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl122
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl123
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl124
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl125
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl126
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl127
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl128
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl129
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl130
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl131
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl132
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl133
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border:1.0pt solid windowtext;}
      		.xl134
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl135
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl136
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl137
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:right;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl138
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:left;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl139
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;}
      		.xl140
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl141
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl142
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl143
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl144
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;}
      		.xl145
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl146
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl147
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;}
      		.xl148
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:left;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl149
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:left;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl150
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			border-top:none;
      			border-right:none;
      			border-bottom:none;
      			border-left:1.0pt solid windowtext;}
      		.xl151
      			{mso-style-parent:style0;
      			color:gray;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl152
      			{mso-style-parent:style0;
      			color:gray;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl153
      			{mso-style-parent:style0;
      			color:gray;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl154
      			{mso-style-parent:style0;
      			color:gray;
      			font-size:10.0pt;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl155
      			{mso-style-parent:style0;
      			color:gray;
      			font-size:10.0pt;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl156
      			{mso-style-parent:style0;
      			color:gray;
      			font-size:10.0pt;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl157
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			vertical-align:top;
      			border-top:1.0pt solid windowtext;
      			border-right:none;
      			border-bottom:1.0pt solid windowtext;
      			border-left:1.0pt solid windowtext;}
      		.xl158
      			{mso-style-parent:style0;
      			font-family:Arial, sans-serif;
      			mso-font-charset:0;
      			text-align:center;
      			vertical-align:top;
      			border-top:1.0pt solid windowtext;
      			border-right:1.0pt solid windowtext;
      			border-bottom:1.0pt solid windowtext;
      			border-left:none;}
      		.xl159
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl160
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:none;
      			border-bottom:none;
      			border-left:none;}
      		.xl161
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:.5pt solid windowtext;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl162
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:none;
      			border-left:.5pt solid windowtext;}
      		.xl163
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;}
      		.xl164
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:none;
      			border-left:none;}
      		.xl165
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:.5pt solid windowtext;}
      		.xl166
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:none;
      			border-right:none;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.xl167
      			{mso-style-parent:style0;
      			text-decoration:underline;
      			text-underline-style:single;
      			text-align:center;
      			border-top:none;
      			border-right:.5pt solid windowtext;
      			border-bottom:.5pt solid windowtext;
      			border-left:none;}
      		.top-left { border-top-left-radius: 5px; border-collapse: initial;}
      		.top-right { border-top-right-radius: 5px; border-collapse: initial;}
      		.bottom-left{ border-bottom-left-radius: 5px; border-collapse: initial;}
      		.bottom-right { border-bottom-right-radius: 5px; border-collapse: initial;}
      		.marca-agua{

      			margin-left: 3000px;
      			background: url("images/prueba.png");
      		}
      	</style>
             <br><br>
            <table border="0" cellpadding="0" cellspacing="0" style=" border-collapse:  separate;table-layout:fixed;" >
                <col width="80" style=" width:60pt" >
                <col class="xl101" width="90" style=" mso-width-source:userset;mso-width-alt:3291;  width:68pt" >
                <col class="xl101" width="80" span="2" style=" mso-width-source:userset;mso-width-alt:  2925;width:60pt" >
                <col class="xl101" width="154" style=" mso-width-source:userset;mso-width-alt:5632;  width:116pt" >
                <col class="xl101" width="39" style=" mso-width-source:userset;mso-width-alt:1426;  width:29pt" >
                <col class="xl101" width="8" style=" mso-width-source:userset;mso-width-alt:292;  width:6pt" >
                <col class="xl101" width="99" style=" mso-width-source:userset;mso-width-alt:3620;  width:74pt" >
                <col class="xl101" width="36" style=" mso-width-source:userset;mso-width-alt:1316;  width:27pt" >
                <col class="xl101" width="41" style=" mso-width-source:userset;mso-width-alt:1499;  width:31pt" >
                <col class="xl101" width="48" style=" mso-width-source:userset;mso-width-alt:1755;  width:36pt" >
                <col width="80" style=" width:60pt" >
                <tr height="20" style="height:15.0pt">
                    <td height="20" style="height:15.0pt"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style="height:15.0pt">
                    <td height="20" class="xl101"></td>
                    <td class="xl101" style="position: absolute;" name="logoDoc"><img src="../../../'.$this->pedido[0]["logo"].'" width="'.$this->pedido[0]["tamano_logo"].'" height="'.($this->pedido[0]["tamano_logo"]/1.2).'"></td>
                    <td class="xl101" name="nomEmpDoc" colspan="4" style="font-size:15.0pt; position: absolute;font-weight: bold;">'.$this->pedido[0]["nombre_empresa"].'</td>

                    <td class="xl101"></td>
                    <td class="xl111 top-left" style="border-left:1.5pt solid black; border-top:1.5pt solid black">&nbsp;</td>
                    <td class="xl112" style="border-top:1.5pt solid black">&nbsp;</td>
                    <td colspan="4" class="xl148 top-right" style="border-right:1.5pt solid black; border-top:1.5pt solid black">Rif:&nbsp;<label  name="rifDoc" colspan="3" style="position: absolute;">'.$this->pedido[0]["rif"].'</label></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="20" style="height:15.0pt">
                  <td height="20" style="height:15.0pt"></td>
                  <td class="xl101"></td>
                    <td class="xl101" name="nomEmpDoc" colspan="4" >'.$this->pedido[0]["direc_filcal_1"].'</td>
                    <td class="xl101"></td>
                    <td class="xl150" style="border-left:1.5pt solid black;">&nbsp;</td>
                    <td class="xl101">Factura</td>
                    <td class="xl101"></td>
                    <td class="xl102" style="border-right:1.5pt solid black;" colspan="2"></td>
                </tr>
                <tr height="20" style="height:15.0pt">
                  <td height="20" style="height:15.0pt"></td>
                  <td class="xl101"></td>
                    <td class="xl101" name="direDoc" colspan="3" style="font-size:10.0pt;position: absolute;">'.$this->pedido[0]["direc_filcal_2"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl150" style="border-left:1.5pt solid black;">&nbsp;</td>
                    <td class="xl101">N° de Control</td>
                    <td class="xl101"></td>
                    <td class="xl102" style="border-right:1.5pt solid black;" colspan="2"></td>
                </tr>
                <tr height="21" style="height:15.75pt">
                  <td height="20" style="height:15.0pt"></td>
                  <td class="xl101" ></td>
                    <td class="xl101" name="dire2Doc" style="font-size:10.0pt;position: absolute;" colspan="3" >'.$this->pedido[0]["telefono_otro"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl115 bottom-left" style="border-left:1.5pt solid black; border-bottom:1.5pt solid black">&nbsp;</td>
                    <td class="xl105" style="border-bottom:1.5pt solid black">&nbsp;</td>
                    <td class="xl105" style="border-bottom:1.5pt solid black">&nbsp;</td>
                    <td class="xl105" style="border-bottom:1.5pt solid black">&nbsp;</td>
                    <td class="xl106 bottom-right" style="border-right:1.5pt solid black; border-bottom:1.5pt solid black" colspan="2">&nbsp;</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="20" style="height:15.0pt">
                    <td height="21" style="height:15.75pt"></td>
                    <td class="xl101"></td>
                    <td class="xl101" name="tlfDoc" style="font-size:10.0pt;position: absolute;" colspan="3" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl147"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class=xl108 colspan="12" style=" mso-ignore:colspan" >ESTA FACTURA VA SIN TACHADURAS NI ENMIENDADURAS</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="9" style=" mso-height-source:userset;height:6.75pt" >
                    <td height="9" style=" height:6.75pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="21" style=" height:15.75pt" >
                    <td colspan="12" class="xl151" style=" border-right:1.5pt solid black;border-left:1.5pt solid black;border-bottom:1.5pt solid black;border-top:1.5pt solid black;" >ESPACIO   RESERVADO PARA EL PIE DE IMPRENTA</td>
                    <td ></td>
                    <td colspan="2" style=" mso-ignore:colspan" ></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
            </table>
      ';

        }else{
      $html = '
        <style>
          tr
            {mso-height-source:auto;}
          col
            {mso-width-source:auto;}
          br
            {mso-data-placement:same-cell;}
          .style0
            {mso-number-format:General;
            text-align:general;
            vertical-align:bottom;
            white-space:nowrap;
            mso-rotate:0;
            mso-background-source:auto;
            mso-pattern:auto;
            color:black;
            font-size:11.0pt;
            font-weight:400;
            font-style:normal;
            text-decoration:none;
            font-family:Calibri, sans-serif;
            mso-font-charset:0;
            border:none;
            mso-protection:locked visible;
            mso-style-name:Normal;
            mso-style-id:0;}
          td
            {mso-style-parent:style0;
            padding-top:1px;
            padding-right:1px;
            padding-left:1px;
            mso-ignore:padding;
            color:black;
            font-size:11.0pt;
            font-weight:400;
            font-style:normal;
            text-decoration:none;
            font-family:Calibri, sans-serif;
            mso-font-charset:0;
            mso-number-format:General;
            text-align:general;
            vertical-align:bottom;
            border:none;
            mso-background-source:auto;
            mso-pattern:auto;
            mso-protection:locked visible;
            white-space:nowrap;
            mso-rotate:0;}
          .xl65
            {mso-style-parent:style0;
            border:.5pt solid windowtext;}
          .xl66
            {mso-style-parent:style0;
            text-align:center;
            border:.5pt solid windowtext;}
          .xl67
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl68
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl69
            {mso-style-parent:style0;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl70
            {mso-style-parent:style0;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl71
            {mso-style-parent:style0;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl72
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl73
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl74
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl75
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl76
            {mso-style-parent:style0;
            font-size:8.0pt;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl77
            {mso-style-parent:style0;
            font-weight:700;}
          .xl78
            {mso-style-parent:style0;
            text-align:left;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl79
            {mso-style-parent:style0;
            text-align:left;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl80
            {mso-style-parent:style0;
            text-align:left;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl81
            {mso-style-parent:style0;
            text-align:left;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl82
            {mso-style-parent:style0;
            text-align:right;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl83
            {mso-style-parent:style0;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl84
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl85
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl86
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl87
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl88
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl89
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl90
            {mso-style-parent:style0;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl91
            {mso-style-parent:style0;
            font-size:9.0pt;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl92
            {mso-style-parent:style0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl93
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl94
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl95
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl96
            {mso-style-parent:style0;
            text-align:center;}
          .xl97
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl98
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl99
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl100
            {mso-style-parent:style0;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl101
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;}
          .xl102
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl103
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border:1.0pt solid windowtext;}
          .xl104
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border:1.0pt solid windowtext;}
          .xl105
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl106
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl107
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:left;
            border:1.0pt solid windowtext;}
          .xl108
            {mso-style-parent:style0;
            font-size:8.0pt;
            font-family:Arial, sans-serif;
            mso-font-charset:0;}
          .xl109
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;}
          .xl110
            {mso-style-parent:style0;
            font-weight:700;
            font-family:Arial, sans-serif;
            mso-font-charset:0;}
          .xl111
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl112
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl113
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl114
            {mso-style-parent:style0;
            font-size:9.0pt;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl115
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl116
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:right;
            border:1.0pt solid windowtext;}
          .xl117
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl118
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl119
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl120
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl121
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl122
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl123
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl124
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl125
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl126
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl127
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl128
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl129
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl130
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl131
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl132
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl133
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border:1.0pt solid windowtext;}
          .xl134
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl135
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl136
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl137
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:right;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl138
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:left;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl139
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;}
          .xl140
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl141
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl142
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl143
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl144
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;}
          .xl145
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl146
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl147
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            font-family:Arial, sans-serif;
            mso-font-charset:0;}
          .xl148
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:left;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl149
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:left;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl150
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:1.0pt solid windowtext;}
          .xl151
            {mso-style-parent:style0;
            color:gray;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl152
            {mso-style-parent:style0;
            color:gray;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl153
            {mso-style-parent:style0;
            color:gray;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl154
            {mso-style-parent:style0;
            color:gray;
            font-size:10.0pt;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl155
            {mso-style-parent:style0;
            color:gray;
            font-size:10.0pt;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl156
            {mso-style-parent:style0;
            color:gray;
            font-size:10.0pt;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl157
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            vertical-align:top;
            border-top:1.0pt solid windowtext;
            border-right:none;
            border-bottom:1.0pt solid windowtext;
            border-left:1.0pt solid windowtext;}
          .xl158
            {mso-style-parent:style0;
            font-family:Arial, sans-serif;
            mso-font-charset:0;
            text-align:center;
            vertical-align:top;
            border-top:1.0pt solid windowtext;
            border-right:1.0pt solid windowtext;
            border-bottom:1.0pt solid windowtext;
            border-left:none;}
          .xl159
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl160
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:none;
            border-bottom:none;
            border-left:none;}
          .xl161
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:.5pt solid windowtext;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl162
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:none;
            border-left:.5pt solid windowtext;}
          .xl163
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;}
          .xl164
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:none;
            border-left:none;}
          .xl165
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:.5pt solid windowtext;}
          .xl166
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:none;
            border-right:none;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .xl167
            {mso-style-parent:style0;
            text-decoration:underline;
            text-underline-style:single;
            text-align:center;
            border-top:none;
            border-right:.5pt solid windowtext;
            border-bottom:.5pt solid windowtext;
            border-left:none;}
          .top-left { border-top-left-radius: 5px; border-collapse: initial;}
          .top-right { border-top-right-radius: 5px; border-collapse: initial;}
          .bottom-left{ border-bottom-left-radius: 5px; border-collapse: initial;}
          .bottom-right { border-bottom-right-radius: 5px; border-collapse: initial;}
          .marca-agua{

            margin-left: 3000px;
            background: url("images/prueba.png");
          }
        </style>


             <br><br>
            <table border="0" cellpadding="0" cellspacing="0" style=" border-collapse:  separate;table-layout:fixed;" >
                <col width="80" style=" width:60pt" >
                <col class="xl101" width="90" style=" mso-width-source:userset;mso-width-alt:3291;  width:68pt" >
                <col class="xl101" width="80" span="2" style=" mso-width-source:userset;mso-width-alt:  2925;width:60pt" >
                <col class="xl101" width="154" style=" mso-width-source:userset;mso-width-alt:5632;  width:116pt" >
                <col class="xl101" width="39" style=" mso-width-source:userset;mso-width-alt:1426;  width:29pt" >
                <col class="xl101" width="8" style=" mso-width-source:userset;mso-width-alt:292;  width:6pt" >
                <col class="xl101" width="99" style=" mso-width-source:userset;mso-width-alt:3620;  width:74pt" >
                <col class="xl101" width="36" style=" mso-width-source:userset;mso-width-alt:1316;  width:27pt" >
                <col class="xl101" width="41" style=" mso-width-source:userset;mso-width-alt:1499;  width:31pt" >
                <col class="xl101" width="48" style=" mso-width-source:userset;mso-width-alt:1755;  width:36pt" >
                <col width="80" style=" width:60pt" >
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td class="xl101" style="position: absolute;" name="logoDoc"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class=" xl111 top-left"  style=" border-left:1.5pt solid black; border-top:1.5pt solid black" >&nbsp;</td>
                    <td class="xl112" style=" border-top:1.5pt solid black" >&nbsp;</td>
                    <td colspan="4" class=" xl148 top-right"  style=" border-right:1.5pt solid black; border-top:1.5pt solid black" >Rif:&nbsp;<label  name="rifDoc" colspan="3" style="position: absolute;">'.$this->pedido[0]["rif"].'</label></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101" name="nomEmpDoc" style="font-size:15.0pt; position: absolute;font-weight: bold;">'.$this->pedido[0]["nombre_empresa"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl150" style=" border-left:1.5pt solid black;" >&nbsp;</td>
                    <td class="xl101">Factura</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl102" style=" border-right:1.5pt solid black;"  colspan="2">&nbsp;</td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101" name="direDoc" style="font-size:10.0pt;position: absolute;">'.$this->pedido[0]["direc_filcal_1"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl150" style=" border-left:1.5pt solid black;" >&nbsp;</td>
                    <td class="xl101">N° de Control</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl102" style=" border-right:1.5pt solid black;"  colspan="2">&nbsp;</td>
                </tr>
                <tr height="21" style=" height:15.75pt" >
                    <td height="21" style=" height:15.75pt" ></td>
                    <td class="xl101" name="dire2Doc" style="font-size:10.0pt;position: absolute;">'.$this->pedido[0]["direc_filcal_2"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class=" xl115 bottom-left"  style=" border-left:1.5pt solid black; border-bottom:1.5pt solid black" >&nbsp;</td>
                    <td class="xl105" style=" border-bottom:1.5pt solid black" >&nbsp;</td>
                    <td class="xl105" style=" border-bottom:1.5pt solid black" >&nbsp;</td>
                    <td class="xl105" style=" border-bottom:1.5pt solid black" >&nbsp;</td>
                    <td class=" xl106 bottom-right"  style=" border-right:1.5pt solid black; border-bottom:1.5pt solid black"  colspan="2">&nbsp;</td>
                </tr>
                <tr height="20" style=" height:15.0pt" >

                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101" name="tlfDoc" style="font-size:10.0pt;position: absolute;">'.$this->pedido[0]["telefono_otro"].'</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl147"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class=xl108 colspan="12" style=" mso-ignore:colspan" >ESTA FACTURA VA SIN TACHADURAS NI ENMIENDADURAS</td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="9" style=" mso-height-source:userset;height:6.75pt" >
                    <td height="9" style=" height:6.75pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                </tr>
                <tr height="21" style=" height:15.75pt" >
                    <td colspan="12" class="xl151" style=" border-right:1.5pt solid black;border-left:1.5pt solid black;border-bottom:1.5pt solid black;border-top:1.5pt solid black;" >ESPACIO   RESERVADO PARA EL PIE DE IMPRENTA</td>
                    <td ></td>
                    <td colspan="2" style=" mso-ignore:colspan" ></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
                <tr height="20" style=" height:15.0pt" >
                    <td height="20" style=" height:15.0pt" ></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td class="xl101"></td>
                    <td></td>
                </tr>
            </table>
      ';
    }

    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, "");

    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


    $pdf->SetMargins(10, 0, 0, false);
    $html = '
    	<div style="padding-left: 15pt; font-size:17.0pt;">
    		<b>Modelo: '.ControladorProductos::ctrMostrarDetalleProductos($this->pedido[0]["id_producto"],'nombre').' </b> <p></p>
    		Por favor revise los siguientes datos antes de completar su orden: <p></p>
    		Nombre de la empresa o persona:'.$this->pedido[0]["nombre_empresa"].'<br>
    		<label name="nomEmpDoc"></label><p></p><br>
    		RIF: &nbsp; <label name="rifDoc">'.$this->pedido[0]["rif"].'</label><br>
    		N° de control inicial: &nbsp; <label name="conIniDoc">'.$this->pedido[0]["num_control_ini"].'</label><br>
    		N° de control final: &nbsp; <label name="conFinDoc">'.$this->pedido[0]["num_control_ini"].'</label><br>
    		N° de factura inicial: &nbsp; <label name="facIniDoc">'.$this->pedido[0]["num_factura_ini"].'</label><br>
    		N° de factura final: &nbsp; <label name="facFinDoc">'.$this->pedido[0]["num_factura_ini"].'</label><br>
    		Tipo de presentación : &nbsp; <label name="tipPreDoc">'.$this->pedido[0]["tipo_presentacion"].'</label><br>
    		Preferencias de impresión : &nbsp; <label name="preImpDoc">'.$this->pedido[0]["preferencia_impresion"].'</label><br>
    		Preferencias de papel : &nbsp; <label name="prePapDoc">'.$this->pedido[0]["preferencia_papel"].'</label><br>
    		Opciones de color : &nbsp; <label name="OpcColDoc">'.$this->pedido[0]["opciones_color"].'</label><p></p><br>
    		Cantidad de <label name="tipPreDoc">'.$this->pedido[0]["tipo_presentacion"].'</label> a imprimir: <label name="canImpDoc">'.$this->pedido[0]["cantidad"].'</label>
    		<p></p><p></p><p></p><p></p><p></p><p></p><p></p>
    		<div class="nota" style="font-size:12.0pt;">
    			<b>Nota:</b> El usuario es el único responsable por los datos suministrados durante la edición del documento, como
    			también de las posibles omisiones de caracteres o faltas de ortografía. La elección de un tamaño muy grande de
    			logo y/o letra podrían causar que parte de la información suministrada por el usuario se refleje incorrectamente. El
    			usuario debe asegurarse que toda la información introducida está siendo reflejada de forma satisfactoria.
    		</div>
    	</div>

    ';

    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, "");
    // ---------------------------------------------------------

    $pdf->SetMargins(10, 0, 0, false);
    $html = '

    	<div class="nota" style=" font-size:12.0pt;">
    		Señores<br>
    		GrafoFormas HB C.A.<br>
    		Presente.<p></p><br><br><br>
    		Estimados señores,<p></p><br><br>
    		<p style="text-indent: 1cm;">Por medio de la presente y en cumplimiento con el artículo 44 la providencia 0071 del Servicio Nacional Integrado sin de Administración Aduanera Muestra y Tributaria, confirmo la solicitud de impresión de mi pedido acorde a las siguientes especificaciones:</p><br><br>
    		Nombre de la empresa o persona:<br> <label name="nomEmpDoc"> '.$this->pedido[0]["nombre_empresa"].'</label><br><br>
    		RIF: &nbsp; <label name="rifDoc">'.$this->pedido[0]["rif"].'</label><br>
    		Numero Facturas a imprimir: <label name="canImpDoc">'.$this->pedido[0]["cantidad"].'</label><br>
    		Numero de Factura inicial: &nbsp; <label name="facIniDoc">'.$this->pedido[0]["num_factura_ini"].'</label><br>
    		Numero de Factura final: &nbsp; <label name="facFinDoc">'.$this->pedido[0]["num_factura_ini"].'</label><br>
    		Numero de Control inicial: &nbsp; <label name="conIniDoc">'.$this->pedido[0]["num_control_ini"].'</label><br>
    		Numero de Control final: &nbsp; <label name="conFinDoc">'.$this->pedido[0]["num_control_ini"].'</label><br><br>
    		Atentamete,<p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p><p></p>
    		<div style="text-indent: 5cm">
    		_____________________________________ <br>
    						<div style="text-indent: 8.5cm">El Usuario</div>
    		</div>

     	</div>

    ';

    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, "");
    // ---------------------------------------------------------

    $pdf->SetMargins(10, 0, 0, false);
    $html = '
    	<div style="padding-left: 15pt; font-size:12.0pt;">
    		<h4><b>Imagen del RIF</b></h4><br>
    		<p></p>
    		<div id="imgDoc2"><img src="../../../'.$this->pedido[0]["img_rif"].'"  width="680" height="600"></div>
     	</div>
    ';

    // output the HTML content
    $pdf->writeHTML($html, true, false, true, false, "");
    //Close and output PDF document
    $pdf->Output("Plantilla", "I");

    //============================================================+
    // END OF FILE
    //============================================================+
  }
}

$a = new imprimirFactura();
$a -> pedido = ControladorPedidos::ctrMostrarDetalleEnvio($_GET["pedido"]);
$a -> traerImpresionFactura();
